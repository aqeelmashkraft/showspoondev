<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Artist_dashboard extends Main_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html

     */
    public function __construct() {
        parent::__construct();
        $this->load->model('artist_dashboard_model');
        //$this->load->model('Users_model');
        if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true && $_SESSION['user_id']) {
            
        } else {
            redirect('register/login');
        }
    }

    public function index() {
        $data['venues'] = $this->artist_dashboard_model->get_venues();
        $data['venuescount'] = $this->artist_dashboard_model->get_venues_count();
        $data['artist'] = $this->artist_dashboard_model->get_artist();
        $data['mygig'] = $this->artist_dashboard_model->get_my_gig();
        $data['mygigcount'] = $this->artist_dashboard_model->get_gig_my_count();
        $data['gigs'] = $this->artist_dashboard_model->get_gig();
        $data['gigscount'] = $this->artist_dashboard_model->get_gig_count();

        $this->load->view('artist_dashboard', $data);
    }

    public function artist_edit_band() {
        $id = $this->uri->segment(3);
        $data['artist_member'] = $this->artist_dashboard_model->get_band($id);
        $data['id'] = $id;
        $this->load->view('band_edit', $data);
    }

    public function artist_edit_profile() {
        $id = 0;
        if (isset($_SESSION['user_id']) && $_SESSION['logged_in'] === true) {
            $id = $_SESSION['user_id'];
        }
        $data['files'] = $this->artist_dashboard_model->get_files();
        $data['users'] = $this->Users_model->get_uder_by_id($id);
        $data['artist_band_type'] = $this->Users_model->get_artist_band_type_dropdown();
        $data['artist_gig_type'] = $this->Users_model->get_artist_gig_type_dropdown();
        $data['artist_genre'] = $this->Users_model->get_artist_genre_dropdown();
        $data['cities'] = $this->Users_model->get_city_dropdown();
        $data['mediatype'] = $this->Users_model->get_artist_media_type_dropdown();
        //$data['media'] = $this->artist_dashboard_model->get_artist_media_by_id();
        $this->load->view('artist_edit_profile', $data);
    }

    public function edit_profile() {
        $art_genre = implode(',', $this->input->post('art_genre'));
        $art_type_show = implode(',', $this->input->post('art_type_show'));
        $data_to_store = array(
            'name' => $this->input->post('art_name'),
            'genre_id' => $art_genre,
            'band_type_id' => $this->input->post('art_type'),
            'gig_type_id' => $art_type_show,
            'zip' => $this->input->post('art_zip'),
            //'location' => $this->input->post('art_location'),
            'fb_page_link' => $this->input->post('fb_page_link'),
            'short_description' => $this->input->post('art_short_description'),
            'biography' => $this->input->post('art_biography'),
            'city' => $this->input->post('art_city')
        );

        $this->Users_model->update_artist($data_to_store);
        $this->session->set_flashdata('flash_message', 'updated');
        redirect('artist_edit_profile');
    }

    public function edit_band() {
        $data_to_store = array(
            'name' => $this->input->post('band_name'),
            'id' => $this->input->post('band_id'),
            'role' => $this->input->post('role')
        );

        $this->artist_dashboard_model->update_band($data_to_store);
        $this->session->set_flashdata('flash_message', 'updated_band');
        redirect('artist_dashboard/artist_edit_band/' . $data_to_store['id']);
    }

    public function artist_delete_band() {
        $id = $this->uri->segment(3);
        $file = $this->get_file($id);
        @unlink('./files/' . $file->image_url);
		@unlink('./files/thumbs/' . $file->image_url);
		$this->db->where('id', $id);
        $this->db->delete('artist_member');
        $this->session->set_flashdata('flash_message', 'Delete_member');
        $this->session->set_flashdata('flash_active', 'avtive');
        //echo $file->image_url;

        redirect('artist_edit_profile');
    }

    public function artist_delete_media() {
        $id = $this->uri->segment(3);
		$file = $this->get_artist_media($id);
		@unlink('./files/artist_gallery/thumbs/' . $file->image_url);
		@unlink('./files/artist_gallery/thumbs_gallary/' . $file->image_url);
		@unlink('./files/artist_gallery/thumbs_search/' . $file->image_url);
		@unlink('./files/artist_gallery/' . $file->image_url);
		$this->db->where('id', $id);
        $this->db->delete('artist_photo');
	
    }
	public function get_artist_media($file_id) {
        return $this->db->select()
                        ->from('artist_photo')
                        ->where('id', $file_id)
                        ->get()
                        ->row();
    }

    public function artist_delete_video() {
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        $this->db->delete('artist_media');
    }

    public function artist_delete_soundcloud() {
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        $this->db->delete('artist_media');
    }

    public function set_Featured() {
        $id = $this->uri->segment(3);
        $this->artist_dashboard_model->set_Featured($id);
        $this->session->set_flashdata('featured_image', 'featured');
    }

    public function get_file($file_id) {
        return $this->db->select()
                        ->from('artist_member')
                        ->where('id', $file_id)
                        ->get()
                        ->row();
    }

    public function add_gig() {

        $data['artist_genre'] = $this->Users_model->get_artist_genre_dropdown();
        $data['artist_gig_type'] = $this->Users_model->get_artist_gig_type_dropdown();
        $data['cities'] = $this->Users_model->get_city_dropdown();
        $this->load->view('add_gig', $data);
    }

    public function creategig() {
        // $userfile= $this->do_upload_gig_image_url(); 
        $data['artist_genre'] = $this->Users_model->get_artist_genre_dropdown();
        $data['artist_gig_type'] = $this->Users_model->get_artist_gig_type_dropdown();
        $data['cities'] = $this->Users_model->get_city_dropdown();
        $config = array(
            'allowed_types' => 'jpg|jpeg|gif|png', //only accept these file types
            'max_size' => 2048, //2MB max
            'upload_path' => './uploads/gigs/', //upload directory
            'min_width' => 275,
            'min_height'=> 275,
            'max_width'=> 1024,
            'max_height' => 768
        );

        if ($_FILES['userfile']['error'] == 4) {
            $userfile = '';
            $this->artist_dashboard_model->create($userfile);
            $this->session->set_flashdata('flash_message', 'insertgig');
            redirect('artist_dashboard/add_gig');
        } else {

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('userfile')) {
                $data['status'] = 'error';
                $data['msg'] = $this->upload->display_errors('', '');
                //redirect('artist_dashboard/add_gig',$data);
                $this->load->view('add_gig', $data);
            } else {
                //$this->upload->do_upload();
                $image_data = $this->upload->data(); //upload the image

                $this->load->library('image_lib');
                $config = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => './uploads/gigs/large/',
                    'maintain_ratio' => false,
                    'width' => 275,
                    'height' => 275
                );

                $this->image_lib->initialize($config);

                if (!$this->image_lib->resize())
                    echo $this->image_lib->display_errors();
                $this->image_lib->clear();


                $config2 = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => './uploads/gigs/thumbs/',
                    'maintain_ratio' => false,
                    'width' => 104,
                    'height' => 104
                );

                $this->image_lib->initialize($config2);

                if (!$this->image_lib->resize())
                    echo $this->image_lib->display_errors();
                $this->image_lib->clear();

                $userfile = $image_data['file_name'];


                $this->artist_dashboard_model->create($userfile);
                $this->session->set_flashdata('flash_message', 'insertgig');
                redirect('artist_dashboard/add_gig');
            }
        }
    }

    public function edit_gig() {
        $id = $this->uri->segment(3);
        $data['gig_by_id'] = $this->artist_dashboard_model->get_my_gig_id($id);
        $data['artist_genre'] = $this->Users_model->get_artist_genre_dropdown();
        $data['artist_gig_type'] = $this->Users_model->get_artist_gig_type_dropdown();
        $data['cities'] = $this->Users_model->get_city_dropdown();
        $this->load->view('edit_gig_artist', $data);
    }

    public function artist_my_gigs() {
        $data['mygig'] = $this->artist_dashboard_model->get_my_gig();

        $this->load->view('my_gigs_artist', $data);
    }

    public function edit_gig_update() {

        $id = $this->input->post('gig_id');
        //$art_genre = implode(',', $this->input->post('art_genre'));
        $travelaccomodation = (isset($_POST['travel&accomodation'])) ? 1 : 0;
        //$fooddrink_ticket = (isset($_POST['food&drink_ticket'])) ? 1 : 0;
        $data_to_store = array(
            'gig_name' => $this->input->post('gig_name'),
            //'type' => $art_genre,
            'zip' => $this->input->post('gig_zip'),
            'location' => $this->input->post('gig_location'),
            'description' => $this->input->post('gig_description'),
            'Accepting_application_start_date' => $this->input->post('gig_date_start'),
            'accepting_application_end_date' => $this->input->post('gig_date_end'),
            'Max_croner_amount' => $this->input->post('Max_croner_amount'),
            'parcentage_of_ticketsale' => $this->input->post('parcentage_of_ticketsale'),
            'travel&accomodation' => $travelaccomodation,
            //'food&drink_ticket'=>$fooddrink_ticket,
            'numbers_of_bands' => $this->input->post('numbers_of_bands'),
            'Review_date' => date('Y-m-j H:i:s'),
            'city' => $this->input->post('gig_city'),
            'gig_time' => $this->input->post('gig_hh'),
            'gig_time_end' => $this->input->post('gig_hh_end'),
                //'requirements' => $this->input->post('requirements'),
        );

        $this->artist_dashboard_model->update_gig($data_to_store, $id);
        $this->session->set_flashdata('flash_message', 'updated');
        redirect('artist_dashboard/edit_gig/' . $id);
    }

    public function delete_gig() {
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        $this->db->delete('artist_gigs');
        $this->session->set_flashdata('flash_message', 'delete');
        redirect('artist_dashboard');
    }

    public function upload_media() {
        //print_r($_REQUEST);

        $data_to_store = array(
            'type' => $this->input->post('media_type'),
            'caption' => $this->input->post('video_title'),
            'url' => $this->input->post('video_link'),
            'artist_id' => $_SESSION['user_id']
        );

        $this->artist_dashboard_model->insert_media($data_to_store);
    }

    public function upload_file() {
        $status = "";
        $msg = "";
        $file_element_name = 'userfile';

        if (empty($_POST['title'])) {
            $status = "error";
            $msg = "Please enter a title";
        }

        if ($status != "error") {
            $config['upload_path'] = './files/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = TRUE;
//print_r($_FILES);
//echo '<br>';
//print($_FILES['userfile']['error']);
//exit();
            if ($_FILES['userfile']['error'] == 4) {

                $file_id = $this->artist_dashboard_model->insert_file('', $_POST['title'], $_POST['role']);
                if ($file_id) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                    $this->session->set_flashdata('flash_message', $status);
                } else {
                    unlink($data['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            } else {

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload($file_element_name)) {
                    $status = 'error';
                    $msg = $this->upload->display_errors('', '');
                    //var_dump($msg);
                    // exit();
                } else {
                    $data = $this->upload->data();
                    //your desired  
                    $config = array(
                        'image_library' => 'gd2',
                        'source_image' => $data['full_path'],
                        'new_image' => './files/thumbs/',
                        'maintain_ratio' => true,
                        'width' => 150,
                        'height' => 130
                    );

                    $this->load->library('image_lib', $config);
                    //$this->image_lib->crop();
                    if (!$this->image_lib->resize())
                        echo $this->image_lib->display_errors();

                    $file_id = $this->artist_dashboard_model->insert_file($data['file_name'], $_POST['title'], $_POST['role']);
                    if ($file_id) {
                        $status = "success";
                        $msg = "File successfully uploaded";
                        $this->session->set_flashdata('flash_message', $status);
                    } else {
                        unlink($data['full_path']);
                        $status = "error";
                        $msg = "Something went wrong when saving the file, please try again.";
                    }
                }
                @unlink($_FILES[$file_element_name]);
            }
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }

    public function files() {
        $files = $this->artist_dashboard_model->get_files();
        $this->load->view('files', array('files' => $files));
    }
///////////////////////Upload Artist Media Files Gallery/////////////////////////////
    public function upload_file_artist() {
        $status = "";
        $msg = "";
        $file_element_name = 'userfile';


        if ($status != "error") {
            $config['upload_path'] = './files/artist_gallery/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
            $config['max_size'] = 1024 * 8;
            $config['min_width'] = 553;
            $config['min_height'] = 360;
            $config['max_width'] = 1024;
            $config['max_height'] = 768;
            $config['encrypt_name'] = TRUE;


            $this->load->library('upload', $config);



            if (!$this->upload->do_upload($file_element_name)) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
                $data = $this->upload->data();
                //your desired  
                
                $this->load->library('image_lib');
                
                //thumbs 1
                $config1 = array(
                    'image_library' => 'gd2',
                    'source_image' => $data['full_path'],
                    'new_image' => './files/artist_gallery/thumbs_gallary/',
                    'maintain_ratio' => false,
                    'width' => 553,
                    'height' => 360
                );

                $this->image_lib->initialize($config1);
                 if (!$this->image_lib->resize())
                    echo $this->image_lib->display_errors();
                 $this->image_lib->clear();
                
                //thumbs 2
                $config = array(
                    'image_library' => 'gd2',
                    'source_image' => $data['full_path'],
                    'new_image' => './files/artist_gallery/thumbs_search/',
                    'maintain_ratio' => false,
                    'width' => 275,
                    'height' => 275
                );

                $this->image_lib->initialize($config);
                 if (!$this->image_lib->resize())
                    echo $this->image_lib->display_errors();
                 $this->image_lib->clear();
                
                 //thumbs 3
                $config2 = array(
                    'image_library' => 'gd2',
                    'source_image' => $data['full_path'],
                    'new_image' => './files/artist_gallery/thumbs/',
                    'maintain_ratio' => true,
                    'width' => 150,
                    'height' => 130
                );
                $this->image_lib->initialize($config2);
                //$this->load->library('image_lib', $config);
                //$this->image_lib->crop();
                if (!$this->image_lib->resize())
                    echo $this->image_lib->display_errors();
                $this->image_lib->clear();

                $file_id = $this->artist_dashboard_model->insert_artis_file($data['file_name']);
                if ($file_id) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                    $this->session->set_flashdata('flash_message', $status);
                } else {
                    unlink($data['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }

    public function files_artist() {
        $files = $this->artist_dashboard_model->get_files_artist();
        $this->load->view('files_artist', array('files' => $files));
    }

    public function files_media() {
        $data['media'] = $this->artist_dashboard_model->get_artist_media_by_id();
        //$files = $this->artist_dashboard_model->get_files_artist();
        $this->load->view('files_media', $data);
    }

    public function files_media_soundcloud() {
        $data['mediasound'] = $this->artist_dashboard_model->get_artist_media_by_soundcloud_id();
        //$files = $this->artist_dashboard_model->get_files_artist();
        $this->load->view('files_media_soundcloud', $data);
    }

    function _createThumbnail($fileName) {
        $config['image_library'] = 'gd2';
        $config['source_image'] = './files/' . $fileName;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 75;
        $config['height'] = 75;

        $this->load->library('image_lib', $config);
        if (!$this->image_lib->resize())
            echo $this->image_lib->display_errors();
    }

    function sessions() {
        $this->session->set_flashdata('flash_active2', 'active2');
    }

    function sessions3() {
        $this->session->set_flashdata('flash_active3', 'active3');
    }

    function sessions4() {
        $this->session->set_flashdata('flash_active4', 'active4');
    }

    public function my_feedback() {

        $data['feedback'] = $this->artist_dashboard_model->get_my_feedback();
        $this->load->view('my_feedback_artist', $data);
    }

    public function my_applications() {

        $data['my_applications'] = $this->artist_dashboard_model->get_my_application();
        $this->load->view('my_application_artist', $data);
    }

}
