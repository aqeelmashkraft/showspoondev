<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artist_detail extends Main_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 
	 */
        public function __construct()
        {
            parent::__construct();
            $this->load->model('artist_detail_model');
            if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {
            
            }else{
              redirect('register/login');  
            } 
           
        }
		public function index()
		{	
			//$paramt= $this->uri->uri_to_assoc(3);
			$id =  $this->uri->segment(4);
	 	    //$id= $paramt['id'];
			
			$data['id']= $id;
			$data['band_member'] = $this->artist_detail_model->get_band_member_by_id();
			$data['artistbyid'] = $this->artist_detail_model->get_artist_by_id();
			$data['mygigbyid'] = $this->artist_detail_model->get_my_gig_by_id();
			$data['artistgalllary'] = $this->artist_detail_model->get_artist_photo_by_id();
                        $data['media'] = $this->artist_detail_model->get_artist_media_by_id();
                        $data['mediasound'] = $this->artist_detail_model->get_artist_media_by_soundcloud_id();
                        $data['mygig_dropdown'] = $this->artist_detail_model->get_my_gig_dropdown();
                        
			$this->load->view('artist_detail',$data);
		}
                public function message()
	       {
                     $data_store = array(
                                'receiver_id' =>$this->input->post('art_id'),
                                'sender_id'=>$this->input->post('ven_id'),
                                'subject'=>$this->input->post('artist_subject'),
                                'message'=>$this->input->post('artist_message'),
                                'type'=>$this->input->post('type'),
                                'created_on' => date('Y-m-j H:i:s'),
                            );
                  // print_r($data_store);
                  // exit();
                     $this->artist_detail_model->insert_message($data_store);
               }
		
                public function invite()
	       {
                     $data_store = array(
                                'receiver_id' =>$this->input->post('art_id'),
                                'sender_id'=>$this->input->post('ven_id'),
                                'message'=>$this->input->post('venue_message'),
                                'gig_id'=>$this->input->post('gig_id'),
                                'created_on' => date('Y-m-j H:i:s')
                            );
                  
                     $this->artist_detail_model->insert_invitation($data_store);
               }
	
}
