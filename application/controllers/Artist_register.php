<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artist_register extends Main_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 
	 */
	 
	public function index()
	{	
             $data['artist_band_type'] = $this->Users_model->get_artist_band_type_dropdown();
             $data['artist_gig_type'] = $this->Users_model->get_artist_gig_type_dropdown();
             $data['artist_genre'] = $this->Users_model->get_artist_genre_dropdown();
             $this->load->view('artist_register',$data);
	}
	// login function
		public function login() {
			
		// create the data object
		$data = new stdClass();
		
		// set validation rules
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('artist_type', 'Account Type', 'required');
		
		if ($this->form_validation->run() == false) {
			
			// validation not ok, send validation errors to the view
			$this->load->view('header');
			$this->load->view('login');
			$this->load->view('footer');
			
		} else {
			
			// set variables from the form
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$artist_type = $this->input->post('artist_type');
                        
                        
                    if($artist_type==0){
                            
                            if ($this->Users_model->user_login($username, $password)) {

                                    $user_id = $this->Users_model->get_user_id_from_username($username);
                                    $user    = $this->Users_model->get_user($user_id);

                                    // set session user datas
                                    $_SESSION['user_id']      = (int)$user->id;
                                    $_SESSION['username']     = (string)$user->email;
                                    $_SESSION['logged_in']    = (bool)true;
                                    $_SESSION['artist_type']    = $artist_type;

                                    // user login ok
                                    //$this->load->view('artist_dashboard');
                                    redirect('artist_dashboard');

                            } else {

                                    // login failed
                                    $data->error = 'Wrong username or password.';

                                    // send error to the view
                                    $this->load->view('header');
                                    $this->load->view('login',$data );
                                    $this->load->view('footer');

                            }
                    }else {
                        
                        
                        if ($this->Users_model->venue_login($username, $password)) {

                                    $user_id = $this->Users_model->get_venue_id_from($username);
                                    $user    = $this->Users_model->get_venue($user_id);

                                    // set session user datas
                                    $_SESSION['venue_id']      = (int)$user->id;
                                    $_SESSION['venue_username']     = (string)$user->email;
                                    $_SESSION['logged_in']    = (bool)true;
                                    $_SESSION['artist_type']    = $artist_type;

                                    // user login ok
                                    //$this->load->view('artist_dashboard');
                                    redirect('venue_dashboard');

                            } else {

                                    // login failed
                                    $data->error = 'Wrong username or password.';

                                    // send error to the view
                                    $this->load->view('header');
                                    $this->load->view('login',$data );
                                    $this->load->view('footer');

                            }
                        
                        
                    }
		}
		
	}
	// Rtist_dashboard function
	
	public function create(){ 
			$this->Users_model->create();
    }
	public function checkusername(){ 
			$art_email = $this->input->post('art_email');
			$this->Users_model->check($art_email);
    }
		 
	public function logout() {
		
		
		if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {
			
			// remove session datas
			foreach ($_SESSION as $key => $value) {
				unset($_SESSION[$key]);
			}
			
			// user logout ok
			//$this->load->view('home');
			redirect('/');
			
		} else {
			
			// there user was not logged in, we cannot logged him out,
			// redirect him to site root
			redirect('/');
			
		}
		
	}
        
        
        function forget_password()
	{
		 $email= $this->input->post('email');
                 
		 $result = $this->Users_model->forgetpassword($email);

		  //echo $result;
		 echo json_encode(array( 'msg'=>$result));
		
	} 
	
}
