<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gig_details extends Main_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html

     */
    public function __construct() {
        parent::__construct();
        $this->load->model('venue_gig_model');
    }

    public function index() {
        $id = $this->uri->segment(3);


        $data['id'] = $id;
        $data['gigbyid'] = $this->venue_gig_model->get_gig_by_id();
        $data['disableapplybutton'] = $this->venue_gig_model->disableApply();
        $data['rate'] = $this->venue_gig_model->get_rate_value();
        $data['comments'] = $this->venue_gig_model->get_comment();
        

        $this->load->view('venue_gig',$data);
    }
    public function appay_for_gig() {
                 $message_gig= $this->input->post('message_gig'); 
                 $gig_id= $this->input->post('gig_id'); 
                                 
		 $result = $this->venue_gig_model->appayforgig($message_gig,$gig_id);
		 echo json_encode(array( 'msg'=>$result));
    }
    
     
               
                public function comment()
	       {
                    $user_id=0;
                            if(isset($_SESSION['user_id'])){
                                $user_id=$_SESSION['user_id'];
                            }
                            $venue_id=0;
                            if(isset($_SESSION['venue_id'])){
                                $venue_id=$_SESSION['venue_id'];
                            }
                     $data_store = array(
                                'gig_id' =>$this->input->post('gig_id'),
                                'artist_id'=>$user_id,
                                'venue_id'=>$venue_id,
                                'message'=>$this->input->post('ven_comment'),
                              'created_on' => date('Y-m-j H:i:s')
                            );
                   
                     $this->venue_gig_model->insert_comment($data_store);
               }
               

}
