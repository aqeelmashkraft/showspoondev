<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gigs_search extends Main_Controller {

    
    public function __construct() {
        parent::__construct();
        $this->load->model('gigs_search_model');
        $this->load->library("pagination");
    }

    public function index() {

        $paramt = $this->uri->uri_to_assoc(3);
        $gigs = str_replace('%20', ' ', $paramt['gigs']);
        $data['gigs'] = $gigs;
        $data['cities'] = $this->Users_model->get_city_dropdown();
        $data['artist_gig_type'] = $this->Users_model->get_artist_gig_type_dropdown();
        $data['total_records'] = $this->gigs_search_model->record_count();

        $config = array();
        $config["base_url"] = base_url() . "gigs_search/index/gigs/$gigs";
        $page_url = base_url() . "gigs_search/index/artist/$gigs";
        $config["total_rows"] = $this->gigs_search_model->record_count();
        $config["per_page"] = 9;

        //pagination customization using bootstrap styles
        $config['full_tag_open'] = '<div class="pagination pagination-centered" id="ajax_pagingsearc"><ul>'; // I added class name 'page_test' to used later for jQuery
        $config['full_tag_close'] = '</ul></div><!--pagination-->';
        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';



        $this->pagination->initialize($config);

        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
        $data["search_results"] = $this->gigs_search_model->get_search_results($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        if ($this->input->post('ajax')) {
            $this->load->view('gigs_ajax_results', $data);
        } else {
            $this->load->view('gigs_search', $data);
        }
    }

    public function search_filters() {

        $data["total_records"] = $this->gigs_search_model->record_count_ajax();
        $config = array();
        $config["base_url"] = base_url() . "gigs_search/search_filters";
        //$page_url = base_url() . "search/index/venue/$venue";
        $config["total_rows"] = $this->gigs_search_model->record_count_ajax();
        $config["per_page"] = 9;

        //pagination customization using bootstrap styles
        $config['full_tag_open'] = '<div class="pagination pagination-centered" id="ajax_pagingsearc"><ul>'; // I added class name 'page_test' to used later for jQuery
        $config['full_tag_close'] = '</ul></div><!--pagination-->';
        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';



        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        $data["search_results"] = $this->gigs_search_model->get_ajax_results($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        if ($this->input->post('ajax')) {
            $this->load->view('gigs_ajax_results', $data);
        } else {
            $this->load->view('gigs_search', $data);
        }

        //$this->load->view('venue_ajax_results',$data);
    }

}
