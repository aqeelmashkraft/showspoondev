<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends Main_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 
	 */
	 
	public function index()
	{	
            $id =  $this->uri->segment(2);
	 	    //$id= $paramt['id'];
			
	     $data['id']= $id;
             $data['artist_band_type'] = $this->Users_model->get_artist_band_type_dropdown();
             $data['artist_gig_type'] = $this->Users_model->get_artist_gig_type_dropdown();
             $data['artist_genre'] = $this->Users_model->get_artist_genre_dropdown();
             $data['cities'] = $this->Users_model->get_city_dropdown();
             
             $this->load->view('artist_register',$data);
	}
	// login function
		public function login() {
			
		// create the data object
               $access_gigs =  $this->uri->segment(2);
                if($access_gigs=='access_gigs'){
                    $this->session->set_flashdata('flash_access_gigs', 'access_gigs');
                }
                if($access_gigs=='access_artists'){
                    $this->session->set_flashdata('flash_access_gigs', 'access_gigs');
                }
                if($access_gigs=='access_venues'){
                    $this->session->set_flashdata('flash_access_gigs', 'access_gigs');
                }
				if($access_gigs=='access_venues_as_artist'){
                    $this->session->set_flashdata('flash_access_gigs', 'access_venues_as_artist');
                }
		$data = new stdClass();
		$this->config->set_item('language', 'norwegian');
		// set validation rules
		$this->form_validation->set_rules('username', 'Brukernavn', 'required');
		$this->form_validation->set_rules('password', 'passord', 'required');
		$this->form_validation->set_rules('artist_type', 'Account Type', 'required');
		
		if ($this->form_validation->run() == false) {
			
			// validation not ok, send validation errors to the view
			$this->load->view('header');
			$this->load->view('login');
			$this->load->view('footer');
			
		} else {
			
			// set variables from the form
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$artist_type = $this->input->post('artist_type');
                        
                        
                    if($artist_type==0){
                            
                            if ($this->Users_model->user_login($username, $password)) {

                                    $user_id = $this->Users_model->get_user_id_from_username($username);
                                    $user    = $this->Users_model->get_user($user_id);
					if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {
															foreach ($_SESSION as $key => $value) {
															unset($_SESSION[$key]);
														}
														}
                                    // set session user datas
                                    $_SESSION['user_id']      = (int)$user->id;
                                    $_SESSION['username']     = (string)$user->email;
                                    $_SESSION['logged_in']    = (bool)true;
                                    $_SESSION['artist_type']    = $artist_type;
				    $_SESSION['artist_location']    = (string)$user->city;
				    $_SESSION['user_name']    = (string)$user->name;
                                    // user login ok
                                    //$this->load->view('artist_dashboard');
                                    if($access_gigs=='access_gigs'){
                                        redirect('gigs_search/index/gigs/all'); 
                                    }
                                    if($access_gigs=='access_venues'){
                                        redirect('search/index/venue/all'); 
                                    }
                                    redirect('artist_dashboard');

                            } else {

                                    // login failed
                                    $data->error = 'Wrong username or password.';

                                    // send error to the view
                                    $this->load->view('header');
                                    $this->load->view('login',$data );
                                    $this->load->view('footer');

                            }
                    }else {
                        
                        
                        if ($this->Users_model->venue_login($username, $password)) {

                                    $user_id = $this->Users_model->get_venue_id_from($username);
                                    $user    = $this->Users_model->get_venue($user_id);
									
                                    // set session user datas
                                    $_SESSION['venue_id']      = (int)$user->id;
                                    $_SESSION['venue_username']     = (string)$user->email;
                                    $_SESSION['logged_in']    = (bool)true;
                                    $_SESSION['artist_type']    = $artist_type;
                                    $_SESSION['venue_location']    = (string)$user->city;
                                    $_SESSION['venue_name']    = (string)$user->name;

                                    // user login ok
                                    //$this->load->view('artist_dashboard');
                                    if($access_gigs=='access_gigs'){
                                        redirect('gigs_search/index/gigs/all'); 
                                    }
                                    if($access_gigs=='access_artists'){
                                        redirect('artist_search/index/artist/all'); 
                                    }
                                    redirect('venue_dashboard');

                            } else {

                                    // login failed
                                    $data->error = 'Wrong username or password.';

                                    // send error to the view
                                    $this->load->view('header');
                                    $this->load->view('login',$data );
                                    $this->load->view('footer');

                            }
                        
                        
                    }
		}
		
	}
	// Rtist_dashboard function
	
	public function create(){ 
			$this->Users_model->create();
    }
	public function checkusername(){ 
			$art_email = $this->input->post('art_email');
			$this->Users_model->check($art_email);
    }
	public function checkVenue(){ 
			$ven_email = $this->input->post('ven_email');
			$this->Users_model->checkVenue($ven_email);
    }
		 
	public function logout() {
		
		
		if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {
			
			// remove session datas
			foreach ($_SESSION as $key => $value) {
				unset($_SESSION[$key]);
			}
			
			// user logout ok
			//$this->load->view('home');
			redirect('/');
			
		} else {
			
			// there user was not logged in, we cannot logged him out,
			// redirect him to site root
			redirect('/');
			
		}
		
	}
        public function thank_you_for_registration() {
            if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {
                foreach ($_SESSION as $key => $value) {
				unset($_SESSION[$key]);
			}
                         $this->load->view('confregistration');
            }  else {
               $this->load->view('confregistration'); 
            }
             
             
        }
        public function forgot() {
             $this->load->view('header');
             $this->load->view('forgot_password');
             $this->load->view('footer');
        }
        function forget_password()
	{
		 $email= $this->input->post('email');
		 $artist_type= $this->input->post('artist_type');
		 
                 
		 $result = $this->Users_model->forgetpassword($email,$artist_type);

		  //echo $result;
		 echo json_encode(array( 'msg'=>$result));
		
	}
        
        public  function reset_password($id= FALSE )
        {
            $id= $this->uri->segment(2);
			$artist_type= $this->uri->segment(4);
			 
            if($id){
                
                $this->form_validation->set_rules('passwrod', 'Password', 'trim|required|min_length[6]|max_length[12]|matches[new_password]');
 		$this->form_validation->set_rules('new_password', 'Password Confirmation', 'required');
 		if ($this->form_validation->run() == FALSE)
 		{
 		
 		$this->load->view('header');
                $this->load->view('resetpassword');
                $this->load->view('footer');
 		}
 		else
 		{
 			
 			$result = $this->Users_model->check_key($id,$artist_type);
 			//echo $result;
                        //exit();
 			($result==1)?  $this->session->set_flashdata('success', 'updatedpassword'): 
 			$this->session->set_flashdata('error', 'You have some thing wrong. please click to forget password button and get new link');
 			
 			redirect(site_url('login'));
 		}
             //$this->load->view('header');
             //$this->load->view('resetpassword');
             //$this->load->view('footer');  
            }else{
                $data['wrong_link']= "You have some thing wrong";
 		$data['go_to']= "login";
 		$data['content'] = 'warning_page';
 		$this->load->view('warning_page', $data);
            }
            
        }
	
}
