<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends Main_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 
	 */
        public function __construct()
        {
            parent::__construct();
            $this->load->model('search_model');
            $this->load->library("pagination");
        }
		public function index()
		{	
                    
                        $paramt= $this->uri->uri_to_assoc(3);
                        $venue= str_replace('%20', ' ',$paramt['venue']);
			$data['venue']= $venue;
                        $data['artist_genre'] = $this->Users_model->get_artist_genre_dropdown();
                        $data['cities'] = $this->Users_model->get_city_dropdown();
                        $data["total_records"] = $this->search_model->record_count();
                        
                        //$config["base_url"] = base_url() . "search/search_filters/area/$area/search-by/$search_by/page/";
                        
			$config = array();
                        $config["base_url"] = base_url() . "search/index/venue/$venue";
                        $page_url = base_url() . "search/index/venue/$venue";
                        $config["total_rows"] = $this->search_model->record_count();
                        $config["per_page"] = 9;
                        
                        //pagination customization using bootstrap styles
                        $config['full_tag_open'] = '<div class="pagination pagination-centered" id="ajax_pagingsearc"><ul>'; // I added class name 'page_test' to used later for jQuery
                        $config['full_tag_close'] = '</ul></div><!--pagination-->';
                        $config['first_link'] = '&laquo; First';
                        $config['first_tag_open'] = '<li class="prev page">';
                        $config['first_tag_close'] = '</li>';

                        $config['last_link'] = 'Last &raquo;';
                        $config['last_tag_open'] = '<li class="next page">';
                        $config['last_tag_close'] = '</li>';

                        $config['next_link'] = 'Next &rarr;';
                        $config['next_tag_open'] = '<li class="next page">';
                        $config['next_tag_close'] = '</li>';

                        $config['prev_link'] = '&larr; Previous';
                        $config['prev_tag_open'] = '<li class="prev page">';
                        $config['prev_tag_close'] = '</li>';

                        $config['cur_tag_open'] = '<li class="active"><a href="">';
                        $config['cur_tag_close'] = '</a></li>';

                        $config['num_tag_open'] = '<li class="page">';
                        $config['num_tag_close'] = '</li>';

                     
                        
                        $this->pagination->initialize($config);
                        
                        $page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
                        $data["search_results"] = $this->search_model->get_search_results($config["per_page"], $page);
                        $data["links"] = $this->pagination->create_links();
                        if($this->input->post('ajax')) {
                         $this->load->view('venue_ajax_results',$data);
                        } 
                        else {
                         $this->load->view('venues_search',$data);
                       }
			
			//$data["search_results"] = $this->search_model ->get_search_results();
                        //$data['artist_genre'] = $this->Users_model->get_artist_genre_dropdown();
		
			//$this->load->view('venues_search',$data);
		}
               public function search_filters(){
                   
                    //$paramt= $this->uri->uri_to_assoc(3);
                       // $venue= str_replace('%20', ' ',$paramt['venue']);
			//$data['venue']= $venue;
                   
                   $config = array();
                        $config["base_url"] = base_url() . "search/search_filters";
                        //$page_url = base_url() . "search/index/venue/$venue";
                        $data["total_records"] = $this->search_model->record_count_ajax();
                        $config["total_rows"] = $this->search_model->record_count_ajax();
                        $config["per_page"] = 9;
                        
                        //pagination customization using bootstrap styles
                        $config['full_tag_open'] = '<div class="pagination pagination-centered" id="ajax_pagingsearc"><ul>'; // I added class name 'page_test' to used later for jQuery
                        $config['full_tag_close'] = '</ul></div><!--pagination-->';
                        $config['first_link'] = '&laquo; First';
                        $config['first_tag_open'] = '<li class="prev page">';
                        $config['first_tag_close'] = '</li>';

                        $config['last_link'] = 'Last &raquo;';
                        $config['last_tag_open'] = '<li class="next page">';
                        $config['last_tag_close'] = '</li>';

                        $config['next_link'] = 'Next &rarr;';
                        $config['next_tag_open'] = '<li class="next page">';
                        $config['next_tag_close'] = '</li>';

                        $config['prev_link'] = '&larr; Previous';
                        $config['prev_tag_open'] = '<li class="prev page">';
                        $config['prev_tag_close'] = '</li>';

                        $config['cur_tag_open'] = '<li class="active"><a href="">';
                        $config['cur_tag_close'] = '</a></li>';

                        $config['num_tag_open'] = '<li class="page">';
                        $config['num_tag_close'] = '</li>';

                     
                        
                        $this->pagination->initialize($config);
                        
                        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
                   
                        $data["search_results"] = $this->search_model ->get_ajax_results($config["per_page"], $page);
                        $data["links"] = $this->pagination->create_links();
                        if($this->input->post('ajax')) {
                         $this->load->view('venue_ajax_results',$data);
                        } 
                        else {
                         $this->load->view('venue_ajax_results',$data);
                       }
                       
                        //$this->load->view('venue_ajax_results',$data);
                   
               } 
		
	
 	
	
}
