<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Venue_dashboard extends Main_Controller {

    
    public function __construct() {
        parent::__construct();
        $this->load->model('venue_dashboard_model');
        if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true && $_SESSION['venue_id']) {
            
        } else {
            redirect('register/login');
        }
    }

    public function index() {
        $data['venue'] = $this->venue_dashboard_model->get_venue();
        $data['artist'] = $this->venue_dashboard_model->get_artist();
        $data['mygig'] = $this->venue_dashboard_model->get_my_gig();
        //$data["total_msg"] = $this->Users_model->msg_rec_count();
        $this->load->view('venue_dashboard', $data);
    }

    public function add_gig() {

        $data['artist_genre'] = $this->Users_model->get_artist_genre_dropdown();
        $data['artist_gig_type'] = $this->Users_model->get_artist_gig_type_dropdown();
        $data['cities'] = $this->Users_model->get_city_dropdown();
        $this->load->view('add_gig_venue', $data);
    }
    public function venue_my_gigs() {
        $data['mygig'] = $this->venue_dashboard_model->get_my_gig();
        $data['art_dropdown'] = $this->venue_dashboard_model->get_artist_dropdown();
        
        $this->load->view('my_gigs_venue', $data);
    }
    public function creategig() {
        $data['artist_genre'] = $this->Users_model->get_artist_genre_dropdown();
        $data['artist_gig_type'] = $this->Users_model->get_artist_gig_type_dropdown();
        $data['cities'] = $this->Users_model->get_city_dropdown();
        $config = array(
            'allowed_types' => 'jpg|jpeg|png', //only accept these file types
            'max_size' => 2048, //2MB max
            'upload_path' => './uploads/gigs/', //upload directory
            'min_width' => 275,
            'min_height'=> 275,
            'max_width'=> 1024,
            'max_height' => 768
        );
        
        if ($_FILES['userfile']['error'] == 4) {
           
            $userfile ='';

            $this->venue_dashboard_model->create($userfile);
            $this->session->set_flashdata('flash_message', 'insertgig');
            redirect('venue_dashboard/add_gig');
            
            
        }else{
           
                $this->load->library('upload', $config);
            if (!$this->upload->do_upload('userfile')) {
                $data['status'] = 'error';
                $data['msg'] = $this->upload->display_errors('', '');
                //redirect('artist_dashboard/add_gig',$data);
                $this->load->view('add_gig_venue', $data);
            } else {

                $image_data = $this->upload->data(); //upload the image


                $config = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => './uploads/gigs/thumbs/',
                    'maintain_ratio' => true,
                    'width' => 104,
                    'height' => 104
                );

                $this->load->library('image_lib', $config);
                if (!$this->image_lib->resize())
                    echo $this->image_lib->display_errors();
                $userfile = $image_data['file_name'];

                $this->venue_dashboard_model->create($userfile);
                $this->session->set_flashdata('flash_message', 'insertgig');
                redirect('venue_dashboard/add_gig');
            } 
        }
        

       
    }

   

    public function edit_gig() {
        $id = $this->uri->segment(3);
        $data['gig_by_id'] = $this->venue_dashboard_model->get_my_gig_id($id);
        $data['artist_genre'] = $this->Users_model->get_artist_genre_dropdown();
        $data['artist_gig_type'] = $this->Users_model->get_artist_gig_type_dropdown();
        $data['cities'] = $this->Users_model->get_city_dropdown();
        $this->load->view('edit_gig_venue', $data);
    }

    public function edit_gig_update() {

        $id = $this->input->post('gig_id');
       // $art_genre = implode(',', $this->input->post('art_genre'));
        $travelaccomodation = (isset($_POST['travel&accomodation'])) ? 1 : 0;
        //$fooddrink_ticket = (isset($_POST['food&drink_ticket'])) ? 1 : 0;
        $data_to_store = array(
            'gig_name' => $this->input->post('gig_name'),
            //'type' => $art_genre,
            'zip' => $this->input->post('gig_zip'),
            'location' => $this->input->post('gig_location'),
            'description' => $this->input->post('gig_description'),
            'Accepting_application_start_date' => $this->input->post('gig_date_start'),
            'accepting_application_end_date' => $this->input->post('gig_date_end'),
            'Max_croner_amount' => $this->input->post('Max_croner_amount'),
            'parcentage_of_ticketsale' => $this->input->post('parcentage_of_ticketsale'),
            'travel&accomodation' => $travelaccomodation,
            //'food&drink_ticket' => $fooddrink_ticket,
            //'requirements'=>$this->input->post('requirements'),
	    'numbers_of_bands'=>$this->input->post('numbers_of_bands'),
            'gig_time'=>$this->input->post('gig_hh'),
            'gig_time_end' => $this->input->post('gig_hh_end'),
            'Review_date' => date('Y-m-j H:i:s'),
            'city' => $this->input->post('gig_city')
        );

        $this->venue_dashboard_model->update_gig($data_to_store, $id);
        $this->session->set_flashdata('flash_message', 'updated');
        redirect('venue_dashboard/edit_gig/' . $id);
    }

    public function delete_gig() {
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        $this->db->delete('artist_gigs');
        $this->session->set_flashdata('flash_message', 'delete');
        redirect('venue_dashboard');
    }

    public function venue_edit_profile() {

        $data['venuebyid'] = $this->venue_dashboard_model->get_venue_by_id();
        $data['artist_genre'] = $this->Users_model->get_artist_genre_dropdown();
        $data['cities'] = $this->Users_model->get_city_dropdown();
        $this->load->view('venue_edit_profile', $data);
    }

    public function edit_profile() {
        //execute the upload function
        //$userfile= $this->do_upload(); 

        $status = "";
        $msg = "";
        $config = array(
            'allowed_types' => 'pdf', //only accept these file types
            'max_size' => 2048, //2MB max
            'upload_path' => './uploads/pdf/' //upload directory
        );
        if ($_FILES['userfile']['error'] == 4) {
            
            $userfile = '';
            $art_genre = implode(',', $this->input->post('art_genre'));
            $data_to_store = array(
                'name' => $this->input->post('ven_name'),
                'genre' => $art_genre,
                'zip' => $this->input->post('ven_zip'),
                'location' => $this->input->post('ven_location'),
                'descriptiopn' => $this->input->post('ven_description'),
                'city' => $this->input->post('ven_city'),
                'age' => $this->input->post('ven_age'),
                'ven_capacity' => $this->input->post('ven_capacity'),
                'rider_document_url_pdf' => $userfile
            );



            $this->venue_dashboard_model->update_venue($data_to_store);
            $this->session->set_flashdata('flash_message', 'updated');
            redirect('venue_dashboard/venue_edit_profile');
            
        }else{
          
             $this->load->library('upload', $config);
        if (!$this->upload->do_upload('userfile')) {
            $status = 'error';
            $msg = $this->upload->display_errors('', '');
            $data['status'] = 'error';
            $data['msg'] = $this->upload->display_errors('', '');
            $data['venuebyid'] = $this->venue_dashboard_model->get_venue_by_id();
            $data['artist_genre'] = $this->Users_model->get_artist_genre_dropdown();
            $data['cities'] = $this->Users_model->get_city_dropdown();
            $this->load->view('venue_edit_profile', $data);
        } else {

            $image_data = $this->upload->data();
            $userfile = $image_data['file_name'];
            $art_genre = implode(',', $this->input->post('art_genre'));
            $data_to_store = array(
                'name' => $this->input->post('ven_name'),
                'genre' => $art_genre,
                'zip' => $this->input->post('ven_zip'),
                'location' => $this->input->post('ven_location'),
                'descriptiopn' => $this->input->post('ven_description'),
                'city' => $this->input->post('ven_city'),
                'age' => $this->input->post('ven_age'),
                'ven_capacity' => $this->input->post('ven_capacity'),
                'rider_document_url_pdf' => $userfile
            );



            $this->venue_dashboard_model->update_venue($data_to_store);
            $this->session->set_flashdata('flash_message', 'updated');
            redirect('venue_dashboard/venue_edit_profile');
        }
        }
       
    }

    public function my_feedback() {
        
        $data['feedback'] = $this->venue_dashboard_model->get_my_feedback();
        $this->load->view('my_feedback', $data);
    }
    public function my_applications() {
        
        $data['my_applications'] = $this->venue_dashboard_model->get_my_application();
        $this->load->view('my_application', $data);
    }
   
///////////////////////upload files of venues gallary/media /////////////////////////////////////////////
    public function upload_file() {
        $status = "";
        $msg = "";
        $file_element_name = 'userfile';


        if ($status != "error") {
            $config['upload_path'] = './files/venues_gallery/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 1024 * 8;
            $config['min_width'] = 553;
            $config['min_height'] = 360;
            $config['max_width'] = 1024;
            $config['max_height'] = 768;
            $config['encrypt_name'] = TRUE;


            $this->load->library('upload', $config);



            if (!$this->upload->do_upload($file_element_name)) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
                $data = $this->upload->data();
                // print_r($data);
                //exit();
                //your desired  
                 $this->load->library('image_lib');
                $config = array(
                    'image_library' => 'gd2',
                    'source_image' => $data['full_path'],
                    'new_image' => './files/venues_gallery/thumbs_gallary/',
                    'maintain_ratio' => false,
                    'width' => 553,
                    'height' => 360
                );


                //$this->load->library('image_lib', $config);
                 $this->image_lib->initialize($config);
                if (!$this->image_lib->resize())
                    echo $this->image_lib->display_errors();
                
                ///thumbs 2
                $config = array(
                    'image_library' => 'gd2',
                    'source_image' => $data['full_path'],
                    'new_image' => './files/venues_gallery/thumbs_search/',
                    'maintain_ratio' => false,
                    'width' => 275,
                    'height' => 275
                );


                //$this->load->library('image_lib', $config);
                 $this->image_lib->initialize($config);
                if (!$this->image_lib->resize())
                    echo $this->image_lib->display_errors();
                
                
                ///thumbs 3
                
                $config = array(
                    'image_library' => 'gd2',
                    'source_image' => $data['full_path'],
                    'new_image' => './files/venues_gallery/thumbs/',
                    'maintain_ratio' => true,
                    'width' => 104,
                    'height' => 104
                );


                //$this->load->library('image_lib', $config);
                 $this->image_lib->initialize($config);
                if (!$this->image_lib->resize())
                    echo $this->image_lib->display_errors();


                $file_id = $this->venue_dashboard_model->insert_file($data['file_name']);
                if ($file_id) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                    $this->session->set_flashdata('flash_message', $status);
                } else {
                    unlink($data['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }

    public function files() {
        $files = $this->venue_dashboard_model->get_files();
         $this->session->set_flashdata('flash_active', 'active');
        $this->load->view('files_venue', array('files' => $files));
    }
 function sessions(){
   $this->session->set_flashdata('flash_active2', 'active2');
}
public function invite() {
    //print_r($_REQUEST);                  
               
    for ($i = 0; $i < count($_REQUEST['art_id']); $i++) {
         //print($i);
        $data_store = array(
                                'receiver_id' =>$_REQUEST['art_id'][$i],
                                'sender_id'=>$this->input->post('ven_id'),
                                'message'=>$this->input->post('venue_message'),
                                'gig_id'=>$this->input->post('gig_id'),
                                'created_on' => date('Y-m-j H:i:s')
                            );
        
        $this->venue_dashboard_model->insert_invitation($data_store);
        //print($i);
            //mysql_query("INSERT INTO pro_types (pro_id, ptype_id) VALUES('" . $_REQUEST['pro_id'] . "', '" . $_REQUEST['ptype'][$i] . "')");
        }
}
 public function venue_delete_media() {
        $id = $this->uri->segment(3);
        $this->db->where('id', $id);
        $this->db->delete('venue_photo');

    }
    public function set_Featured() {
        $id = $this->uri->segment(3);
        $this->venue_dashboard_model->set_Featured($id);
        $this->session->set_flashdata('featured_image', 'featured');
    }

}
