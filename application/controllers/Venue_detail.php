<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Venue_detail extends Main_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 
	 */
        public function __construct()
        {
            parent::__construct();
            $this->load->model('venue_detail_model');
            if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true && isset($_SESSION['venue_id'])) {
             redirect('register/login');
        } else {
           
        }
           
        }
		public function index()
		{	
			//$paramt= $this->uri->uri_to_assoc(3);
			$id =  $this->uri->segment(4);
	 	    //$id= $paramt['id'];
			
			$data['id']= $id;
			//$data['band_member'] = $this->artist_detail_model->get_band_member_by_id();
			$data['venueDetail'] = $this->venue_detail_model->get_venue_by_id();
			$data['mygigbyid'] = $this->venue_detail_model->get_my_gig_by_id();
                        $data['venuephoto'] = $this->venue_detail_model->get_venue_photo_by_id();
                        $data['commentcount'] = $this->venue_detail_model->get_venue_comment_by_id();
                        $data['rate'] = $this->venue_detail_model->get_rate_value();
                        
		    $this->load->view('venue_detail',$data);
		}
                
               public function rating()
	       {
                   if(isset($_POST['action']))
                    {
                       if(htmlentities($_POST['action'], ENT_QUOTES, 'UTF-8') == 'rating')
                        {
                            $type = intval($_POST['classBox']);
                            $user_id=0;
                            if(isset($_SESSION['user_id'])){
                                $user_id=$_SESSION['user_id'];
                            }
                            $venue_id=0;
                            if(isset($_SESSION['venue_id'])){
                                $venue_id=$_SESSION['venue_id'];
                            }
                            if($type==4){
                                $id = intval($_POST['idBox']);
                                $rate = floatval($_POST['rate']);
                                $type = intval($_POST['classBox']);
                                $data_to_store = array(
                                    'rating_type' =>$type,
                                    'venue_id' =>$venue_id ,
                                    'artist_id'=>$user_id,
                                    'rating'=>$rate,
                                    'gig_id'=>$id
                                );
                                $this->venue_detail_model->insert_rating($data_to_store);  
                            }else{
                              $id = intval($_POST['idBox']);
                           //echo '<br/>';
                            $rate = floatval($_POST['rate']);
                           //echo '<br/>';
                            $type = intval($_POST['classBox']);
                            $data_to_store = array(
                                'rating_type' =>$type,
                                'venue_id' =>$id ,
                                'artist_id'=>$_SESSION['user_id'],
                                'rating'=>$rate,
                            );
                           
                         $this->venue_detail_model->insert_rating($data_to_store);  
                            }
                            
         
       
                        }
                    }
                    
                    
               } 
               
               public function comment()
	       {
                     $data_store = array(
                                'venue_id' =>$this->input->post('ven_id'),
                                'artist_id'=>$_SESSION['user_id'],
                                'comments'=>$this->input->post('ven_comment')
                            );
                   
                     $this->venue_detail_model->insert_comment($data_store);
               }
               
               public function message()
	       {
                     $data_store = array(
                                'receiver_id' =>$this->input->post('ven_id'),
                                'sender_id'=>$this->input->post('art_id'),
                                'subject'=>$this->input->post('artist_subject'),
                                'message'=>$this->input->post('artist_message'),
                                'type'=>$this->input->post('type')
                            );
                  // print_r($data_store);
                  // exit();
                     $this->venue_detail_model->insert_message($data_store);
               }
               
             
		
	
}
