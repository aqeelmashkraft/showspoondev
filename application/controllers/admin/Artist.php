<?php
/**
 * ark Admin Panel for Codeigniter 
 * Author: Abhishek R. Kaushik
 * downloaded from http://devzone.co.in
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Artist extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
         $this->load->model('admin/venue_model');
         $this->load->model('admin/artist_model');
        $this->load->library('form_validation');
        if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }
    }

    public function index() {
     
	$data['query'] = $this->artist_model->get_artist();
        $this->load->view('admin/vwArtists',$data); 
    }
    public function add() {
          $data['artist_band_type'] = $this->Users_model->get_artist_band_type_dropdown();
             $data['artist_gig_type'] = $this->Users_model->get_artist_gig_type_dropdown();
             $data['artist_genre'] = $this->Users_model->get_artist_genre_dropdown();
             $data['cities'] = $this->Users_model->get_city_dropdown();
       $this->load->view('admin/addvenues',$data); 
    }

    public function editartist() {
		
        $id = $this->uri->segment(5);
        //$data['files'] = $this->artist_dashboard_model->get_files();
        $data['users'] = $this->Users_model->get_uder_by_id($id);
        $data['artist_band_type'] = $this->Users_model->get_artist_band_type_dropdown();
        $data['artist_gig_type'] = $this->Users_model->get_artist_gig_type_dropdown();
        $data['artist_genre'] = $this->Users_model->get_artist_genre_dropdown();
        $data['cities'] = $this->Users_model->get_city_dropdown();
        $data['mediatype'] = $this->Users_model->get_artist_media_type_dropdown();
        //$data['media'] = $this->artist_dashboard_model->get_artist_media_by_id();
        $this->load->view('admin/editArtist', $data);
    }
	public function edit_profile() {
           
        $id=$this->uri->segment(5);
        $art_genre = implode(',', $this->input->post('art_genre'));
        $art_type_show = implode(',', $this->input->post('art_type_show'));
        $is_active = $this->input->post('is_active');
        
        // if ($is_active) {
            $query = $this->db->query("SELECT * FROM artist_gigs WHERE artist_id=".$id. "  ");
            foreach ($query->result_array() as $row) {
                //$return[$row['id']] = $row['name'];
                $data2['is_active'] = $is_active;
                $this->db->where('id', $row['id']);
                $this->db->update("artist_gigs", $data2);
            }
            
            $query2 = $this->db->query("SELECT * FROM artist_photo WHERE artist_id=".$id. "  ");
            foreach ($query2->result_array() as $row2) {
                //$return[$row['id']] = $row['name'];
                $data3['is_active'] = $is_active;
                $this->db->where('id', $row2['id']);
                $this->db->update("artist_photo", $data3);
            }
            
            $query3 = $this->db->query("SELECT * FROM artist_member WHERE artist_id=".$id. "  ");
            foreach ($query3->result_array() as $row3) {
                //$return[$row['id']] = $row['name'];
                $data4['is_active'] = $is_active;
                $this->db->where('id', $row3['id']);
                $this->db->update("artist_member", $data4);
            }
            $query4 = $this->db->query("SELECT * FROM artist_media WHERE artist_id=".$id. "  ");
            foreach ($query4->result_array() as $row4) {
                //$return[$row['id']] = $row['name'];
                $data5['is_active'] = $is_active;
                $this->db->where('id', $row4['id']);
                $this->db->update("artist_media", $data5);
            }
            
        //}
        $data_to_store = array(
            'name' => $this->input->post('art_name'),
            'genre_id' => $art_genre,
            'band_type_id' => $this->input->post('art_type'),
            'gig_type_id' => $art_type_show,
            'zip' => $this->input->post('art_zip'),
            //'location' => $this->input->post('art_location'),
            'fb_page_link' => $this->input->post('fb_page_link'),
            'short_description' => $this->input->post('art_short_description'),
            'biography' => $this->input->post('art_biography'),
            'is_active' =>$is_active,
            'city' => $this->input->post('art_city')
        );

        $this->artist_model->update_artist($data_to_store,$id);
        $this->session->set_flashdata('flash_message', 'updated');
        redirect('admin/artist/editartist/id/'.$this->uri->segment(5));
    }
    
    
    ///////////////////////Upload Artist Media Files Gallery/////////////////////////////
    public function upload_file_artist() {
        $status = "";
        $msg = "";
        $file_element_name = 'userfile';
        $id = $this->uri->segment(4);

        if ($status != "error") {
            $config['upload_path'] = './files/artist_gallery/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
            $config['max_size'] = 1024 * 8;
            $config['min_width'] = 553;
            $config['min_height'] = 360;
            $config['max_width'] = 1024;
            $config['max_height'] = 768;
            $config['encrypt_name'] = TRUE;


            $this->load->library('upload', $config);



            if (!$this->upload->do_upload($file_element_name)) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
                $data = $this->upload->data();
                //your desired  
                
                $this->load->library('image_lib');
                
                //thumbs 1
                $config1 = array(
                    'image_library' => 'gd2',
                    'source_image' => $data['full_path'],
                    'new_image' => './files/artist_gallery/thumbs_gallary/',
                    'maintain_ratio' => false,
                    'width' => 553,
                    'height' => 360
                );

                $this->image_lib->initialize($config1);
                 if (!$this->image_lib->resize())
                    echo $this->image_lib->display_errors();
                 $this->image_lib->clear();
                
                //thumbs 2
                $config = array(
                    'image_library' => 'gd2',
                    'source_image' => $data['full_path'],
                    'new_image' => './files/artist_gallery/thumbs_search/',
                    'maintain_ratio' => false,
                    'width' => 275,
                    'height' => 275
                );

                $this->image_lib->initialize($config);
                 if (!$this->image_lib->resize())
                    echo $this->image_lib->display_errors();
                 $this->image_lib->clear();
                
                 //thumbs 3
                $config2 = array(
                    'image_library' => 'gd2',
                    'source_image' => $data['full_path'],
                    'new_image' => './files/artist_gallery/thumbs/',
                    'maintain_ratio' => true,
                    'width' => 150,
                    'height' => 130
                );
                $this->image_lib->initialize($config2);
                //$this->load->library('image_lib', $config);
                //$this->image_lib->crop();
                if (!$this->image_lib->resize())
                    echo $this->image_lib->display_errors();
                $this->image_lib->clear();

                $file_id = $this->artist_model->insert_artis_file($data['file_name'],$id);
                if ($file_id) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                    $this->session->set_flashdata('flash_message', $status);
                } else {
                    unlink($data['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }
    
    public function upload_media() {
        $id = $this->uri->segment(4);

        $data_to_store = array(
            'type' => $this->input->post('media_type'),
            'caption' => $this->input->post('video_title'),
            'url' => $this->input->post('video_link'),
            'artist_id' => $id
        );

        $this->artist_model->insert_media($data_to_store);
    }
     public function artist_delete_video() {
        $id = $this->uri->segment(4);
        $this->db->where('id', $id);
        $this->db->delete('artist_media');
    }
    
    public function files_artist() {
        $files = $this->artist_model->get_files_artist();
        $this->load->view('admin/files_artist', array('files' => $files));
    }
     public function files_media() {
        $data['media'] = $this->artist_model->get_artist_media_by_id();
        //$files = $this->artist_dashboard_model->get_files_artist();
        $this->load->view('admin/files_media', $data);
    }
    
      public function files_media_soundcloud() {
        $data['mediasound'] = $this->artist_model->get_artist_media_by_soundcloud_id();
        //$files = $this->artist_dashboard_model->get_files_artist();
        $this->load->view('admin/files_media_soundcloud', $data);
    }
    public function set_Featured() {
        $id = $this->uri->segment(4);
        $art_id = $this->uri->segment(5);
        $this->artist_model->set_Featured($id,$art_id);
        $this->session->set_flashdata('featured_image', 'featured');
    }
    
    public function artist_delete_soundcloud() {
        $id = $this->uri->segment(4);
        $this->db->where('id', $id);
        $this->db->delete('artist_media');
    }
    
     public function artist_delete_media() {
        $id = $this->uri->segment(4);
		$file = $this->get_artist_media($id);
		@unlink('./files/artist_gallery/thumbs/' . $file->image_url);
		@unlink('./files/artist_gallery/thumbs_gallary/' . $file->image_url);
		@unlink('./files/artist_gallery/thumbs_search/' . $file->image_url);
		@unlink('./files/artist_gallery/' . $file->image_url);
		$this->db->where('id', $id);
        $this->db->delete('artist_photo');
	
    }
	public function get_artist_media($file_id) {
        return $this->db->select()
                        ->from('artist_photo')
                        ->where('id', $file_id)
                        ->get()
                        ->row();
    }
    
    
    ////////////////////////////Upload Artsit Member/////////////////////////////////
    
        public function upload_file() {
        $status = "";
        $msg = "";
        $file_element_name = 'userfile';
        $id = $this->uri->segment(4);

        if (empty($_POST['title'])) {
            $status = "error";
            $msg = "Please enter a title";
        }

        if ($status != "error") {
            $config['upload_path'] = './files/';
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
            $config['max_size'] = 1024 * 8;
            $config['encrypt_name'] = TRUE;
//print_r($_FILES);
//echo '<br>';
//print($_FILES['userfile']['error']);
//exit();
            if ($_FILES['userfile']['error'] == 4) {

                $file_id = $this->artist_model->insert_file('', $_POST['title'], $_POST['role'],$id);
                if ($file_id) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                    $this->session->set_flashdata('flash_message', $status);
                } else {
                    unlink($data['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            } else {

                $this->load->library('upload', $config);

                if (!$this->upload->do_upload($file_element_name)) {
                    $status = 'error';
                    $msg = $this->upload->display_errors('', '');
                    //var_dump($msg);
                    // exit();
                } else {
                    $data = $this->upload->data();
                    //your desired  
                    $config = array(
                        'image_library' => 'gd2',
                        'source_image' => $data['full_path'],
                        'new_image' => './files/thumbs/',
                        'maintain_ratio' => true,
                        'width' => 150,
                        'height' => 130
                    );

                    $this->load->library('image_lib', $config);
                    //$this->image_lib->crop();
                    if (!$this->image_lib->resize())
                        echo $this->image_lib->display_errors();

                    $file_id = $this->artist_model->insert_file($data['file_name'], $_POST['title'], $_POST['role'],$id);
                    if ($file_id) {
                        $status = "success";
                        $msg = "File successfully uploaded";
                        $this->session->set_flashdata('flash_message', $status);
                    } else {
                        unlink($data['full_path']);
                        $status = "error";
                        $msg = "Something went wrong when saving the file, please try again.";
                    }
                }
                @unlink($_FILES[$file_element_name]);
            }
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }
    
     public function files() {
        $files = $this->artist_model->get_files();
        $this->load->view('files', array('files' => $files));
    }
     public function delete_venue() {
        $id = $this->uri->segment(5);
        $this->db->where('id', $id);
        $this->db->delete('venues');
        $this->session->set_flashdata('flash_message', 'delete');
        redirect('admin/venues');
    }
    
    
     

    

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */