<?php
/**
 * ark Admin Panel for Codeigniter 
 * Author: Abhishek R. Kaushik
 * downloaded from http://devzone.co.in
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gigs extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
         $this->load->model('admin/venue_model');
         $this->load->model('admin/artist_model');
         $this->load->model('admin/gigs_model');
        $this->load->library('form_validation');
        if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }
    }

    public function index() {
     
	$data['query'] = $this->gigs_model->get_gigs();
        $this->load->view('admin/vwGigs',$data); 
    }

    public function addgig() {
        $data['artist_genre'] = $this->Users_model->get_artist_genre_dropdown();
        $data['artist_gig_type'] = $this->Users_model->get_artist_gig_type_dropdown();
        $data['cities'] = $this->Users_model->get_city_dropdown();
        $this->load->view('admin/addGigs', $data);
    }
        public function creategig() {
        $data['artist_genre'] = $this->Users_model->get_artist_genre_dropdown();
        $data['artist_gig_type'] = $this->Users_model->get_artist_gig_type_dropdown();
        $data['cities'] = $this->Users_model->get_city_dropdown();
        $config = array(
            'allowed_types' => 'jpg|jpeg|png', //only accept these file types
            'max_size' => 2048, //2MB max
            'upload_path' => './uploads/gigs/', //upload directory
            'min_width' => 275,
            'min_height'=> 275,
            'max_width'=> 1024,
            'max_height' => 768
        );
        
        if ($_FILES['userfile']['error'] == 4) {
           
            $userfile ='';

            $this->gigs_model->create($userfile);
            $this->session->set_flashdata('flash_message', 'insertgig');
             redirect('admin/gigs/addGigs');
            
            
        }else{
           
                $this->load->library('upload', $config);
            if (!$this->upload->do_upload('userfile')) {
                $data['status'] = 'error';
                $data['msg'] = $this->upload->display_errors('', '');
                //redirect('artist_dashboard/add_gig',$data);
                $this->load->view('admin/addGigs', $data);
            } else {

                $image_data = $this->upload->data(); //upload the image


                $config = array(
                    'image_library' => 'gd2',
                    'source_image' => $image_data['full_path'],
                    'new_image' => './uploads/gigs/thumbs/',
                    'maintain_ratio' => true,
                    'width' => 104,
                    'height' => 104
                );

                $this->load->library('image_lib', $config);
                if (!$this->image_lib->resize())
                    echo $this->image_lib->display_errors();
                $userfile = $image_data['file_name'];

                $this->gigs_model->create($userfile);
                $this->session->set_flashdata('flash_message', 'insertgig');
                redirect('admin/gigs/addGigs');
            } 
        }
        

       
    }

    public function editgigs() {
        $id = $this->uri->segment(5);
        $data['gig_by_id'] = $this->gigs_model->get_my_gig_id($id);
        $data['artist_genre'] = $this->Users_model->get_artist_genre_dropdown();
        $data['artist_gig_type'] = $this->Users_model->get_artist_gig_type_dropdown();
        $data['cities'] = $this->Users_model->get_city_dropdown();
        $this->load->view('admin/editGigs', $data);
    }
    
      public function edit_gig_update() {

        $id = $this->input->post('gig_id');
       // $art_genre = implode(',', $this->input->post('art_genre'));
        $travelaccomodation = (isset($_POST['travel&accomodation'])) ? 1 : 0;
        //$fooddrink_ticket = (isset($_POST['food&drink_ticket'])) ? 1 : 0;
        $is_active = $this->input->post('is_active');
        $data_to_store = array(
            'gig_name' => $this->input->post('gig_name'),
            //'type' => $art_genre,
            'zip' => $this->input->post('gig_zip'),
            'location' => $this->input->post('gig_location'),
            'description' => $this->input->post('gig_description'),
            'Accepting_application_start_date' => $this->input->post('gig_date_start'),
            'accepting_application_end_date' => $this->input->post('gig_date_end'),
            'Max_croner_amount' => $this->input->post('Max_croner_amount'),
            'parcentage_of_ticketsale' => $this->input->post('parcentage_of_ticketsale'),
            'travel&accomodation' => $travelaccomodation,
            //'food&drink_ticket' => $fooddrink_ticket,
            //'requirements'=>$this->input->post('requirements'),
	    'numbers_of_bands'=>$this->input->post('numbers_of_bands'),
            'gig_time'=>$this->input->post('gig_hh'),
            'gig_time_end' => $this->input->post('gig_hh_end'),
            'Review_date' => date('Y-m-j H:i:s'),
            'is_active' => $is_active,
            'city' => $this->input->post('gig_city')
        );

        $this->gigs_model->update_gig($data_to_store, $id);
        $this->session->set_flashdata('flash_message', 'updated');
        redirect('admin/gigs/editgigs/id/' . $id);
    }
    
     public function delete_gig() {
        $id = $this->uri->segment(5);
        $this->db->where('id', $id);
        $this->db->delete('artist_gigs');
        $this->session->set_flashdata('flash_message', 'delete');
        redirect('admin/gigs');
    }
    
    public function edit_profile() {
        //execute the upload function
        //$userfile= $this->do_upload(); 
        $id= $this->input->post('venueid');
        $status = "";
        $msg = "";
        $config = array(
            'allowed_types' => 'pdf', //only accept these file types
            'max_size' => 2048, //2MB max
            'upload_path' => './uploads/pdf/' //upload directory
        );
        if ($_FILES['userfile']['error'] == 4) {
            
            $userfile = '';
            $art_genre = implode(',', $this->input->post('art_genre'));
            $data_to_store = array(
                'name' => $this->input->post('ven_name'),
                'genre' => $art_genre,
                'zip' => $this->input->post('ven_zip'),
                'location' => $this->input->post('ven_location'),
                'descriptiopn' => $this->input->post('ven_description'),
                'city' => $this->input->post('ven_city'),
                'age' => $this->input->post('ven_age'),
                'ven_capacity' => $this->input->post('ven_capacity'),
                'rider_document_url_pdf' => $userfile
            );



            $this->venue_model->update_venue($data_to_store,$id);
            $this->session->set_flashdata('flash_message', 'updated');
            redirect('admin/venues/editVenues/id/'.$this->uri->segment(5));
            
        }else{
          
             $this->load->library('upload', $config);
        if (!$this->upload->do_upload('userfile')) {
            $status = 'error';
            $msg = $this->upload->display_errors('', '');
            $data['status'] = 'error';
            $data['msg'] = $this->upload->display_errors('', '');
            $data['venuebyid'] = $this->venue_model->get_venue_by_id();
            $data['artist_genre'] = $this->venue_model->get_artist_genre_dropdown();
            $data['cities'] = $this->venue_model->get_city_dropdown();
            //$this->load->view('admin/venues/editVenues/id/'.$this->uri->segment(5), $data);
             $this->load->view('admin/editVenues',$data);
        } else {

            $image_data = $this->upload->data();
            $userfile = $image_data['file_name'];
            $art_genre = implode(',', $this->input->post('art_genre'));
            $data_to_store = array(
                'name' => $this->input->post('ven_name'),
                'genre' => $art_genre,
                'zip' => $this->input->post('ven_zip'),
                'location' => $this->input->post('ven_location'),
                'descriptiopn' => $this->input->post('ven_description'),
                'city' => $this->input->post('ven_city'),
                'age' => $this->input->post('ven_age'),
                'ven_capacity' => $this->input->post('ven_capacity'),
                'rider_document_url_pdf' => $userfile
            );



            $this->venue_model->update_venue($data_to_store,$id);
            $this->session->set_flashdata('flash_message', 'updated');
            redirect('admin/venues/editVenues/id/'.$this->uri->segment(5));
        }
        }
       
    }

     

    

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */