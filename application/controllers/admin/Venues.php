<?php
/**
 * ark Admin Panel for Codeigniter 
 * Author: Abhishek R. Kaushik
 * downloaded from http://devzone.co.in
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Venues extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
         $this->load->model('admin/venue_model');
        $this->load->library('form_validation');
        if (!$this->session->userdata('is_admin_login')) {
            redirect('admin/home');
        }
    }

    public function index() {
     
	$data['query'] = $this->venue_model->get_venues();
        $data['query_emp']=$this->venue_model->get_gigs();
        $this->load->view('admin/vwVenues',$data); 
    }
    public function add() {
          $data['artist_band_type'] = $this->Users_model->get_artist_band_type_dropdown();
             $data['artist_gig_type'] = $this->Users_model->get_artist_gig_type_dropdown();
             $data['artist_genre'] = $this->Users_model->get_artist_genre_dropdown();
             $data['cities'] = $this->Users_model->get_city_dropdown();
       $this->load->view('admin/addvenues',$data); 
    }

    public function createVenue(){
             $data['artist_band_type'] = $this->Users_model->get_artist_band_type_dropdown();
             $data['artist_gig_type'] = $this->Users_model->get_artist_gig_type_dropdown();
             $data['artist_genre'] = $this->Users_model->get_artist_genre_dropdown();
             $data['cities'] = $this->Users_model->get_city_dropdown();
               $ven_email = $this->input->post('ven_email');
               $val=$this->venue_model->checkVenue($ven_email);
               if($val==1){
                  $this->session->set_flashdata('flash_message', 'error'); 
                  $this->load->view('admin/addvenues',$data); 
                  //redirect('admin/venues/add');
               }  else {
                   $this->venue_model->createVenue();
               }
             
        }
    public function editvenues() {
        $id = $this->uri->segment(5);
        $data['venuebyid'] = $this->venue_model->get_venue_by_id();
        $data['artist_genre'] = $this->venue_model->get_artist_genre_dropdown();
        $data['cities'] = $this->venue_model->get_city_dropdown();
        $this->load->view('admin/editVenues',$data);
    }
    
     public function delete_venue() {
        $id = $this->uri->segment(5);
        
        $query = $this->db->query("SELECT * FROM artist_gigs WHERE venues_id=".$id."  ");
             foreach ($query->result_array() as $row) {
                
                $this->db->where('id', $row['id']);
                $this->db->delete('artist_gigs');
                 }
        
        $this->db->where('id', $id);
        $this->db->delete('venues');
        $this->session->set_flashdata('flash_message', 'delete');
        redirect('admin/venues');
    }
    
    public function edit_profile() {
        //execute the upload function
        //$userfile= $this->do_upload(); 
        $id= $this->input->post('venueid');
        $is_active = $this->input->post('is_active');
       // print($is_active);
        //exit();
       // if($is_active){
            $query = $this->db->query("SELECT * FROM artist_gigs WHERE venues_id=".$id."  ");
             foreach ($query->result_array() as $row) {
                     //$return[$row['id']] = $row['name'];
                 $data2['is_active'] =$is_active;
                $this->db->where('id', $row['id']);
                $this->db->update("artist_gigs", $data2);
                 }
       // }
        $status = "";
        $msg = "";
        $config = array(
            'allowed_types' => 'pdf', //only accept these file types
            'max_size' => 2048, //2MB max
            'upload_path' => './uploads/pdf/' //upload directory
        );
        if ($_FILES['userfile']['error'] == 4) {
            
            $userfile = '';
            $art_genre = implode(',', $this->input->post('art_genre'));
            $data_to_store = array(
                'name' => $this->input->post('ven_name'),
                'genre' => $art_genre,
                'zip' => $this->input->post('ven_zip'),
                'location' => $this->input->post('ven_location'),
                'descriptiopn' => $this->input->post('ven_description'),
                'city' => $this->input->post('ven_city'),
                'age' => $this->input->post('ven_age'),
                'is_active' =>$is_active,
                'ven_capacity' => $this->input->post('ven_capacity'),
                
                'rider_document_url_pdf' => $userfile
            );



            $this->venue_model->update_venue($data_to_store,$id);
            $this->session->set_flashdata('flash_message', 'updated');
            redirect('admin/venues/editVenues/id/'.$this->uri->segment(5));
            
        }else{
          
             $this->load->library('upload', $config);
        if (!$this->upload->do_upload('userfile')) {
            $status = 'error';
            $msg = $this->upload->display_errors('', '');
            $data['status'] = 'error';
            $data['msg'] = $this->upload->display_errors('', '');
            $data['venuebyid'] = $this->venue_model->get_venue_by_id();
            $data['artist_genre'] = $this->venue_model->get_artist_genre_dropdown();
            $data['cities'] = $this->venue_model->get_city_dropdown();
            //$this->load->view('admin/venues/editVenues/id/'.$this->uri->segment(5), $data);
             $this->load->view('admin/editVenues',$data);
        } else {

            $image_data = $this->upload->data();
            $userfile = $image_data['file_name'];
            $art_genre = implode(',', $this->input->post('art_genre'));
            $data_to_store = array(
                'name' => $this->input->post('ven_name'),
                'genre' => $art_genre,
                'zip' => $this->input->post('ven_zip'),
                'location' => $this->input->post('ven_location'),
                'descriptiopn' => $this->input->post('ven_description'),
                'city' => $this->input->post('ven_city'),
                'age' => $this->input->post('ven_age'),
                'ven_capacity' => $this->input->post('ven_capacity'),
                'is_active' =>$is_active,
                'rider_document_url_pdf' => $userfile
            );



            $this->venue_model->update_venue($data_to_store,$id);
            $this->session->set_flashdata('flash_message', 'updated');
            redirect('admin/venues/editVenues/id/'.$this->uri->segment(5));
        }
        }
       
    }

    
    ///////////////////////upload files of venues gallary/media /////////////////////////////////////////////
    public function upload_file() {
        $status = "";
        $msg = "";
        $file_element_name = 'userfile';
       $id = $this->uri->segment(4);
     

        if ($status != "error") {
            $config['upload_path'] = './files/venues_gallery/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = 1024 * 8;
            $config['min_width'] = 553;
            $config['min_height'] = 360;
            $config['max_width'] = 1024;
            $config['max_height'] = 768;
            $config['encrypt_name'] = TRUE;


            $this->load->library('upload', $config);



            if (!$this->upload->do_upload($file_element_name)) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
                $data = $this->upload->data();
                // print_r($data);
                //exit();
                //your desired  
                 $this->load->library('image_lib');
                $config = array(
                    'image_library' => 'gd2',
                    'source_image' => $data['full_path'],
                    'new_image' => './files/venues_gallery/thumbs_gallary/',
                    'maintain_ratio' => false,
                    'width' => 553,
                    'height' => 360
                );


                //$this->load->library('image_lib', $config);
                 $this->image_lib->initialize($config);
                if (!$this->image_lib->resize())
                    echo $this->image_lib->display_errors();
                
                ///thumbs 2
                $config = array(
                    'image_library' => 'gd2',
                    'source_image' => $data['full_path'],
                    'new_image' => './files/venues_gallery/thumbs_search/',
                    'maintain_ratio' => false,
                    'width' => 275,
                    'height' => 275
                );


                //$this->load->library('image_lib', $config);
                 $this->image_lib->initialize($config);
                if (!$this->image_lib->resize())
                    echo $this->image_lib->display_errors();
                
                
                ///thumbs 3
                
                $config = array(
                    'image_library' => 'gd2',
                    'source_image' => $data['full_path'],
                    'new_image' => './files/venues_gallery/thumbs/',
                    'maintain_ratio' => true,
                    'width' => 104,
                    'height' => 104
                );


                //$this->load->library('image_lib', $config);
                 $this->image_lib->initialize($config);
                if (!$this->image_lib->resize())
                    echo $this->image_lib->display_errors();

                $file_id = $this->venue_model->insert_file($data['file_name'],$id);
                if ($file_id) {
                    $status = "success";
                    $msg = "File successfully uploaded";
                    $this->session->set_flashdata('flash_message', $status);
                } else {
                    unlink($data['full_path']);
                    $status = "error";
                    $msg = "Something went wrong when saving the file, please try again.";
                }
            }
            @unlink($_FILES[$file_element_name]);
        }
        echo json_encode(array('status' => $status, 'msg' => $msg));
    }
     public function files() {
        $files = $this->venue_model->get_files();
         $this->session->set_flashdata('flash_active', 'active');
        $this->load->view('admin/files_venue', array('files' => $files));
    }

     public function venue_delete_media() {
        $id = $this->uri->segment(4);
        $this->db->where('id', $id);
        $this->db->delete('venue_photo');

    }
    public function set_Featured() {
        $id = $this->uri->segment(4);
        $ven_id = $this->uri->segment(5);
        $this->venue_model->set_Featured($id,$ven_id);
        $this->session->set_flashdata('featured_image', 'featured');
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */