<?php
$lang['required']		= "Feltet %s er obligatorisk.";
$lang['not_allowed']		= "Du har ikke tillatelse til � poste.";
$lang['captcha']	    	= "Captcha er feil.";
$lang['blocked_words']    	= "Din post inneholder blokkerte ord.";
$lang['robot']             	= "Robot, stikk av!";
$lang['empty']			= "Feltet %s kan ikke v�re tomt.";
$lang['valid_lang']		= "Vennligst velg ditt spr�k.";
$lang['isset']			= "Feltet %s m� ha en verdi.";
$lang['valid_email']		= "Feltet %s m� inneholde en gyldig epost adresse.";
$lang['valid_emails']		= "Feltet %s m� inneholde gyldige epost adresser.";
$lang['valid_url']		= "Feltet %s m� inneholde en gyldig URL.";
$lang['valid_ip']		= "Feltet %s m� inneholde en gyldig IP.";
$lang['min_length']		= "Feltet %s m� inneholde minst %s tegn.";
$lang['max_length']		= "Feltet %s kan ikke v�re lengre enn %s tegn.";
$lang['exact_length']		= "Feltet %s m� v�re akkurat %s tegn.";
$lang['alpha']			= "Feltet %s kan bare inneholde alfabetiske tegn.";
$lang['alpha_numeric']		= "Feltet %s kan bare inneholde alfanumeriske tegn.";
$lang['alpha_dash']		= "Feltet %s kan bare inneholde alfanumeriske tegn, understrek og bindestrek.";
$lang['numeric']		= "Feltet %s m� kun inneholde tall.";
$lang['is_numeric']		= "Feltet %s m� kun inneholde numeriske tegn.";
$lang['integer']		= "Feltet %s m� inneholde et heltall.";
$lang['regex_match']		= "Feltet %s har ikke korrekt format.";
$lang['matches']		= "Feltet %s er ikke lik feltet %s.";
$lang['is_unique'] 		= "Feltet %s m� inneholde en unik verdi.";
$lang['is_natural']		= "Feltet %s m� inneholde positive nummer.";
$lang['is_natural_no_zero']	= "Feltet %s m� inneholde et tall st�rre enn null.";
$lang['decimal']		= "Feltet %s m� inneholde et desimalnummer.";
$lang['less_than']		= "Feltet %s m� inneholde et nummer mindre enn %s.";
$lang['greater_than']		= "Feltet %s m� inneholde et nummer st�rre enn %s.";
/* End of file form_validation_lang.php */
/* Location: ./application/language/norwegian/form_validation_lang.php */