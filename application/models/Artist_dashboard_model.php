<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Artist_dashboard_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    //Get function.

    public function get_venues() {
        $this->db->from('venues');
        $query = $this->db->query("SELECT v.*,c.CityName FROM venues AS v LEFT OUTER JOIN cities AS c ON c.CityID=v.city WHERE  v.city='" . $_SESSION['artist_location'] . "'  limit 0,10");
        return $query->result_array();
    }
    public function get_venues_count() {
        $sql="SELECT id FROM venues AS v  WHERE v.city='" . $_SESSION['artist_location'] . "'";
        $query = $this->db->query($sql);
	return $query->num_rows();
    }
    public function get_band() {
        $id = $this->uri->segment(3);
        $query = $this->db->query("SELECT * FROM artist_member WHERE id =" . $id . " ");
        return $query->result_array();
    }

    function update_band($data) {
        // print_r($data);
        // die();
        $this->db->where('id', $data['id']);
        $this->db->update('artist_member', $data);
    }

    public function create($userfile) {
        //$art_genre = implode(',', $this->input->post('art_genre'));
        $travelaccomodation = (isset($_POST['travel&accomodation'])) ? 1 : 0;
        // $fooddrink_ticket = (isset($_POST['food&drink_ticket'])) ? 1 : 0;
        $data = array(
            'gig_name' => $this->input->post('gig_name'),
            'location' => $this->input->post('gig_location'),
            'zip' => $this->input->post('gig_zip'),
            'city' => $this->input->post('gig_city'),
            //'type'=>$art_genre,
            'artist_id' => $_SESSION['user_id'],
            'description' => $this->input->post('gig_description'),
            'Accepting_application_start_date' => $this->input->post('gig_date_start'),
            'accepting_application_end_date' => $this->input->post('gig_date_end'),
            'Max_croner_amount' => $this->input->post('Max_croner_amount'),
            'parcentage_of_ticketsale' => $this->input->post('parcentage_of_ticketsale'),
            'travel&accomodation' => $travelaccomodation,
            //'food&drink_ticket'=>$fooddrink_ticket,
            'created_on' => date('Y-m-j H:i:s'),
            'image_url' => $userfile,
            //'requirements' => $this->input->post('requirements'),
            'gig_time' => $this->input->post('gig_hh'),
            'gig_time_end' => $this->input->post('gig_hh_end')
        );
        $this->db->insert('artist_gigs', $data);
    }

    public function get_my_gig_id($id) {
        //echo "SELECT * FROM artist_gigs WHERE venues_id=".$id."  ";
        // exit();
        $query = $this->db->query("SELECT * FROM artist_gigs WHERE id=" . $id . "  ");
        return $query->result_array();
    }

    function update_gig($data, $id) {
        // print_r($data);
        //exit();
        $this->db->where('id', $id);
        $this->db->update('artist_gigs', $data);
    }

    public function get_artist() {

        $query = $this->db->query("SELECT genre_id,gig_type_id FROM artist WHERE id=" . $_SESSION['user_id'] . "");
        foreach ($query->result() as $row) {

            $query2 = $this->db->query("SELECT a.*,b.name AS band_typ_name,(SELECT ap.image_url From artist_photo AS ap Where ap.artist_id=a.id and ap.set_featured=1) as featuredimg,
				(SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (" . $row->genre_id . ")  ) AS gener,
				(SELECT  GROUP_CONCAT(ag.name) From artist_gig_type As ag Where ag.id IN (" . $row->gig_type_id . ")) as gigs FROM `artist` as a
				LEFT OUTER JOIN artist_band_type AS b ON b.id=a.band_type_id
				WHERE a.id=" . $_SESSION['user_id'] . " ");
        }
        return $query2->result_array();
    }

    public function get_my_gig() {

        $query = $this->db->query("SELECT g.*,c.CityName FROM artist_gigs As g  LEFT OUTER JOIN cities AS c ON c.CityID=g.city WHERE g.artist_id=" . $_SESSION['user_id'] . "  Order By g.id DESC limit 0,10");
        return $query->result_array();
    }
 public function get_gig_my_count() {
        $sql="SELECT id FROM artist_gigs AS g  WHERE g.artist_id=" . $_SESSION['user_id'] . " ";
        $query = $this->db->query($sql);
	return $query->num_rows();
    }
    public function get_my_feedback() {

        $query = $this->db->query("SELECT c.*,a.name As art_name,a.id As art_id,v.id as ven_id,v.name AS ven_name FROM gig_comments AS c LEFT OUTER JOIN artist As a ON a.id=c.artist_id LEFT OUTER JOIN venues As v ON v.id=c.venue_id WHERE c.artist_id =" . $_SESSION['user_id'] . " ");
        return $query->result_array();
    }

    public function get_my_application() {

        $query = $this->db->query("SELECT m.*,a.name AS art_name,a.id As art_id ,v.id as ven_id,v.name AS ven_name,g.gig_name  FROM gig_messages As m LEFT OUTER JOIN artist As a ON a.id=m.artist_id LEFT OUTER JOIN venues As v ON v.id=m.venue_id LEFT OUTER JOIN artist_gigs AS g on g.id=m.gig_id WHERE m.artist_id =" . $_SESSION['user_id'] . " ");
        return $query->result_array();
    }

    public function get_gig() {

        $query = $this->db->query("SELECT g.*,c.CityName,a.name As artist_name,v.name as venue_name FROM artist_gigs AS g  
			LEFT OUTER JOIN cities AS c ON c.CityID=g.city
			LEFT OUTER JOIN artist AS a ON a.id=g.artist_id
			LEFT OUTER JOIN venues AS v ON v.id=g.venues_id			
			WHERE 
			g.city='" . $_SESSION['artist_location'] . "' Order By g.id DESC limit 0,10");
        return $query->result_array();
    }

    public function get_gig_count() {
        $sql="SELECT id FROM artist_gigs AS g  WHERE g.city='" . $_SESSION['artist_location'] . "'";
        $query = $this->db->query($sql);
	return $query->num_rows();
    }
   
    public function insert_file($filename, $title, $role) {
        $data = array(
            'image_url' => $filename,
            'name' => $title,
            'role' => $role,
            'artist_id' => $_SESSION['user_id'],
        );
        $this->db->insert('artist_member', $data);
        return $this->db->insert_id();
    }

    public function insert_media($data_to_store) {

        $this->db->insert('artist_media', $data_to_store);
        return $this->db->insert_id();
    }

    public function insert_artis_file($filename) {
        $setfeatured = $_POST['setfeatured'];
        if ($setfeatured == 1) {
            //echo 'if';
            $set_featured = $this->db->get_where("artist_photo", array('set_featured' => 1, 'artist_id' => $_SESSION['user_id']), 1);

            if ($set_featured->num_rows() > 0) {

                $row = $set_featured->row_array();
                $id = $row['id'];
                $data['set_featured'] = 0;
                $this->db->where('id', $row['id']);
                $this->db->update("artist_photo", $data);
            } else {
                //return "Id not found.";
                $data = array(
                    'image_url' => $filename,
                    'artist_id' => $_SESSION['user_id'],
                    'set_featured' => $setfeatured
                );
                $this->db->insert('artist_photo', $data);
                return $this->db->insert_id();
            }
        }
        $data = array(
            'image_url' => $filename,
            'artist_id' => $_SESSION['user_id'],
            'set_featured' => $setfeatured
        );
        $this->db->insert('artist_photo', $data);
        return $this->db->insert_id();
    }

    public function get_files() {

        $query = $this->db->query("SELECT * FROM artist_member  WHERE artist_id=" . $_SESSION['user_id'] . " ");
        return $query->result();
    }

    public function get_files_artist() {
        $query = $this->db->query("SELECT * FROM artist_photo  WHERE artist_id=" . $_SESSION['user_id'] . " ");
        return $query->result();
    }

    public function get_messages() {
        $query = $this->db->query("SELECT m.*,a.name AS sender_artist,a.id AS senderartisid FROM `messages` As m LEFT OUTER join artist AS a ON m.sender_id=a.id WHERE m.receiver_id=" . $_SESSION['user_id'] . " and m.type=0
				UNION
				SELECT m.*,v.name AS sender_venue,v.id AS sendervenueid FROM `messages` As m LEFT OUTER join venues AS v ON m.sender_id=v.id WHERE m.receiver_id=" . $_SESSION['user_id'] . " and m.type=1  ");
        return $query->result_array();
    }
    public function get_messages_sender() {
        $query = $this->db->query("SELECT m.*,v.name AS sender_name,v.id AS senderid FROM `messages` As m LEFT OUTER join venues AS v ON m.sender_id=v.id WHERE m.receiver_id=" . $_SESSION['user_id'] . " AND type=0 ");
        return $query->result_array();
    }

    public function get_artist_media_by_id() {
        $id = $_SESSION['user_id'];
        $query = $this->db->query("SELECT * FROM artist_media WHERE artist_id=" . $id . " And type=1");
        return $query->result_array();
    }

    public function get_artist_media_by_soundcloud_id() {
        $id = $_SESSION['user_id'];
        $query = $this->db->query("SELECT * FROM artist_media WHERE artist_id=" . $id . " And type=2 ORDER BY id DESC");
        return $query->result_array();
    }

    public function set_Featured($id2) {
       $set_featured = $this->db->get_where("artist_photo", array('set_featured' => 1, 'artist_id' => $_SESSION['user_id']), 1);
            if ($set_featured->num_rows() > 0) {

                $row = $set_featured->row_array();
                $id = $row['id'];
                $data['set_featured'] = 0;
                $this->db->where('id', $row['id']);
                $this->db->update("artist_photo", $data);
                
                $data2['set_featured'] = 1;
                $this->db->where('id', $id2);
                $this->db->update("artist_photo", $data2);
            }
    }
}

?>
