<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Artist_detail_model extends CI_Model {
    
    public function __construct() 
     {
           parent::__construct(); 
           $this->load->database();
     }

	 //Get function.
		
		public function get_artist_by_id(){		
			 $id =  $this->uri->segment(4);
			 $query = $this->db->query("SELECT genre_id,gig_type_id FROM artist WHERE id=".$id."");
			 foreach ($query->result() as $row)
				{       		
			   $query2 = $this->db->query("SELECT a.*,b.name AS band_typ_name,
				(SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (".$row->genre_id.")  ) AS gener,
				(SELECT  GROUP_CONCAT(ag.name) From artist_gig_type As ag Where ag.id IN (".$row->gig_type_id.")) as gigs FROM `artist` as a
				LEFT OUTER JOIN artist_band_type AS b ON b.id=a.band_type_id
				WHERE a.id=".$id." ");
				}
				return $query2->result_array();
		}
		
		
		
		public function get_my_gig_by_id(){
			 $id =  $this->uri->segment(4);
			$query = $this->db->query("SELECT * FROM artist_gigs WHERE artist_id=".$id." ORDER BY id DESC limit 0,6");
			return $query->result_array();
			 
		}
		
		public function get_band_member_by_id(){
			 $id =  $this->uri->segment(4);
			$query = $this->db->query("SELECT * FROM artist_member WHERE artist_id=".$id."");
			return $query->result_array();
			 
		}
                
                
            public function get_artist_photo_by_id(){		
			 $id =  $this->uri->segment(4);
			 $query = $this->db->query("SELECT * FROM artist_photo WHERE artist_id=".$id."");			 
			 return $query->result_array();
		}
                
                 public function get_artist_media_by_id(){		
			 $id =  $this->uri->segment(4);
			 $query = $this->db->query("SELECT * FROM artist_media WHERE artist_id=".$id." And type=1 limit 0,3");			 
			 return $query->result_array();
		}
                public function get_artist_media_by_soundcloud_id(){		
			 $id =  $this->uri->segment(4);
			 $query = $this->db->query("SELECT * FROM artist_media WHERE artist_id=".$id." And type=2 limit 1");			 
			 return $query->result_array();
		}
		
            public function insert_message($data_to_store) {
				
						$id=$data_to_store['receiver_id'];
						$subject=$data_to_store['subject'];
						$msg=$data_to_store['message'];
                                                $query = $this->db->query("SELECT * From artist AS a  WHERE a.id=".$id."");                                                                             
                                                              
						if ($query->num_rows() > 0) {
							$row = $query->row_array();
							
								$email=$row['email'];
								
								$this->sent_email($email, $msg, $subject);
							     
						}
                     $this->db->insert('messages', $data_to_store);
                     return $this->db->insert_id();
                }
                
                public function insert_invitation($data_to_store) {
                    $id=$data_to_store['receiver_id'];
                    $subject="Invitation For gig";
                    $msg=$data_to_store['message'];
                    $query = $this->db->query("SELECT * From artist AS a  WHERE a.id=".$id."");                                                                             

                    if ($query->num_rows() > 0) {
                            $row = $query->row_array();

                                    $email=$row['email'];

                                    $this->sent_email($email, $msg, $subject);

                    }
                     $this->db->insert('requests', $data_to_store);
                     return $this->db->insert_id();
                }     
				
				
	public function sent_email($email, $msg, $subject)
	 {
            $this->load->library('email');
            //$config['protocol'] = 'smtp';
           // $config['smtp_host'] = 'ssl://smtp.gmail.com';
            //$config['smtp_port'] = '465';
           // $config['smtp_timeout'] = '7';
           // $config['smtp_user'] = 'roomexploreruk@gmail.com';
           // $config['smtp_pass'] = 'Trekkie2007';
            $config['charset'] = 'utf-8';
            $config['newline'] = "\r\n";
            $config['mailtype'] = 'html'; // or html
            $config['validation'] = TRUE; // bool whether to
            $this->email->initialize($config);
            $this->email->from('support@showspoon.com', 'Showspoon');
            $this->email->to($email);
            $this->email->subject($subject);
            $this->email->message($msg);
            $this->email->send();
	 }
         
       function get_my_gig_dropdown() {
           if(isset($_SESSION['venue_id'])){
             $result = $this->db->query("SELECT * FROM artist_gigs WHERE venues_id=".$_SESSION['venue_id']." ORDER BY id DESC");
             $return = array();
             if ($result->num_rows() > 0) {
                 $return[''] = 'Please Select Gig';
                 foreach ($result->result_array() as $row) {
                     $return[$row['id']] = $row['gig_name'];
                 }
             }
             return $return;
           }
        
    }
}

?>
