<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Artist_search_model extends CI_Model {
    
    public function __construct() 
     {
           parent::__construct(); 
           $this->load->database();
     }

	 //Get function.
	
    public function get_search_results($limit, $start){
              // print($start);
               //exit();
		$paramt= $this->uri->uri_to_assoc(3);
			$artist= str_replace('%20', ' ',$paramt['artist']);
			
			$sql='';
			if($artist !='all')
			{ 
				$sql="AND name LIKE '".$artist."%'" ;
			}
                        
		$query = $this->db->query("SELECT a.*,(SELECT ap.image_url From artist_photo AS ap Where ap.artist_id=a.id and ap.set_featured=1) as featuredimg ,(SELECT count(*)  FROM gig_comments AS gg WHERE gg.artist_id=a.id) AS review FROM artist AS a WHERE 1 ".$sql." limit ".$start.",".$limit." ");
		return $query->result_array();
    }
    // Count Return
    function record_count()
    {
        $paramt= $this->uri->uri_to_assoc(3);
			$artist= str_replace('%20', ' ',$paramt['artist']);
			
			$sql='';
			if($artist !='all')
			{ 
				$sql="AND name LIKE '".$artist."%'" ;
			}
                        
		$query = $this->db->query("SELECT id FROM artist WHERE 1 ".$sql."  ");
		return $query->num_rows();
      
    }
    public function get_ajax_results($limit, $start){
                 
				 $sql="SELECT a.*,(SELECT ap.image_url From artist_photo AS ap Where ap.artist_id=a.id and ap.set_featured=1) as featuredimg ,(SELECT count(*)  FROM gig_comments AS gg WHERE gg.artist_id=a.id) AS review  FROM artist AS a WHERE 1 ";
				          
                if(isset($_REQUEST['genre'])){
					 $genre= implode(',', $this->input->post('genre'));
					 $sql.="AND a.genre_id IN(".$genre.")";
                  }
                 if(isset($_REQUEST['artist_name'])){
					$artist_name=  $this->input->post('artist_name');
                    if(!empty($artist_name)){
                         if($artist_name!='all'){
                         $sql.="AND a.name LIKE '".$artist_name."%'";
                         }
                    }
                  }
                  
                 if(isset($_REQUEST['ven_city'])){
                 $ven_city=  $this->input->post('ven_city');
                    if(!empty($ven_city)){
                         $sql.="AND a.city='".$ven_city."'";
                    }
                  }
                  if(isset($_REQUEST['art_type'])){
                       $gig_type_id= implode(',', $this->input->post('art_type'));
			$sql.="AND a.gig_type_id IN(".$gig_type_id.")";
                  }
                  
                 $sql.="Group By  a.name limit ".$start.",".$limit."";
				 //echo $sql;
				 //exit();
             
                  $query = $this->db->query($sql);
		return $query->result_array();
    }
    
    function record_count_ajax()
    {
         $sql="SELECT a.id FROM artist AS a  WHERE 1  ";
		 
		
                
                if(isset($_REQUEST['genre'])){
                    $genre= implode(',', $this->input->post('genre'));
                    $sql.="AND a.genre_id in(".$genre.")";
                  }
                  
                 if(isset($_REQUEST['artist_name'])){
					$artist_name=  $this->input->post('artist_name');
                    if(!empty($artist_name)){
                         $sql.="AND a.name LIKE '".$artist_name."%'";
                    }
                  }
                  
                  if(isset($_REQUEST['ven_city'])){
                 $ven_city=  $this->input->post('ven_city');
                    if(!empty($ven_city)){
                         $sql.="AND a.city='".$ven_city."'";
                    }
                  }
                  if(isset($_REQUEST['art_type'])){
                       $gig_type_id= implode(',', $this->input->post('art_type'));
			$sql.="AND a.gig_type_id IN(".$gig_type_id.")";
                  }
                  $sql.="Group By  a.name";
               //echo  $sql;
              //exit();
		$query = $this->db->query($sql);
		return $query->num_rows();
      
    }
		
        public function get_artist(){

                 $query = $this->db->query("SELECT genre_id,gig_type_id FROM artist WHERE id=".$_SESSION['user_id']."");
                 foreach ($query->result() as $row)
                        {

                   $query2 = $this->db->query("SELECT a.*,b.name AS band_typ_name,
                        (SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (".$row->genre_id.")  ) AS gener,
                        (SELECT  GROUP_CONCAT(ag.name) From artist_gig_type As ag Where ag.id IN (".$row->gig_type_id.")) as gigs FROM `artist` as a
                        LEFT OUTER JOIN artist_band_type AS b ON b.id=a.band_type_id
                        WHERE a.id=".$_SESSION['user_id']." ");
                        }
                        return $query2->result_array();
        }
	
    
    
}

?>
