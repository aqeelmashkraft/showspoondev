<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Files_Model extends CI_Model {
 
    public function insert_file($filename, $title,$role)
    {
        $data = array(
            'image_url'      => $filename,
            'name'         => $title,
			'role'         => $role
        );
        $this->db->insert('artist_member', $data);
        return $this->db->insert_id();
    }
	
	public function get_files()
	{
		return $this->db->select()
				->from('files')
				->get()
				->result();
	}
 
}


?>
