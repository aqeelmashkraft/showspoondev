<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Venue_detail_model extends CI_Model {
    
    public function __construct() 
     {
           parent::__construct(); 
           $this->load->database();
     }

	 //Get function.
		
		public function get_venue_by_id(){		
			 $id =  $this->uri->segment(4);
			 $query = $this->db->query("SELECT genre FROM venues WHERE id=".$id."");
			 foreach ($query->result() as $row)
				{       		
			   $query2 = $this->db->query("SELECT a.*,c.CityName,
				(SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (".$row->genre.")  ) AS gener FROM `venues` as a
			 Left Outer JOIN cities As c ON c.CityID=a.city	WHERE a.id=".$id." ");
				}
				return $query2->result_array();
		}
                
                public function get_venue_photo_by_id(){		
			 $id =  $this->uri->segment(4);
			 $query = $this->db->query("SELECT * FROM venue_photo WHERE venue_id=".$id."");			 
			 return $query->result_array();
		}
                
                
                public function get_venue_comment_by_id(){		
			 $id =  $this->uri->segment(4);
			 $query = $this->db->query("SELECT count(*) As comment_count FROM gig_comments WHERE venue_id=".$id."");			 
			 return $query->result_array();
		}
		
		
		
		public function get_my_gig_by_id(){
			$id =  $this->uri->segment(4);
			$query = $this->db->query("SELECT * FROM artist_gigs WHERE artist_id=0 AND venues_id=".$id."  ORDER BY id DESC limit 0,6");
			return $query->result_array();
			 
		}
		public function insert_rating($data_to_store) {

                     $this->db->insert('gig_rating', $data_to_store);
                     return $this->db->insert_id();
                }
                public function insert_comment($data_to_store) {

                     $this->db->insert('gig_comments', $data_to_store);
                     return $this->db->insert_id();
                }
				
                public function insert_message($data_to_store) {
					
					    $id=$data_to_store['receiver_id'];
						$subject=$data_to_store['subject'];
						$msg=$data_to_store['message'];
					$query = $this->db->query("SELECT * From venues AS v  WHERE v.id=".$id."");                                                                             
                                                              
						if ($query->num_rows() > 0) {
							$row = $query->row_array();
							
								$email=$row['email'];
								
								$this->sent_email($email, $msg, $subject);
							     
						}

                     $this->db->insert('messages', $data_to_store);
                     return $this->db->insert_id();
                }
				
				
				
	public function sent_email($email, $msg, $subject)
	 {
            $this->load->library('email');
           // $config['protocol'] = 'smtp';
           // $config['smtp_host'] = 'ssl://smtp.gmail.com';
           // $config['smtp_port'] = '465';
           // $config['smtp_timeout'] = '7';
            //$config['smtp_user'] = 'roomexploreruk@gmail.com';
           // $config['smtp_pass'] = 'Trekkie2007';
            $config['charset'] = 'utf-8';
            $config['newline'] = "\r\n";
            $config['mailtype'] = 'html'; // or html
            $config['validation'] = TRUE; // bool whether to
            $this->email->initialize($config);
            $this->email->from('support@showspoon.com', 'Showspoon');
            $this->email->to($email);
            $this->email->subject($subject);
            $this->email->message($msg);
            $this->email->send();
	 }
				
				
                  public function get_rate_value() {
                     
                      $id =  $this->uri->segment(4);
		      $query = $this->db->query("SELECT avg(`rating`) as Crowd,(SELECT avg(`rating`) FROM `gig_rating` WHERE `venue_id`=".$id." AND `rating_type`=2 ) AS Stage,(SELECT avg(`rating`) FROM `gig_rating` WHERE `venue_id`=".$id." AND `rating_type`=3 ) AS Backstage
                                    FROM `gig_rating` WHERE `venue_id`=".$id." AND `rating_type`=1");
		      return $query->result_array(); 
                 
                   
               }

}

?>
