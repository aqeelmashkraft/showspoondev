<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Venue_gig_model extends CI_Model {
    
    public function __construct() 
     {
           parent::__construct(); 
           $this->load->database();
     }

	 //Get function.
		
    public function get_gig_by_id(){
            $id =  $this->uri->segment(3);
            $query = $this->db->query("SELECT id,type FROM artist_gigs WHERE  id=".$id." ");
            foreach ($query->result() as $row)
                    {
                $query2 = $this->db->query("SELECT a.*,c.CityName,aaa.name As artist_name,v.name as venue_name,(SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (".$row->type.")  ) AS gener
                                         FROM `artist_gigs` as a
                                         LEFT OUTER JOIN cities AS c ON c.CityID=a.city
                                         LEFT OUTER JOIN artist AS aaa ON aaa.id=a.artist_id
                                         LEFT OUTER JOIN venues AS v ON v.id=a.venues_id
                                         WHERE a.id=".$row->id."");
                                                       
            }
            return $query2->result_array();
            

    }
    public function appayforgig($message_gig,$gig_id) {
        
        $query = $this->db->query("SELECT a.*,aaa.email As art_email,aaa.id As art_id,v.email As ven_email,v.id AS ven_id FROM `artist_gigs` as a                                                                               
                                         LEFT OUTER JOIN artist AS aaa ON aaa.id=a.artist_id
                                         LEFT OUTER JOIN venues AS v ON v.id=a.venues_id
                                         WHERE a.id=".$gig_id."");
        if ($query->num_rows() > 0) {

        $row = $query->row_array();
        if(!empty($row['art_email'])){
            $email=$row['art_email'];
           
            
        }  else {
             $email=$row['ven_email'];
                
        }
        if(isset($_SESSION['user_id'])){
                 $art_id=$_SESSION['user_id'];
            }else{
              $art_id=$row['art_id']; 
            } 
        
            if(isset($_SESSION['venue_id'])){
                $ven_id=$_SESSION['venue_id'];
           }else{
             $ven_id=$row['ven_id']; 
           }  
            
        
        
        $msg= "$message_gig";
   	$subject= "Application for Gig";
   	  
        $data = array(
            'message' =>$message_gig,
            'artist_id' =>$art_id,
            'venue_id'=>$ven_id,
            'gig_id'=>$gig_id
            
        );
        $this->db->insert('gig_messages', $data);
        
        
   	 $this->sent_email($email, $msg, $subject);
   	 
   	 return "Message has been sent.";
        
        }
        
       
    }
    
   public function disableApply() {
         $id=$this->uri->segment(3);
         $query = $this->db->query("SELECT a.id,aaa.id As art_id,v.id AS ven_id FROM `artist_gigs` as a                                                                               
                            LEFT OUTER JOIN artist AS aaa ON aaa.id=a.artist_id
                            LEFT OUTER JOIN venues AS v ON v.id=a.venues_id
                            WHERE a.id=" . $id . "");
          if ($query->num_rows() > 0) {
              $row = $query->row_array();
              if(!empty($row['art_id'])){
                  if(isset($_SESSION['user_id'])){
                    if($row['art_id']==$_SESSION['user_id']){
                       return TRUE;
                       
                    }
                  }
              }else{                
                 if(isset($_SESSION['venue_id'])){
                    if($row['ven_id']==$_SESSION['venue_id']){
                          return TRUE;
                       
                    }
                   
              }
          
          
          }
    }
   } 
    public function sent_email($email, $msg, $subject)
	 {
	 	$this->load->library('email');
	 	$this->email->from('ch.aqeel.asghar@gmail.com', 'Admin');
	 	$this->email->to($email);
	 	$this->email->subject($subject);
	 	$this->email->message( $msg);
	 	$this->email->send();
	 }
		
		
		
    
}

?>
