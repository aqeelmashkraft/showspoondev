<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Venue_gig_model extends CI_Model {
    
    public function __construct() 
     {
           parent::__construct(); 
           $this->load->database();
     }

	 //Get function.
		
    public function get_gig_by_id(){
            $id =  $this->uri->segment(3);
          /*  $query = $this->db->query("SELECT id,type FROM artist_gigs WHERE  id=".$id." ");
            foreach ($query->result() as $row)
                    {
                $query2 = $this->db->query("SELECT a.*,c.CityName,aaa.name As artist_name,aaa.id as art_id,v.name as venue_name,v.id As ven_id,(SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (".$row->type.")  ) AS gener
                                         FROM `artist_gigs` as a
                                         LEFT OUTER JOIN cities AS c ON c.CityID=a.city
                                         LEFT OUTER JOIN artist AS aaa ON aaa.id=a.artist_id
                                         LEFT OUTER JOIN venues AS v ON v.id=a.venues_id
                                         WHERE a.id=".$row->id."");
                                                       
            }*/
            $query2 = $this->db->query("SELECT a.*,c.CityName,aaa.name As artist_name,aaa.id as art_id,v.name as venue_name,v.id As ven_id
                                         FROM `artist_gigs` as a
                                         LEFT OUTER JOIN cities AS c ON c.CityID=a.city
                                         LEFT OUTER JOIN artist AS aaa ON aaa.id=a.artist_id
                                         LEFT OUTER JOIN venues AS v ON v.id=a.venues_id
                                         WHERE a.id=". $id."");
            return $query2->result_array();
            

    }
    public function appayforgig($message_gig,$gig_id) {
        
        $query = $this->db->query("SELECT a.*,v.email As ven_email,v.id AS ven_id FROM `artist_gigs` as a                                                                               
                                LEFT OUTER JOIN artist AS aaa ON aaa.id=a.artist_id
                                LEFT OUTER JOIN venues AS v ON v.id=a.venues_id
                                WHERE a.id=".$gig_id."");
        if ($query->num_rows() > 0) {

                $row = $query->row_array();
                if(!empty($row['ven_email'])){
                    $email=$row['ven_email'];
                    $ven_id=$row['ven_id'];
                }      
      
                $msg= "$message_gig";
                $subject= "Application for Gig";

                $data = array(
                    'message' =>$message_gig,
                    'artist_id' =>$_SESSION['user_id'],
                    'venue_id'=>$ven_id,
                    'gig_id'=>$gig_id
                  );
        $this->db->insert('gig_messages', $data);
        
        
   	 $this->sent_email($email, $msg, $subject);
   	 
   	 return "Message has been sent.";
        
        }
        
       
    }
    
   public function disableApply() {
         $id=$this->uri->segment(3);
         $query = $this->db->query("SELECT a.id,aaa.id As art_id,v.id AS ven_id FROM `artist_gigs` as a                                                                               
                            LEFT OUTER JOIN artist AS aaa ON aaa.id=a.artist_id
                            LEFT OUTER JOIN venues AS v ON v.id=a.venues_id
                            WHERE a.id=" . $id . "");
          if ($query->num_rows() > 0) {
              $row = $query->row_array();
              if(!empty($row['art_id'])){
                  if(isset($_SESSION['user_id'])){
                    if($row['art_id']==$_SESSION['user_id']){
                       return TRUE;
                       
                    }
                  }
              }else{                
                 if(isset($_SESSION['venue_id'])){
                    if($row['ven_id']==$_SESSION['venue_id']){
                          return TRUE;
                       
                    }
                   
              }
          
          
          }
    }
   } 
    public function sent_email($email, $msg, $subject)
	 {
            $this->load->library('email');
            //$config['protocol'] = 'smtp';
            //$config['smtp_host'] = 'ssl://smtp.gmail.com';
            //$config['smtp_port'] = '465';
           // $config['smtp_timeout'] = '7';
           // $config['smtp_user'] = 'roomexploreruk@gmail.com';
           // $config['smtp_pass'] = 'Trekkie2007';
            $config['charset'] = 'utf-8';
            $config['newline'] = "\r\n";
            $config['mailtype'] = 'html'; // or html
            $config['validation'] = TRUE; // bool whether to
            $this->email->initialize($config);
            $this->email->from('support@showspoon.com', 'Showspoon');
            $this->email->to($email);
            $this->email->subject($subject);
            $this->email->message($msg);
            $this->email->send();
	 }
		
		
            public function insert_rating($data_to_store) {

                $this->db->insert('gig_feedback', $data_to_store);
                return $this->db->insert_id();
            }	
              public function get_rate_value() {
                     
                      $id =  $this->uri->segment(3);
		      $query = $this->db->query("SELECT avg(`rating`) as rating_val  FROM `gig_rating` WHERE `gig_id`=".$id." AND `rating_type`=4");
		      return $query->result_array(); 
                 
                   
               }
                public function insert_comment($data_to_store) {

                     $this->db->insert('gig_feedback', $data_to_store);
                     return $this->db->insert_id();
                }
                public function get_comment() {
                     $id =  $this->uri->segment(3);
                   $query=  $this->db->query("SELECT g.*,a.name As artist_name,a.id AS artistid,v.name As venue_name ,v.id As venueid FROM `gig_feedback` AS g 
                                    LEFT OUTER JOIN artist AS a ON a.id=g.artist_id
                                    LEFT OUTER JOIN venues AS v ON v.id=g.venue_id
                                    WHERE g.gig_id=".$id."  Order By g.id DESC limit 0,3");
                    return $query->result_array(); 
                    }
    
}

?>
