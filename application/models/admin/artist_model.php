<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Artist_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_artist() {
        $query = $this->db->query("SELECT a.*,(SELECT ap.image_url From artist_photo AS ap Where ap.artist_id=a.id and ap.set_featured=1) as featuredimg ,(SELECT count(*)  FROM gig_comments AS gg WHERE gg.artist_id=a.id) AS review ,(SELECT c.CityName From cities AS c Where c.CityID=a.city ) as CityName FROM artist AS a WHERE 1  ");
        return $query->result_array();
    }
    function update_artist($data,$id) {
      
        $this->db->where('id', $id);
        $this->db->update('artist', $data);
    }
    // User Artist Registration
    public function create() {
        $password = $this->input->post('art_password');
        $art_genre = implode(',', $this->input->post('art_genre'));
        $art_type_show = implode(',', $this->input->post('art_type_show'));
        $art_type = $this->input->post('art_type');
        $email=$this->input->post('art_email');
        $data2['art_email']=$this->input->post('art_email');
        $data2['name']=$this->input->post('art_name');
        $subject='Velkommen til showspoon';
        $data = array(
            'name' => $this->input->post('art_name'),
            'email' => $this->input->post('art_email'),
            'genre_id' => $art_genre,
            'location' => $this->input->post('art_location'),
            'city' => $this->input->post('art_cities'),
            'gig_type_id' => $art_type_show,
            'band_type_id' => $art_type,
            'password' => $this->hash_password($password),
            'created_on' => date('Y-m-j H:i:s')
        );
        $this->db->insert('artist', $data);
        $message = $this->load->view('template/email/registering',$data2,TRUE);
        $this->sent_email($email, $message, $subject);
    }

     public function insert_artis_file($filename,$id) {
        $setfeatured = $_POST['setfeatured'];
        if ($setfeatured == 1) {
            //echo 'if';
            $set_featured = $this->db->get_where("artist_photo", array('set_featured' => 1, 'artist_id' => $id), 1);

            if ($set_featured->num_rows() > 0) {

                $row = $set_featured->row_array();
                $id = $row['id'];
                $data['set_featured'] = 0;
                $this->db->where('id', $row['id']);
                $this->db->update("artist_photo", $data);
            } else {
                //return "Id not found.";
                $data = array(
                    'image_url' => $filename,
                    'artist_id' => $id,
                    'set_featured' => $setfeatured
                );
                $this->db->insert('artist_photo', $data);
                return $this->db->insert_id();
            }
        }
        $data = array(
            'image_url' => $filename,
            'artist_id' => $id,
            'set_featured' => $setfeatured
        );
        $this->db->insert('artist_photo', $data);
        return $this->db->insert_id();
    }
    
     public function get_files_artist() {
         $id = $this->uri->segment(4);
        $query = $this->db->query("SELECT * FROM artist_photo  WHERE artist_id=" . $id . " ");
        return $query->result();
    }
    
      public function set_Featured($id2,$art_id) {
       $set_featured = $this->db->get_where("artist_photo", array('set_featured' => 1, 'artist_id' => $art_id), 1);
            if ($set_featured->num_rows() > 0) {

                $row = $set_featured->row_array();
                $id = $row['id'];
                $data['set_featured'] = 0;
                $this->db->where('id', $row['id']);
                $this->db->update("artist_photo", $data);
                
                $data2['set_featured'] = 1;
                $this->db->where('id', $id2);
                $this->db->update("artist_photo", $data2);
            }else{
                $data2['set_featured'] = 1;
                $this->db->where('id', $id2);
                $this->db->update("artist_photo", $data2);
            }
    }
    
      public function insert_file($filename, $title, $role,$id) {
        $data = array(
            'image_url' => $filename,
            'name' => $title,
            'role' => $role,
            'artist_id' => $id,
        );
        $this->db->insert('artist_member', $data);
        return $this->db->insert_id();
    }
    
    public function get_files() {
        $id = $this->uri->segment(4);
        $query = $this->db->query("SELECT * FROM artist_member  WHERE artist_id=" .$id. " ");
        return $query->result();
    }
     public function get_artist_media_by_id() {
        $id = $this->uri->segment(4);
        $query = $this->db->query("SELECT * FROM artist_media WHERE artist_id=" . $id . " And type=1 ORDER BY id DESC");
        return $query->result_array();
    }
     public function insert_media($data_to_store) {

        $this->db->insert('artist_media', $data_to_store);
        return $this->db->insert_id();
    }
     public function get_artist_media_by_soundcloud_id() {
         $id = $this->uri->segment(4);
        $query = $this->db->query("SELECT * FROM artist_media WHERE artist_id=" . $id . " And type=2 ORDER BY id DESC");
        return $query->result_array();
    }
  function check($username) {
        $query = $this->db->query("SELECT email FROM artist WHERE email='" . $username . "' ;");

        if ($query->num_rows() > 0) {
            echo '1';
        } else {
            echo '0';
        }
    }

    public function get_user($user_id) {

        $this->db->from('artist');
        $this->db->where('id', $user_id);
        return $this->db->get()->row();
    }
    
  

}

?>
