<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Gigs_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get_gigs() {
        $sql='';
        if ($this->uri->segment(4)=='venid') {
           $id=$this->uri->segment(5);
            $sql=" AND g.venues_id='".$id."' ";
        }
        $query = $this->db->query("SELECT g.*,c.CityName,a.name As artist_name,v.name as venue_name FROM artist_gigs AS g  
			LEFT OUTER JOIN cities AS c ON c.CityID=g.city
			LEFT OUTER JOIN artist AS a ON a.id=g.artist_id
			LEFT OUTER JOIN venues AS v ON v.id=g.venues_id	 WHERE 1 $sql");
		return $query->result_array();
    }
    public function get_my_gig_id($id){
            $query = $this->db->query("SELECT * FROM artist_gigs WHERE id=".$id."  ");
            return $query->result_array();
			 
        }
        
          function update_gig($data,$id) {
            // print_r($data);
            // exit();
            $this->db->where('id', $id);
            $this->db->update('artist_gigs', $data);
            
       }
           public function create($userfile){
	   //$art_genre = implode(',', $this->input->post('art_genre'));
	   $travelaccomodation = (isset($_POST['travel&accomodation'])) ? 1 : 0;
	  // $fooddrink_ticket = (isset($_POST['food&drink_ticket'])) ? 1 : 0;
           //$time="".$_REQUEST['gig_hh'].":".$_REQUEST['gig_mm'].": ".$_REQUEST['gig_ampm']."";
                    $data = array(
				'gig_name'=>  $this->input->post('gig_name'),
				'location'=>$this->input->post('gig_location'),
				'zip'=>$this->input->post('gig_zip'),
				'city'=>$this->input->post('gig_city'),
				'description'=>$this->input->post('gig_description'),
				'Accepting_application_start_date'=>$this->input->post('gig_date_start'),
				'accepting_application_end_date'=>$this->input->post('gig_date_end'),
			        'Max_croner_amount'=>$this->input->post('Max_croner_amount'),
				'parcentage_of_ticketsale'=>$this->input->post('parcentage_of_ticketsale'),
				'travel&accomodation'=>$travelaccomodation,
				//'food&drink_ticket'=>$fooddrink_ticket,
				//'type'=>$art_genre,
				//'requirements'=>$this->input->post('requirements'),
				'numbers_of_bands'=>$this->input->post('numbers_of_bands'),
				'gig_time'=>$this->input->post('gig_hh'),
                                'gig_time_end' => $this->input->post('gig_hh_end'),
				'venues_id'=>$_SESSION['venue_id'],
                                'created_on'=>date('Y-m-j H:i:s'),
                                'image_url'=>$userfile
				);
        $this->db->insert('artist_gigs', $data);
    }
}

?>
