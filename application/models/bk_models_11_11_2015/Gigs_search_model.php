<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gigs_search_model extends CI_Model {
    
    public function __construct() 
     {
           parent::__construct(); 
           $this->load->database();
     }

	 //Get function.
	
    public function get_search_results($limit, $start){
              // print($start);
               //exit();
		$paramt= $this->uri->uri_to_assoc(3);
			$gigs= str_replace('%20', ' ',$paramt['gigs']);
			
			$sql='';
			if($gigs !='all')
			{ 
				$sql="AND g.gig_name LIKE '".$gigs."%' OR g.location LIKE '".$gigs."%'" ;
			}
                        $sql1='';
                        if(isset($_SESSION['venue_id'])){
                            $sql1=' AND g.artist_id!=0 ';
                        }
		$query = $this->db->query("SELECT g.*,c.CityName,a.name As artist_name,v.name as venue_name FROM artist_gigs AS g  
			LEFT OUTER JOIN cities AS c ON c.CityID=g.city
			LEFT OUTER JOIN artist AS a ON a.id=g.artist_id
			LEFT OUTER JOIN venues AS v ON v.id=g.venues_id	 WHERE 1 ".$sql1." ".$sql." ORDER BY g.created_on DESC limit ".$start.",".$limit." ");
		return $query->result_array();
    }
    // Count Return
    function record_count()
    {
        $paramt= $this->uri->uri_to_assoc(3);
			$gigs= str_replace('%20', ' ',$paramt['gigs']);
			
			$sql='';
			if($gigs !='all')
			{ 
				$sql="AND g.gig_name LIKE '".$gigs."%' OR g.location LIKE '".$gigs."%'" ;
			}
                        $sql1='';
                        if(isset($_SESSION['venue_id'])){
                            $sql1=' AND g.artist_id!=0  ';
                        }
                        
		$query = $this->db->query("SELECT g.id FROM artist_gigs AS g WHERE 1 ".$sql1." ".$sql."  ");
		return $query->num_rows();
      
    }
    public function get_ajax_results($limit, $start){
                 //print_r($_REQUEST);
	$sql="SELECT g.*,c.CityName,a.name As artist_name,v.name as venue_name FROM artist_gigs AS g  
			LEFT OUTER JOIN cities AS c ON c.CityID=g.city
			LEFT OUTER JOIN artist AS a ON a.id=g.artist_id
			LEFT OUTER JOIN venues AS v ON v.id=g.venues_id WHERE 1 ";
			//print_r($_REQUEST);	          
                        if(isset($_SESSION['venue_id'])){
                            $sql.='  AND g.artist_id!=0 ';
                        }
        
                 if(isset($_REQUEST['gig_name'])){
			$gig_name=  $this->input->post('gig_name');
                    if(!empty($gig_name)){
                        if($gig_name!='all'){
                            $sql.="AND g.gig_name LIKE '".$gig_name."%' OR g.location LIKE '".$gig_name."%'";
                        }
                         
                    }
                  }
                  
                 if(isset($_REQUEST['gig_city'])){
                 $gig_city=  $this->input->post('gig_city');
                    if(!empty($gig_city)){
                         $sql.="AND g.city='".$gig_city."'";
                    }
                  }
                  if(isset($_REQUEST['gig_type'])){
                       $gig_type= implode(',', $this->input->post('gig_type'));
			$sql.="AND g.type IN(".$gig_type.")";
                  }
                  
                  if(isset($_REQUEST['start_date_to']) && isset($_REQUEST['end_date_to'])){
			 if($_REQUEST['start_date_to']!='' && $_REQUEST['end_date_to']!='' ){

                    $sql.=" AND g.Accepting_application_start_date >='".$_REQUEST['start_date_to']."' AND g.accepting_application_end_date  <= '".$_REQUEST['end_date_to']."'  ";                
                }
		 }
                  
                 $sql.=" ORDER BY g.created_on DESC limit ".$start.",".$limit."";
				// echo $sql;
				 //exit();
             
                  $query = $this->db->query($sql);
		return $query->result_array();
    }
    
    function record_count_ajax()
    {
         $sql="SELECT g.id FROM artist_gigs AS g  WHERE 1  ";
		 
                if(isset($_SESSION['venue_id'])){
                $sql.='  AND g.artist_id!=0  ';
                }

                if(isset($_REQUEST['gig_name'])){
			$gig_name=  $this->input->post('gig_name');
                    if(!empty($gig_name)){                        
                         if($gig_name!='all'){
                           $sql.="AND g.gig_name LIKE '".$gig_name."%' OR g.location LIKE '".$gig_name."%' ";
                        }
                    }
                  }
                  
                 if(isset($_REQUEST['gig_city'])){
                 $gig_city=  $this->input->post('gig_city');
                    if(!empty($gig_city)){
                         $sql.="AND g.city='".$gig_city."'";
                    }
                  }
                  if(isset($_REQUEST['gig_type'])){
                       $gig_type= implode(',', $this->input->post('gig_type'));
			$sql.="AND g.type IN(".$gig_type.")";
                  }
                  
                   if(isset($_REQUEST['start_date_to']) && isset($_REQUEST['end_date_to'])){
			 if($_REQUEST['start_date_to']!='' && $_REQUEST['end_date_to']!='' ){

                    $sql.=" AND g.Accepting_application_start_date >='".$_REQUEST['start_date_to']."' AND g.accepting_application_end_date  <= '".$_REQUEST['end_date_to']."'  ";                
                }
		 }
                
                  $sql.=" ORDER BY g.created_on DESC";
               //echo  $sql;
              //exit();
		$query = $this->db->query($sql);
		return $query->num_rows();
      
    }
		
        public function get_artist(){

                 $query = $this->db->query("SELECT genre_id,gig_type_id FROM artist WHERE id=".$_SESSION['user_id']."");
                 foreach ($query->result() as $row)
                        {

                   $query2 = $this->db->query("SELECT a.*,b.name AS band_typ_name,
                        (SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (".$row->genre_id.")  ) AS gener,
                        (SELECT  GROUP_CONCAT(ag.name) From artist_gig_type As ag Where ag.id IN (".$row->gig_type_id.")) as gigs FROM `artist` as a
                        LEFT OUTER JOIN artist_band_type AS b ON b.id=a.band_type_id
                        WHERE a.id=".$_SESSION['user_id']." ");
                        }
                        return $query2->result_array();
        }
	
    
    
}

?>
