<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    //Get function.

    public function get_search_results($limit, $start) {
        // print($start);
        //exit();
        $paramt = $this->uri->uri_to_assoc(3);
        $venue = str_replace('%20', ' ', $paramt['venue']);

        $sql = '';
        if ($venue != 'all') {
            $sql = "AND v.name LIKE '" . $venue . "%' OR v.location LIKE '" . $venue . "%'";
        }

        $query = $this->db->query("SELECT v.*,(SELECT vp.url From venue_photo AS vp Where vp.venue_id=v.id and vp.set_featured=1) as featuredimg,(SELECT count(*)  FROM gig_comments AS gg WHERE gg.venue_id=v.id) AS review FROM venues AS v WHERE 1 " . $sql . " limit " . $start . "," . $limit . " ");
        return $query->result_array();
    }

    // Count Return
    function record_count() {
        $paramt = $this->uri->uri_to_assoc(3);
        $venue = str_replace('%20', ' ', $paramt['venue']);

        $sql = '';
        if ($venue != 'all') {
            $sql = "AND name LIKE '" . $venue . "%' OR location LIKE '" . $venue . "%'";
        }

        $query = $this->db->query("SELECT id FROM venues WHERE 1 " . $sql . "  ");
        return $query->num_rows();
    }

    public function get_ajax_results($limit, $start) {

                $sql0='';
                if(isset($_REQUEST['genre'])){
                 $genre= implode(',', $this->input->post('genre'));
                 $sql0="AND genre IN(".$genre.")";
                  }
				  $sql1='';
                 if(isset($_REQUEST['ven_name'])){
                 $ven_name=  $this->input->post('ven_name');
                    if(!empty($ven_name)){
                         $sql1="AND v.name LIKE '".$ven_name."%' OR v.location LIKE '".$ven_name."%'";
                    }
                  }
                  $sql2='';
                 if(isset($_REQUEST['ven_city'])){
                 $ven_city=  $this->input->post('ven_city');
                    if(!empty($ven_city)){
                         $sql2="AND v.city='".$ven_city."'";
                    }
                  }
                  $sql4='';
                  if (isset($_REQUEST['start_date']) && isset($_REQUEST['end_date'])) {
                    if ($_REQUEST['start_date'] != '' && $_REQUEST['end_date'] != '') {
                        $sql4="and g.venues_id!=0 AND Accepting_application_start_date >='" . $_REQUEST['start_date'] . "' AND accepting_application_end_date  <= '" . $_REQUEST['end_date'] . "' Group BY v.name ";
                    }
                }

        $sql3=" limit ".$start.",".$limit."";
        
        $sql= "SELECT v.*,(SELECT vp.url From venue_photo AS vp Where vp.venue_id=v.id and vp.set_featured=1) as featuredimg,(SELECT count(*)  FROM gig_comments AS gg WHERE gg.venue_id=v.id) AS review FROM venues AS v LEFT OUTER JOIN artist_gigs As g ON g.venues_id=v.id WHERE 1 
            " . $sql0 . " " . $sql1 . " " . $sql2 . " ".$sql4." 
            UNION
            SELECT v.*,(SELECT vp.url From venue_photo AS vp Where vp.venue_id=v.id and vp.set_featured=1) as featuredimg,(SELECT count(*)  FROM gig_comments AS gg WHERE gg.venue_id=v.id) AS review FROM venues AS v LEFT OUTER JOIN artist_gigs As g ON g.venues_id=v.id WHERE 1 
             " . $sql0 . " " . $sql1 . " " . $sql2 . " ".$sql4." " . $sql3 . "
             ";
        

        
        //echo  $sql;
        // exit();

        $query = $this->db->query($sql);
        //$query = $this->db->query($sql); 
        //echo  $sql;
        //exit();
        return $query->result_array();
    }

    function record_count_ajax() {
        
        $sql1='';
        if (isset($_REQUEST['start_date']) && isset($_REQUEST['end_date'])) {
            if ($_REQUEST['start_date'] != '' && $_REQUEST['end_date'] != '') {

                $sql1="and g.venues_id!=0 AND Accepting_application_start_date >='" . $_REQUEST['start_date'] . "' AND accepting_application_end_date  <= '" . $_REQUEST['end_date'] . "' Group BY v.name ";
            }
        }
        $sql2='';
        if (isset($_REQUEST['genre'])) {
            $genre = implode(',', $this->input->post('genre'));
            $sql2="AND genre in(" . $genre . ")";
        }
        $sql3='';
        if (isset($_REQUEST['ven_name'])) {
            $ven_name = $this->input->post('ven_name');
            if (!empty($ven_name)) {
                $sql3="AND v.name LIKE '" . $ven_name . "%'";
            }
        }
        $sql4='';
        if (isset($_REQUEST['ven_city'])) {
            $ven_city = $this->input->post('ven_city');
            if (!empty($ven_city)) {
                $sql4="AND v.city='" . $ven_city . "'";
            }
        }
        //$sql.="Group By  v.name";
        
        $sql = "SELECT v.id FROM venues AS v LEFT OUTER JOIN artist_gigs As g ON g.venues_id=v.id WHERE 1  ". $sql1." ". $sql2." ". $sql3." ". $sql4."  "
                . "  UNION "
                . "SELECT v.id FROM venues AS v LEFT OUTER JOIN artist_gigs As g ON g.venues_id=v.id WHERE 1  ". $sql1." ". $sql2." ". $sql3." ". $sql4." ";

        
        // echo  $sql;
        //exit();
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    public function get_artist() {

        $query = $this->db->query("SELECT genre_id,gig_type_id FROM artist WHERE id=" . $_SESSION['user_id'] . "");
        foreach ($query->result() as $row) {

            $query2 = $this->db->query("SELECT a.*,b.name AS band_typ_name,
                        (SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (" . $row->genre_id . ")  ) AS gener,
                        (SELECT  GROUP_CONCAT(ag.name) From artist_gig_type As ag Where ag.id IN (" . $row->gig_type_id . ")) as gigs FROM `artist` as a
                        LEFT OUTER JOIN artist_band_type AS b ON b.id=a.band_type_id
                        WHERE a.id=" . $_SESSION['user_id'] . " ");
        }
        return $query2->result_array();
    }

}

?>
