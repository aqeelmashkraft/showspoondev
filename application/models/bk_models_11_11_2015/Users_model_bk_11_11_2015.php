<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    // User Artist Registration
    public function create() {
        $password = $this->input->post('art_password');
        $art_genre = implode(',', $this->input->post('art_genre'));
        $art_type_show = implode(',', $this->input->post('art_type_show'));
        $art_type = $this->input->post('art_type');
        $email=$this->input->post('art_email');
        $data2['art_email']=$this->input->post('art_email');
        $data2['name']=$this->input->post('art_name');
        $subject='Velkommen til showspoon';
        $data = array(
            'name' => $this->input->post('art_name'),
            'email' => $this->input->post('art_email'),
            'genre_id' => $art_genre,
            'location' => $this->input->post('art_location'),
            'city' => $this->input->post('art_cities'),
            'gig_type_id' => $art_type_show,
            'band_type_id' => $art_type,
            'password' => $this->hash_password($password),
            'created_on' => date('Y-m-j H:i:s')
        );
        $this->db->insert('artist', $data);
        $message = $this->load->view('template/email/registering',$data2,TRUE);
        $this->sent_email($email, $message, $subject);
    }

    public function createVenue() {
        $password = $this->input->post('ven_password');
        $ven_genre = implode(',', $this->input->post('ven_genre'));
        $email=$this->input->post('ven_email');
        $data2['ven_email']=$this->input->post('ven_email');
        $data2['name']=$this->input->post('ven_name');
        $subject='Velkommen til showspoon';
        $data = array(
            'name' => $this->input->post('ven_name'),
            'email' => $this->input->post('ven_email'),
            'genre' => $ven_genre,
            'location' => $this->input->post('ven_location'),
            'city' => $this->input->post('ven_cities'),
            'zip' => $this->input->post('ven_zip_code'),
            'password' => $this->hash_password($password)
        );
        $this->db->insert('venues', $data);
        $message = $this->load->view('template/email/registeringvenue',$data2,TRUE);
        $this->sent_email($email, $message, $subject);
    }

    //  User Login Function
    public function user_login($username, $password) {

        $this->db->select('password');
        $this->db->from('artist');
        $this->db->where('email', $username);
        $hash = $this->db->get()->row('password');

        return $this->verify_password_hash($password, $hash);
    }

    public function venue_login($username, $password) {

        $this->db->select('password');
        $this->db->from('venues');
        $this->db->where('email', $username);
        $hash = $this->db->get()->row('password');

        return $this->verify_password_hash($password, $hash);
    }

    // Get User ID ;
    public function get_user_id_from_username($username) {

        $this->db->select('id');
        $this->db->from('artist');
        $this->db->where('email', $username);

        return $this->db->get()->row('id');
    }

    public function get_venue_id_from($username) {

        $this->db->select('id');
        $this->db->from('venues');
        $this->db->where('email', $username);

        return $this->db->get()->row('id');
    }

    function check($username) {
        $query = $this->db->query("SELECT email FROM artist WHERE email='" . $username . "' ;");

        if ($query->num_rows() > 0) {
            echo '1';
        } else {
            echo '0';
        }
    }

    function checkVenue($username) {
        $query = $this->db->query("SELECT email FROM venues WHERE email='" . $username . "' ;");

        if ($query->num_rows() > 0) {
            echo '1';
        } else {
            echo '0';
        }
    }

    function get_artist_band_type_dropdown() {
        $this->db->from('artist_band_type');
        $this->db->order_by('id');
        $result = $this->db->get();
        $return = array();
        if ($result->num_rows() > 0) {
            $return[''] = 'Velg artisttype';
            foreach ($result->result_array() as $row) {
                $return[$row['id']] = $row['name'];
            }
        }
        return $return;
    }

    function get_city_dropdown() {
        $this->db->from('cities');
        $this->db->order_by('CityID');
        $result = $this->db->get();
        $return = array();
        if ($result->num_rows() > 0) {
            $return[''] = 'Velg by';
            foreach ($result->result_array() as $row) {
                $return[$row['CityID']] = $row['CityName'];
            }
        }
        return $return;
    }

    function get_artist_media_type_dropdown() {
        $this->db->from('artist_media_type');
        $this->db->order_by('id');
        $result = $this->db->get();
        $return = array();
        if ($result->num_rows() > 0) {
            $return[''] = 'Please select a media type';
            foreach ($result->result_array() as $row) {
                $return[$row['id']] = $row['type_name'];
            }
        }
        return $return;
    }

    function get_artist_gig_type_dropdown() {
        $this->db->from('artist_gig_type');
        $this->db->order_by('id');
        $result = $this->db->get();
        $return = array();
        if ($result->num_rows() > 0) {
            //$return[''] = 'Please Select Types Of Shows';
            foreach ($result->result_array() as $row) {
                $return[$row['id']] = $row['name'];
            }
        }
        return $return;
    }

    function get_artist_genre_dropdown() {
        $this->db->from('artist_genre');
        $this->db->order_by('id');
        $result = $this->db->get();
        $return = array();
        if ($result->num_rows() > 0) {
            //$return[''] = 'Please Select Types Of Shows';
            foreach ($result->result_array() as $row) {
                $return[$row['id']] = $row['name'];
            }
        }
        return $return;
    }

    //Get function.

    public function get_uder_by_id($id) {
        $this->db->select('*');
        $this->db->from('artist');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function update_artist($data) {
        if (isset($_SESSION['user_id']) && $_SESSION['logged_in'] === true) {
            $id = $_SESSION['user_id'];
        }
        $this->db->where('id', $id);
        $this->db->update('artist', $data);
    }

    public function get_user($user_id) {

        $this->db->from('artist');
        $this->db->where('id', $user_id);
        return $this->db->get()->row();
    }

    public function get_venue($user_id) {

        $this->db->from('venues');
        $this->db->where('id', $user_id);
        return $this->db->get()->row();
    }

    //Creates a password hash
    private function hash_password($password) {

        return password_hash($password, PASSWORD_BCRYPT);
    }

    // verify_password_hash function.

    private function verify_password_hash($password, $hash) {

        return password_verify($password, $hash);
    }

    public function forgetpassword($email = FALSE,$artist_type=FALSE) {
		//echo $email;
        if ($email === FALSE) {
            return "Email is not entered!";
        }
		if($artist_type=='0'){
			$tablename='artist';
		}else{
			$tablename='venues';
		}

        $check_email = $this->db->get_where($tablename, array('email' => $email), 1);

        if ($check_email->num_rows() > 0) {

            $row = $check_email->row_array();
            $email = $row['email'];

            $login_key = rand(1, 200) . time() . $row['id'];
            $data['Login_Key'] = $login_key;
            $this->db->where('id', $row['id']);
            $this->db->update($tablename, $data);

            $name = $row['name'];
            $url = site_url("reset_password/$login_key/type/$artist_type");
            //$msg = "Hi $name\n Please follow link for reset password\n $url ";
            $subject = " Showspoon - Glemt passord lenke";
            $data['name']=$name;
            $data['url'] = $url;
            $data['message_body'] = "f�lg <a href='$url'>link<a> for tilbakestilling av passord\n";
                    
            $message = $this->load->view('template/email/forgot_password',$data,TRUE);
            $this->sent_email($email, $message, $subject);

            return "E-post har blitt sendt. Vennligst sjekk din e-post.";
        } else {
            return "E-post ikke funnet.";
        }
    }

    public function check_key($id = False,$artist_type=False) {
		if($artist_type=='0'){
			$tablename='artist';
		}else{
			$tablename='venues';
		}
		
        $check_key = $this->db->get_where($tablename, array('Login_Key' => $id), 1);

        if ($check_key->num_rows() > 0) {
            $this->Password = $this->hash_password($this->input->post("passwrod"));
            $this->db->update($tablename, $this, array('Login_Key' => $id));

            return 1;
        } else {
            return 0;  //"You have some thing wrong. please click to forget password button and get new link.";
        }
    }

    public function sent_email($email, $msg, $subject) {

        $this->load->library('email');
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.gmail.com';
        $config['smtp_port'] = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user'] = 'roomexploreruk@gmail.com';
        $config['smtp_pass'] = 'Trekkie2007';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html'; // or html
        $config['validation'] = TRUE; // bool whether to
        $this->email->initialize($config);
        $this->email->from('ch.aqeel.asghar@gmail.com', 'Showspoon');
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($msg);
        $this->email->send();
    }
    
    public function msg_rec_count() {
        if (isset($_SESSION['venue_id'])) {
            $query = $this->db->query("SELECT id As totalmsg FROM `messages` WHERE receiver_id=" . $_SESSION['venue_id'] . "  AND type=0 ");
            return $query->num_rows();
        }
        if (isset($_SESSION['user_id'])) {
            $query = $this->db->query("SELECT id As totalmsg FROM `messages` WHERE receiver_id=" . $_SESSION['user_id'] . "  AND type=1 ");
            return $query->num_rows();
        }
    }

}

?>
