<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Venue_dashboard_model extends CI_Model {
    
    public function __construct() 
     {
           parent::__construct(); 
           $this->load->database();
     }

	 //Get function.
	
       public function get_artist()
        {
	      $query = $this->db->query("SELECT a.*,c.CityName FROM artist AS a Left Outer Join cities AS c ON c.CityID=a.city WHERE a.city='".$_SESSION['venue_location']."' ");
              return $query->result_array(); 	
        }
	  public function get_band(){
		   $id =  $this->uri->segment(3);
		   $query = $this->db->query("SELECT * FROM artist_member WHERE id =".$id." ");
		  return $query->result_array(); 
		  
	  }
	   
	   
	    public function create($userfile){
	   //$art_genre = implode(',', $this->input->post('art_genre'));
	   $travelaccomodation = (isset($_POST['travel&accomodation'])) ? 1 : 0;
	  // $fooddrink_ticket = (isset($_POST['food&drink_ticket'])) ? 1 : 0;
           //$time="".$_REQUEST['gig_hh'].":".$_REQUEST['gig_mm'].": ".$_REQUEST['gig_ampm']."";
                    $data = array(
				'gig_name'=>  $this->input->post('gig_name'),
				'location'=>$this->input->post('gig_location'),
				'zip'=>$this->input->post('gig_zip'),
				'city'=>$this->input->post('gig_city'),
				'description'=>$this->input->post('gig_description'),
				'Accepting_application_start_date'=>$this->input->post('gig_date_start'),
				'accepting_application_end_date'=>$this->input->post('gig_date_end'),
			        'Max_croner_amount'=>$this->input->post('Max_croner_amount'),
				'parcentage_of_ticketsale'=>$this->input->post('parcentage_of_ticketsale'),
				'travel&accomodation'=>$travelaccomodation,
				//'food&drink_ticket'=>$fooddrink_ticket,
				//'type'=>$art_genre,
				//'requirements'=>$this->input->post('requirements'),
				'numbers_of_bands'=>$this->input->post('numbers_of_bands'),
				'gig_time'=>$this->input->post('gig_hh'),
                                'gig_time_end' => $this->input->post('gig_hh_end'),
				'venues_id'=>$_SESSION['venue_id'],
                                'created_on'=>date('Y-m-j H:i:s'),
                                'image_url'=>$userfile
				);
        $this->db->insert('artist_gigs', $data);
    }
		
		public function get_venue(){
			 
			 $query = $this->db->query("SELECT genre FROM venues WHERE id=".$_SESSION['venue_id']."");
			 foreach ($query->result() as $row)
				{
        		
			   $query2 = $this->db->query("SELECT a.*,
				(SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (".$row->genre.")  ) AS gener_name
				 FROM `venues` as a
				WHERE a.id=".$_SESSION['venue_id']." ");
				}
				return $query2->result_array();
		}
		
		
		
		public function get_my_gig(){
			
			$query = $this->db->query("SELECT g.*,c.CityName FROM artist_gigs As g Left Outer Join cities AS c ON c.CityID=g.city WHERE g.venues_id=".$_SESSION['venue_id']." ");
			return $query->result_array();
			 
		}
                public function get_my_feedback(){
			
			$query = $this->db->query("SELECT c.*,a.name As art_name,a.id As art_id FROM gig_comments AS c LEFT OUTER JOIN artist As a ON a.id=c.artist_id WHERE c.venue_id =".$_SESSION['venue_id']." ");
			return $query->result_array();
			 
		}
                 public function get_my_application(){
			
			$query = $this->db->query("SELECT m.*,a.name AS art_name,a.id As art_id FROM gig_messages As m LEFT OUTER JOIN artist As a ON a.id=m.artist_id WHERE m.venue_id =".$_SESSION['venue_id']." ");
			return $query->result_array();
			 
		}
               
                 public function get_my_gig_id($id){
			//echo "SELECT * FROM artist_gigs WHERE venues_id=".$id."  ";
                       // exit();
			$query = $this->db->query("SELECT * FROM artist_gigs WHERE id=".$id."  ");
			return $query->result_array();
			 
		}
            function update_gig($data,$id) {
            // print_r($data);
            // exit();
            $this->db->where('id', $id);
            $this->db->update('artist_gigs', $data);
            
       }
       
        function update_venue($data) {
            // print_r($data);
            // exit();
            $this->db->where('id', $_SESSION['venue_id']);
            $this->db->update('venues', $data);
            
       }
       
       
       public function get_venue_by_id(){
			 
            $query = $this->db->query("SELECT * FROM venues WHERE id=".$_SESSION['venue_id']."");
           
            return $query->result_array();
	}
		
		public function get_gig(){
			
			$query = $this->db->query("SELECT * FROM artist_gigs WHERE 1");
			return $query->result_array();
		}
		
	
    public function insert_file($filename)
    {
		$setfeatured=$_POST['setfeatured'];
       if($setfeatured==1){
           //echo 'if';
            $set_featured = $this->db->get_where("venue_photo", array('set_featured' =>1,'venue_id'=>$_SESSION['venue_id']), 1);

                if ($set_featured->num_rows() > 0) {

                    $row = $set_featured->row_array();
                    $id = $row['id'];
                    $data['set_featured'] =0;
                    $this->db->where('id', $row['id']);
                    $this->db->update("venue_photo", $data);
                }  else {
                    //return "Id not found.";
                     $data = array(
                    'url'    =>$filename,
                    'venue_id'   =>$_SESSION['venue_id'],
                                'set_featured'=>$setfeatured   
                );
                //print_r($data);
                $this->db->insert('venue_photo', $data);
                return $this->db->insert_id();
                }
           
       }
	   
        $data = array(
            'url'    =>$filename,
            'venue_id'   =>$_SESSION['venue_id'],
			'set_featured'=>$setfeatured   
        );
        //print_r($data);
        $this->db->insert('venue_photo', $data);
        return $this->db->insert_id();
    }
	
	public function get_files()
	{

				$query = $this->db->query("SELECT * FROM venue_photo  WHERE venue_id=".$_SESSION['venue_id']." ");
              return $query->result(); 
	}
        
        public function get_messages()
        {
	      $query = $this->db->query("SELECT m.*,a.name AS sender_name,a.id AS senderid FROM `messages` As m LEFT OUTER join artist AS a ON m.sender_id=a.id WHERE m.receiver_id=".$_SESSION['venue_id']." AND Type=2 ");
              return $query->result_array(); 	
        }
        function get_artist_dropdown() {
           if(isset($_SESSION['venue_id'])){
             $result = $this->db->query("SELECT * FROM artist");
             $return = array();
             if ($result->num_rows() > 0) {
                 //$return[' '] = 'Please Select a artist';
                 foreach ($result->result_array() as $row) {
                     $return[$row['id']] = $row['name'];
                 }
             }
             return $return;
           }
        
    }
    public function set_Featured($id2) {
       $set_featured = $this->db->get_where("venue_photo", array('set_featured' => 1, 'venue_id' => $_SESSION['venue_id']), 1);
            if ($set_featured->num_rows() > 0) {

                $row = $set_featured->row_array();
                $id = $row['id'];
                $data['set_featured'] = 0;
                $this->db->where('id', $row['id']);
                $this->db->update("venue_photo", $data);
                
                $data2['set_featured'] = 1;
                $this->db->where('id', $id2);
                $this->db->update("venue_photo", $data2);
            }
    }
     public function insert_invitation($data_to_store) {
         //print_r($data_to_store); 
        for ($i = 0; $i < count($data_to_store['receiver_id']); $i++) {
            //echo $id=$data_to_store['receiver_id'].'<br>';
            $id=$data_to_store['receiver_id'];
                    $subject="Invitation For gig";
                    $msg=$data_to_store['message'];
                    $query = $this->db->query("SELECT * From artist AS a  WHERE a.id=".$id."");                                                                             

                    if ($query->num_rows() > 0) {
                            $row = $query->row_array();

                                    $email=$row['email'];

                                    $this->sent_email($email, $msg, $subject);

                    }
                     $this->db->insert('requests', $data_to_store);
                     return $this->db->insert_id();
        }
     }
        
    public function sent_email($email, $msg, $subject)
	 {
            $this->load->library('email');
            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'ssl://smtp.gmail.com';
            $config['smtp_port'] = '465';
            $config['smtp_timeout'] = '7';
            $config['smtp_user'] = 'roomexploreruk@gmail.com';
            $config['smtp_pass'] = 'Trekkie2007';
            $config['charset'] = 'utf-8';
            $config['newline'] = "\r\n";
            $config['mailtype'] = 'html'; // or html
            $config['validation'] = TRUE; // bool whether to
            $this->email->initialize($config);
            $this->email->from('ch.aqeel.asghar@gmail.com', 'Showspoon');
            $this->email->to($email);
            $this->email->subject($subject);
            $this->email->message($msg);
            $this->email->send();
	 }
}

?>
