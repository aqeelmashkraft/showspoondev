<?php $this->load->view('header'); ?>

<?php $this->load->view('mydashboard_artist_links'); ?>

<div class="GreyDashboard  side-collapse-container">
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url() ?>artist_dashboard">Dashboard</a></li>
                    <li class="active"><a href="#">Add Gig</a></li>
                </ul>
            </div><!-- col-md-12 -->

            <div class="col-md-12 col-sm-12 col-xs-12 ArtistDashboarItems">



                <!-- Tab content -->
                <div class="tab-content">
                    <?php //print_r($artist_member);?>
                    <?php
                    if ($this->session->flashdata('flash_message')) {
                        if ($this->session->flashdata('flash_message') == 'insertgig') {
                            echo '<div class="alert alert-success">';
                            echo '<a class="close" data-dismiss="alert">×</a>';
                            echo 'Gig Added with success.';
                            echo '</div>';
                        }
                    }
                    ?>

                    <?php if (isset($msg)) : ?>
                        <div class="col-md-12">
                            <div class="alert alert-danger" role="alert">
                                <?= $msg ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="col-md-12 col-sm-12 col-xs-12 ">
                        <form method="post"  action="<?php echo base_url() ?>artist_dashboard/creategig" id="gig_add" enctype="multipart/form-data">	                                                                                                                                                                  <form id="myForm" class="form-horizontal fv-form fv-form-bootstrap" method="post" action="http://localhost/showspoon/artist_dashboard/edit_profile" novalidate="novalidate"><button type="submit" class="fv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                                <div class="col-md-6 col-sm-6 col-xs-6 ">
                                            <div class="form-group">
                                            <label>Gig Name</label>
                                            <input type="text" class="form-control" placeholder="Gig Name" value="<?php echo set_value('gig_name'); ?>" name="gig_name"  required>
                                        </div><!-- form-group -->

                                        <div class="form-group">
                                            <label>Location</label>
                                            <input type="text" placeholder="Location" name="gig_location" value="<?php echo set_value('gig_location'); ?>" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Zip</label>
                                            <input type="text" placeholder="Zip" name="gig_zip" value="<?php echo set_value('gig_zip'); ?>" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <label>City</label>
                                                    <!--<input type="text" placeholder="City" name="gig_city" class="form-control" required>-->
                                            <?php echo form_dropdown('gig_city', $cities, set_value('gig_city'), 'class="form-control select select-primary select-block mbl"  placeholder="City" '); ?>
                                        </div>
                                       <?php /* <div class="form-group">
                                            <label>Type of Gigs</label>
                                            <?php echo form_dropdown('art_genre[]', $artist_genre, set_value('art_genre[]'), 'class="form-control multiselect mbl" multiple="multiple" placeholder="Genre"'); ?>
                                            <?php //echo form_dropdown('art_type_show[]', $artist_gig_type,'', 'class="form-control multiselect mbl" multiple="multiple" placeholder="Types of Shows"');  ?>

                                        </div> ?*/?>
                                        <div class="form-group">
                                            <label>Max croner amount</label>

                                            <input type="text" class="form-control " value="<?php echo set_value('Max_croner_amount'); ?>" placeholder="Max croner amount"  name="Max_croner_amount" required />

                                        </div>
                                <div class="form-group">
                                    <label>Requirements</label>
                                    <textarea placeholder="Requirements" name="requirements" class="form-control" required ><?php echo set_value('requirements'); ?></textarea>
                                </div>
                                
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 ">
                                    
                                    <div class="form-group">
                                    <label>Parcentage of ticket sale</label>

                                    <input type="text" class="form-control " value="<?php echo set_value('parcentage_of_ticketsale'); ?>" placeholder="Parcentage of ticket sale" name="parcentage_of_ticketsale" required/>

                                </div>
                                     <div class="form-group">
                                    <label>Number of Bands</label>

                                    <input type="number" class="form-control" value="<?php echo set_value('numbers_of_bands'); ?>" placeholder="Numbers of bands" name="numbers_of_bands" required/>

                                </div>
                                <div class="form-group">
                                    <!--<div class="col-md-6 no-gutter check-bottom">
                                <input type="checkbox"  id="travel&accomodation" class="check-fix" name="travel&accomodation"  value="<?php echo set_value('travel&accomodation'); ?>"/>
                                        <label>Food &amp; drink </label>
                                    </div>-->
                                    <!--<div class="col-md-6 no-gutter check-bottom">-->
                                 <input type="checkbox" id="food&drink_ticket" class="check-fix" name="food&drink_ticket"  value="<?php echo set_value('travel&accomodation'); ?>" />
                                    <label>Travel &amp; accommodation</label>                                  
                                   <!-- </div>-->
                                </div> 

                            
                            <div class="col-md-6 no-gutter">
                                <div class="form-group">
                                    <label>Event Start Date</label>

                                    <input type="text" class="form-control " value="<?php echo set_value('gig_date_start'); ?>" placeholder="From" id="datepicker-01" name="gig_date_start" />

                                </div>
                            </div><!-- col-md-6 -->
                             <div class="col-md-6 ">        
                                <div class="form-group">
                                    <label>Event End Date</label>
                                    <input type="text" class="form-control" value="<?php echo set_value('gig_date_end'); ?>"  placeholder="To" id="datepicker-02" name="gig_date_end" />
                                </div>
                            </div><!-- col-md-6 -->
                                    <div class="col-md-6 no-gutter">
                                        <div class="form-group">
                                             <label>Start Time</label>
                                             <i>HH:MM AM/PM</i>
                                            <input type="time" id="timepicker-01" name="gig_hh" class="form-control" value="<?php echo set_value('gig_hh'); ?>" placeholder="HH:MM AM/PM"> 
                                        </div>
                                    </div><!-- col-md-6 -->
                                    <div class="col-md-6 ">        
                                <div class="form-group">
                                    <label>End Time</label>
                                     <i>HH:MM AM/PM</i>
                                    <input type="time" id="timepicker-02" name="gig_hh_end" class="form-control" value="<?php echo set_value('gig_hh_end'); ?>" placeholder="HH:MM AM/PM">
                                </div>
                            </div>
                                   <!-- <div class="col-md-3 alignment-hack">
                                        <div class="form-group">
                                             <label></label>
                                            <input type="text" id="timepicker-01" class="form-control" value="Minutes" placeholder="Minutes"> 
                                        </div>
                                    </div>--><!-- col-md-3-->
                                   <!-- <div class="col-md-3 alignment-hack">
                                        <div class="form-group">
                                             <label></label>
                                            <input type="text" id="timepicker-01" class="form-control" value="" placeholder="am/pm"> 
                                        </div>
                                    </div>--><!-- col-md-3-->
                            <div class="clearfix"></div>
                                <div class="form-group">
                                    <label>Image</label>
                                     <i>MAX SIZE: 2MB And only accept these file types jpg,jpeg,png</i>
                                    <input name="userfile" id="userfile" type="file">
                                </div>          
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea placeholder="Description" name="gig_description" class="form-control" required ><?php echo set_value('gig_description'); ?></textarea>
                                </div>

                                    
                                </div>
                                
                                <div class="col-md-12 col-sm-12 col-xs-12 ">
                                    <input type="submit" class="btn btn-primary btn-artist secondary lg" id="submitbtn" value="Save"> 
                                </div>
                                <!-- form-group -->
                               
                                <!--<a class="btn btn-primary btn-artist secondary lg" href="#">Update</a>-->
                            </form>			
                    </div>

                </div><!-- tab-content -->

            </div><!-- col-md-12 -->

        </div><!-- row -->
    </div><!-- container -->
</div><!-- GreyDashboard -->

<?php $this->load->view('footer'); ?>