<?php $this->load->view('header'); ?>

<?php $this->load->view('mydashboard_artist_links'); ?>

<div class="GreyDashboard  side-collapse-container">
	<div class="container">
		<div class="row">			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="media">
					<div class="media-left">
						<img src="<?php echo base_url();?>assets/images/artish-avatar.jpg" alt="The headbangers" class="center-block img-responsive" />
					</div><!-- media-left -->
					<div class="media-body">
					<?php //print_r($artist); ?>
						<h2 class="media-heading"><?php echo $artist[0]['name'];?> <a href="<?php echo base_url();?>artist_edit_profile"><i class="fa fa-pencil"></i></a></h2>
						<p class="tagband"><?php echo $artist[0]['band_typ_name'];?></p>
						<p class="artistgenre"><?php echo str_replace(",", "/", $artist[0]['gener']);?></p>
					</div><!-- media-body -->
				</div><!-- media -->
			</div><!-- col-md-12 -->
			
			<div class="col-md-12 col-sm-12 col-xs-12 ArtistDashboarItems mth">
				
				<div class="panel">
					<div class="panel-heading">
						<h5>Venues</h5>
                                                <?php //print_r($venues); ?>
<!--							<div class="form-inline">
								<div class="form-group col-md-3 col-sm-3">
									<input type="text" class="form-control" name="venue_name" id="venueid" placeholder="Enter Venue Name">
								</div>
									<div class="form-group col-md-3 col-sm-3">
										<button type="button" class="btn btn-default  secondary" id="seach_btn">Search</button>
									 </div>
						     </div>-->
                        <div class="col-md-2 form-group pull-right mbn no-gutter">
                                                         <input type="text" name="venue_name" id="venueid" placeholder="Search venue" class="form-control smallSearchDash">
                                                         <button type="button" class="btn btn-default  secondary" style="display: none;" id="seach_btn">Search</button>
                                                     </div>
								
						<!--<a class="btn btn-primary btn-artist secondary" href="#">Search Venue <i class="fa fa-search"></i></a>-->
					</div><!-- panel-heading -->
					
					<div class="panel-body">
                                            <?php 
                                                         if (count($venues) > 0) {
                                                    ?>
						<div class="table-responsive">
							<table class="table table-hover table-condensed">
								<thead>
									<tr>
										<td>Name</td>
										<td>Genre</td>
										<td>Age</td>
										<td>Type</td>
										<td>Location</td>
									</tr>
								</thead>
								<tbody>
								<?php
								foreach($venues as $key => $value)
								{
								  
								  $query = $this->db->query("SELECT a.id,(SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (".$value['genre'].")  ) AS tt FROM `venues` as a WHERE a.id= ".$value['id']."");
							   foreach ($query->result() as $row)
									{
								 	
								  echo '<tr>';
								  echo '<td><a href="'.base_url().'venue_detail/index/id/'.$value['id'].'">'.$value['name'].'</td></a>';
								  echo '<td>'.$row->tt.'</td>';
								  echo '<td>'.$value['age'].'</td>';
								  echo '<td>'.$value['name'].'</td>';
								  echo '<td>'.$value['location'].','.$value['CityName'].'</td>';
								  
								  echo '</tr>';
                                                 
								 } 
								}
								?>
									<!--<tr>
										<td>The Good Ship</td>
										<td>Rock / Cover / Hip Hop</td>
										<td>25</td>
										<td>Open Mic</td>
										<td>City Center, Oslo</td>
									</tr>
									<tr>
										<td>Rock Island</td>
										<td>Rock / Hip Hop</td>
										<td>20</td>
										<td>One time gig</td>
										<td>City Center, Oslo</td>
									</tr>
									<tr>
										<td>Jimmy Breaks All</td>
										<td>Rock / Hip Hop</td>
										<td>28</td>
										<td>Open Mic / Opening Act</td>
										<td>City Center, Oslo</td>
									</tr>-->
								</tbody>
							</table>
                                                         
						</div><!-- table-responsive -->
                                                <?php }else{?>
                                                          <p><strong>No venue's available in you city</strong></p>
                                                         <?php }?>
					</div><!-- panel-body -->
					
				</div><!-- panel -->
				
				<div class="panel">
					<div class="panel-heading">
						<h5>Gigs</h5>
                        <div class="col-md-2 form-group pull-right no-gutter mbn">
                                                    <input type="text" name="gig_name" id="gigid" placeholder="Search Gigs" class="form-control smallSearchDash">
                                                   
                                                </div>
						<a class="btn btn-primary btn-artist secondary" href="#" style="display: none;" >Search Gigs <i class="fa fa-search"></i></a>
					</div><!-- panel-heading -->
					
					<div class="panel-body">
						<div class="table-responsive">
                                                    <?php 
                                                         if (count($gigs) > 0) {
                                                    ?>
							<table class="table table-hover table-condensed">
								<thead>
									<tr>
										<td>&nbsp;</td>
										<td>Name</td>
										<td>Genre</td>
										<td>Date</td>
										<td>By</td>
										<td>Location</td>
									</tr>
								</thead>
								<tbody>
								<?php
								foreach($gigs as $key => $value)
								{
                                                                     $type='(Artist)';
                                                                    if($value['artist_id']==0){
                                                                        $type='(Venue)';
                                                                    }
                                                                    $imgpath="".base_url()."assets/images/no-image.jpg";
                                                                            if(!empty($value['image_url'])){
                                                                                 $imgpath="".base_url()."uploads/gigs/".$value['image_url'];
                                                                             }
                                                                $query = $this->db->query("SELECT (SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (".$value['type'].")  ) AS gener
                                                                     FROM `artist_gigs` as a
                                                                         WHERE a.id=".$value['id']."");
                                                                     foreach ($query->result() as $row)
                                                                         
                                                                         
									{?>
									<tr>
										<td>
										<a href="<?php echo base_url()?>gig_details/id/<?php echo $value['id']; ?>">
											<img src="<?php echo $imgpath;?>" alt="" class="thumbnail mg-responsive gigstableimage">
										</a>

									   
									   </td>
										<td><?php echo $value['gig_name']; ?></td>
										<td><?php echo $row->gener; ?></td>
										<td><?php echo $value['created_on'];?></td>
										<td><?php if(!empty($value['artist_name'])){echo $value['artist_name'];}else {echo $value['venue_name'];}?><span class="type"><?php echo $type; ?></span></td>
										<td><?php echo $value['location'];?> ,<?php echo $value['CityName'];?></td>
									</tr>
								<?php }}?>
									
								</tbody>
							</table>
                            <?php }else{ ?>
                                <p><strong>No gigs available in in your city</strong></p>
                            <?php }?>
						</div><!-- table-responsive -->
					</div><!-- panel-body -->
					
				</div><!-- panel -->
				
				<div class="panel">
					<div class="panel-heading">
						<h5>My Gigs</h5>
						<a class="btn btn-primary btn-artist secondary" href="<?php echo base_url()?>artist_dashboard/add_gig">Add Gig <i class="fa fa-plus"></i></a>
					</div><!-- panel-heading -->
					<?php //print_r($mygig);?>
					<div class="panel-body">
                                            <?php
						  if($this->session->flashdata('flash_message')){
							if($this->session->flashdata('flash_message') == 'delete')
							{
							  echo '<div class="alert alert-danger">';
							  echo '<a class="close" data-dismiss="alert">×</a>';
							  echo 'Gig deleted successfully.';
							  echo '</div>';       
							}
						  }
						?>
                                                     <?php 
                                                         if (count($mygig) > 0) {
                                                    ?>
						<div class="table-responsive">
							<table class="table table-hover table-condensed">
								<thead>
									<tr>
										<td>Name</td>
										<td>Genre</td>
										<td>Date</td>
										<td>Location</td>
										<td>Action</td>
									</tr>
								</thead>
								<tbody>
								<?php
								foreach($mygig as $key => $value)
								{
							   $query = $this->db->query("SELECT (SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (".$value['type'].")  ) AS gener
			                    FROM `artist_gigs` as a
				                WHERE a.id=".$value['id']." AND a.artist_id=".$value['artist_id']." ");
								foreach ($query->result() as $row)
									{
							   ?>
									<tr>
                                                                            <td><a href="<?php echo base_url()?>gig_details/id/<?php echo $value['id']; ?>"><?php echo $value['gig_name']; ?></a></td>
										<td><?php echo $row->gener; ?></td>
										<td><?php echo $value['created_on'];?></td>
										<td><?php echo $value['location'];?> ,<?php echo $value['CityName'];?></span></td>
										<td>
										<a href="<?php echo base_url()?>artist_dashboard/edit_gig/<?php echo $value['id']; ?>" class="ActionControls"><i class="fa fa-pencil"></i></a>
										<a href="<?php echo base_url()?>artist_dashboard/delete_gig/<?php echo $value['id']; ?>"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
									
								<?php }} ?>
									<!--<tr>
										<td>Abitudini Live</td>
										<td>Death Metal</td>
										<td>30 Sep 2015</td>
										<td>Open Mic</span></td>
										<td>
											<a href="#" class="ActionControls"><i class="fa fa-pencil"></i></a>
											<a href="#"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
									<tr>
										<td>Abitudini Live</td>
										<td>Death Metal</td>
										<td>30 Sep 2015</td>
										<td>Open Mic</span></td>
										<td>
											<a href="#" class="ActionControls"><i class="fa fa-pencil"></i></a>
											<a href="#"><i class="fa fa-trash"></i></a>
										</td>
									</tr>-->
								</tbody>
							</table>
						</div>
                                                         <?php }else {?><!-- table-responsive -->
                                                         <p><strong>You did not have any gigs</strong></p>
                                                         <?php }?>
					</div><!-- panel-body -->
					
				</div><!-- panel -->
				
			</div><!-- col-md-12 -->
			
		</div><!-- row -->
	</div><!-- container -->
</div><!-- GreyDashboard -->


<?php $this->load->view('footer'); ?>