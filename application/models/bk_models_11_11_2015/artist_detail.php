<?php $this->load->view('header'); ?>

<div class="GreySection side-collapse-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 no-gutter">
                <div class="col-md-6 col-sm-12 col-xs-12 ArtistInfo">
                    <h2><?php echo $artistbyid[0]['name']?></h2>
					<?php //print_r($artistbyid);?>
                    <p class="artistDetails"><span class="btn badge"><?php echo $artistbyid[0]['band_typ_name']?></span> <?php echo $artistbyid[0]['gener']?></p>
					<p class="artistDetails v2"><span class="btn badge">interested in</span> <?php echo $artistbyid[0]['gigs']?></p>
                    <p class="artistMapLocation"><i class="fa fa-map-marker"></i> <?php echo $artistbyid[0]['zip']?> <?php  echo $artistbyid[0]['city']?>, Norway.</p>
                    <p class="artistDescription"><?php echo $artistbyid[0]['short_description']?></p>
                    
                    <div class="col-md-12 col-sm-12 col-xs-12 no-gutter taligntab">
                        <!--<input id="input-id" type="number" class="rating" min=0 max=5 step=1 data-size="xs" data-rtl="false">-->
                       <!-- <i class="fa fa-star cStar"></i>
						<i class="fa fa-star cStar"></i>
						<i class="fa fa-star cStar"></i>
						<i class="fa fa-star-o cStar"></i>
						<i class="fa fa-star-o cStar"></i>-->
						
						  <div class="fb-like" data-href="<?php echo base_url()?>artist_detail/index/id/<?php echo $artistbyid[0]['id']?>" data-width="45" data-layout="button_count" data-action="like" data-show-faces="true" data-share="flase"></div>
						<svg height="0" width="0">
						  <filter id="fb-filter">
							<feColorMatrix type="hueRotate" values="0"/>
						  </filter>
						</svg>
						<style>
						  .fb-like, .fb-send, .fb-share-button {
							-webkit-filter: url(#fb-filter); 
							filter: url(#fb-filter);
						  }
						</style>
						
                    </div><!-- col-md-6 -->
					<div class="clearfix"></div>
                    <div class="col-md-6 col-sm-12 col-xs-12 no-gutter">
                        <a class="btn btn-primary btn-artist secondary contrast col-md-6" data-toggle="modal" data-target="#exampleModal2">Invite</a>
                         <button type="button" class="btn btn-primary btn-artist secondary contrast comments col-md-6" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Message</button>
                    </div><!-- col-md-12 -->
                    
                </div><!-- col-md-6 -->
                
                <div class="col-md-6 col-sm-12 col-xs-12 SliderBox">
					<div class="carousel slide article-slide" id="myCarousel">
					  <div class="carousel-inner cont-slider">
                                               <?php
                                               $counter=0;
                                               foreach($artistgalllary as $key => $value)
                                               {
                                                   $counter++;
                                                  if($counter==1){
                                              ?>
                                              
						<div class="item active">
						  <img src="<?php echo base_url();?>files/artist_gallery/<?php echo $value['image_url'];  ?>" class="center-block img-responsive">
						</div>
                                                  <?php }else{?>
                                              <div class="item">
						  <img src="<?php echo base_url();?>files/artist_gallery/<?php echo $value['image_url'];  ?>" class="center-block img-responsive">
						</div>
                                               <?php }}?>
						<?php /*<div class="item active">
						  <img src="<?php echo base_url();?>assets/images/slide2.jpg" class="center-block img-responsive">
						</div>
						<div class="item">
						  <img src="<?php echo base_url();?>assets/images/slide3.jpg" class="center-block img-responsive">
						</div>
						<div class="item">
						  <img src="<?php echo base_url();?>assets/images/slide1.jpg" class="center-block img-responsive">
						</div>  */?>             
					  </div>

					  <!-- Indicators -->
					  <ol class="carousel-indicators">
                                              <?php
                                              $counter2=(1-2);
                                               foreach($artistgalllary as $key => $value)
                                               {
                                                   $counter2++;
												 // echo  $counter2;
                                                   if($counter2==0){
                                              ?>
                                             
						<li class="active" data-slide-to="<?php echo $counter2;?>" data-target="#myCarousel">
						  <img alt="" title="" src="<?php echo base_url();?>files/artist_gallery/<?php echo $value['image_url'];  ?>">
						</li>
                                                   <?php }else{?>
                                                
						<li class="" data-slide-to="<?php echo $counter2;?>" data-target="#myCarousel">
						  <img alt="" title="" src="<?php echo base_url();?>files/artist_gallery/<?php echo $value['image_url'];  ?>">
						</li>
                                               <?php }}?>
						<?php /*<li class="active" data-slide-to="0" data-target="#myCarousel">
						  <img alt="" title="" src="<?php echo base_url();?>assets/images/slide2.jpg">
						</li>
						<li class="" data-slide-to="1" data-target="#myCarousel">
						  <img alt="" title="" src="<?php echo base_url();?>assets/images/slide3.jpg">
						</li>
						<li class="" data-slide-to="2" data-target="#myCarousel">
						  <img alt="" title="" src="<?php echo base_url();?>assets/images/slide1.jpg">
						</li>  */?>             
					  </ol>                 
					</div>
                </div><!-- col-md-6 -->
            </div><!-- col-md-12 -->
        </div><!-- row -->
    </div><!-- container -->
</div><!-- GreySection -->

<div class="SkewSection  side-collapse-container">
    <div class="container">
        <div class="row">
            
            <div class="col-md-6 col-sm-12 col-xs-12 GreySkew">
                <div class="col-md-12 panel BioBox">
                    <div class="panel-body">
                        <h4 class="bio">Bio</h4>
                        <p class="biodetail"><?php echo $artistbyid[0]['biography']?></p>
                    </div>
                </div>
            </div><!-- col-md-6 -->
            
            <div class="col-md-5 col-sm-12 col-xs-12 col-md-push-1 BlackSkew">
                <h4>Band Member</h4>
				<?php //print_r($band_member);?>
				<?php 
				foreach($band_member as $key => $value)
				{
				?>
				<div class="col-md-6 col-sm-4 col-xs-12">
					<h5><?php echo $value['name'];?></h5>
					<p class="position"><?php echo $value['role'];?></p>
				</div><!-- col-md-6 -->
				<?php }?>
				<?php /* <div class="col-md-6 col-sm-4 col-xs-12">
					<h5>Ryan Peake</h5>
					<p class="position">Bass Guiatarist</p>
				</div><!-- col-md-6 -->
				<div class="col-md-6 col-sm-4 col-xs-12">
					<h5>Mike Kroeger</h5>
					<p class="position">Drummer</p>
				</div><!-- col-md-6 --> */ ?>
            </div><!-- col-md-4 -->
            
        </div><!-- row -->
    </div><!-- container -->
</div><!-- SkewSection -->

<div class="LightGrey  side-collapse-container">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-12 col-xs-12">
				<h4>Youtube Videos</h4>
                                <?php
                                    //print_r($media);
                                   // exit();
                                    foreach($media as $key => $value)
				  { ?>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="embed-responsive embed-responsive-16by9">
                                             <object width="150" height="87" data="<?php echo str_replace('watch?v=', 'v/', $value['url']) ;?>" type="application/x-shockwave-flash"><param name="src" value="<?php echo str_replace('watch?v=', 'v/', $value['url']) ;?>" /></object>
					  
					</div>
				</div><!-- col-md-4 -->
                                <?php }?>
				
			</div><!-- col-md-6 -->
			<div class="col-md-6 col-sm-12 col-xs-12">
				<h4>Soundcloud</h4>
                                <?php
                                    //print_r($media);
                                   // exit();
                                    foreach($mediasound as $key => $value)
				    { 
                                        $url=$value['url'];
                                        //Get the JSON data of song details with embed code from SoundCloud oEmbed
                                        $getValues=file_get_contents('http://soundcloud.com/oembed?format=js&url='.$url.'&iframe=true');
                                        //Clean the Json to decode
                                        $decodeiFrame=substr($getValues, 1, -2);
                                        //json decode to convert it as an array
                                        $jsonObj = json_decode($decodeiFrame);

                                        //Change the height of the embed player if you want else uncomment below line
                                        // echo $jsonObj->html;
                                        //Print the embed player to the page
                                       
                                    ?>
				<div class="col-md-12 col-sm-12 col-xs-12 no-gutter">
                                    <?php  echo str_replace('height="450"', 'height="81"', $jsonObj->html);?>
<!--					<object height="81" width="100%" id="yourPlayerId" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">
					  <param name="movie" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fmatas%2Fhobnotropic&enable_api=true&object_id=yourPlayerId"></param>
					  <param name="allowscriptaccess" value="always"></param>
					  <embed allowscriptaccess="always" height="81" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fmatas%2Fhobnotropic&enable_api=true&object_id=yourPlayerId" type="application/x-shockwave-flash" width="100%" name="yourPlayerId"></embed>
					</object>-->
				</div><!-- col-md-12 -->
                                  <?php }?>
			</div><!-- col-md-6 -->
			
			<div class="col-md-12 col-sm-12 col-xs-12 gigography">
				<h4>Gigography</h4>
				<?php
				foreach($mygigbyid as $key => $value)
				{
                                    $imgpath="".base_url()."assets/images/no-image.jpg";
                                if(!empty($value['image_url'])){
                                         $imgpath="".base_url()."/gigs/thumbs/".$value['image_url'];
                                    }
			        $query = $this->db->query("SELECT (SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (".$value['type'].")  ) AS gener
				FROM `artist_gigs` as a
				WHERE a.id=".$value['id']."");
				foreach ($query->result() as $row)
					{
                                    
                                    
				?>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="panel">
						<div class="panel-body">
							<div class="media">
							  <div class="media-left">
								  <img class="media-object" src="<?php echo $imgpath; ?>" alt="The long Road">
							  </div>
							  <div class="media-body">
								<h4 class="media-heading"><?php echo $value['gig_name'];?></h4>
								<p><?php echo $value['created_on'];?></p>
							  </div>
								<p class="gigdetails"><?php echo $value['description'];?></p>
								<a href="<?php echo base_url();?>gig_details/id/<?php echo $value['id']?>" class="viewdetails">View Details</a>
							</div>
						</div>
					</div>
				</div><!-- col-md-4 -->
				<?php }} ?>
			<?php /*	
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="panel">
						<div class="panel-body">
							<div class="media">
							  <div class="media-left">
								  <img class="media-object" src="<?php echo base_url();?>assets/images/longride.jpg" alt="The long Road">
							  </div>
							  <div class="media-body">
								<h4 class="media-heading">The long Road</h4>
								<p>28th August 2015s</p>
							  </div>
								<p class="gigdetails">A rock festival which will be organized by the citizens foundation and sponsored by Jzz FM 99. A three day concert which will</p>
								<a href="#" class="viewdetails">View Details</a>
							</div>
						</div>
					</div>
				</div><!-- col-md-4 -->
				
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="panel">
						<div class="panel-body">
							<div class="media">
							  <div class="media-left">
								  <img class="media-object" src="<?php echo base_url();?>assets/images/longride.jpg" alt="The long Road">
							  </div>
							  <div class="media-body">
								<h4 class="media-heading">The long Road</h4>
								<p>28th August 2015s</p>
							  </div>
								<p class="gigdetails">A rock festival which will be organized by the citizens foundation and sponsored by Jzz FM 99. A three day concert which will</p>
								<a href="#" class="viewdetails">View Details</a>
							</div>
						</div>
					</div>
				</div><!-- col-md-4 --> */?>
				
			</div><!-- col-md-12 -->
			
		</div><!-- row -->
	</div><!-- container -->
</div><!-- LightGrey -->

<!----------------------Send Message Model------------------------->
                               
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="exampleModalLabel">Send message</h4>
                                            </div>
                                            <form method="post" action="" id="message_to_venue">
                                            <div class="modal-body">
                                                
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Subject:</label>
                                                        <input name="artist_subject" type="text" class="form-control" id="artist_subject">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="message-text" class="control-label">Message:</label>
                                                        <textarea name="artist_message" class="form-control" id="artist_message"></textarea>
                                                        <input name="art_id" type="hidden" value="<?php  echo $id;?>" class="form-control" id="art_id">
                                                        <input name="ven_id" type="hidden" value="<?php echo $_SESSION['venue_id'];?>" class="form-control" id="ven_id">
                                                        <input name="type" type="hidden" value="1" class="form-control" id="type">
                                                    </div>
                                               
                                                <div id="successmsg"></div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <!--<button type="submit" class="btn btn-primary">Send message</button>-->
                                                <input type="submit" name="submit"class="btn btn-primary" value="Send message" />
                                            </div>
                                          </form>
                                        </div>
                                    </div>
                                </div>
<!-------------------------------------Send invite------------------------------------------------->
  <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="exampleModalLabel">Send invitation</h4>
                                            </div>
                                            <form method="post" action="" id="invite_to_artist">
                                            <div class="modal-body">
                                                
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Gig:</label>
                                                          <?php echo form_dropdown('gig_id', $mygig_dropdown, '', 'class="form-control select select-primary select-block mbl" id="gig_id" placeholder="Gig"'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="message-text" class="control-label">Message:</label>
                                                        <textarea name="venue_message" class="form-control" id="venue_message"></textarea>
                                                        <input name="art_id" type="hidden" value="<?php  echo $id;?>" class="form-control" id="art_id">
                                                        <input name="ven_id" type="hidden" value="<?php echo $_SESSION['venue_id'];?>" class="form-control" id="ven_id">
                                                       
                                                    </div>
                                               
                                                <div id="successmsg2"></div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <!--<button type="submit" class="btn btn-primary">Send message</button>-->
                                                <input type="submit" name="submit"class="btn btn-primary" value="invite" />
                                            </div>
                                          </form>
                                        </div>
                                    </div>
                                </div>
<?php $this->load->view('footer'); ?>