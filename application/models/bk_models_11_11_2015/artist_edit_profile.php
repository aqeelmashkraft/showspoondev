<?php $this->load->view('header'); ?>
<?php $this->load->view('mydashboard_artist_links'); ?>


<div class="GreyDashboard  side-collapse-container">
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url() ?>artist_dashboard">Dashboard</a></li>
                    <li class="active"><a href="#">Update Profile</a></li>
                </ul>
            </div><!-- col-md-12 -->

            <div class="col-md-12 col-sm-12 col-xs-12 ArtistDashboarItems">
                <?php
                $gallary = $this->uri->segment(2);

                $activcls = 'active';
                $activ = '';
                $activ2 = '';
                if ($this->session->flashdata('flash_active')) {
                    if ($this->session->flashdata('flash_active') == 'avtive') {
                        $activ = 'active';
                        $activcls = '';
                        $activ2 = '';
                    }
                }

                if ($this->session->flashdata('flash_active2')) {
                    if ($this->session->flashdata('flash_active2') == 'active2') {
                        $activ = '';
                        $activcls = '';
                        $activ2 = 'active';
                    }
                }
                ?>
                <ul class="nav nav-tabs nav-append-content">
                    <li class="<?php echo $activcls; ?>"><a href="#profile" data-toggle="tab">Profile</a></li>
                    <li class="<?php echo $activ; ?>"><a href="#bandmembers" data-toggle="tab">Band Members</a></li>
                    <li class="<?php echo $activ2; ?>" ><a href="#gallery" data-toggle="tab">Media</a></li>
                </ul><!-- nav -->

                <!-- Tab content -->
                <div class="tab-content">
                    <div class="tab-pane <?php echo $activcls; ?>" id="profile">
                        <div class="col-md-12 col-sm-12 col-xs-12 xm-no-gutter">
                            <?php //print_r($users); ?>
                            <?php
                            $genre_id['genre_id'] = explode(',', $users[0]['genre_id']);
                            $gig_type_id['gig_type_id'] = explode(',', $users[0]['gig_type_id'])
                            ?>
                            <?php
                            //var_dump($this->session->flashdata('flash_message'));
                            //flash messages
                            if ($this->session->flashdata('flash_message')) {
                                if ($this->session->flashdata('flash_message') == 'updated') {
                                    echo '<div class="alert alert-success">';
                                    echo '<a class="close" data-dismiss="alert">×</a>';
                                    echo 'Profile is updated with success.';
                                    echo '</div>';
                                }
                            }
                            ?>
                            <form id="myForm" class=""  method="post" action="<?php echo base_url() ?>artist_dashboard/edit_profile">

                                <div class="col-md-6 col-sm-6 col-xs-12 xm-no-gutter">
                                    <div class="form-group">
                                        <label>Band Name</label>
                                        <input type="text" class="form-control" placeholder="Band Name" name="art_name" value="<?php echo $users[0]['name']; ?>"  required />
                                    </div><!-- form-group -->

                                    <div class="form-group">
                                        <label>Genre</label>
                                        <?php echo form_dropdown('art_genre[]', $artist_genre, $genre_id['genre_id'], 'class="form-control multiselect mbl" multiple="multiple"  placeholder="Genre"'); ?>

                                    </div><!-- form-group -->

                                    <div class="form-group">
                                        <label>Band Type</label>
                                        <?php echo form_dropdown('art_type', $artist_band_type, $users[0]['band_type_id'], 'class="form-control select select-primary select-block mbl" placeholder="Type"'); ?>

                                    </div><!-- form-group -->

                                    <div class="form-group">
                                        <label>Type of Gigs</label>
                                        <?php echo form_dropdown('art_type_show[]', $artist_gig_type, $gig_type_id['gig_type_id'], 'class="form-control multiselect mbl" multiple="multiple" placeholder="Types of Shows"'); ?>

                                    </div><!-- form-group -->

                                    <div class="form-group">
                                        <label>Zip</label>
                                        <input type="text" class="form-control" placeholder="Zip" name="art_zip" value="<?php echo $users[0]['zip']; ?>" required />

                                    </div>

                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 xm-no-gutter">


                                    <!--   <div class="form-group" >
                                       <label>Location</label>
                                       <input type="hidden" placeholder="Location" name="art_location" value="<?php echo $users[0]['location']; ?>" class="form-control" />
                                   </div> -->
                                    <div class="form-group">
                                        <label>City</label>
                                        <input type="hidden" placeholder="Location" name="art_location" value="<?php echo $users[0]['location']; ?>" class="form-control" />
                                        <?php echo form_dropdown('art_city', $cities, $users[0]['city'], 'class="form-control select select-primary select-block mbl"  placeholder="City"'); ?>
                                          <!--<input type="text" placeholder="Location" name="art_city" value="<?php echo $users[0]['city']; ?>" class="form-control" />-->
                                    </div>
                                    <div class="form-group">
                                        <label>Short Description</label>
                                        <textarea placeholder="Description" name="art_short_description" class="form-control" required ><?php echo $users[0]['short_description']; ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Biography</label>
                                        <textarea placeholder="Biography" name="art_biography"  class="form-control" required ><?php echo $users[0]['biography']; ?></textarea>
                                    </div>
                                    <!-- form-group -->

                                </div>
                                <div class="col-md-12 col-sm-6 col-xs-12 xm-no-gutter">
                                    <input type="submit" class="btn btn-primary btn-artist secondary lg" id="submitbtn" value="Update">
                                </div>

                                <!--<a class="btn btn-primary btn-artist secondary lg" href="#">Update</a>-->
                            </form>		
                        </div><!-- col-md-12 -->
                    </div><!-- profile -->


                    <div class="tab-pane <?php echo $activ; ?>" id="bandmembers">
                        <div class="col-md-12 col-sm-12 col-xs-12 no-gutter">


                            <?php
                            if ($this->session->flashdata('flash_message')) {
                                if ($this->session->flashdata('flash_message') == 'Delete_member') {
                                    echo '<div class="alert alert-danger">';
                                    echo '<a class="close" data-dismiss="alert">×</a>';
                                    echo 'Artist member is delete with success.';
                                    echo '</div>';
                                }
                            }
                            ?>
                            <?php
                            //print_r($files);
                            $count = 0;
                            if (count($files) > 0) {
                                $count = count($files);
                                $str = '<p>You have  ' . $count . ' band members</p>';
                            } else {
                                $str = '<p>You do not have any band members</p>';
                            }
                            ?>
                            <div class="col-md-6 col-sm-6 col-xs-12"><?php echo $str; ?></div>

                            <div class="col-md-6 col-sm-6 col-xs-12"><a class="btn btn-primary btn-artist secondary" href="#" id="btn_add_band"><i class="fa fa-plus"></i> Add New Band Members</a></div>
                            <div class="clearfix"></div>
                            <div id="files" class="col-md-12 mtm"></div>
                            <?php /* ?>
                              <div class="col-md-4">
                              <div class="panel BandMember">
                              <div class="panel-body">
                              <div class="media">
                              <div class="media-left">
                              <img class="media-object" src="<?php echo base_url() ?>assets/images/sample-images/avatar.jpg" alt="" class="center-block img-responsive" />
                              </div>
                              <div class="media-body">
                              <h4 class="media-heading">Media heading</h4>
                              <p>Guitarist</p>
                              <a href="#" class="btn btn-inverse btn-sm editmember">Edit</a>
                              <a href="#" class="btn btn-inverse btn-sm editmember black">Delete</a>
                              </div>
                              </div>
                              </div><!-- panel-body -->
                              </div><!-- panel -->
                              </div><!-- col-md-4 -->

                              <div class="col-md-4">
                              <div class="panel BandMember">
                              <div class="panel-body">
                              <div class="media">
                              <div class="media-left">
                              <img class="media-object" src="<?php echo base_url() ?>assets/images/sample-images/avatar.jpg" alt="" class="center-block img-responsive" />
                              </div>
                              <div class="media-body">
                              <h4 class="media-heading">Media heading</h4>
                              <p>Guitarist</p>
                              <a href="#" class="btn btn-inverse btn-sm editmember">Edit</a>
                              <a href="#" class="btn btn-inverse btn-sm editmember black">Delete</a>
                              </div>
                              </div>
                              </div><!-- panel-body -->
                              </div><!-- panel -->
                              </div><!-- col-md-4 -->

                              <div class="col-md-4">
                              <div class="panel BandMember">
                              <div class="panel-body">
                              <div class="media">
                              <div class="media-left">
                              <img class="media-object" src="<?php echo base_url() ?>assets/images/sample-images/avatar.jpg" alt="" class="center-block img-responsive" />
                              </div>
                              <div class="media-body">
                              <h4 class="media-heading">Media heading</h4>
                              <p>Guitarist</p>
                              <a href="#" class="btn btn-inverse btn-sm editmember">Edit</a>
                              <a href="#" class="btn btn-inverse btn-sm editmember black">Delete</a>
                              </div>
                              </div>
                              </div><!-- panel-body -->
                              </div><!-- panel -->
                              </div><!-- col-md-4 -->
                              <?php */ ?>

                            <br /><br /><br />

                            <form method="post" action="" id="upload_file">
                                <div class="col-md-12 col-sm-12 col-xs-12 no-gutter AddNewMember mbl" id="AddNewMember">
                                    <div class="col-md-3 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;">
                                                    <img data-src="holder.js/100x100" alt="">
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100px; max-height: 100px;"></div>
                                                <div>
                                                    <span class="btn btn-primary btn-file">
                                                        <span class="fileinput-new"><span class="fui-image"></span> Select image</span>
                                                        <span class="fileinput-exists"><span class="fui-gear"></span> Change</span>
                                                        <input type="file" name="userfile" id="userfile" size="20" >
                                                    </span>
                                                    <a href="#" class="btn btn-primary btn-file fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>  Remove</a>
                                                </div>
                                            </div><!-- fileinput -->
                                        </div><!-- form-group -->
                                        <small class="help-block" data-fv-validator="notEmpty" id="error2" data-fv-for="art_location" data-fv-result="INVALID" style=""></small>
                                    </div><!-- col-md-4 -->
                                    <div class="col-md-3 col-sm-4 col-xs-12">
                                        <input type="text" class="form-control mbl mth" name="title" id="title" placeholder="Name" required/>
                                    </div><!-- col-md-3 -->
                                    <div class="col-md-3 col-sm-4 col-xs-12">
                                        <input type="text" class="form-control mbl mth"  name="role" id="role"  placeholder="Role" required/>
                                    </div><!-- col-md-4 -->
                                    <div class="col-md-3 col-sm-4 col-xs-12">

                                        <input type="submit" name="submit" class="btn btn-primary btn-file mbl mth" id="submit" value="Save" />
                                        <!--<a href="#" class="btn btn-primary btn-file mbl mth">Save</a>
                                        <a href="#" class="btn btn-primary btn-file mbl mth">Delete</a>-->
                                    </div>
                                </div><!-- col-md-12 -->
                            </form>
                            <!--<a class="btn btn-primary btn-artist secondary lg" href="#">Update</a>-->
                        </div><!-- col-md-12 -->
                    </div><!-- bandmembers -->


                    <div class="tab-pane <?php echo $activ2; ?>" id="gallery">
                        <div class="col-md-12 no-gutter">
                            <h5>Photo Gallery</h5>
                        </div><!-- col-md-12 -->
                        <div class="col-md-12 col-sm-12 col-xs-12 GalleryTab mbh">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <form method="post" action="" id="upload_file_artist">
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;">
                                                <img data-src="holder.js/100x100" alt="">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100px; max-height: 100px;"></div>
                                            <div class="clearfix"></div>
                                            <label class="checkbox col-md-8" for="checkbox1">
                                                <input type="checkbox" name="setfeatured"   id="checkbox1" > Set Featured
                                            </label>
                                            <div class="clearfix"></div>
                                            <div>
                                                <span class="btn btn-primary btn-file">
                                                    <span class="fileinput-new">
                                                        <span class="fui-image"></span> Select image
                                                    </span>
                                                    <span class="fileinput-exists"><span class="fui-gear"></span> Change</span>
                                                    <input type="file" name="userfile" id="userfile3" size="20" >
                                                </span>
                                                <a href="#" class="btn btn-primary btn-file fileinput-exists" data-dismiss="fileinput">
                                                    <span class="fui-trash"></span>  Remove
                                                </a>
                                                <input type="submit" name="submit" class="btn btn-primary btn-file" id="submitvenue" value="Save" />
                                            </div>
                                            <small class="help-block" data-fv-validator="notEmpty" id="error" data-fv-for="art_location" data-fv-result="INVALID" style=""></small>
                                        </div><!-- fileinput -->
                                    </div><!-- form-group -->
                                </form> 
                            </div>
                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <div id="files3">

                                </div>    
                            </div>


                        </div><!-- col-md-4 -->

                        <div class="col-md-12 no-gutter">
                            <h5>Videos</h5>
                        </div><!-- col-md-12 -->
                        <div class="col-md-12 col-sm-12 col-xs-12 GalleryTab mbh" id="files4">
                           
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 GalleryTab mbh">

                            <div class="col-md-12 no-gutter videoblock">

                                <form method="post" action="" id="upload_file_media">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <h5>Add New Videos</h5>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <input type="hidden"  value="1" id="media_type" name="media_type" class="form-control mbl" />
                                            <input type="text"  placeholder="Video Title" id="video_title" name="video_title" class="form-control mbl" required />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">

                                            <input type="url" placeholder="Video Link" id="video_link" name="video_link" class="form-control mbl" required="required" />
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <!--<a class="btn btn-primary btn-file mbl" href="#">Save</a>-->

                                        <input type="submit" name="submit" class="btn btn-primary btn-file mbl" id="submitvenue" value="Save" />
                                    </div><!-- col-md-4 --> 
                                    <div id="success"></div>
                                </form>

                            </div><!-- videoblock -->

                            <!--<div class="col-md-12">
                                    <a class="btn btn-primary btn-file mbl" href="#">Add More Video</a>
                            </div>--><!-- col-md-12 -->

                        </div><!-- col-md-12 -->


                        <div class="col-md-12 no-gutter">
                            <h5>SoundCloud</h5>
                        </div><!-- col-md-12 -->
                        <div class="col-md-12 col-sm-12 col-xs-12 GalleryTab mbh" id="files5">
                           
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 GalleryTab mbl">

                            <div class="col-md-12 no-gutter SoundCloudBlock mbl">
                                <form method="post" action="" id="upload_file_media_sound">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <h5>Add New SoundCloud Link </h5>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <input type="hidden"  value="2" id="media_type2" name="media_type" class="form-control mbl" />
                                            <input type="text"  placeholder="Title" id="video_title2" name="video_title" class="form-control mbl" required />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">

                                            <input type="url" placeholder="Link" id="video_link2" name="video_link" class="form-control mbl" required="required" />
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <!--<a class="btn btn-primary btn-file mbl" href="#">Save</a>-->

                                        <input type="submit" name="submit" class="btn btn-primary btn-file mbl" id="submitsound" value="Save" />
                                    </div><!-- col-md-4 --> 
                                    <div id="success2"></div>
                                </form>

                                <!--<div class="col-md-12 col-sm-12 col-xs-12">
                                    <object height="81" width="100%" id="yourPlayerId" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">
                                        <param name="movie" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fmatas%2Fhobnotropic&enable_api=true&object_id=yourPlayerId"></param>
                                        <param name="allowscriptaccess" value="always"></param>
                                        <embed allowscriptaccess="always" height="81" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fmatas%2Fhobnotropic&enable_api=true&object_id=yourPlayerId" type="application/x-shockwave-flash" width="100%" name="yourPlayerId"></embed>
                                    </object>
                                </div>--><!-- col-md-12 -->
                            </div><!-- videoblock -->

                            <div class="col-md-12">
                                <!--<a class="btn btn-primary btn-file mbl" href="#">Add More Sound Samples</a>-->
                            </div><!-- col-md-12 -->

                        </div><!-- col-md-12 -->

                        <!--<a class="btn btn-primary btn-artist secondary lg" href="#">Update</a>-->

                    </div><!-- gallery -->

                </div><!-- tab-content -->

            </div><!-- col-md-12 -->

        </div><!-- row -->
    </div><!-- container -->
</div><!-- GreyDashboard -->


<?php $this->load->view('footer'); ?>