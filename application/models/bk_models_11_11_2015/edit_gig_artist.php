<?php $this->load->view('header'); ?>

<?php $this->load->view('mydashboard_artist_links'); ?>

<div class="GreyDashboard  side-collapse-container">
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url() ?>artist_dashboard">Dashboard</a></li>
                    <li class="active"><a href="#">Edit Gig</a></li>
                </ul>
            </div><!-- col-md-12 -->

            <div class="col-md-12 col-sm-12 col-xs-12 ArtistDashboarItems">



                <!-- Tab content -->
                <div class="tab-content">
                    <?php
                    //print_r($gig_by_id);
                    //$type['type'] = explode(',', $gig_by_id[0]['type']);
                    ?>
                    <?php
                    if ($this->session->flashdata('flash_message')) {
                        if ($this->session->flashdata('flash_message') == 'updated') {
                            echo '<div class="alert alert-success">';
                            echo '<a class="close" data-dismiss="alert">×</a>';
                            echo 'Gig Updated with success.';
                            echo '</div>';
                        }
                    }
                    ?>

                    <div class="col-md-12 col-sm-12 col-xs-12 ">
                        <form method="post"  action="<?php echo base_url() ?>artist_dashboard/edit_gig_update" id="gig_add">	                                                                                                                                                                  <form id="myForm" class="form-horizontal fv-form fv-form-bootstrap" method="post" action="http://localhost/showspoon/artist_dashboard/edit_profile" novalidate="novalidate"><button type="submit" class="fv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                               
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                   
                                    <div class="form-group">
                                    <label>Gig Name</label>
                                    <input type="text" class="form-control" placeholder="Gig Name" value='<?php echo $gig_by_id[0]['gig_name'] ?>' name="gig_name"  required>
                                </div><!-- form-group -->

                                <div class="form-group">
                                    <label>Location</label>
                                    <input type="text" placeholder="Location" name="gig_location" value='<?php echo $gig_by_id[0]['location'] ?>' class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>Zip</label>
                                    <input type="text" placeholder="Zip" name="gig_zip" value='<?php echo $gig_by_id[0]['zip'] ?>' class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label>City</label>
                                    <?php echo form_dropdown('gig_city', $cities, $gig_by_id[0]['city'], 'class="form-control select select-primary select-block mbl"  placeholder="City"'); ?>
                                    <?php /* <input type="text" placeholder="City" name="gig_city"  value='<?php echo $gig_by_id[0]['city']; ?>'class="form-control" required> */ ?>
                                </div>
                               <?php /* <div class="form-group">
                                    <label>Type of Gigs</label>
                                    <?php echo form_dropdown('art_genre[]', $artist_genre, $type['type'], 'class="form-control multiselect mbl" multiple="multiple" placeholder="Genre"'); ?>
                                    <?php //echo form_dropdown('art_type_show[]', $artist_gig_type,'', 'class="form-control multiselect mbl" multiple="multiple" placeholder="Types of Shows"');  ?>

                                </div> */?>
                                <div class="form-group">
                                    <label>Max croner amount</label>

                                    <input type="text" class="form-control " value='<?php echo $gig_by_id[0]['Max_croner_amount'] ?>' placeholder="Max croner amount"  name="Max_croner_amount" required />

                                </div>
                                <div class="form-group">
                                    <label>Requirements</label>
                                    <textarea placeholder="Requirements" name="requirements" class="form-control" required ><?php echo $gig_by_id[0]['requirements']; ?></textarea>
                                </div>
                                    
                               </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    
                                    <div class="form-group">
                                    <label>Parcentage of ticket sale</label>

                                    <input type="text" class="form-control " value='<?php echo $gig_by_id[0]['parcentage_of_ticketsale'] ?>' placeholder="Parcentage of ticket sale" name="parcentage_of_ticketsale" required/>

                                </div>
                                <div class="form-group">

                                    <label>Travel & accomodation</label>

                                    <input type="checkbox"  id="travel&accomodation" name="travel&accomodation"   <?php if ($gig_by_id[0]['travel&accomodation'] == 1) {
                                        echo 'checked';
                                    } ?>/>


                                   <!-- <label>Food & drink </label>

                                    <input type="checkbox" id="food&drink_ticket"  name="food&drink_ticket"     <?php if ($gig_by_id[0]['food&drink_ticket'] == 1) {
                                        echo 'checked';
                                    } ?> />-->

                                </div> 
                                    <div class="col-md-6 no-gutter">
                                        <div class="form-group">
                                             <label>Event Time</label>
                                             <i>HH:MM AM/PM</i>
                                             <input type="time" id="timepicker-01" name="gig_hh" value="<?php echo $gig_by_id[0]['gig_time']?>" class="form-control"  placeholder="Hours"> 
                                        </div>
                                    </div><!-- col-md-6 -->
                                     <div class="col-md-6 ">        
                                <div class="form-group">
                                    
                                    <label> End Time</label>
                                    <i>HH:MM AM/PM</i>
                                    <input type="time" class="form-control" value="<?php echo $gig_by_id[0]['gig_time_end']?>"  placeholder="To"  name="gig_hh_end" />
                                </div>
                            </div><!-- col-md-6 -->
                                  <!--  <div class="col-md-3 alignment-hack">
                                        <div class="form-group">
                                             <label></label>
                                             <input type="text" id="timepicker-01" name="gig_mm" class="form-control"  placeholder="Minutes"> 
                                        </div>
                                    </div>--><!-- col-md-3-->
                                  <!--  <div class="col-md-3 alignment-hack">
                                        <div class="form-group">
                                             <label></label>
                                            <input type="text" id="timepicker-01" name="gig_ampm" class="form-control"  placeholder="am/pm"> 
                                        </div>
                                    </div>--><!-- col-md-3-->
                                    <div class="clearfix"></div>
                                <div class="form-group">
                                    <label>Event Start Date</label>

                                    <input type="text" class="form-control " value='<?php echo $gig_by_id[0]['Accepting_application_start_date'] ?>' placeholder="From" id="datepicker-01" name="gig_date_start" />

                                </div>
                                <div class="form-group">
                                    <label>Event End Date</label>
                                    <input type="text" class="form-control" value='<?php echo $gig_by_id[0]['accepting_application_end_date'] ?>' placeholder="To" id="datepicker-02" name="gig_date_end" />
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea placeholder="Description"  name="gig_description" class="form-control" required ><?php echo $gig_by_id[0]['description']; ?></textarea>
                                </div>
                                   
                                </div>
                                
                                
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                     <input type="hidden" value="<?php echo$id = $this->uri->segment(3); ?>" name='gig_id' />
                                <input type="submit" class="btn btn-primary btn-artist secondary lg" id="submitbtn" value="Update">
                                </div>
                                <!-- form-group -->
                               
                                <!--<a class="btn btn-primary btn-artist secondary lg" href="#">Update</a>-->
                            </form>			
                    </div>

                </div><!-- tab-content -->

            </div><!-- col-md-12 -->

        </div><!-- row -->
    </div><!-- container -->
</div><!-- GreyDashboard -->


<?php $this->load->view('footer'); ?>