<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Showspoon</title>

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/flat-ui-pro.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.css">
    <link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/star-rating.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic,700,900,100,100italic' rel='stylesheet' type='text/css'>
    <link rel="icon" href="<?php echo base_url();?>assets/images/favicon.png" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery/jRating.jquery.css" type="text/css" />
       <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>


<!--    <script src="https://maps.googleapis.com/maps/api/js"></script>-->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
    <body>
        
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=656181691090460";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
        
        
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container width-control">
            <div class="navbar-header">
              <button data-toggle="collapse-side" data-target=".side-collapse" data-target-2=".side-collapse-container" type="button" class="navbar-toggle pull-right"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
                  <a class="navbar-brand" href="<?php echo base_url(); ?>">
                  <img class="img-responsive logooverflow" src="<?php echo base_url();?>assets/images/logo.png"  alt="Showspoon"/>
                </a>

            </div>
			
            <div class="side-collapse in" id="bs-example-navbar-collapse-1">
				<div role="navigation" class="navbar-collapse">
					
				  <ul class="nav navbar-nav navbar-right">
					  <?php if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) : ?>
                                                    <?php if($_SESSION['artist_type']==0){?>
                                      <a href="#" class="login"><span class="label label-info">Artist</span>&nbsp;&nbsp;<?php echo $_SESSION['user_name'];?></a>
                                                    <?php }else{?>
                                                          <a href="#" class="login"><span class="label label-primary">Venue</span>&nbsp;&nbsp;<?php echo $_SESSION['venue_name'];?></a> 
                                                    <?php }?>
						<?php else : ?>
							<a href="<?php echo base_url() ?>login" class="login"><i class="fa fa-lock"></i> Login</a>
						<?php endif; ?>
					<br clear="all" />
                                        <?php if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) { ?>
                                              <?php if($_SESSION['artist_type']==0){?>
                                          <li><a href="<?php echo base_url() ?>artist_dashboard">Home</a></li>
                                              <?php }else{?>
                                           <li><a href="<?php echo base_url() ?>venue_dashboard">Home</a></li>
                                              <?php }?>
                                              <?php }else{ ?>
                                         <li><a href="<?php echo base_url() ?>">Home</a></li>
                                              <?php }?>
                                        
					  
					
					 <?php if (isset($_SESSION['venue_id'] )) { ?>
					<li><a href="<?php echo base_url() ?>artist_search/index/artist/all">Artists</a></li>
					 <?php }else{?>
                                        <li><a href="<?php echo base_url() ?>login/access_artists">Artists</a></li>
                                         <?php }?>
					  <?php if (isset($_SESSION['user_id'] )) { ?>
					<li><a href="<?php echo base_url() ?>search/index/venue/all">Venues</a></li>
					  <?php }else{ ?>
                                        <li><a href="<?php echo base_url() ?>login/access_venues">Venues</a></li>
                                        <?php } ?>
                                        <?php if(isset($_SESSION['venue_id']) || isset($_SESSION['user_id'])){?>
					<li><a href="<?php echo base_url() ?>gigs_search/index/gigs/all">Gigs</a></li>
                                        <?php }else{?>
                                        <li><a href="<?php echo base_url() ?>login/access_gigs">Gigs</a></li>
                                        <?php }?>
                      <!--<li><a href="#">How it works</a></li>-->
					<!--<li><a href="<?php echo base_url() ?>faq">FAQs </a></li>
                      <li><a href="#">About Us</a></li>-->
					<li><a href="<?php echo base_url() ?>contactus">Contact Us</a></li>
					<li class="visible-sm visible-xs"><a href="<?php echo base_url() ?>login">Login</a></li>

					<?php if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) : ?>
					<li class="dropdown">
					  <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="fui-user"></span><b class="caret"></b></a>
					  <span class="dropdown-arrow"></span>
					  <ul class="dropdown-menu">
                                              <?php if($_SESSION['artist_type']==0){?>
						<li><a href="<?php echo base_url() ?>artist_dashboard">My Dashboard</a></li>
                                              <?php }else{?>
                                                <li><a href="<?php echo base_url() ?>venue_dashboard">My Dashboard</a></li>
                                              <?php }?>
                                                 <?php if (isset($_SESSION['venue_id'] )) { 
                                                     $query = $this->db->query("SELECT id As totalmsg FROM `messages` WHERE receiver_id=" . $_SESSION['venue_id'] . "  AND type=0 ");
                                                  ?>
						<li><a href="<?php echo base_url() ?>venue_messages">Messages <span class="navbar-new"><?php  echo $query->num_rows(); ?></span></a></li>
                                                 <?php }else{
                                                     $query2 = $this->db->query("SELECT id As totalmsg FROM `messages` WHERE receiver_id=" . $_SESSION['user_id'] . "  AND type=1 ");
                                                  ?>
                                                <li><a href="<?php echo base_url() ?>artist_messages">Messages <span class="navbar-new"><?php  echo $query2->num_rows(); ?></span></a></li>
                                                <?php }?>
						<li><a href="<?php echo base_url() ?>register/logout">Logout</a></li>
					  </ul>
                	</li> 
					<?php endif; ?>

				  </ul>
				</div>
            </div>
          </div>
        </nav>
