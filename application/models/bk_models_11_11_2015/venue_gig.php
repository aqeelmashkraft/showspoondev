<?php $this->load->view('header'); ?>
<div class="GreySection side-collapse-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 no-gutter">
                <div class="col-md-8 col-sm-12 col-xs-12 ArtistInfo VenueInfo">
                    <h2 class="venueheading"><?php echo $gigbyid[0]['gig_name']?></h2>
                    <?php //print_r($gigbyid); ?>
                    <p class="artistDetails"><span class="btn badge">GIG</span>By:  <?php if(!empty($gigbyid[0]['venue_name'])){echo '<a href="'.base_url().'venue_detail/index/id/'.$gigbyid[0]['ven_id'].'">'.$gigbyid[0]['venue_name'].'</a>'; }else{echo '<a href="'.base_url().'artist_detail/index/id/'.$gigbyid[0]['art_id'].'">'.$gigbyid[0]['artist_name'].'</a>';}?>  <!--By: Power Rock House--></p>
                    
					<p class="artistDetails v2"><?php echo str_replace(","," / ",$gigbyid[0]['gener']);?></p>
					<!--<p class="artistDetails v2">Age : 20 to 40</p>-->
                                        <?php $date = date_create($gigbyid[0]['Accepting_application_start_date']);?>
                    <p class="artistMapLocation"><?php echo date_format($date, 'l jS F Y \@ g:ia');?><!--Monday 25th August 2015 @ 10:00 PM--></p>
					<p class="artistDetails v2 venuelocation"><a href="#" class="redpin"><i class="fa fa-map-marker"></i></a> <?php echo $gigbyid[0]['location']?>  <?php echo $gigbyid[0]['zip']?>  <?php echo $gigbyid[0]['CityName']?>, Norway.</p>
                                        
                    <div class="col-md-12 col-sm-12 col-xs-12 no-gutter taligntab">
                        <div class="rating" style="width:126px;float:left; display:inline;">
                                    <div class="exemple">
                                        <div class="exemple4 blackStar" data-average="<?php echo round($rate[0]['rating_val'],2);?>" data-class="4" data-id="<?php echo $id; ?>" ></div>
                                        </div>
                                </div>
                        <!--<input id="input-id" type="number" class="rating" min=0 max=5 step=1 data-size="xs" data-rtl="false">-->
                          
						
                                             <div class="fb-like" data-href="<?php echo base_url()?>gig_details/id/<?php echo $gigbyid[0]['id']?>" data-width="45" data-layout="button_count" data-action="like" data-show-faces="true" data-share="flase"></div>
                                                
						<!--<div class="fb-like" data-href="http://facebook.com" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
						<svg height="0" width="0">
						  <filter id="fb-filter">
							<feColorMatrix type="hueRotate" values="0"/>
						  </filter>
						</svg>-->
						<style>
						  .fb-like, .fb-send, .fb-share-button {
							-webkit-filter: url(#fb-filter); 
							filter: url(#fb-filter);
						  }
						</style>
						
                    </div><!-- col-md-12 -->
					<div class="col-md-12 col-sm-12 col-xs-12 acceptingapplications no-gutter">
						<p class="acceptHeading">We are accepting applications for this gig</p>
                                                <?php $date = date_create($gigbyid[0]['accepting_application_end_date']);?>
						<p class="accpetdetail">Last date to receive applicatios is <?php echo date_format($date, 'l jS F Y');?></p>
					</div>
                    <div class="col-md-6 col-sm-12 col-xs-12 no-gutter">
                        <?php if($disableapplybutton==1){
                                                    
                            ?>
                        <a class="btn btn-primary btn-artist secondary contrast green" href="#">You are host</a>
                        <?php }else{?>
                        <?php if(!empty($gigbyid[0]['ven_id'])){?>
                         <a class="btn btn-primary btn-artist secondary contrast green pull-left" data-toggle="modal" data-target="#feedback">Apply Now</a>
                        <a class="btn btn-primary btn-artist secondary contrast pull-left pull-left" style="margin-left:5px;" data-toggle="modal" data-target="#applygig">Feedback</a>
                        <?php }else{?>
                            <a class="btn btn-primary btn-artist secondary contrast green pull-left">Apply here</a>
                            <a class="btn btn-primary btn-artist secondary contrast pull-left" style="margin-left:5px;" data-toggle="modal" data-target="#feedback">Feedback</a>
                        <?php }}?>
                    </div><!-- col-md-12 -->
                    
                </div><!-- col-md-6 -->
                
                <div class="col-md-4 col-sm-12 col-xs-12">
                                        <?php 
                                            $imgpath="".base_url()."assets/images/no-image.jpg";
                                            if(!empty($gigbyid[0]['image_url'])){
                                                 $imgpath="".base_url()."uploads/gigs/".$gigbyid[0]['image_url'];
                                             }
                                        ?>
					<img src="<?php echo $imgpath;?>" alt="Venue Image" class="img-responsive center-block VenueImg" />
                </div><!-- col-md-6 -->
            </div><!-- col-md-12 -->
        </div><!-- row -->
    </div><!-- container -->
</div><!-- GreySection -->


<div class="LightGrey  side-collapse-container">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 no-gutter venuegig">
				<div class="col-md-6 col-sm-12 col-xs-12">
					<h4>Gig Description</h4>
                                       
					<p class="description"><?php echo $gigbyid[0]['description']?></p>
					
					<h4>Compensation for Bands</h4>
					<p class="points"><strong>Maximum Kroner Amount:</strong> <span><?php echo $gigbyid[0]['Max_croner_amount']?> Kr</span></p>
					<p class="points"><strong>Travel and Accomodation:</strong> <span><?php if($gigbyid[0]['travel&accomodation']==0){ echo 'Not Available'; }else{ echo 'Available';}?></span></p>
                                        <p class="points"><strong>Food and Drinks Tickets:</strong> <span><?php if($gigbyid[0]['food&drink_ticket']==0){ echo 'Not Available'; }else{ echo 'Available';}?></span></p>
					<p class="points"><strong>Percentage of Ticket Sales:</strong> <span><?php echo $gigbyid[0]['parcentage_of_ticketsale']?>%</span></p>
					
					
					<div class="col-md-12 no-gutter SocialShareGroup">
                                            <div class="fb-share-button" data-href="<?php echo base_url()?>gig_details/id/<?php echo $gigbyid[0]['id']?>" data-layout="box_count"></div>
						<?php /*<a href="#" class="socialShare"><img src="<?php echo base_url();?>assets/images/facebook1.png" alt="Facebook" class="img-responsive center-block" /></a>
						<a href="#" class="socialShare"><img src="<?php echo base_url();?>assets/images/gplus.png" alt="Google Plus" class="img-responsive center-block" /></a>
						<a href="#" class="socialShare"><img src="<?php echo base_url();?>assets/images/instagram.png" alt="Instagram" class="img-responsive center-block" /></a>
						
						<a href="#" class="socialShare"><img src="<?php echo base_url();?>assets/images/pinterest.png" alt="Pinterest" class="img-responsive center-block" /></a>
						<a href="#" class="socialShare"><img src="<?php echo base_url();?>assets/images/tumbler.png" alt="Tumbler" class="img-responsive center-block" /></a> */?>
					</div><!-- col-md-12 -->
					
				</div><!-- col-md-6 -->
				
				<!-- Show only latest 2 comments -->
				
				<div class="col-md-6 col-sm-8 col-xs-12 col-md-offset-0 col-sm-offset-2 ComentsDisplay">
					<h4>Comments</h4>
					<div class="media bottomborder">
						<div class="media-left">
							<a href="#">
								<img class="media-object imagecontrol" src="<?php echo base_url();?>assets/images/no-image.jpg" alt="no-image" />
							</a>
						</div>
						<div class="media-body">
							<h6 class="media-heading">Media heading</h6>
							<p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. </p>
						</div>
					</div>
					<div class="media bottomborder">
						<div class="media-left">
							<a href="#">
								<img class="media-object imagecontrol" src="<?php echo base_url();?>assets/images/no-image.jpg" alt="no-image" />
							</a>
						</div>
						<div class="media-body">
							<h6 class="media-heading">Media heading</h6>
							<p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. </p>
						</div>
					</div>
				</div><!-- col-md-6 -->
				
			</div><!-- col-md-12 -->
		</div><!-- row -->
	</div><!-- container -->
</div><!-- LightGrey -->


<!-- Apply for Gig -->
<div class="modal fade" id="applygig" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h5 class="modal-title text-center" id="myModalLabel">Application for Gig</h5>
				<h3 class="modal-title text-center" id="myModalLabel"><?php echo $gigbyid[0]['gig_name']?></h3>
                                <?php $date2 = date_create($gigbyid[0]['Accepting_application_start_date']);?>
				<h5 class="modal-title text-center applyDate" id="myModalLabel"><?php echo date_format($date2, 'l jS F Y \@ g:ia');?></h5>
			</div>
			<div class="modal-body">
                            <textarea rows="5" class="form-control" name="message_gig" id="message_gig" placeholder="Enter your message"></textarea>
                            <input type="hidden" value="<?php echo $this->uri->segment(3); ?>" id="gig_id"  name="gig_id" />
                             
				<div id="ajax-results"></div>
                                <button type="button" class="btn btn-primary btn-artist secondary centerbtn" id="app_for_gig">Submit</button>
						
			</div>
		</div>
	</div>
</div>

                     <div class="modal fade" id="feedback" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h5 class="modal-title text-center" id="myModalLabel">Feedback </h5>
				<h3 class="modal-title text-center" id="myModalLabel"></h3>
                              
				<h5 class="modal-title text-center applyDate" id="myModalLabel"></h5>
			</div>
			<div class="modal-body">
                            <div class="col-md-12 col-sm-6 col-xs-12 no-gutter RatingByArtist col-sm-offset-3 col-md-offset-0">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <p class="headingtag">&nbsp;&nbsp;</p>
                                </div>
                                <div class="rating" style="width:126px;float:left; display:inline;">
                                    <div class="exemple">
                                        <div class="basic" data-average="0" data-class="4" data-id="<?php echo $id;?>" ></div>
                                    </div>
                                </div>

                            </div>
                            
                            
                <div class="col-md-12 col-sm-6 col-xs-12 no-gutter RatingByArtist col-sm-offset-3 col-md-offset-0">
                             <form method="post" action="" id="gig_comment_post">
                                 <textarea rows="5" class="form-control" name="ven_comment" id="ven_comment" placeholder="Enter your comment" required></textarea>
                                 <input type="hidden" name="gig_id" id="gig_id" value="<?php echo $id;?>"/>
                                <div id="success"></div>
                                <input type="submit" name="submit"class="btn btn-primary btn-artist secondary centerbtn" value="Submit" />
                          </form>
                </div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('footer'); ?>