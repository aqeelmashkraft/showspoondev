﻿<?php $this->load->view('header'); ?>

<?php $this->load->view('mydashboard_links'); ?>

<div class="GreyDashboard  side-collapse-container">
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url() ?>venue_dashboard">Dashbord</a></li>
                    <li class="active"><a href="#">Legg Gig</a></li>
                </ul>
            </div><!-- col-md-12 -->

            <div class="col-md-12 col-sm-12 col-xs-12 ArtistDashboarItems">



                <!-- Tab content -->
                <div class="tab-content">
                    <?php //print_r($artist_member);?>
                    <?php
                    if ($this->session->flashdata('flash_message')) {
                        if ($this->session->flashdata('flash_message') == 'insertgig') {
                            echo '<div class="alert alert-success">';
                            echo '<a class="close" data-dismiss="alert">×</a>';
                            echo 'Gig Lagt med suksess.';
                            echo '</div>';
                        }
                    }
                    ?>
                    <?php if (isset($msg)) : ?>
                        <div class="col-md-12">
                            <div class="alert alert-danger" role="alert">
                                <?= $msg ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="col-md-12 col-sm-12 col-xs-12 ">
                        <form method="post"  action="<?php echo base_url() ?>venue_dashboard/creategig" id="gig_add" enctype="multipart/form-data">	                                                                                                                                                                  <form id="myForm" class="form-horizontal fv-form fv-form-bootstrap" method="post" action="http://localhost/showspoon/artist_dashboard/edit_profile" novalidate="novalidate"><button type="submit" class="fv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                                <div class="col-md-6 col-sm-6 col-xs-6 ">

                                    <div class="form-group">
                                        <label>Gig Navn</label>
                                        <input type="text" class="form-control" placeholder="Gig Navn" name="gig_name" value="<?php echo set_value('gig_name'); ?>" required>
                                    </div><!-- form-group -->

                                    <div class="form-group">
                                        <label>Beliggenhet</label>
                                        <input type="text" placeholder="Beliggenhet" name="gig_location" value="<?php echo set_value('gig_location'); ?>" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Postkode</label>
                                        <input type="text" placeholder="Postkode" name="gig_zip" value="<?php echo set_value('gig_zip'); ?>" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label>By</label>
                                        <?php echo form_dropdown('gig_city', $cities, set_value('gig_city'), 'class="form-control select select-primary select-block mbl"  placeholder="City"'); ?>

                                    </div>
                                    <div class="form-group hide">
                                        <label>Type of Gigs</label>
                                        <?php echo form_dropdown('art_genre[]', $artist_genre, set_value('art_genre'), 'class="form-control multiselect mbl" multiple="multiple" placeholder="Genre"'); ?>


                                    </div>
                                    <div class="form-group">
                                        <label>Max croner beløp</label>

                                        <input type="number" class="form-control " value="<?php echo set_value('Max_croner_amount'); ?>" placeholder="Max croner beløp"  name="Max_croner_amount" required />

                                    </div>
                                    <div class="form-group">
                                        <label>Andel av billettsalget</label>

                                        <input type="number"  class="form-control " value="<?php echo set_value('parcentage_of_ticketsale'); ?>" placeholder="Andel av billettsalget" name="parcentage_of_ticketsale" required/>

                                    </div>
                                    <div class="form-group hide">
                                        <label>Krav</label>
                                        <textarea placeholder="Krav" name="requirements" class="form-control" required ><?php echo set_value('requirements'); ?></textarea>
                                    </div>

                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 ">

                                    
                                    <div class="form-group">
                                        <label>Antall Bands</label>

                                        <input type="number" class="form-control" value="<?php echo set_value('numbers_of_bands'); ?>" placeholder="Antall Bands" name="numbers_of_bands" required/>

                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 no-gutter check-bottom hide">
                                            <input type="checkbox"  id="food&drink_ticket" class="check-fix" name="food&drink_ticket"  value="<?php echo set_value('travel&accomodation'); ?>"/>
                                            <label>Food &amp; drink </label>
                                        </div>

                                        <label class="checkbox">
                                            <input type="checkbox" id="food&drink_ticket" data-toggle="checkbox" name="travel&accomodation" class="check-fix" value="<?php echo set_value('travel&accomodation'); ?>">Travel & overnatting</label>                       

                                    </div> 

                                    <div class="col-md-6 no-gutter">
                                        <div class="form-group">
                                            <label>Hendelse startdato</label>

                                            <input type="text" class="form-control " value="<?php echo set_value('gig_date_start'); ?>" placeholder="Fra" id="datepicker-01" name="gig_date_start" />

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Hendelse Sluttdato</label>
                                            <input type="text" class="form-control" value="<?php echo set_value('gig_date_end'); ?>"  placeholder="Til" id="datepicker-02" name="gig_date_end" />
                                        </div>  
                                    </div>

                                    <div class="col-md-6 no-gutter">
                                        <div class="form-group">
                                            <label>Starttid:</label>
                                            <i>HH:MM AM/PM</i>
                                            <input type="time" id="timepicker-01" name="gig_hh" class="form-control" value="<?php echo set_value('gig_hh'); ?>"  placeholder="Hours"> 
                                        </div>
                                    </div><!-- col-md-6 -->
                                    <div class="col-md-6 xm-no-gutter">        
                                <div class="form-group">
                                    <label>Sluttid</label>
                                     <i>HH:MM AM/PM</i>
                                    <input type="time" id="timepicker-02" name="gig_hh_end" class="form-control" value="<?php echo set_value('gig_hh_end'); ?>" placeholder="HH:MM AM/PM">
                                </div>
                            </div>
                                    <!-- <div class="col-md-3 alignment-hack">
                                         <div class="form-group">
                                              <label></label>
                                              <input type="text" id="timepicker-01" name="gig_mm" class="form-control"  placeholder="Minutes"> 
                                         </div>
                                     </div><!-- col-md-3-->
                                    <!--<div class="col-md-3 alignment-hack">
                                        <div class="form-group">
                                             <label></label>
                                            <input type="text" id="timepicker-01" name="gig_ampm" class="form-control"  placeholder="am/pm"> 
                                        </div>
                                    </div>--><!-- col-md-3-->
                                    <div class="clearfix"></div>

                                    <div class="form-group">
                                        <label>Bilde</label>
                                        <i>MAX størrelse: 2MB Og bare godta disse filtypene jpg, jpeg, png</i>
                                        <input name="userfile" id="userfile" type="file" >
                                    </div>
                                    <div class="form-group">
                                        <label>Beskrivelse</label>
                                        <textarea placeholder="Beskrivelse" name="gig_description"  rows="5" class="form-control" required ><?php echo set_value('gig_description'); ?></textarea>
                                    </div>

                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 ">
                                    <input type="submit" class="btn btn-primary btn-artist secondary lg" id="submitbtn" value="Lagre">
                                </div>
                                <!-- form-group -->

                                <!--<a class="btn btn-primary btn-artist secondary lg" href="#">Update</a>-->
                            </form>			
                    </div>

                </div><!-- tab-content -->

            </div><!-- col-md-12 -->

        </div><!-- row -->
    </div><!-- container -->
</div><!-- GreyDashboard -->


<?php $this->load->view('footer'); ?>