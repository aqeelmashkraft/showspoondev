<?php
$this->load->view('admin/vwHeader');
?>

<!-- BEGIN CONTAINER -->
<div class="page-container">
    <style>
        .page-container-bg-solid .page-bar {
            margin: 0px 0px 20px 0px;
        }
    </style>
    <?php
    $this->load->view('admin/vwsidebar');
    ?>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

            <!-- BEGIN STYLE CUSTOMIZER -->

            <!-- BEGIN PAGE HEADER-->

            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="<?php echo site_url('admin/venues/')?>">Venues</a>
                       
                    </li>
                    
                </ul>

            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <div class="tabbable-line boxless tabbable-reversed">

                        <div class="tab-content">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>Add Venue
                                    </div>
                                    <div class="tools">

                                        <a href="javascript:;" class="reload">
                                        </a>

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                   
                                    <!-- BEGIN FORM-->
                                    <form  action="<?php echo base_url() ?>admin/venues/createVenue" id="form_sample_reg" method="post" class="form-horizontal">
                                        <div class="form-body">
                                            
                                          <?php
                                            if ($this->session->flashdata('flash_message')) {
                                                if ($this->session->flashdata('flash_message') == 'add') {
                                                    echo '<div class="alert alert-success">';
                                                    echo '<a class="close" data-dismiss="alert">×</a>';
                                                    echo 'Venue is added successfully.';
                                                    echo '</div>';
                                                }
                                                if ($this->session->flashdata('flash_message') == 'error') {
                                                    echo '<div class="alert alert-success">';
                                                    echo '<a class="close" data-dismiss="alert">×</a>';
                                                    echo 'Denne e-postadressen finnes allerede.';
                                                    echo '</div>';
                                                }
                                            }
                                            ?>
                                            
                                            
                                            <div class="form-group" >
                                                <label class="col-md-3 control-label">E-post</label>
                                                <div class="col-md-4">
                                                    <input type="email" placeholder="E-post" name="ven_email"  value="<?php echo set_value('ven_email'); ?>" id="ven_email_id" class="form-control input-circle" required />
                                                 
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Passord</label>
                                                <div class="col-md-4">
                                                    <input type="password" placeholder="Passord" name="ven_password" value="<?php echo set_value('ven_password'); ?>" class="form-control input-circle" required />
                                                </div>
                                            </div>
                        
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Navn p� spillestedet</label>
                                                <div class="col-md-4">
                                                    <input type="text" placeholder="Navn p� spillestedet" name="ven_name" value="<?php echo set_value('ven_name'); ?>" class="form-control input-circle" required/>
                                                </div>
                                            </div>
                       
                           

                            
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Sjanger</label>
                                                <div class="col-md-4">
                                                    <?php echo form_dropdown('ven_genre[]', $artist_genre, set_value('ven_genre'), 'class="form-control multiselect" multiple="multiple" placeholder="Sjanger" required'); ?>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Adresse</label>
                                                <div class="col-md-4">
                                                    <input type="text" placeholder="Adresse" name="ven_location" value="<?php echo set_value('ven_location'); ?>" class="form-control input-circle" required/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Postkode</label>
                                                <div class="col-md-4">
                                                    <input type="text" placeholder="Postkode" name="ven_zip_code" value="<?php echo set_value('ven_zip_code'); ?>" class="form-control input-circle" required />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">By</label>
                                                <div class="col-md-4">
                                                    <?php echo form_dropdown('ven_cities', $cities, set_value('ven_cities'), 'class="form-control select select-primary select-block" placeholder="By" required'); ?>

                                                </div>
                                            </div>
                                            
                                           
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn btn-circle blue">Submit</button>
                                                    <a href="<?php echo site_url('admin/venues/')?>" type="button" class="btn btn-circle default">Cancel</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
<?php
$this->load->view('admin/vwFooter');
?>