<?php
$this->load->view('admin/vwHeader');
?>

<!-- BEGIN CONTAINER -->
<div class="page-container">
    <style>
        .page-container-bg-solid .page-bar {
            margin: 0px 0px 20px 0px;
        }
    </style>
    <?php
    $this->load->view('admin/vwsidebar');
    ?>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

            <!-- BEGIN STYLE CUSTOMIZER -->

            <!-- BEGIN PAGE HEADER-->

            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="<?php echo site_url('admin/venues/') ?>">Venues</a>

                    </li>

                </ul>

            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <div class="tabbable-line boxless tabbable-reversed">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_0" data-toggle="tab">
                                    Profile </a>
                            </li>
                            <li>
                                <a href="#tab_1" data-toggle="tab">
                                    Pictures </a>
                            </li>
                            <li>
                                <a href="#tab_2" data-toggle="tab">
                                    Bandmedlemmer </a>
                            </li>
                            <li>
                                <a href="#tab_3" data-toggle="tab">
                                    Video </a>
                            </li>
                            <li>
                                <a href="#tab_4" data-toggle="tab">
                                    SoundCloud </a>
                            </li>

                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_0">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Edit Artist
                                        </div>
                                        <div class="tools">

                                            <a href="javascript:;" class="reload">
                                            </a>

                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <?php
                                        $genre_id['genre_id'] = explode(',', $users[0]['genre_id']);
                                        $gig_type_id['gig_type_id'] = explode(',', $users[0]['gig_type_id'])
                                        ?>
                                        <?php
                                        //var_dump($this->session->flashdata('flash_message'));
                                        //flash messages
                                        if ($this->session->flashdata('flash_message')) {
                                            if ($this->session->flashdata('flash_message') == 'updated') {
                                                echo '<div class="alert alert-success">';
                                                echo '<a class="close" data-dismiss="alert">×</a>';
                                                echo 'Profilen er oppdatert med suksess.';
                                                echo '</div>';
                                            }
                                        }
                                        ?>
                                        <!-- BEGIN FORM-->
                                        <form id="form_sample_reg" class="form-horizontal"  method="post" action="<?php echo base_url() ?>admin/artist/edit_profile/id/<?php echo $this->uri->segment(5); ?>">
                                            <div class="form-body">

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Artistnavn</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" placeholder="Artistnavn" name="art_name" value="<?php echo $users[0]['name']; ?>"  required />
                                                    </div>
                                                </div><!-- form-group -->

                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Sjanger</label>
                                                    <div class="col-md-4">
                                                        <?php echo form_dropdown('art_genre[]', $artist_genre, $genre_id['genre_id'], 'class="form-control multiselect mbl" multiple="multiple"  placeholder="Sjanger"'); ?>
                                                    </div>
                                                </div><!-- form-group -->

                                                <div class="form-group">
                                                    <label  class="col-md-3 control-label">Artistkategori</label>
                                                    <div class="col-md-4">
                                                        <?php echo form_dropdown('art_type', $artist_band_type, $users[0]['band_type_id'], 'class="form-control select select-primary select-block mbl" placeholder="Type"'); ?>
                                                    </div>
                                                </div><!-- form-group -->

                                                <div class="form-group">
                                                    <label  class="col-md-3 control-label">Type of Gigs</label>
                                                    <div class="col-md-4">
                                                        <?php echo form_dropdown('art_type_show[]', $artist_gig_type, $gig_type_id['gig_type_id'], 'class="form-control multiselect mbl" multiple="multiple" placeholder="Type of Gigs"'); ?>
                                                    </div>
                                                </div><!-- form-group -->

                                                <div class="form-group">
                                                    <label  class="col-md-3 control-label">Postkode</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" placeholder="Postkode" name="art_zip" value="<?php echo $users[0]['zip']; ?>" required />
                                                    </div>
                                                </div>




                                                <!--   <div class="form-group" >
                                                   <label>Location</label>
                                                   <input type="hidden" placeholder="Location" name="art_location" value="<?php echo $users[0]['location']; ?>" class="form-control" />
                                               </div> -->
                                                <div class="form-group">
                                                    <label  class="col-md-3 control-label">By</label>
                                                    <div class="col-md-4">
                                                        <input type="hidden" placeholder="Location" name="art_location" value="<?php echo $users[0]['location']; ?>" class="form-control" />
                                                        <?php echo form_dropdown('art_city', $cities, $users[0]['city'], 'class="form-control select select-primary select-block mbl"  placeholder="BY"'); ?>
                                                          <!--<input type="text" placeholder="Location" name="art_city" value="<?php echo $users[0]['city']; ?>" class="form-control" />-->
                                                    </div>
                                                </div>
                                                <div class="form-group" >
                                                    <label  class="col-md-3 control-label">Facebook-side lenke</label>
                                                    <div class="col-md-4">
                                                        <input type="url" placeholder="Facebook-side lenke" name="fb_page_link" value="<?php echo $users[0]['fb_page_link']; ?>" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label  class="col-md-3 control-label">Kort beskrivelse</label>
                                                    <div class="col-md-4">
                                                        <textarea placeholder="Kort beskrivelse" name="art_short_description" class="form-control" required rows="4" ><?php echo $users[0]['short_description']; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label  class="col-md-3 control-label">Biografi</label>
                                                    <div class="col-md-4">
                                                        <textarea placeholder="Biografi" name="art_biography"  class="form-control" required rows="7"><?php echo $users[0]['biography']; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Enable/Disable Artist</label>
                                                    <div class="col-md-9">                                            

                                                        <label class="switch">

                                                            <input class="make-switch" type="checkbox" name="is_active" <?php
                                                            if ($users[0]['is_active'] != 0) {
                                                                echo 'checked';
                                                            }
                                                            ?> value="1" tabindex="11" data-on="success" data-off="warning">
                                                            <span></span>
                                                        </label>


                                                    </div>
                                                </div>
                                                <!-- form-group -->


                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <input type="submit" class="btn btn-primary btn-artist secondary lg" id="submitbtn" value="Update">
                                                    </div>
                                                </div>


                                            </div>
                                        </form>		
                                        <!-- END FORM-->
                                    </div>
                                </div>


                            </div>
                            <!---tab 1 end --->
                            <div class="tab-pane" id="tab_1">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Add Media
                                        </div>
                                        <div class="tools">

                                            <a href="javascript:;" class="reload">
                                            </a>

                                        </div>
                                    </div>
                                    <div class="portlet-body form" style="overflow: hidden;">

                                        <div class="col-md-4">
                                            <form method="post" action="" id="upload_file_artist">
                                                <div class="form-body">
                                                    <div class="form-group">

                                                        <!--                                                    <label class="col-md-3 control-label">Logo</label>-->

                                                        <div class="col-md-12 col-xs-12">                                                                                                                                     
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                                                </div>
                                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                                                </div>
                                                                <div>
                                                                    <div class="clearfix"></div>
                                                                    <label class="checkbox col-md-8" for="checkbox1">
                                                                        <input type="checkbox"  id="checkbox1" name="setfeatured" data-toggle="checkbox"> Set Utvalgt
                                                                    </label>

                                                                    <div class="clearfix"></div>
                                                                    <span class="btn default btn-file">
                                                                        <span class="fileinput-new">
                                                                            Select image </span>
                                                                        <span class="fileinput-exists">
                                                                            Change </span>
                                                                        <input type="file" name="userfile" id="userfile3" title="Browse file" tabindex="10">
                                                                    </span>
                                                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                                                        Remove </a>
                                                                    <input type="submit" name="submit" class="btn btn-primary btn-file" id="submitvenue" value="Save" />
                                                                    <small class="help-block" data-fv-validator="notEmpty" id="error" data-fv-for="art_location" data-fv-result="INVALID" style=""></small>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>  
                                        </div>
                                        <div class="col-md-8 col-sm-6 col-xs-6">
                                            <div id="files3"></div>
                                        </div>

                                    </div>



                                </div>

                            </div>



                            <!---tab 2 end --->
                            <div class="tab-pane" id="tab_2">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Add Bandmedlemmer
                                        </div>
                                        <div class="tools">

                                            <a href="javascript:;" class="reload">
                                            </a>

                                        </div>
                                    </div>
                                    <div class="portlet-body form" style="overflow: hidden;">


                                        <form method="post" action="" id="upload_file">
                                            <div class="form-body">
                                                <div class="col-md-3 col-xs-12"> 
                                                    <div class="form-group">

                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                                            </div>
                                                            <div>
                                                                <div class="clearfix"></div>
                                                                <label class="checkbox col-md-8" for="checkbox1">
                                                                    <input type="checkbox"  id="checkbox1" name="setfeatured" data-toggle="checkbox"> Set Utvalgt
                                                                </label>

                                                                <div class="clearfix"></div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new">
                                                                        Select image </span>
                                                                    <span class="fileinput-exists">
                                                                        Change </span>
                                                                    <input type="file" name="userfile" id="userfile" title="Browse file" tabindex="10">
                                                                </span>
                                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                                                    Remove </a>

                                                                <small class="help-block" data-fv-validator="notEmpty" id="error" data-fv-for="art_location" data-fv-result="INVALID" style=""></small>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="col-md-3 col-sm-4 col-xs-12">
                                                    <input type="text" class="form-control mbl mth" name="title" id="title" placeholder="Navn" required/>
                                                </div><!-- col-md-3 -->
                                                <div class="col-md-3 col-sm-4 col-xs-12">
                                                    <input type="text" class="form-control mbl mth"  name="role" id="role"  placeholder="Rolle" required/>
                                                </div><!-- col-md-4 -->
                                                <div class="col-md-3 col-sm-4 col-xs-12">

                                                    <input type="submit" name="submit" class="btn btn-primary btn-file mbl mth" id="submit" value="Lagre" />
                                                    <!--<a href="#" class="btn btn-primary btn-file mbl mth">Save</a>
                                                    <a href="#" class="btn btn-primary btn-file mbl mth">Delete</a>-->
                                                </div>
                                            </div>
                                        </form>  

                                        <div id="files" class="col-md-12 mtm"></div>



                                    </div>



                                </div>

                            </div>




                            <div class="tab-pane" id="tab_3">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Add Video
                                        </div>
                                        <div class="tools">

                                            <a href="javascript:;" class="reload">
                                            </a>

                                        </div>
                                    </div>
                                    <div class="portlet-body form" style="overflow: hidden;">


                                        <form method="post" action="" id="upload_file_media">
                                            <div class="form-body">
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <h5>Legg Nye videoer</h5>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <input type="hidden"  value="1" id="media_type" name="media_type" class="form-control mbl" />
                                                        <input type="text"  placeholder="video tittel" id="video_title" name="video_title" class="form-control mbl" required />
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group">

                                                        <input type="url" placeholder="videolink" id="video_link" name="video_link" class="form-control mbl" required="required" />
                                                    </div>
                                                </div>

                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <!--<a class="btn btn-primary btn-file mbl" href="#">Save</a>-->

                                                    <input type="submit" name="submit" class="btn btn-primary btn-file mbl" id="submitvenue" value="Lagre" />
                                                </div><!-- col-md-4 --> 
                                                <div id="success"></div>
                                            </div>
                                        </form>

                                        <div class="col-md-12 col-sm-12 col-xs-12 GalleryTab mbh" id="files4"></div>



                                    </div>



                                </div>

                            </div>


                            <div class="tab-pane" id="tab_4">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>SoundCloud
                                        </div>
                                        <div class="tools">

                                            <a href="javascript:;" class="reload">
                                            </a>

                                        </div>
                                    </div>
                                    <div class="portlet-body form" style="overflow: hidden;">


                                        <form method="post" action="" id="upload_file_media_sound">
                                              <div class="form-body">
                                            <div class="col-md-4 col-sm-6 col-xs-12">
                                                <h5>Legg til ny Soundcloud Link </h5>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <input type="hidden"  value="2" id="media_type2" name="media_type" class="form-control mbl" />
                                                    <input type="text"  placeholder="Tittel" id="video_title2" name="video_title" class="form-control mbl" required />
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">

                                                    <input type="url" placeholder="Link" id="video_link2" name="video_link" class="form-control mbl" required="required" />
                                                </div>
                                            </div>

                                            <div class="col-md-2 col-sm-6 col-xs-12">
                                                <!--<a class="btn btn-primary btn-file mbl" href="#">Save</a>-->

                                                <input type="submit" name="submit" class="btn btn-primary btn-file mbl" id="submitsound" value="Lagre" />
                                            </div><!-- col-md-4 --> 
                                              </div>
                                            <div id="success2"></div>
                                        </form>

                                        <div class="col-md-12 col-sm-12 col-xs-12 GalleryTab mbh" id="files5"></div>



                                    </div>



                                </div>

                            </div>                 

                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <!-- END CONTENT -->

</div>

<!-- END CONTAINER -->
<?php
$this->load->view('admin/vwFooter');
?>