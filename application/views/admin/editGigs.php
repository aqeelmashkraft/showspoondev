<?php
$this->load->view('admin/vwHeader');
?>

<!-- BEGIN CONTAINER -->
<div class="page-container">
    <style>
        .page-container-bg-solid .page-bar {
            margin: 0px 0px 20px 0px;
        }
    </style>
    <?php
    $this->load->view('admin/vwsidebar');
    ?>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

            <!-- BEGIN STYLE CUSTOMIZER -->

            <!-- BEGIN PAGE HEADER-->

            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Form Stuff</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Form Layouts</a>
                    </li>
                </ul>

            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-gift"></i>Edit Gigs
                                </div>
                                
                            </div>
                            <div class="portlet-body form">
                                <?php
                                //print_r($gig_by_id);
                                $type['type'] = explode(',', $gig_by_id[0]['type']);
                                ?>
                                <?php
                                if ($this->session->flashdata('flash_message')) {
                                    if ($this->session->flashdata('flash_message') == 'updated') {
                                        echo '<div class="alert alert-success">';
                                        echo '<a class="close" data-dismiss="alert">×</a>';
                                        echo 'Gig Oppdatert med suksess.';
                                        echo '</div>';
                                    }
                                }
                                ?>
                                
                                    
                                    <form method="post"  action="<?php echo base_url() ?>admin/gigs/edit_gig_update/" id="form_sample_reg">	                                                                                                                                                                  <form id="myForm" class="form-horizontal fv-form fv-form-bootstrap" method="post" action="http://localhost/showspoon/artist_dashboard/edit_profile" novalidate="novalidate"><button type="submit" class="fv-hidden-submit" style="display: none; width: 0px; height: 0px;"></button>
                                            <div class="form-body" style="overflow: hidden;">
                                            <div class="col-md-6 col-sm-6 col-xs-6 ">
                                                <div class="form-group">
                                                    <label >Gig Navn</label>
                                                    <input type="text" class="form-control" placeholder="Gig Name" value='<?php echo $gig_by_id[0]['gig_name'] ?>' name="gig_name"  required>
                                                </div><!-- form-group -->

                                                <div class="form-group">
                                                    <label>Beliggenhet</label>
                                                    <input type="text" placeholder="Beliggenhet" name="gig_location" value='<?php echo $gig_by_id[0]['location'] ?>' class="form-control" required>
                                                </div>
                                                <div class="form-group">
                                                    <label>Postkode</label>
                                                    <input type="text" placeholder="Postkode" name="gig_zip" value='<?php echo $gig_by_id[0]['zip'] ?>' class="form-control" required>
                                                </div>
                                                <div class="form-group">
                                                    <label>By</label>
                                                    <?php echo form_dropdown('gig_city', $cities, $gig_by_id[0]['city'], 'class="form-control select select-primary select-block mbl"  placeholder="City"'); ?>

                                                </div>
                                                <div class="form-group hide">
                                                    <label>Sjanger</label>
                                                    <?php echo form_dropdown('art_genre[]', $artist_genre, $type['type'], 'class="form-control multiselect mbl" multiple="multiple" placeholder="Sjanger"'); ?>
                                                    <?php //echo form_dropdown('art_type_show[]', $artist_gig_type,'', 'class="form-control multiselect mbl" multiple="multiple" placeholder="Types of Shows"');  ?>

                                                </div>
                                                <div class="form-group">
                                                    <label>Max croner beløp</label>

                                                    <input type="text" class="form-control " value='<?php echo $gig_by_id[0]['Max_croner_amount'] ?>' placeholder="Max croner beløp"  name="Max_croner_amount" required />

                                                </div> 
                                                <div class="form-group hide">
                                                    <label>Requirements</label>
                                                    <textarea placeholder="Requirements" name="requirements" class="form-control" required ><?php echo $gig_by_id[0]['requirements']; ?></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label>Andel av billettsalget</label>

                                                    <input type="text" class="form-control " value='<?php echo $gig_by_id[0]['parcentage_of_ticketsale'] ?>' placeholder="Andel av billettsalget" name="parcentage_of_ticketsale" required/>

                                                </div>
                                                <div class="form-group">
                                                    <label>Enable Or Disable Gigs</label>
                                                                                             

                                                        <label class="switch">

                                                            <input class="make-switch" type="checkbox" name="is_active" <?php if ($gig_by_id[0]['is_active'] != 0) {
                                                    echo 'checked';
                                                } ?> value="1" tabindex="11" data-on="success" data-off="warning">
                                                            <span></span>
                                                        </label>


                                                    
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-6 ">

                                                <div class="form-group">
                                                    <label>Antall Bands</label>

                                                    <input type="number" class="form-control" value="<?php echo $gig_by_id[0]['numbers_of_bands']; ?>" placeholder="Antall Bands" name="numbers_of_bands" required/>

                                                </div>
                                                <div class="form-group">



                                                    <input type="checkbox"  id="travel&accomodation" name="travel&accomodation"   <?php
                                                    if ($gig_by_id[0]['travel&accomodation'] == 1) {
                                                        echo 'checked';
                                                    }
                                                    ?>/> <label>Travel & overnatting</label>


                                                    <!-- <label>Food & drink </label>
                 
                                                     <input type="checkbox" id="food&drink_ticket"  name="food&drink_ticket"     <?php
                                                    if ($gig_by_id[0]['food&drink_ticket'] == 1) {
                                                        echo 'checked';
                                                    }
                                                    ?> />-->

                                                </div> 
                                                <div class="col-md-6 no-gutter">
                                                    <div class="form-group">
                                                        <label>Starttid</label>
                                                        <i>HH:MM AM/PM</i>
                                                        <input type="time" id="timepicker-01" name="gig_hh" value="<?php echo $gig_by_id[0]['gig_time'] ?>" class="form-control"  placeholder="Hours"> 
                                                    </div>
                                                </div><!-- col-md-6 -->
                                                <div class="col-md-6 ">        
                                                    <div class="form-group">

                                                        <label> Sluttid</label>
                                                        <i>HH:MM AM/PM</i>
                                                        <input type="time" class="form-control" value="<?php echo $gig_by_id[0]['gig_time_end'] ?>"  placeholder="To"  name="gig_hh_end" />
                                                    </div>
                                                </div><!-- col-md-6 -->
                                                <!-- <div class="col-md-3 alignment-hack">
                                                     <div class="form-group">
                                                          <label></label>
                                                          <input type="text" id="timepicker-01" name="gig_mm" class="form-control"  placeholder="Minutes"> 
                                                     </div>
                                                 </div>--><!-- col-md-3-->
                                                <!-- <div class="col-md-3 alignment-hack">
                                                     <div class="form-group">
                                                          <label></label>
                                                         <input type="text" id="timepicker-01" name="gig_ampm" class="form-control"  placeholder="am/pm"> 
                                                     </div>
                                                 </div>--><!-- col-md-3-->
                                                <div class="clearfix"></div>
                                                <div class="form-group">
                                                    <label>Hendelse startdato</label>

                                                    <input type="text" class="form-control " value='<?php echo $gig_by_id[0]['Accepting_application_start_date'] ?>' placeholder="Fra" id="datepicker-01" name="gig_date_start" />

                                                </div>
                                                <div class="form-group">
                                                    <label>Hendelse Sluttdato</label>
                                                    <input type="text" class="form-control" value='<?php echo $gig_by_id[0]['accepting_application_end_date'] ?>' placeholder="Til" id="datepicker-02" name="gig_date_end" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Beskrivelse</label>
                                                    <textarea placeholder="Beskrivelse" rows="5" name="gig_description" class="form-control" required ><?php echo $gig_by_id[0]['description']; ?></textarea>
                                                </div>
                                            </div>



                                            <!-- form-group -->
                                            <div class="col-md-12 col-sm-12 col-xs-12 ">
                                                <input type="hidden" value="<?php echo$id = $this->uri->segment(5); ?>" name='gig_id' />
                                                <input type="submit" class="btn btn-primary btn-artist secondary lg" id="submitbtn" value="Oppdatering">
                                            </div>

                                            <!--<a class="btn btn-primary btn-artist secondary lg" href="#">Update</a>-->
                                        </div>
                                        </form>			
                                
                            </div>
                        </div>

                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <!-- END CONTENT -->

</div>

<!-- END CONTAINER -->
<?php
$this->load->view('admin/vwFooter');
?>