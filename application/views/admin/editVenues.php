<?php
$this->load->view('admin/vwHeader');
?>

<!-- BEGIN CONTAINER -->
<div class="page-container">
    <style>
        .page-container-bg-solid .page-bar {
            margin: 0px 0px 20px 0px;
        }
    </style>
    <?php
    $this->load->view('admin/vwsidebar');
    ?>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

            <!-- BEGIN STYLE CUSTOMIZER -->

            <!-- BEGIN PAGE HEADER-->

            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="<?php echo site_url('admin/venues/')?>">Venues</a>
                       
                    </li>
                    
                </ul>

            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <div class="tabbable-line boxless tabbable-reversed">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_0" data-toggle="tab">
                                    Profile </a>
                            </li>
                            <li>
                                <a href="#tab_1" data-toggle="tab">
                                    Media </a>
                            </li>

                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_0">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Edit Venue
                                        </div>
                                        <div class="tools">

                                            <a href="javascript:;" class="reload">
                                            </a>

                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <!-- BEGIN FORM-->
                                        <form action="<?php echo base_url() ?>admin/venues/edit_profile/id/<?php echo $this->uri->segment(5); ?>" method="post" enctype="multipart/form-data"class="form-horizontal">
                                            <div class="form-body">
                                                <?php
                                //var_dump($this->session->flashdata('flash_message'));
                                //flash messages
                                if ($this->session->flashdata('flash_message')) {
                                    if ($this->session->flashdata('flash_message') == 'updated') {
                                        echo '<div class="alert alert-success">';
                                        echo '<a class="close" data-dismiss="alert">×</a>';
                                        echo 'Profile is updated with success.';
                                        echo '</div>';
                                    }
                                }
                                ?>
                                <?php if (isset($msg)) : ?>
                                    <div class="col-md-12">
                                        <div class="alert alert-danger" role="alert">
                                            <?= $msg ?>
                                        </div>
                                    </div>
                                        <?php endif; ?>
                                                <?php
                                                //print_r($venuebyid);

                                                $genre_id['genre'] = explode(',', $venuebyid[0]['genre']);
                                                ?>
                                                <div class="form-group">

                                                    <label class="col-md-3 control-label">Spillested navn</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control input-circle" value="<?php echo $venuebyid[0]['name'] ?>" name="ven_name" placeholder="Spillested navn"  required/>
                                                    </div>
                                                </div><!-- form-group -->
                                                <div class="form-group">

                                                    <label class="col-md-3 control-label">Alder</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control input-circle" value="<?php echo $venuebyid[0]['age'] ?>" name="ven_age" placeholder="Alder"  required/>
                                                    </div>
                                                </div><!-- form-group -->

                                                <div class="form-group">

                                                    <label class="col-md-3 control-label">Sjanger</label>
                                                    <div class="col-md-4">
                                                        <?php echo form_dropdown('art_genre[]', $artist_genre, $genre_id['genre'], 'class="form-control multiselect  " multiple="multiple" placeholder="Sjanger"'); ?>
                                                    </div>
                                                </div><!-- form-group -->

                                                <div class="form-group">

                                                    <label class="col-md-3 control-label">Postkode</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control input-circle" placeholder="Postkode" name="ven_zip" value="<?php echo $venuebyid[0]['zip']; ?>" required />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Beliggenhet</label>
                                                    <div class="col-md-4">
                                                        <input type="text" placeholder="Beliggenhet" name="ven_location" value="<?php echo $venuebyid[0]['location']; ?>" class="form-control input-circle" />
                                                    </div>
                                                </div> 


                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">kapasitet</label>
                                                    <div class="col-md-4">
                                                        <input type="text" placeholder="kapasitet" name="ven_capacity" value="<?php echo $venuebyid[0]['ven_capacity']; ?>" class="form-control" /> 
                                                    </div>

                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">By</label>
                                                    <div class="col-md-4">
                                                        <?php echo form_dropdown('ven_city', $cities, $venuebyid[0]['city'], 'class="form-control select select-primary select-block"  placeholder="By"'); ?> 
                                                    </div>


                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Last Teknisk Rider dokument (PDF)</label>
                                                    <div class="col-md-4">
                                                        <input name="userfile" id="userfile" type="file" />
                                                        <input name="userfile" id="userfile" type="hidden" value="<?php echo $venuebyid[0]['rider_document_url_pdf']; ?>" />
                                                    </div>

                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Beskrivelse</label>
                                                    <div class="col-md-4">
                                                        <textarea placeholder="Beskrivelse" name="ven_description" class="form-control" rows="8" required ><?php echo $venuebyid[0]['descriptiopn']; ?></textarea>  
                                                    </div>

                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Enable Venues</label>
                                                    <div class="col-md-9">                                            

                                                        <label class="switch">

                                                            <input class="make-switch" type="checkbox" name="is_active" <?php if ($venuebyid[0]['is_active'] != 0) {
                                                    echo 'checked';
                                                } ?> value="1" tabindex="11" data-on="success" data-off="warning">
                                                            <span></span>
                                                        </label>


                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <input type="hidden" name="venueid" value="<?php echo $this->uri->segment(5); ?>"/>
                                                        <button type="submit" class="btn btn-circle blue">Submit</button>
                                                        <button type="button" class="btn btn-circle default">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- END FORM-->
                                    </div>
                                </div>


                            </div>
                            <!---tab 1 end --->
                            <div class="tab-pane" id="tab_1">
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i>Add Media
                                        </div>
                                        <div class="tools">

                                            <a href="javascript:;" class="reload">
                                            </a>

                                        </div>
                                    </div>
                                    <div class="portlet-body form" style="overflow: hidden;">
                                        
                                        <div class="col-md-4">
                                         <form method="post" action="" id="upload_file_venue">
                                            <div class="form-body">
                                                <div class="form-group">
                                                    
<!--                                                    <label class="col-md-3 control-label">Logo</label>-->

                                                    <div class="col-md-12 col-xs-12">                                                                                                                                     
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                                            </div>
                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                                            </div>
                                                            <div>
                                                                <div class="clearfix"></div>
                                                                <label class="checkbox col-md-8" for="checkbox1">
                                                                    <input type="checkbox"  id="checkbox1" name="setfeatured" data-toggle="checkbox"> Set Utvalgt
                                                                </label>

                                                                <div class="clearfix"></div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new">
                                                                        Select image </span>
                                                                    <span class="fileinput-exists">
                                                                        Change </span>
                                                                    <input type="file" name="userfile" id="userfile2" title="Browse file" tabindex="10">
                                                                </span>
                                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                                                    Remove </a>
                                                                <input type="submit" name="submit" class="btn btn-primary btn-file" id="submitvenue" value="Save" />
                                                                 <small class="help-block" data-fv-validator="notEmpty" id="error" data-fv-for="art_location" data-fv-result="INVALID" style=""></small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                        </form>  
                                        </div>
                                        <div class="col-md-8 col-sm-6 col-xs-6">
                                        <div id="files2"></div>
                                    </div>
                                        
                                    </div>
                                    
                                    
                                    
                                </div>
                               
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <!-- END CONTENT -->

</div>

<!-- END CONTAINER -->
<?php
$this->load->view('admin/vwFooter');
?>