<?php
$this->load->view('admin/vwHeader');
?>

<!-- BEGIN CONTAINER -->
<div class="page-container">
    <style>
        .page-container-bg-solid .page-bar {
                    margin: 0px 0px 20px 0px;
        }
    </style>
	<?php
        $this->load->view('admin/vwsidebar');
        
        ?>
	<!-- BEGIN CONTENT -->
		<div class="page-content-wrapper">
		<div class="page-content">
			
			
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php echo site_url('admin/dashboard')?>">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Artist</a>
						<i class="fa fa-angle-right"></i>
					</li>
					
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-globe"></i>Artist
							</div>
							<div class="tools">
								
								<a href="javascript:;" class="reload">
								</a>
								
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="row">
									<div class="col-md-6">
										<div class="btn-group">
                                                                                    <a id="sample_editable_1_new" href="<?php echo site_url('admin/venues/add')?>" class="btn green">
											Add New <i class="fa fa-plus"></i>
											</a>
										</div>
									</div>
									
								</div>
							</div>
                                                    <?php
                                                    //print_r($query);
                                                                ?>
                                                    <?php
                                                    if ($this->session->flashdata('flash_message')) {
                                                        if ($this->session->flashdata('flash_message') == 'delete') {
                                                            echo '<div class="alert alert-danger">';
                                                            echo '<a class="close" data-dismiss="alert">×</a>';
                                                            echo 'Venue is deleted successfully.';
                                                            echo '</div>';
                                                        }

                                                    }
                                            ?>
							<table class="table table-striped table-bordered table-hover" id="sample_1">
							<thead>
							<tr>
								
								<th>
									 Venue name
								</th>
								<th>
									 Email
								</th>
								<th>
									 Location
								</th>
								<th>
								 Capacity
								</th>
								<th>
									 Status
								</th>
								<th>
									 Action
								</th>
							</tr>
							</thead>
							<tbody>
                                                            <?php
                                                            foreach ($query as $key => $value){
                                                            ?>
							<tr class="odd gradeX">
								
								<td>
									<?php echo $value['name']; ?>
								</td>
								<td>
									<a href="mailto:<?php echo $value['email'] ?>">
									<?php echo $value['email'] ?> </a>
								</td>
								<td>
									<?php echo $value['location'].','.$value['CityName'];?>
								</td>
								<td class="center">
									<?php echo $value['short_description'];?>
								</td>
								<td>
									<?php echo ($value['is_active'] == 1)?"<span class='label label-sm label-success'>Active</span>":"<span class='label label-sm label-danger'>Inactive</span>"; ?>
								</td>
								<td>
                                                            <a href="<?php echo site_url('admin/artist/editartist/id/'.$value['id'])?>" class="fa fa-pencil"></a> &nbsp;<a onClick="return confirm('Do you want to delete <?php echo $value['name'] ?>');" href="<?php echo site_url('admin/venues/delete_venue/id/'.$value['id']) ; ?>" class="fa fa-times"></a>
								</td>
							</tr>
                                                            <?php }?>

							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
			
		</div>
	</div>
	<!-- END CONTENT -->
	
</div>
<!-- END CONTAINER -->
<?php
$this->load->view('admin/vwFooter');
?>