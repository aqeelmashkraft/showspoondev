<?php
$this->load->view('admin/vwHeader');
?>

<!-- BEGIN CONTAINER -->
<div class="page-container">
    <style>
        .page-container-bg-solid .page-bar {
            margin: 0px 0px 20px 0px;
        }
    </style>
    <?php
    $this->load->view('admin/vwsidebar');
    ?>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">


            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Data Tables</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Managed Datatables</a>
                    </li>
                </ul>

            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box grey-cascade">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-globe"></i>Venues
                            </div>
                            <div class="tools">

                                <a href="javascript:;" class="reload">
                                </a>

                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-6">
                                     <!--   <div class="btn-group">
                                            <a id="sample_editable_1_new" href="<?php echo site_url('admin/gigs/addgig') ?>" class="btn green">
                                                Add New <i class="fa fa-plus"></i>
                                            </a>
                                        </div>-->
                                    </div>

                                </div>
                            </div>
                            <?php
                            //print_r($query);
                            ?>
                            <?php
                            if ($this->session->flashdata('flash_message')) {
                                if ($this->session->flashdata('flash_message') == 'delete') {
                                    echo '<div class="alert alert-danger">';
                                    echo '<a class="close" data-dismiss="alert">×</a>';
                                    echo 'Venue is deleted successfully.';
                                    echo '</div>';
                                }
                            }
                            ?>
                            <table class="table table-striped table-bordered table-hover" id="sample_1">
                                <thead>
                                    <tr>

                                        <th>
                                            Gig name
                                        </th>
                                        <th>
                                            Location
                                        </th>
                                        <th>
                                            Musikere/Spillesteder
                                        </th>
                                        <th>
                                            Date
                                        </th>
                                        <th>
                                            Status
                                        </th>
                                        <th>
                                            Action
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                    foreach ($query as $key => $value) {
                                        $type = '<span class="label label-info">Artist</span>';
                                        if ($value['artist_id'] == 0) {
                                            $type = '<span class="label label-primary">Venue</span>';
                                        }
                                         $date = date_create($value['created_on']);
                                        ?>

                                        <tr class="odd gradeX">

                                            <td>
                                                <?php echo $value['gig_name']; ?>
                                            </td>
                                            <td>

                                                <?php echo $value['location']; ?> ,<?php echo $value['CityName']; ?>
                                            </td>
                                            <td>
                                                <?php
                                                if (!empty($value['artist_name'])) {
                                                    echo $value['artist_name'];
                                                } else {
                                                    echo $value['venue_name'];
                                                }
                                                ?> &nbsp;&nbsp;<span class="type"><?php echo $type; ?></span>
                                            </td>
                                            <td class="center">
                                                <?php echo date_format($date, 'jS F Y'); ?>
                                            </td>
                                            <td>
                                                <?php echo ($value['is_active'] == 1) ? "<span class='label label-sm label-success'>Active</span>" : "<span class='label label-sm label-danger'>Inactive</span>"; ?>
                                            </td>
                                            <td>
                                                <a href="<?php echo site_url('admin/gigs/editgigs/id/' . $value['id']) ?>" class="fa fa-pencil"></a> &nbsp;<a onClick="return confirm('Do you want to delete <?php echo $value['gig_name'] ?>');" href="<?php echo site_url('admin/venues/delete_gig/id/' . $value['id']); ?>" class="fa fa-times"></a>
                                            </td>
                                        </tr>
                                    <?php } ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </div>

        </div>
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
<?php
$this->load->view('admin/vwFooter');
?>