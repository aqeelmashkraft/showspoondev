<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" id="nav">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
			<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
			<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
			<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
			<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
			<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
			<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<!-- END SIDEBAR TOGGLER BUTTON -->
				</li>
				<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
				<li class="sidebar-search-wrapper">
					<!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
					<!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
					<!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
					<form class="sidebar-search " action="extra_search.html" method="POST">
						<a href="javascript:;" class="remove">
						<i class="icon-close"></i>
						</a>
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search...">
							<span class="input-group-btn">
							<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
							</span>
						</div>
					</form>
					<!-- END RESPONSIVE QUICK SEARCH FORM -->
				</li>
				<li>
					<a href="<?php echo site_url('admin/dashboard')?>">
					<i class="icon-bar-chart"></i>
					<span class="title">Dashboard</span>
					<span class="selected"></span>
					</a>
				</li>
				
				
				
				<li>
					<a href="<?php echo site_url('admin/venues/')?>">
					<i class="fa fa-building"></i>
					<span class="title">Venues</span>
					<span class="selected"></span>
					</a>
				</li>
				<li>
					<a href="<?php echo site_url('admin/artist/')?>">
					<i class="fa fa-building"></i>
					<span class="title">Artist</span>
					<span class="selected"></span>
					</a>
				</li>
				<li>
					<a href="<?php echo site_url('admin/gigs/')?>">
					<i class="fa fa-building"></i>
					<span class="title">Gigs</span>
					<span class="selected"></span>
					</a>
				</li>
				
				
				

				
				
				
				
				
				
				
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
	<!-- END SIDEBAR -->
    
<script type="text/javascript" >
function setActive() {
     
  aObj = document.getElementById('nav').getElementsByTagName('a');
  aObj2 = document.getElementById('nav').getElementsByTagName('li');
 
  for(i=1;i<aObj.length;i++) { 
    if(document.location.href==aObj[i]) {
      aObj2[i].className='start active open';

    }
	

  }
  
  	 if(document.location.href.indexOf('venues')>=0|| document.location.href.indexOf('editvenues')>=0) {
      aObj2[3].className='start active open';

    }
	
	 if(document.location.href.indexOf('artist')>=0|| document.location.href.indexOf('addemployee')>=0) {
      aObj2[4].className='start active open';

    }
    
    	 if(document.location.href.indexOf('viewlunchtype')>=0|| document.location.href.indexOf('addlunchtype')>=0) {
      aObj2[5].className='start active open';

    }
    
    if(document.location.href.indexOf('viewlunchtype')>=0|| document.location.href.indexOf('editlunchtype')>=0) {
      aObj2[5].className='start active open';
    }
    if(document.location.href.indexOf('viewlunchitem')>=0|| document.location.href.indexOf('viewlunchitem')>=0) {
      aObj2[5].className='start active open';
    }
	
	
 
    if(document.location.href.indexOf('admin/lunchitem')>=0|| document.location.href.indexOf('addlunchitem')>=0)  {
      aObj2[5].className='start active open';
    }
	
	 if(document.location.href.indexOf('admin/company_default_days')>=0)  {
      aObj2[8].className='start active open';
    }
	
	
    if(document.location.href.indexOf('adminmenu')>=0|| document.location.href.indexOf('adminmenu')>=0) {
      aObj2[8].className='start active open';
    }
    if(document.location.href.indexOf('adminmenu')>=0|| document.location.href.indexOf('companymenu')>=0) {
      aObj2[8].className='start active open';
    }
}

window.onload = setActive;
</script>