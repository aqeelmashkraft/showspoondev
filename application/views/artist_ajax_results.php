<?php
if (count($search_results) > 0) {
    $count=0;
    foreach ($search_results as $key => $value) {

        $imgpath = "" . base_url() . "files/artist_gallery/thumbs_search/" . $value['featuredimg'];
        if (empty($value['featuredimg'])) {
            $imgpath="".base_url()."assets/images/no-image.jpg";
        }
        $count++;
        $break = '';
        if ($count && $count % 3 == 0) {
            $break = "<div class='clearfix'></div>";
        }
        ?>
        <div class="col-md-4 col-sm-6 col-xs-12 ArtistBox xm-no-gutter">
            <div class="panel panel-default">
                <div class="panel-body">
                    <a href="<?php echo base_url(); ?>venue_detail/index/id/<?php echo $value['id']; ?>">
                        <img src="<?php echo $imgpath; ?>" alt="No Image" class="img-responsive center-block" />
                    </a>
                </div>
                <div class="panel-footer">
                    <h5 class="artistName"><?php echo $value['name']; ?></h5>
					<!--<p class="artistGenre">Pop/Rock</p>-->
                    <p class="artistGenre"><?php echo $value['review'];?> Anmeldelser</p>
                </div>
            </div>
        </div><!-- col-md-4 -->
        <?php echo $break;?>
    <?php }
} else {
    ?>
    <div class="col-md-4 col-sm-6 col-xs-12 ArtistBox xm-no-gutter">
        <strong>Ingen registrering funnet</strong>
    </div>
<?php } ?>
        <div class="col-md-12 col-sm-12 col-xs-12 no-gutter">
            <div class="col-md-8 col-sm-8 col-xs-12">
                <?php echo $links; ?>   
               

            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 mtl">
                                <p class="showingSearchCount">Viser <?php echo count($search_results);?> Resultater av <?php echo $total_records;?></p>
                            </div>
        </div>


