<?php $this->load->view('header'); ?>

<?php $this->load->view('mydashboard_artist_links'); ?>

<div class="GreyDashboard  side-collapse-container">
    <div class="container">
        <div class="row">			
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="media">
                    <div class="media-left">
                        <?php
                        $imgpath = "" . base_url() . "files/artist_gallery/" . $artist[0]['featuredimg'];
                        if (empty($artist[0]['featuredimg'])) {
                            $imgpath = "" . base_url() . "assets/images/no-image.jpg";
                        }
                        ?>
                        <img src="<?php echo $imgpath; ?>" alt="<?php echo $artist[0]['name']; ?>" class="center-block img-responsive artistFeaturedImage" />
                    </div><!-- media-left -->
                    <div class="media-body">
                        <?php //print_r($artist); ?>
                        <h2 class="media-heading"><?php echo $artist[0]['name']; ?> <a href="<?php echo base_url(); ?>artist_edit_profile"><i class="fa fa-pencil"></i></a></h2>
                        <p class="tagband"><?php echo $artist[0]['band_typ_name']; ?></p>
                        <p class="artistgenre"><?php echo str_replace(",", "/", $artist[0]['gener']); ?></p>
                    </div><!-- media-body -->
                </div><!-- media -->
            </div><!-- col-md-12 -->

            <div class="col-md-12 col-sm-12 col-xs-12 ArtistDashboarItems mtl">

                <div class="panel">
                    <div class="panel-heading">
                        <h5>Spillested</h5>
                        <?php //print_r($venues); ?>
                      
                        <div class="col-md-2 form-group pull-right no-gutter mbn">
                            <div class="input-group customSearchBtn">
                                <input type="text" name="venue_name" id="venueid" placeholder="S�k spillested" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn" type="submit" id="seach_btn"><span class="fui-search"></span></button>
                                </span>
                            </div>

                        </div>

<!--<a class="btn btn-primary btn-artist secondary" href="#">Search Venue <i class="fa fa-search"></i></a>-->
                    </div><!-- panel-heading -->

                    <div class="panel-body">
                        <?php
                        if (count($venues) > 0) {
                            ?>
                            <div class="table-responsive">
                                <table class="table table-hover table-condensed">
                                    <thead>
                                        <tr>
                                            <td>Navn</td>
                                            <td>Sjanger</td>
                                            <td>Alder</td>
                                            <td>Type</td>
                                            <td>by</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($venues as $key => $value) {

                                            $query = $this->db->query("SELECT a.id,(SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (" . $value['genre'] . ")  ) AS tt FROM `venues` as a WHERE a.id= " . $value['id'] . "");
                                            foreach ($query->result() as $row) {

                                                echo '<tr>';
                                                echo '<td><a href="' . base_url() . 'venue_detail/index/id/' . $value['id'] . '">' . $value['name'] . '</td></a>';
                                                echo '<td>' . $row->tt . '</td>';
                                                echo '<td>' . $value['age'] . '</td>';
                                                echo '<td>' . $value['name'] . '</td>';
                                                echo '<td>' . $value['CityName'] . '</td>';

                                                echo '</tr>';
                                            }
                                        }
                                        ?>

                                    </tbody>
                                </table>

                            </div><!-- table-responsive -->
<?php } else { ?>
                            <p class="ptagpadding"><strong>Ingen arenaer tilgjengelig i din by</strong></p>
                        <?php } ?>
                    </div><!-- panel-body -->

                </div><!-- panel -->

                <div class="clearfix"></div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <a href="<?php echo base_url() ?>search/index/venue/all" class="btn smallbtn mbl viewall" >Se alt</a>
                </div>
                <div class="clearfix"></div>

                <div class="panel">
                    <div class="panel-heading">
                        <h5>konserter </h5>
                        <div class="col-md-2 form-group pull-right no-gutter mbn">
                            <div class="input-group customSearchBtn">
                                <input type="text" name="gig_name" id="gigid" placeholder="S�ke Gigs" class="form-control">
                                <span class="input-group-btn">
                                    <button class="btn" type="submit" id="seach_btn_gig"><span class="fui-search"></span></button>
                                </span>
                            </div>

                        </div>
                        <a class="btn btn-primary btn-artist secondary" href="#" style="display: none;" >S�ke Gigs <i class="fa fa-search"></i></a>
                    </div><!-- panel-heading -->

                    <div class="panel-body">
                        <div class="table-responsive">
<?php
if (count($gigs) > 0) {
    ?>
                                <table class="table table-hover table-condensed">
                                    <thead>
                                        <tr>
                                            <td>Navn</td>
                                           <!-- <td>Genre</td>-->
                                           
                                            <td>Musikere/Spillesteder</td>
                                             <td>Dato</td>
                                            <td>Adresse</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                <?php
                                            foreach ($gigs as $key => $value) {
                                                $type = '<span class="label label-info">Artist</span>';
                                                if ($value['artist_id'] == 0) {
                                                    $type = '<span class="label label-primary">Venue</span>';
                                                }
                                                $imgpath = "" . base_url() . "assets/images/no-image.jpg";
                                                if (!empty($value['image_url'])) {
                                                    $imgpath = "" . base_url() . "uploads/gigs/" . $value['image_url'];
                                                }
                                               /* $query = $this->db->query("SELECT (SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (" . $value['type'] . ")  ) AS gener
                                                                                                 FROM `artist_gigs` as a
                                                                                                     WHERE a.id=" . $value['id'] . "");
                                                foreach ($query->result() as $row) {*/
                                                 $date = date_create($value['created_on']);
                                                    ?>
                                                <tr>
                                                    <td><a href="<?php echo base_url() ?>gig_details/id/<?php echo $value['id']; ?>"><?php echo $value['gig_name']; ?></a></td>
                                                    <!--<td><?php echo $row->gener; ?></td>-->
                                                    
                                                    <td><?php
                                                                if (!empty($value['artist_name'])) {
                                                                    echo $value['artist_name'];
                                                                } else {
                                                                    echo $value['venue_name'];
                                                                }
                                                                ?> &nbsp;&nbsp;<span class="type"><?php echo $type; ?></span></td>
                                                    <td><?php echo date_format($date, 'jS F Y'); ?></td>
                                                            <td><?php echo $value['location']; ?> ,<?php echo $value['CityName']; ?></td>
                                                        </tr>
                                                    <?php //}
                                                }
                                                ?>

                                    </tbody>
                                </table>
                            <?php } else { ?>
                                <p class="ptagpadding"><strong>Ingen Konserter tilgjengelig i i din by</strong></p>
                            <?php } ?>
                        </div><!-- table-responsive -->
                    </div><!-- panel-body -->

                </div><!-- panel -->

                <div class="clearfix"></div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <a href="<?php echo base_url() ?>gigs_search/index/gigs/all" class="btn smallbtn mbl viewall" >Se alt</a> 
                </div>
                <div class="clearfix"></div>

                <div class="panel">
                    <div class="panel-heading">
                        <h5>Mine konserter </h5>
                        <a class="btn btn-primary btn-artist secondary" href="<?php echo base_url() ?>artist_dashboard/add_gig">Legg til en konsert + <i class="fa fa-plus"></i></a>
                    </div><!-- panel-heading -->
                    <?php //print_r($mygig); ?>
                    <div class="panel-body">
                        <?php
                        if ($this->session->flashdata('flash_message')) {
                            if ($this->session->flashdata('flash_message') == 'delete') {
                                echo '<div class="alert alert-danger">';
                                echo '<a class="close" data-dismiss="alert">×</a>';
                                echo 'Gig slettet.';
                                echo '</div>';
                            }
                        }
                        ?>
                        <?php
                        if (count($mygig) > 0) {
                            ?>
                            <div class="table-responsive">
                                <table class="table table-hover table-condensed">
                                    <thead>
                                        <tr>
                                            <td>Navn</td>
                                            <!--<td>Genre</td>-->
                                           
                                            <td>Adresse</td>
                                             <td>Dato</td>
                                            <td>Handling</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach ($mygig as $key => $value) {
                                               /* $query = $this->db->query("SELECT (SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (" . $value['type'] . ")  ) AS gener
			                    FROM `artist_gigs` as a
				                WHERE a.id=" . $value['id'] . " AND a.artist_id=" . $value['artist_id'] . " ");
                                                foreach ($query->result() as $row) {*/
                                                 $date = date_create($value['created_on']);
                                                    ?>
                                                <tr>
                                                    <td><a href="<?php echo base_url() ?>gig_details/id/<?php echo $value['id']; ?>"><?php echo $value['gig_name']; ?></a></td>
                                                  <!--  <td><?php echo $row->gener; ?></td>-->
                                                   
                                                    <td><?php echo $value['location']; ?> ,<?php echo $value['CityName']; ?></span></td>
                                                    <td><?php echo date_format($date, 'jS F Y'); ?></td>
                                                    <td>
                                                        <a href="<?php echo base_url() ?>artist_dashboard/edit_gig/<?php echo $value['id']; ?>" class="ActionControls"><i class="fa fa-pencil"></i></a>
                                                        <a href="<?php echo base_url() ?>artist_dashboard/delete_gig/<?php echo $value['id']; ?>"><i class="fa fa-trash"></i></a>
                                                    </td>
                                                </tr>

                                            <?php //}
                                        } ?>
    
                                    </tbody>
                                </table>
                            </div>
<?php } else { ?><!-- table-responsive -->
                            <p class="ptagpadding"><strong>Du har ikke noen gigs</strong></p>
                        <?php } ?>
                    </div><!-- panel-body -->

                </div><!-- panel -->

                <div class="clearfix"></div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <a href="<?php echo base_url() ?>artist_my_gigs" class="btn smallbtn mbl viewall" >Se alt</a>
                </div>
                <div class="clearfix"></div>

            </div><!-- col-md-12 -->

        </div><!-- row -->
    </div><!-- container -->
</div><!-- GreyDashboard -->


<?php $this->load->view('footer'); ?>