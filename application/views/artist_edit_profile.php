<?php $this->load->view('header'); ?>
<?php $this->load->view('mydashboard_artist_links'); ?>


<div class="GreyDashboard  side-collapse-container">
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url() ?>artist_dashboard">Dashboard</a></li>
                    <li class="active"><a href="#">Oppdater profil</a></li>
                </ul>
            </div><!-- col-md-12 -->

            <div class="col-md-12 col-sm-12 col-xs-12 ArtistDashboarItems">
                <?php
                $gallary = $this->uri->segment(2);

                $activcls = 'active';
                $activ = '';
                $activ2 = '';
                if ($this->session->flashdata('flash_active')) {
                    if ($this->session->flashdata('flash_active') == 'avtive') {
                        $activ = 'active';
                        $activcls = '';
                        $activ2 = '';
                    }
                }

                if ($this->session->flashdata('flash_active2')) {
                    if ($this->session->flashdata('flash_active2') == 'active2') {
                        $activ = '';
                        $activcls = '';
                        $activ2 = 'active';
                    }
                }
                if ($this->session->flashdata('flash_active3')) {
                    if ($this->session->flashdata('flash_active3') == 'active3') {
                        $activ = '';
                        $activcls = 'active';
                        $activ2 = '';
                    }
                }
                if ($this->session->flashdata('flash_active4')) {
                    if ($this->session->flashdata('flash_active4') == 'active4') {
                        $activ = 'active';
                        $activcls = '';
                        $activ2 = '';
                    }
                }
                ?>
                <ul class="nav nav-tabs nav-append-content">
                    <li class="<?php echo $activcls; ?>"><a href="#profile" data-toggle="tab">Profil</a></li>
                    <li class="<?php echo $activ; ?>"><a href="#bandmembers" data-toggle="tab">Bandmedlemmer</a></li>
                    <li class="<?php echo $activ2; ?>" ><a href="#gallery" data-toggle="tab">Media</a></li>
                </ul><!-- nav -->

                <!-- Tab content -->
                <div class="tab-content">
                    <div class="tab-pane <?php echo $activcls; ?>" id="profile">
                        <div class="col-md-12 col-sm-12 col-xs-12 xm-no-gutter">
                            <?php //print_r($users); ?>
                            <?php
                            $genre_id['genre_id'] = explode(',', $users[0]['genre_id']);
                            $gig_type_id['gig_type_id'] = explode(',', $users[0]['gig_type_id'])
                            ?>
                            <?php
                            //var_dump($this->session->flashdata('flash_message'));
                            //flash messages
                            if ($this->session->flashdata('flash_message')) {
                                if ($this->session->flashdata('flash_message') == 'updated') {
                                    echo '<div class="alert alert-success">';
                                    echo '<a class="close" data-dismiss="alert">×</a>';
                                    echo 'Profilen er oppdatert med suksess.';
                                    echo '</div>';
                                }
                            }
                            ?>
                            <form id="myForm" class=""  method="post" action="<?php echo base_url() ?>artist_dashboard/edit_profile">

                                <div class="col-md-6 col-sm-6 col-xs-12 xm-no-gutter">
                                    <div class="form-group">
                                        <label>Artistnavn</label>
                                        <input type="text" class="form-control" placeholder="Artistnavn" name="art_name" value="<?php echo $users[0]['name']; ?>"  required />
                                    </div><!-- form-group -->

                                    <div class="form-group">
                                        <label>Sjanger</label>
                                        <?php echo form_dropdown('art_genre[]', $artist_genre, $genre_id['genre_id'], 'class="form-control multiselect mbl" multiple="multiple"  placeholder="Sjanger"'); ?>

                                    </div><!-- form-group -->

                                    <div class="form-group">
                                        <label>Artistkategori</label>
                                        <?php echo form_dropdown('art_type', $artist_band_type, $users[0]['band_type_id'], 'class="form-control select select-primary select-block mbl" placeholder="Type"'); ?>

                                    </div><!-- form-group -->

                                    <div class="form-group">
                                        <label>Type of Gigs</label>
                                        <?php echo form_dropdown('art_type_show[]', $artist_gig_type, $gig_type_id['gig_type_id'], 'class="form-control multiselect mbl" multiple="multiple" placeholder="Type of Gigs"'); ?>

                                    </div><!-- form-group -->

                                    <div class="form-group">
                                        <label>Postkode</label>
                                        <input type="text" class="form-control" placeholder="Postkode" name="art_zip" value="<?php echo $users[0]['zip']; ?>" required />

                                    </div>

                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 xm-no-gutter">


                                    <!--   <div class="form-group" >
                                       <label>Location</label>
                                       <input type="hidden" placeholder="Location" name="art_location" value="<?php echo $users[0]['location']; ?>" class="form-control" />
                                   </div> -->
                                    <div class="form-group">
                                        <label>By</label>
                                        <input type="hidden" placeholder="Location" name="art_location" value="<?php echo $users[0]['location']; ?>" class="form-control" />
                                        <?php echo form_dropdown('art_city', $cities, $users[0]['city'], 'class="form-control select select-primary select-block mbl"  placeholder="BY"'); ?>
                                          <!--<input type="text" placeholder="Location" name="art_city" value="<?php echo $users[0]['city']; ?>" class="form-control" />-->
                                    </div>
                                      <div class="form-group" >
                                    <label>Facebook-side lenke</label>
                                    <input type="url" placeholder="Facebook-side lenke" name="fb_page_link" value="<?php echo $users[0]['fb_page_link']; ?>" class="form-control" />
                                </div>
                                    <div class="form-group">
                                        <label>Kort beskrivelse</label>
                                        <textarea placeholder="Kort beskrivelse" name="art_short_description" class="form-control" required rows="4" ><?php echo $users[0]['short_description']; ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Biografi</label>
                                        <textarea placeholder="Biografi" name="art_biography"  class="form-control" required rows="7"><?php echo $users[0]['biography']; ?></textarea>
                                    </div>
                                    <!-- form-group -->

                                </div>
                                <div class="col-md-12 col-sm-6 col-xs-12 xm-no-gutter">
                                    <input type="submit" class="btn btn-primary btn-artist secondary lg" id="submitbtn" value="Update">
                                </div>

                                <!--<a class="btn btn-primary btn-artist secondary lg" href="#">Update</a>-->
                            </form>		
                        </div><!-- col-md-12 -->
                    </div><!-- profile -->


                    <div class="tab-pane <?php echo $activ; ?>" id="bandmembers">
                        <div class="col-md-12 col-sm-12 col-xs-12 no-gutter">


                            <?php
                            if ($this->session->flashdata('flash_message')) {
                                if ($this->session->flashdata('flash_message') == 'Delete_member') {
                                    echo '<div class="alert alert-danger">';
                                    echo '<a class="close" data-dismiss="alert">×</a>';
                                    echo 'Artist medlem er slett med suksess.';
                                    echo '</div>';
                                }
                            }
                            ?>
                            <?php
                            //print_r($files);
                            $count = 0;
                            if (count($files) > 0) {
                                $count = count($files);
                                $str = '<p>Du har  ' . $count . ' Bandmedlemmer</p>';
                            } else {
                                $str = '<p>Du trenger ikke ha noen bandmedlemmer</p>';
                            }
                            ?>
                            <div class="col-md-6 col-sm-6 col-xs-12"><?php echo $str; ?></div>

                            <div class="col-md-6 col-sm-6 col-xs-12"><a class="btn btn-primary btn-artist secondary" href="javascript:void(0)" id="btn_add_band"><i class="fa fa-plus"></i> Legg til ny bandmedlemmene</a></div>
							
							<form method="post" action="" id="upload_file">
                                <div class="col-md-12 col-sm-12 col-xs-12 no-gutter AddNewMember mtl" id="AddNewMember">
                                    <div class="col-md-3 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;">
                                                    <img data-src="holder.js/100x100" alt="">
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100px; max-height: 100px;"></div>
                                                <div>
                                                    <span class="btn btn-primary btn-file">
                                                        <span class="fileinput-new"><span class="fui-image"></span> Velg bilde</span>
                                                        <span class="fileinput-exists"><span class="fui-gear"></span> Endring</span>
                                                        <input type="file" name="userfile" id="userfile" size="20" >
                                                    </span>
                                                    <a href="#" class="btn btn-primary btn-file fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>  fjerne</a>
                                                </div>
                                            </div><!-- fileinput -->
                                        </div><!-- form-group -->
                                        <small class="help-block" data-fv-validator="notEmpty" id="error2" data-fv-for="art_location" data-fv-result="INVALID" style=""></small>
                                    </div><!-- col-md-4 -->
                                    <div class="col-md-3 col-sm-4 col-xs-12">
                                        <input type="text" class="form-control mbl mth" name="title" id="title" placeholder="Navn" required/>
                                    </div><!-- col-md-3 -->
                                    <div class="col-md-3 col-sm-4 col-xs-12">
                                        <input type="text" class="form-control mbl mth"  name="role" id="role"  placeholder="Rolle" required/>
                                    </div><!-- col-md-4 -->
                                    <div class="col-md-3 col-sm-4 col-xs-12">

                                        <input type="submit" name="submit" class="btn btn-primary btn-file mbl mth" id="submit" value="Lagre" />
                                        <!--<a href="#" class="btn btn-primary btn-file mbl mth">Save</a>
                                        <a href="#" class="btn btn-primary btn-file mbl mth">Delete</a>-->
                                    </div>
                                </div><!-- col-md-12 -->
                            </form>
							
                            <div class="clearfix"></div>
                            <div id="files" class="col-md-12 mtm"></div>
                            <?php ?>
                            <!--<a class="btn btn-primary btn-artist secondary lg" href="#">Update</a>-->
                        </div><!-- col-md-12 -->
                    </div><!-- bandmembers -->


                    <div class="tab-pane <?php echo $activ2; ?>" id="gallery">
                        <div class="col-md-12 no-gutter">
                            <h5>Pictures</h5>
                        </div><!-- col-md-12 -->
                        <div class="col-md-12 col-sm-12 col-xs-12 GalleryTab mbh">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <form method="post" action="" id="upload_file_artist">
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;">
                                                <img data-src="holder.js/100x100" alt="">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100px; max-height: 100px;"></div>
                                            <div class="clearfix"></div>
                                            <label class="checkbox col-md-8" for="checkbox1">
                                                <input type="checkbox" name="setfeatured"   id="checkbox1" > Set Utvalgt
                                            </label>
                                            <div class="clearfix"></div>
                                            <div>
                                                <span class="btn btn-primary btn-file">
                                                    <span class="fileinput-new">
                                                        <span class="fui-image"></span> Velg bilde
                                                    </span>
                                                    <span class="fileinput-exists"><span class="fui-gear"></span> Endring</span>
                                                    <input type="file" name="userfile" id="userfile3" size="20" >
                                                </span>
                                                <a href="#" class="btn btn-primary btn-file fileinput-exists" data-dismiss="fileinput">
                                                    <span class="fui-trash"></span>  fjerne
                                                </a>
                                                <input type="submit" name="submit" class="btn btn-primary btn-file" id="submitvenue" value="Lagre" />
                                            </div>
                                            <small class="help-block" data-fv-validator="notEmpty" id="error" data-fv-for="art_location" data-fv-result="INVALID" style=""></small>
                                        </div><!-- fileinput -->
                                    </div><!-- form-group -->
                                </form> 
                            </div>
                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <div id="files3">

                                </div>    
                            </div>


                        </div><!-- col-md-4 -->

                        <div class="col-md-6 no-gutter">
                            <h5>Videoer</h5>
                        </div><!-- col-md-12 -->
						<div class="col-md-6 ">
                           <a class="btn btn-primary btn-artist secondary" href="javascript:void(0)" id="btn_add_video"><i class="fa fa-plus"></i> Legg til ny Video Link</a>
                        </div><!-- col-md-12 -->
						
						<div class="col-md-12 col-sm-12 col-xs-12 GalleryTab mbh" id="AddNewvideo">

                            <div class="col-md-12 no-gutter videoblock">

                                <form method="post" action="" id="upload_file_media">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <h5>Legg Nye videoer</h5>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <input type="hidden"  value="1" id="media_type" name="media_type" class="form-control mbl" />
                                            <input type="text"  placeholder="video tittel" id="video_title" name="video_title" class="form-control mbl" required />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">

                                            <input type="url" placeholder="videolink" id="video_link" name="video_link" class="form-control mbl" required="required" />
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <!--<a class="btn btn-primary btn-file mbl" href="#">Save</a>-->

                                        <input type="submit" name="submit" class="btn btn-primary btn-file mbl" id="submitvenue" value="Lagre" />
                                    </div><!-- col-md-4 --> 
                                    <div id="success"></div>
                                </form>

                            </div><!-- videoblock -->

                            <!--<div class="col-md-12">
                                    <a class="btn btn-primary btn-file mbl" href="#">Add More Video</a>
                            </div>--><!-- col-md-12 -->

                        </div><!-- col-md-12 -->
						
                        <div class="col-md-12 col-sm-12 col-xs-12 GalleryTab mbh" id="files4"></div>
						
                        <div class="col-md-6 no-gutter">
                            <h5>SoundCloud</h5>
                        </div><!-- col-md-12 -->
						<div class="col-md-6 ">
                           <a class="btn btn-primary btn-artist secondary" href="javascript:void(0)" id="btn_add_sound_link"><i class="fa fa-plus"></i> Legg til ny Soundcloud Link</a>
                        </div><!-- col-md-12 -->
						
						<div class="col-md-12 col-sm-12 col-xs-12 GalleryTab mbl" id="AddNewsouncloud">

                            <div class="col-md-12 no-gutter SoundCloudBlock mbl" >
                                <form method="post" action="" id="upload_file_media_sound">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <h5>Legg til ny Soundcloud Link </h5>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <input type="hidden"  value="2" id="media_type2" name="media_type" class="form-control mbl" />
                                            <input type="text"  placeholder="Tittel" id="video_title2" name="video_title" class="form-control mbl" required />
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">

                                            <input type="url" placeholder="Link" id="video_link2" name="video_link" class="form-control mbl" required="required" />
                                        </div>
                                    </div>

                                    <div class="col-md-2 col-sm-6 col-xs-12">
                                        <!--<a class="btn btn-primary btn-file mbl" href="#">Save</a>-->

                                        <input type="submit" name="submit" class="btn btn-primary btn-file mbl" id="submitsound" value="Lagre" />
                                    </div><!-- col-md-4 --> 
                                    
                                </form>

                                <!--<div class="col-md-12 col-sm-12 col-xs-12">
                                    <object height="81" width="100%" id="yourPlayerId" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000">
                                        <param name="movie" value="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fmatas%2Fhobnotropic&enable_api=true&object_id=yourPlayerId"></param>
                                        <param name="allowscriptaccess" value="always"></param>
                                        <embed allowscriptaccess="always" height="81" src="http://player.soundcloud.com/player.swf?url=http%3A%2F%2Fsoundcloud.com%2Fmatas%2Fhobnotropic&enable_api=true&object_id=yourPlayerId" type="application/x-shockwave-flash" width="100%" name="yourPlayerId"></embed>
                                    </object>
                                </div>--><!-- col-md-12 -->
                            </div><!-- videoblock -->

                            <div class="col-md-12">
                               <div id="success2"></div> <!--<a class="btn btn-primary btn-file mbl" href="#">Add More Sound Samples</a>-->
                            </div><!-- col-md-12 -->

                        </div><!-- col-md-12 -->
						
                        <div class="col-md-12 col-sm-12 col-xs-12 GalleryTab mbh" id="files5"></div>
                        <!--<a class="btn btn-primary btn-artist secondary lg" href="#">Update</a>-->

                    </div><!-- gallery -->

                </div><!-- tab-content -->

            </div><!-- col-md-12 -->

        </div><!-- row -->
    </div><!-- container -->
</div><!-- GreyDashboard -->


<?php $this->load->view('footer'); ?>