<?php $this->load->view('header'); ?>

<?php $this->load->view('mydashboard_artist_links'); ?>

<div class="GreyDashboard  side-collapse-container">
	<div class="container">
		<div class="row">
			
			<div class="col-md-12 col-sm-12 col-xs-12 ArtistDashboarItems">
				
				<div class="panel">
					<div class="panel-heading">
						<h5>Meldinger fra kunstner / arrangements</h5>
						
					</div><!-- panel-heading -->
					
					<div class="panel-body" style="padding: 10px;">
						<div class="table-responsive">
                                                    <?php if(count($messages)>0){ ?>
							<table class="table table-hover table-condensed">
								<thead>
									<tr>
										<td>Fra Artist / spillested</td>
										<td>Emne</td>
										<td>Beskjed</td>
										<td>Dato</td>
										
									</tr>
								</thead>
								<tbody>
                                                                    <?php
																	//print_r($messages);
																	//die();
                                                                        foreach($messages as $key => $value)
                                                                        { ?>
																		<tr>
                                                                            <td>
                                                                                <?php if($value['type']==1){ ?>
                                                                                <a href="<?php echo base_url();?>venue_detail/index/id/<?php echo $value['senderartisid']?>"><?php echo $value['sender_artist'];?>&nbsp;&nbsp; <span class="label label-primary">Venue</span></a>
                                                                                <?php }else{ ?>
                                                                                   <a href="<?php echo base_url();?>artist_detail/index/id/<?php echo $value['senderartisid']?>"><?php echo $value['sender_artist'];?>&nbsp;&nbsp;<span class="label label-info">Artist</span></a>
                                                                                <?php }?>
                                                                            </td>
										<td><?php echo $value['subject'];?></td>
										<td><?php echo $value['message'];?></td>
										<td><?php echo $value['created_on'];?></td>
										
									</tr>
                                                                        <?php }?>
									
								</tbody>
							</table>
                                                    <?php }else{?>
                                                    <p class="ptagpadding"><strong>Du har ikke mottatt noen melding</strong></p>
                                                    <?php }?>
						</div><!-- table-responsive -->
					</div><!-- panel-body -->
					
				</div><!-- panel -->
				
                        </div>
				
			
		</div><!-- row -->
	</div><!-- container -->
</div><!-- GreyDashboard -->


<?php $this->load->view('footer'); ?>