<?php $this->load->view('header'); ?>

<div class="artistRegister side-collapse-container">
    <div class="container">
        <div class="row">

            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3 panel" >
                <h3>Registrering deg <strong>Showspoon</strong></h3>
                <p class="subheading">Kontodetaljer</p>
                <div id="response" class="subheading"></div>
                <div class="col-md-10 col-md-offset-1">
                  <div class="form-group">
                    <select class="form-control select select-primary select-block mbl some_id" id="someid" name="artist_type" >
                        <option value="" >Musikere/Spillested</option>
                        <option value="0" <?php if ($id == '0') echo"selected"; ?>>Musikere</option>
                        <option value="1"<?php if ($id == '1') echo"selected"; ?>>Spillested</option>

                    </select>
                </div>  
                </div>
                
                <div id="artDiv">
                    <form id="registerForm" class=""  method="post" action="<?php echo base_url() ?>register/create">
                        <div class="col-md-10 col-md-offset-1">
                            
                            <div class="form-group">
                          

                                <input type="email" placeholder="E-post" name="art_email" id="art_email_id" class="form-control" />
                                <div id="tick"  style="display: none;    color: #e74c3c;font-size: 11px;line-height: 10px;margin-bottom: 0;">Denne e-postadressen finnes allerede</div>
                            </div>
                            
                            <div class="form-group">
                                <input type="password" placeholder="Passord" name="art_password" class="form-control" />
                            </div>
                        </div>

                        <div class="col-md-10 col-md-offset-1">
                            <p class="subheading">Musikere detaljer</p>
                            <div class="form-group">
                                <?php echo form_dropdown('art_type', $artist_band_type, '', 'class="form-control select select-primary select-block " placeholder="Type"'); ?>
                           
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Artistnavn" name="art_name" class="form-control" />
                            </div>
                            <div class="form-group">
                                <input type="hidden" placeholder="Location" value="oslo" name="art_location" class="form-control" />
                            </div>
                             <div class="form-group">
                                <?php echo form_dropdown('art_cities', $cities, '', 'class="form-control select select-primary select-block " placeholder="City"'); ?>
        
                            </div>
                            <div class="form-group">
                                <?php echo form_dropdown('art_genre[]', $artist_genre, '', 'class="form-control multiselect " multiple="multiple" placeholder="Sjanger"'); ?>
                               
                            </div>
                            
                            <div class="form-group hide">
                                <?php echo form_dropdown('art_type_show[]', $artist_gig_type, '1', 'class="form-control multiselect " multiple="multiple" placeholder="Types of Shows"'); ?>
                              
                            </div> 
                            <div class="form-group">
                            <label class="checkbox col-md-10 col-md-offset-2 col-sm-8 col-sm-offset-4 col-xs-12" for="checkbox1">
                                <input type="checkbox" value="" name="art_agree"  id="checkbox1" data-toggle="checkbox">Jeg har lest og forst�tt  <a href="#" data-toggle="modal" data-target="#myModal">vilk�r og betingelser.</a>
                            </label>
                            </div>
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Term & Use.</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eros est, 
                                                congue ac sem non, rhoncus egestas dui. Mauris placerat a sapien id interdum.
                                                Nulla vitae dignissim purus. Proin posuere neque ut tincidunt condimentum. Nunc 
                                                ac pellentesque lectus. Suspendisse potenti. Nam vitae placerat velit. Integer 
                                                posuere velit vel scelerisque egestas. Nunc mollis, ligula pulvinar scelerisque dignissim, velit nunc
                                                fermentum ex, sed luctus odio nulla et dolor. Sed in lectus aliquet mauris tincidunt rutrum. Ut mattis 
                                                felis in elit porta, at tincidunt neque volutpat. Vivamus at enim tincidunt, ultrices urna non, cursus sem. 
                                                Suspendisse finibus, tortor et dapibus posuere, velit tortor faucibus velit, nec molestie sem dui a libero.</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary btn-artist secondary centerbtn" id="submitbtn" value="Register">


                        </div>
                    </form>
                </div>

                <div id="venDiv">

                    <form id="registerFormVenue" class=""  method="post" action="<?php echo base_url() ?>venue_register/createVenue">
                        <div class="col-md-10 col-md-offset-1">

                            <div class="form-group" >
                                <input type="email" placeholder="E-post" name="ven_email"  id="ven_email_id" class="form-control" />
								<div id="venid"></div>
                                                                 <div id="tick2"  style="display: none;    color: #e74c3c;font-size: 11px;line-height: 10px;margin-bottom: 0;">Denne e-postadressen finnes allerede</div>
                            </div>
                            <div class="form-group">
                                <input type="password" placeholder="Passord" name="ven_password" class="form-control" />
                            </div>
                        </div>

                        <div class="col-md-10 col-md-offset-1">
                            <p class="subheading">Spillested detaljer</p>

                            <div class="form-group">
                                <input type="text" placeholder="Navn p� spillestedet" name="ven_name" class="form-control " />
                            </div>
                            <div class="form-group">
                                <?php echo form_dropdown('ven_genre[]', $artist_genre, '', 'class="form-control multiselect" multiple="multiple" placeholder="Sjanger"'); ?>
    
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Adresse" name="ven_location" class="form-control " />
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Postkode" name="ven_zip_code" class="form-control" />
                            </div>
                            <div class="form-group">
                                <?php echo form_dropdown('ven_cities', $cities, '', 'class="form-control select select-primary select-block" placeholder="City"'); ?>
        
                            </div>
                            <div class="form-group">
                                <label class="checkbox col-md-10 col-md-offset-2 col-sm-8 col-sm-offset-4 col-xs-12" for="checkbox2">
                                    <input type="checkbox" value="" name="ven_agree"  id="checkbox2" data-toggle="checkbox">Jeg har lest og forst�tt  <a href="#" data-toggle="modal" data-target="#myModal2" >vilk�r og betingelser.</a>
                                </label>
                            </div>
                            <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title" id="myModalLabel">Term &amp; Use.</h4>
                                </div>
                                <div class="modal-body">
                                  <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eros est, 
                                          congue ac sem non, rhoncus egestas dui. Mauris placerat a sapien id interdum.
                                          Nulla vitae dignissim purus. Proin posuere neque ut tincidunt condimentum. Nunc 
                                          ac pellentesque lectus. Suspendisse potenti. Nam vitae placerat velit. Integer 
                                          posuere velit vel scelerisque egestas. Nunc mollis, ligula pulvinar scelerisque dignissim, velit nunc
                                          fermentum ex, sed luctus odio nulla et dolor. Sed in lectus aliquet mauris tincidunt rutrum. Ut mattis 
                                          felis in elit porta, at tincidunt neque volutpat. Vivamus at enim tincidunt, ultrices urna non, cursus sem. 
                                          Suspendisse finibus, tortor et dapibus posuere, velit tortor faucibus velit, nec molestie sem dui a libero.
                                  </p>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                 
                                </div>
                              </div>
                            </div>
                          </div>
                            <input type="submit" class="btn btn-primary btn-artist secondary centerbtn" id="submitbtnven" value="Register">
                            <!--<a class="btn btn-primary btn-artist secondary centerbtn" href="#">Register</a>-->

                        </div>
                    </form>
                </div>



            </div><!-- col-md-6 -->
        </div><!-- row -->
    </div><!-- container -->
</div><!-- artistRegister -->


<?php $this->load->view('footer'); ?>
<script>
    /////////////////////////////////Registration page Divs Show Hide//////////////////////////
    $(document).ready(function() {
    var regid=<?php echo $id;?>;
		//alert(regid);
		if(regid==1){
			$("#artDiv").hide();
			$("#venDiv").show();
		}
		if(regid==0){
			$("#artDiv").show();
			$("#venDiv").hide();
		}
		$('.some_id').on('change', function() {
			//alert( this.value )			
			if(this.value==1){
				$("#artDiv").hide();
				$("#venDiv").show();
			}
			if(this.value==0){
				$("#artDiv").show();
				$("#venDiv").hide();
			}
			
		  //alert( this.value ); // or $(this).val()
		});
                
                });
</script>