<?php $this->load->view('header'); ?>



<div class="WhiteSection topgap v2 side-collapse-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 no-gutter">

                <div class="col-md-12"><h2>Musikere</h2></div>

                <div class="col-md-12 Searchgap">
                    <a class="btn btn-primary roombutton"><strong>Showet / Gjemme seg </strong></a>
                    <div class="col-md-3 col-sm-4  col-xs-12 no-gutter boderedfilter SearchParameters LeftGap">
                        <div class="col-md-12">
                            <h4>Musikere</h4>
                            <?php 
                            //if($venue !='all'){
                             //   echo $venue;
                           // }
                            ?>
							<div class="form-group no-gutter">
                                <div class="input-group customSearchBtn">
                                    <input type="text" name="artist_name" id="artist_name" placeholder="S�ke Musikere" class="form-control" value="<?php if($artist !='all'){ echo $artist;}?>">
                                    <span class="input-group-btn">
                                        <button class="btn" type="submit" id="searchartistid"><span class="fui-search"></span></button>
                                    </span>
                                </div>                
                            </div>
							
							
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <h4 class="heading-top">By</h4>
                            <?php echo form_dropdown('ven_city', $cities, '', 'class="form-control select select-primary select-block mbl" id="ven_city" placeholder="City"'); ?>
                        </div>
                        <div class="clearfix"></div>
               
                        <div class="col-md-12">
                            <h4 class="heading-top">Sjangere</h4>
                            <?php echo form_dropdown('art_genre[]', $artist_genre, '', 'class="form-control multiselect" multiple="multiple" id="art_genre_id" placeholder="Sjangere"'); ?>
                          
                        </div><!-- col-md-12 -->
							
                        <div class="clearfix"></div>
						<div class="col-md-12">
						 <h4 class="heading-top">Gig Type</h4>
                                <?php echo form_dropdown('art_type_show[]', $artist_gig_type, '', 'class="form-control multiselect mbl" multiple="multiple" id="art_gig_type" placeholder="Gig Type"'); ?>
                              
                            </div>
							<div class="clearfix"></div>
                        <div class="col-md-12">
                            <h4 class="heading-top"></h4>
                            <button type="button" class="btn btn-artist secondary smallbtn" id="ajax_seach_btn_artist" >S�ke</button>
                        </div>
                        <div class="clearfix"></div>
                    </div><!-- col-md-3 -->


                    <div class="col-md-9 col-sm-8 col-xs-12 no-gutter" id="ajax_results">
                        <?php //echo $venue;//print_r($search_results);?>
                        <?php
                        if (count($search_results) > 0) {
                            $count=0;
                            foreach ($search_results as $key => $value) {
                                
                                $imgpath="".base_url()."files/artist_gallery/thumbs_search/".$value['featuredimg'];
                                if(empty($value['featuredimg'])){
                                    $imgpath="".base_url()."assets/images/no-image.jpg";
                                }
                                $count++;
                                $break = '';
                                if ($count && $count % 3 == 0) {
                                    $break = "<div class='clearfix'></div>";
                                }
                                ?>
                                <div class="col-md-4 col-sm-6 col-xs-12 ArtistBox xm-no-gutter">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <a href="<?php echo base_url(); ?>artist_detail/index/id/<?php echo $value['id']; ?>">
                                                <img src="<?php echo $imgpath; ?>" alt="No Image" class="img-responsive center-block searchListThumbmail" />
                                            </a>
                                        </div>
                                        <div class="panel-footer">
                                            <h5 class="artistName"><?php echo $value['name']; ?></h5>
											<!--<p class="artistGenre">Pop/Rock</p>-->
                                            <p class="artistGenre"><?php echo $value['review'];?> Anmeldelser</p>
                                        </div>
                                    </div>
                                </div><!-- col-md-4 -->
                                <?php echo $break;?>
                            <?php }
                        } else {
                            ?>
                            <div class="col-md-4 col-sm-6 col-xs-12 ArtistBox xm-no-gutter">
                                <strong>Ingen registrering funnet</strong>
                            </div>
                        <?php } ?>
                        
                        <div class="col-md-12 col-sm-12 col-xs-12 no-gutter">
                            <div class="col-md-8 col-sm-8 col-xs-12">
                            	<?php echo $links; ?>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 mtl">
                                <p class="showingSearchCount">Viser <?php echo count($search_results);?> Resultater av<?php echo $total_records;?></p>
                            </div>
                        </div>
                               
                        

                    </div><!-- col-md-9 -->

                </div><!-- col-md-12 -->


            </div><!-- col-md-12 -->
        </div><!-- row -->
    </div><!-- container -->
</div>

<?php $this->load->view('footer'); ?>