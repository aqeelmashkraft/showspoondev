<?php $this->load->view('header'); ?>

<?php $this->load->view('mydashboard_artist_links'); ?>

<div class="GreyDashboard  side-collapse-container">
	<div class="container">
		<div class="row">
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<ul class="breadcrumb">
				  <li><a href="<?php echo base_url() ?>artist_edit_profile">Update Profile</a></li>
				  <li class="active"><a href="#">Update Band Member</a></li>
				</ul>
			</div><!-- col-md-12 -->
			
			<div class="col-md-12 col-sm-12 col-xs-12 ArtistDashboarItems">
				
				

				<!-- Tab content -->
				<div class="tab-content">
				<?php //print_r($artist_member);?>
						<?php
						  if($this->session->flashdata('flash_message')){
							if($this->session->flashdata('flash_message') == 'updated_band')
							{
							  echo '<div class="alert alert-success">';
							  echo '<a class="close" data-dismiss="alert">×</a>';
							  echo 'Member is updated with success.';
							  echo '</div>';       
							}
						  }
					?>
					<form method="post"  action="<?php echo base_url() ?>artist_dashboard/edit_band" id="band_edit">
							<div class="col-md-12 col-sm-12 col-xs-12 no-gutter AddNewMember mbl" >
								<div class="col-md-3 col-sm-4 col-xs-12">
									<div class="form-group">
										<div class="fileinput fileinput-new" data-provides="fileinput">
											<div class="fileinput-new thumbnail" style="width: 100px; height: 100px;">
												<img data-src="holder.js/100x100" alt="">
											</div>
											<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 100px; max-height: 100px;"></div>
											<div>
												<span class="btn btn-primary btn-file">
												<span class="fileinput-new"><span class="fui-image"></span> Select image</span>
												<span class="fileinput-exists"><span class="fui-gear"></span> Change</span>
												<input type="file" name="userfile" id="userfile" size="20">
												</span>
												<a href="#" class="btn btn-primary btn-file fileinput-exists" data-dismiss="fileinput"><span class="fui-trash"></span>  Remove</a>
											</div>
										</div><!-- fileinput -->
									</div><!-- form-group -->
								</div><!-- col-md-4 -->
								<div class="col-md-3 col-sm-4 col-xs-12">
									<input type="text" class="form-control mbl mth" name="band_name" id="title"  value="<?php echo $artist_member[0]['name']; ?>" placeholder="Name" />
								</div><!-- col-md-3 -->
								<div class="col-md-3 col-sm-4 col-xs-12">
                                                                    <input type="text" class="form-control mbl mth"  name="role" id="role" value="<?php echo $artist_member[0]['role']; ?>"  placeholder="Position" />
								</div><!-- col-md-4 -->
								<div class="col-md-3 col-sm-4 col-xs-12">
								<input type="hidden" value="<?php echo $artist_member[0]['id']; ?>"  name="band_id"/>
								<input type="hidden" value="<?php echo $artist_member[0]['image_url']; ?>"  name="image_url"/>
                                                                    <input type="submit" name="submit" class="btn btn-primary btn-file mbl mth" id="submit" value="update" />
									<!--<a href="#" class="btn btn-primary btn-file mbl mth">Save</a>
									<a href="#" class="btn btn-primary btn-file mbl mth">Delete</a>-->
								</div>
							</div><!-- col-md-12 -->
                         </form>
				
				</div><!-- tab-content -->
				
			</div><!-- col-md-12 -->
			
		</div><!-- row -->
	</div><!-- container -->
</div><!-- GreyDashboard -->


<?php $this->load->view('footer'); ?>