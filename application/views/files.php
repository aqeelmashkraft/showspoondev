<?php
if (isset($files) && count($files))
{
	$count= count($files);
?>
			<?php
			//var_dump($this->session->flashdata('flash_message'));
			//flash messages
			if($this->session->flashdata('flash_message')){
			if($this->session->flashdata('flash_message') == 'success')
			{
			echo '<div class="alert alert-success">';
			echo '<a class="close" data-dismiss="alert">×</a>';
			echo 'File successfully uploaded.';
			echo '</div>';       
			}
			}
			?>

            <?php
            foreach ($files as $file)
            {
				 $edit = base_url().'artist_dashboard/artist_edit_band/'.$file->id;
				 $delete = base_url().'artist_dashboard/artist_delete_band/'.$file->id;
                                 $imgpath=base_url().'assets/images/no-image.jpg';  
                                 if(!empty($file->image_url)){
                                  $imgpath=base_url().'files/thumbs/'.$file->image_url;   
                                 }
                                 
				 
                ?>
		<div class="col-md-4">
			<div class="panel BandMember">
				<div class="panel-body">
					<div class="media">
						<div class="media-left">
						  <img class="media-object center-block img-responsive imagecontrol" src="<?php echo $imgpath; ?>" alt="" />
						</div>
						<div class="media-body">
						  <h4 class="media-heading"><?php echo $file->name?></h4>
						  <p style="margin-bottom:2px;"><?php echo $file->role?></p>
						  <a href='<?php echo $edit; ?>' data-id='<?php echo $file->id; ?>' class="" id="editmember"><i class="fa fa-pencil"></i></a>
						  <a href="<?php echo $delete ;?>" class=""><i class="fa fa-trash-o"></i></a>
						</div>
					</div>
				</div><!-- panel-body -->
			</div><!-- panel -->
		</div><!-- col-md-4 -->
                <?php
            }
            ?>


    <?php
}
else
{
    ?>
    <p>Du har ikke noe bilde</p>
    <?php
}
?>