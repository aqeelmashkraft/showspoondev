<div class="footer">
    <div class="container">
        <div class="row">

            <div class="col-md-6 col-sm-12 col-xs-12 col-md-push-6">
                <ul class="socialbox">
                    <li>
                        <a href="#"><img src="<?php echo base_url();?>assets/images/facebook.png" alt="Facebook" class="center-block img-responsive" /></a>
                    </li>
                    
                </ul>
                <ul class="legallinks">
                    <li><a href="#">Brukeravtale</a><span class="sep">|</span></li>
                    <li><a href="#">Personvernerklæring</a><span class="sep">|</span></li>
                    <li><a href="#">Vilkå &amp; vilkå</a></li>
                </ul>
            </div><!-- col-md-6 -->
            
            <div class="col-md-6 col-sm-12 col-xs-12 col-md-pull-6">
                <p class="copyright">OPPHAVSRETT &copy; 2015 showspoon. Alle rettigheter reservert.</p>
            </div><!-- col-md-6 -->
            
        </div><!-- row -->
    </div><!-- container -->
</div><!-- footer -->


<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/js/formValidation.min.js"></script>
<script src="<?php echo base_url();?>assets/js/framework/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/flat-ui-pro.js"></script>
<script src="<?php echo base_url();?>assets/js/star-rating.min.js"></script>
<script src="<?php echo base_url();?>assets/js/custom.js"></script>
<script src="<?php echo base_url();?>assets/vjs.zencdn.net/4.7/video.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery/jRating.jquery.js"></script> 
<script type="text/javascript">
        $(document).ready(function() {
                   
            $('.basic').jRating();
            $('.basic2').jRating();
            $('.basic3').jRating();

            $('.exemple2').jRating({
                type: 'small',
                length: 40,
                decimalLength: 1,
            });

            $('.exemple3').jRating({
                step: true,
                length: 20
            });

            $('.exemple4').jRating({
                isDisabled: true
            });

            $('.exemple5').jRating({
                length: 10,
                decimalLength: 1,
                onSuccess: function() {
                    alert('Success : your rate has been saved :)');
                },
                onError: function() {
                    alert('Error : please retry');
                }
            });

            $(".exemple6").jRating({
                length: 10,
                decimalLength: 1,
                showRateInfo: false
            });

            $('.exemple7').jRating({
                step: true,
                length: 20,
                canRateAgain: true,
                nbRates: 3
            });
        });
    </script>


<script src="<?php echo base_url()?>assets/js/site.js"></script>
<script src="<?php echo base_url()?>assets/js/ajaxfileupload.js"></script>
<script type="text/javascript">
  var baseurl = "<?php print base_url(); ?>";
</script>
<script src="<?php echo base_url()?>assets/js/custom_functions.js"></script>

</body>
</html>


