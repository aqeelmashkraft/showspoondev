<div class="footer">
    <div class="container">
        <div class="row">

            <div class="col-md-6 col-sm-12 col-xs-12 col-md-push-6">
                <ul class="socialbox">
                    <li>
                        <a href="#"><img src="<?php echo base_url();?>assets/images/facebook.png" alt="Facebook" class="center-block img-responsive" /></a>
                    </li>
                    <li>
                        <a href="#"><img src="<?php echo base_url();?>assets/images/twitter.png" alt="Facebook" class="center-block img-responsive" /></a>
                    </li>
                    <li>
                        <a href="#"><img src="<?php echo base_url();?>assets/images/youtube.png" alt="Facebook" class="center-block img-responsive" /></a>
                    </li>
                </ul>
                <ul class="legallinks">
                    <li><a href="#">User Agreement</a><span class="sep">|</span></li>
                    <li><a href="#">Privacy Policy</a><span class="sep">|</span></li>
                    <li><a href="#">Terms &amp; Conditions</a></li>
                </ul>
            </div><!-- col-md-6 -->
            
            <div class="col-md-6 col-sm-12 col-xs-12 col-md-pull-6">
                <p class="copyright">Copyright &copy; 2015 showspoon. All rights reserved.</p>
            </div><!-- col-md-6 -->
            
        </div><!-- row -->
    </div><!-- container -->
</div><!-- footer -->


<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/js/formValidation.min.js"></script>
<script src="<?php echo base_url();?>assets/js/framework/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/flat-ui-pro.js"></script>
<script src="<?php echo base_url();?>assets/js/star-rating.min.js"></script>
<script src="<?php echo base_url();?>assets/js/custom.js"></script>
<script src="<?php echo base_url();?>assets/vjs.zencdn.net/4.7/video.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery/jRating.jquery.js"></script> 
<script type="text/javascript">
        $(document).ready(function() {
                   
            $('.basic').jRating();
            $('.basic2').jRating();
            $('.basic3').jRating();

            $('.exemple2').jRating({
                type: 'small',
                length: 40,
                decimalLength: 1,
            });

            $('.exemple3').jRating({
                step: true,
                length: 20
            });

            $('.exemple4').jRating({
                isDisabled: true
            });

            $('.exemple5').jRating({
                length: 10,
                decimalLength: 1,
                onSuccess: function() {
                    alert('Success : your rate has been saved :)');
                },
                onError: function() {
                    alert('Error : please retry');
                }
            });

            $(".exemple6").jRating({
                length: 10,
                decimalLength: 1,
                showRateInfo: false
            });

            $('.exemple7').jRating({
                step: true,
                length: 20,
                canRateAgain: true,
                nbRates: 3
            });
        });
    </script>


<script src="<?php echo base_url()?>assets/js/site.js"></script>
<script src="<?php echo base_url()?>assets/js/ajaxfileupload.js"></script>

<script>
$(document).ready(function() {
    
     $('#app_for_gig').click(function() {        
        var message_gig =$('#message_gig').val();
        var gig_id =$('#gig_id').val();
	$.ajax({
		  url: '<?php echo base_url() ?>gig_details/appay_for_gig',
		  type: 'POST',
		  data: { 
                    'message_gig': message_gig, 
                    'gig_id': gig_id
                },
		  success: function(data) {
			//called when successful
			var response= $.parseJSON(data);
			$('#ajax-results').html(response.msg);
		  },
		  error: function(e) {
			//called when there is an error
			//console.log(e.message);
		  }
		});
     });
	/////////////////////////Foget Password/////////////////////////////////// 
   $('#forget_id').click(function() {       
	var email =$('#email').val();
	var artist_type =$('#artist_type').val();
	
       var alldata= {
	    	     	email:email,
			        artist_type:artist_type
	    	     };
	$.ajax({
		  url: '<?php echo base_url() ?>register/forget_password',
		  type: 'POST',
		  data:alldata,
		  success: function(data) {
			//called when successful
			var response= $.parseJSON(data);
			$('#ajaxphp-results').html(response.msg);
		  },
		  error: function(e) {
			//called when there is an error
			//console.log(e.message);
		  }
		});

});
    
  ///////////////////////Comment post On Venue Detail////////////////////////////
  
    $('#comment_post').submit(function(e) {
            //alert('ok');
		e.preventDefault();
		$.ajax({
			url :'<?php echo base_url() ?>venue_detail/comment', 
            type:'POST',
			data: {
				'ven_comment': $('#ven_comment').val(),
                                 'ven_id': $('#ven_id').val()
			},
			success	: function (data)
			{
			  $('#ven_comment').val('');
                          $('#success').html('<p>Comment is posted successfully </p>');
			}
		});
		return false;
	});
        
        $('#message_to_artist').submit(function(e) {
            //alert('ok2');
		e.preventDefault();
		$.ajax({
			url :'<?php echo base_url() ?>venue_detail/message', 
                        type:'POST',
			data: {
				'artist_subject': $('#artist_subject').val(),
                                'artist_message': $('#artist_message').val(),
                                'art_id': $('#art_id').val(),
                                'ven_id': $('#ven_id').val(),
                                'type': $('#type').val()
			},
			success	: function (data)
			{
			  $('#artist_subject').val('');
			  $('#artist_message').val('');
                          $('#successmsg').html('<p>Message is send successfully </p>');
			}
		});
		return false;
	});
        
        
        $('#message_to_venue').submit(function(e) {
           // alert('ok');
		e.preventDefault();
		$.ajax({
			url :'<?php echo base_url() ?>artist_detail/message', 
                        type:'POST',
			data: {
				'artist_subject': $('#artist_subject').val(),
                                'artist_message': $('#artist_message').val(),
                                'art_id': $('#art_id').val(),
                                'ven_id': $('#ven_id').val(),
                                'type': $('#type').val()
			},
			success	: function (data)
			{
			  $('#artist_subject').val('');
			  $('#artist_message').val('');
                          $('#successmsg').html('<p>Message is send successfully </p>');
			}
		});
		return false;
	});
        
        
        
	
	/////////////////////////////////Check UserName of Artist ///////////////////////
	
 $('#art_email_id').keyup(username_check);
	
	function username_check(){
		
	var username = $('#art_email_id').val();
	if(username == "" || username.length < 4){
	$('#art_email_id').css('border', '3px #CCC solid');
	$('#tick').hide();
	}else{
		$.ajax({
			url:'<?php echo base_url() ?>register/checkusername',
			type:'POST',
		    data: 'art_email='+ username,
		    cache: false,
		success: function (response)  {
		if(response == 1){
			$('#art_email_id').css('border', '3px #C33 solid');
			$('#submitbtn').attr("disabled","disabled");				
			$('#tick').hide();
			$('#cross').fadeIn();
		}else{
			$('#submitbtn').removeAttr("disabled");
			$('#art_email_id').css('border', '3px #090 solid');
			$('#cross').hide();
			$('#tick').fadeIn();
		}
	  }

	});
}
}
 ////////////////////////////////Validation Artist Registration///////////////////////////
    $('#registerForm')
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
		artist_type: {
                    validators: {
                        notEmpty: {
                            message: 'This  is required'
                        }
                    }
                },
                art_email: {
                    validators: {
                        notEmpty: {
                            message: 'Email address  is required'
                        },
						emailAddress: {
                        message: 'Please enter a valid email address'
						}                        
                    }
                },
                art_password: {
                    validators: {
                        notEmpty: {
                            message: 'Password  is required'
                        }
                    }
                },
                art_name: {
                    validators: {
                        notEmpty: {
                            message: 'Name  is required'
                        }
                    }
                },
		art_location: {
                    validators: {
                        notEmpty: {
                            message: 'Location  is required'
                        }
                    }
                },
                art_cities: {
                    validators: {
                        notEmpty: {
                            message: 'City  is required'
                        }
                    }
                },
                 art_agree: {
                    validators: {
                        notEmpty: {
                            message: 'This  is required'
                        }
                    }
                },
				'art_genre[]': {
                    validators: {
                        notEmpty: {
                            message: 'Genre  is required'
                        }
                    }
                },
				'art_type_show[]':{
					validators: {
                        notEmpty: {
                            message: 'Types of show  is required'
                        }
                    }
				},
				art_type:{
					validators: {
                        notEmpty: {
                            message: ' Artist type  is required'
                        }
                    }
				}
            }
        }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();

            var $form = $(e.target),
                fv    = $form.data('formValidation');

            // Use Ajax to submit form data
            $.ajax({
                url: $form.attr('action'),
                type: 'POST',
                data: $form.serialize(),
                success: function(result) {
                    
		   window.location.href ="<?php echo base_url() ?>thank_you_for_registration";

		//$("#response").html('Thank! For registration');
		//$('#registerForm').hide();
					
                    // ... Process the result ...
                }
            });
        });
		
		
                
                
  	/////////////////////////////////Check UserName of Venue ///////////////////////              
                
    $('#ven_email_id').keyup(venue_check);
	
	function venue_check(){
	var username = $('#ven_email_id').val();
	if(username == "" || username.length < 4){
	$('#ven_email_id').css('border', '3px #CCC solid');
	$('#tick').hide();
	}else{
		$.ajax({
            url:'<?php echo base_url() ?>register/checkVenue',
			type:'POST',
            data: 'ven_email='+ username,
            cache: false,
		success: function (response)  {
		if(response == 1){
			//alert('1');
			$('#ven_email_id').css('border', '3px #C33 solid');
			$('#submitbtnven').attr("disabled","disabled");				
			$('#tick').hide();
			$('#cross').fadeIn();
		}else{
			//alert('0');
			$('#submitbtnven').removeAttr("disabled");
			$('#ven_email_id').css('border', '3px #090 solid');
			$('#cross').hide();
			$('#tick').fadeIn();
		}
	  }

	});
}
}
 
 ////////////////////////////////Validation Artist Registration/////////////////////////// 
		
    $('#registerFormVenue')
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                ven_email: {
                    validators: {
                        notEmpty: {
                            message: 'Email address  is required'
                        },
						emailAddress: {
                        message: 'Please enter a valid email address'
						}                        
                    }
                },
                ven_password: {
                    validators: {
                        notEmpty: {
                            message: 'Password  is required'
                        }
                    }
                },
                ven_name: {
                    validators: {
                        notEmpty: {
                            message: 'Name  is required'
                        }
                    }
                },
				ven_agree: {
                    validators: {
                        notEmpty: {
                            message: 'This  is required'
                        }
                    }
                },
		ven_location: {
                    validators: {
                        notEmpty: {
                            message: 'Location  is required'
                        }
                    }
                },
                ven_cities: {
                    validators: {
                        notEmpty: {
                            message: 'City  is required'
                        }
                    }
                },				
		 'ven_genre[]': {
                    validators: {
                        notEmpty: {
                            message: 'Genre  is required'
                        }
                    }
                },
			agree:{
					validators: {
                        notEmpty: {
                            message: 'This  is required'
                        }
                    }
				}
				
            }
        }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();

            var $form = $(e.target),
                fv    = $form.data('formValidation');

            // Use Ajax to submit form data
            $.ajax({
                url: $form.attr('action'),
                type: 'POST',
                data: $form.serialize(),
                success: function(result) {
					//alert(result);
		//$("#response").html('Thank! For registration');
               // $('#registerFormVenue').hide();
                window.location.href ="<?php echo base_url() ?>thank_you_for_registration";
					
                    // ... Process the result ...
                }
            });
        });

/////////////////////////////Other Form Validation/////////////////////////////////////////		
       
        $('#myForm').formValidation();
		$('#gig_add').formValidation();
                //$('#upload_file_media').formValidation();
                
                
/////////////////////////////////Registration page Divs Show Hide//////////////////////////            
		//$("#artDiv").show();
		//$("#venDiv").hide();
		//alert(<?php echo $id;?>)
		var regid=<?php echo $id;?>;
		//alert(regid);
		if(regid==1){
			$("#artDiv").hide();
			$("#venDiv").show();
		}
		if(regid==0){
			$("#artDiv").show();
			$("#venDiv").hide();
		}
		$('.some_id').on('change', function() {
			//alert( this.value )			
			if(this.value==1){
				$("#artDiv").hide();
				$("#venDiv").show();
			}
			if(this.value==0){
				$("#artDiv").show();
				$("#venDiv").hide();
			}
			
		  //alert( this.value ); // or $(this).val()
		});

///////////////////////////////Venue Search On Enter Kye////////////////////////////////////		
		$("#venueid").keypress(function(e) {
			if(e.which == 13 ) {
				var venue = $("#venueid").val();
				if(venue==''){
					venue='all';
				}
		 
	    window.location.href ="<?php echo base_url() ?>search/index/venue/"+venue+"";
			}
		});

///////////////////////////////Artist Search On Enter Kye////////////////////////////////////		
		$("#artistid").keypress(function(e) {
			if(e.which == 13 ) {
				var artist = $("#artistid").val();
				if(artist==''){
					artist='all';
				}
		 
	    window.location.href ="<?php echo base_url() ?>artist_search/index/artist/"+artist+"";
			}
		});
		
    $("#seach_btn").click(function() { 
		var venue = $("#venueid").val();
		if(venue==''){
			venue='all';
		}
		//alert(venue);		 
	    window.location.href ="<?php echo base_url() ?>search/index/venue/"+venue+"";

	   });
///////////////////////////////Add Band member Hide Show Divs//////////////////////////   
	  $("#AddNewMember").hide();
	   $("#btn_add_band").click(function() { 
		   $("#AddNewMember").show();
	   });
	   
	   
	   $("#editmember").click(function(e) {

		 e.preventDefault();
          var editid = $(this).data('id');  
		  alert(editid);
		});	   

});

$( "#ajax_seach_btn" ).click(function() {
  //alert( "Handler for .click() called." );
  applyPagination();
  search_result();
});

applyPagination();
 function applyPagination() {
     //alert('ok');
        $("#ajax_pagingsearc a").click(function() {
        var url = $(this).attr("href");
    //alert(url);
   var art_genre_id=$("#art_genre_id").val();
   var start_date=$("#datepicker-01").val();
   var end_date=$("#datepicker-02").val();
   var ven_name=$("#ven_name").val();
   var ven_city=$("#ven_city").val();
		var genre_id = [];
            if(art_genre_id==null){
               // alert(art_genre_id);
                art_genre_id=genre_id;
            };
   
            var alldata= {
	    	     	genre:art_genre_id,
			ajax:"1",
			start_date_to:start_date,
                        end_date_to:end_date,
                        ven_name:ven_name,
                        ven_city:ven_city
	    	     };
          $.ajax({
            type: "POST",
            data: alldata,
            url: url,
           success: function(msg) {
			   //alert(msg);
            
              $("#ajax_results").html(msg);
              applyPagination();
            }
          });
        return false;
        });
      }
      
function search_result(){
 
            var art_genre_id=$("#art_genre_id").val();
			var start_date=$("#datepicker-01").val();
            var end_date=$("#datepicker-02").val();
             var ven_name=$("#ven_name").val();
              var ven_city=$("#ven_city").val();
			var genre_id = [];
            if(art_genre_id==null){
                //alert(art_genre_id);
                art_genre_id=genre_id;
            };
            var alldata= {
	    	    genre:art_genre_id,
		    ajax:"1",
		    start_date_to:start_date,
                    end_date_to:end_date,
                    ven_name:ven_name,
                    ven_city:ven_city
	    	     };
            //alert(art_genre_id);
             var response = '';
            $.ajax({ 
	        type: "POST",              
                url: "<?php echo base_url() ?>search/search_filters",
                data: alldata,
		dataType: "html",
		           success : function(text)
		           {
		               response = text;
                               //alert(response);
		               $("#ajax_results").html(text);
                                applyPagination();
		               
		              
	               }
		  });
        }
		
///////////////////////////////////Artist Search Section///////////////////////////////////////


$( "#ajax_seach_btn_artist" ).click(function() {
  alert( "Handler for .click() called." );
  applyPaginationArtist();
  artist_search_result();
});

applyPaginationArtist();
 function applyPaginationArtist() {
     //alert('ok');
        $("#ajax_pagingsearc a").click(function() {
        var url = $(this).attr("href");
   // alert(url);
   var art_genre_id=$("#art_genre_id").val();
   var artist_name=$("#artist_name").val();
   var ven_city=$("#ven_city").val();
		var genre_id = [];
            if(art_genre_id==null){
                art_genre_id=genre_id;
            };
   
            var alldata= {
	    	     	genre:art_genre_id,
			        ajax:"1",
                    artist_name:artist_name,
                    ven_city:ven_city
	    	     };
          $.ajax({
            type: "POST",
            data: alldata,
            url: url,
           success: function(msg) {
			   //alert(msg);            
              $("#ajax_results").html(msg);
              applyPaginationArtist();
            }
          });
        return false;
        });
      }




function artist_search_result(){
 
            var art_genre_id=$("#art_genre_id").val();
			//alert(art_genre_id);
            var artist_name=$("#artist_name").val();
			//alert(artist_name);
            var ven_city=$("#ven_city").val();
			var genre_id = [];
            if(art_genre_id==null){
                art_genre_id=genre_id;
            };
            var alldata= {
	    	        genre:art_genre_id,
		            ajax:"1",
                    artist_name:artist_name,
                    ven_city:ven_city
	    	     };
            //alert(alldata);
             var response = '';
            $.ajax({ 
	        type: "POST",              
                url: "<?php echo base_url() ?>artist_search/search_filters",
                data: alldata,
		        dataType: "html",
		           success : function(text)
		           {
		               response = text;
                               //alert(response);
		               $("#ajax_results").html(text);
                                applyPaginationArtist();
		               
		              
	               }
		  });
        }	
</script>

</body>
</html>


