<div class="footer">
    <div class="container">
        <div class="row">

            <div class="col-md-6 col-sm-12 col-xs-12 col-md-push-6">
                <ul class="socialbox">
                    <li>
                        <a href="#"><img src="<?php echo base_url();?>assets/images/facebook.png" alt="Facebook" class="center-block img-responsive" /></a>
                    </li>
                    <li>
                        <a href="#"><img src="<?php echo base_url();?>assets/images/twitter.png" alt="Facebook" class="center-block img-responsive" /></a>
                    </li>
                    <li>
                        <a href="#"><img src="<?php echo base_url();?>assets/images/youtube.png" alt="Facebook" class="center-block img-responsive" /></a>
                    </li>
                </ul>
                <ul class="legallinks">
                    <li><a href="#">User Agreement</a><span class="sep">|</span></li>
                    <li><a href="#">Privacy Policy</a><span class="sep">|</span></li>
                    <li><a href="#">Terms &amp; Conditions</a></li>
                </ul>
            </div><!-- col-md-6 -->
            
            <div class="col-md-6 col-sm-12 col-xs-12 col-md-pull-6">
                <p class="copyright">Copyright &copy; 2015 showspoon. All rights reserved.</p>
            </div><!-- col-md-6 -->
            
        </div><!-- row -->
    </div><!-- container -->
</div><!-- footer -->



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url();?>assets/js/jquery.validate.min.js"></script>
<script>
$(document).ready(function() {
$("#registerForm").validate({


		rules: {

		   art_password: {
			minlength: 5,
			required: true
		   },
			art_name: {
			required: true

			},
			art_email: {
				email:true,
				required: true,
			},
			art_location:{
				required: true
			},
			art_genre:{
				required: true
			},
			art_type:{
				required: true
			},



		},
			messages: {
			art_name: "Please enter your name.",
			art_password:{
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long"

			},
			art_email: {

				required: "Please enter email address",
				email:"Please enter a valid email address"
			}


			},
		submitHandler: function(form) {
            $.ajax({
                url:"<?php echo base_url() ?>artist_register/create",
                type:'POST',
                data: $(form).serialize(),
				success: function ()  {
                $("#response").html('Thank! For registration');
				  $('#registerForm').hide();
              }

            });

		return false;
          }

		});
}); // end document.ready
</script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/js/flat-ui-pro.js"></script>
<script src="<?php echo base_url();?>assets/js/star-rating.min.js"></script>
<script src="<?php echo base_url();?>assets/js/custom.js"></script>
<script src="<?php echo base_url();?>assets/vjs.zencdn.net/4.7/video.js"></script>



</body>
</html>


