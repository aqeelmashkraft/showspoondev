<div class="artistRegister side-collapse-container">
    <div class="container">
        <div class="row container-center2">

            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3 panel">
                <h3>GLEMT PASSORD</h3>

                <div class="col-md-10 col-md-offset-1 mtl">


                    <?= form_open() ?>
					<div class="form-group bottom-space">
                        <select class="form-control select select-primary select-block mbl" id="artist_type" name="artist_type" >
                            <option value="" >Velg kontotype</option>
                            <option value="0" selected>Musikere</option>
                            <option value="1">Spillested</option>

                        </select>
                    </div>
                    <input type="text" id="email" class="form-control"
                           id="recipient-name" placeholder="Skriv inn epostadressen din">
                           <div id="ajaxphp-results"></div>
                    <button type="button" class="btn btn-primary btn-artist secondary centerbtn" id="forget_id">Send passord</button>
                    <!--<div class="alert alert-info">
                    
                      <button class="close fui-cross" data-dismiss="alert"></button>
                      <p>Your email or password is not correct.</p>
                    </div><!-- alert -->
                    </form>
                    
                    

                </div><!-- col-md-10 -->


            </div><!-- col-md-6 -->
        </div><!-- row -->
    </div><!-- container -->
</div><!-- artistRegister -->
