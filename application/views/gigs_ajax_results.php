<?php
if (count($search_results) > 0) {
    $count=0;
    foreach ($search_results as $key => $value) {

        $imgpath = "" . base_url() . "uploads/gigs/large/". $value['image_url'];
        if (empty($value['image_url'])) {
            $imgpath="".base_url()."assets/images/no-image.jpg";
        }
        $count++;
        $break = '';
        if ($count && $count % 3 == 0) {
            $break = "<div class='clearfix'></div>";
        }
        $date = date_create($value['Accepting_application_start_date']);
/*$query = $this->db->query("SELECT (SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (".$value['type'].")  ) AS gener
        FROM `artist_gigs` as a
            WHERE a.id=".$value['id']."");
        foreach ($query->result() as $row)
                {*/
?>
        <div class="col-md-4 col-sm-6 col-xs-12 ArtistBox xm-no-gutter">
            <div class="panel panel-default">
                <div class="panel-body">
                    <a href="<?php echo base_url(); ?>gig_details/id/<?php echo $value['id']; ?>">
                        <img src="<?php echo $imgpath; ?>" alt="No Image" class="img-responsive center-block searchListThumbmail" />
                    </a>
                </div>
                <div class="panel-footer">
                    <h5 class="artistName"><?php echo $value['gig_name']; ?></h5>
                    <?php if($value['venues_id']==0){?>
                                             <span class="label label-info">Artist</span>
                                            <?php }?>
                                           <?php if($value['artist_id']==0){?>
                                              <span class="label label-primary">Venue</span>
                                           <?php }?>
		
                    <p class="artistGenre"><?php echo date_format($date, 'l jS F Y \@'); echo $value['gig_time']?></p>
                </div>
            </div>
        </div><!-- col-md-4 -->
        <?php echo $break; ?>
    <?php /*}*/}
} else {
    ?>
    <div class="col-md-4 col-sm-6 col-xs-12 ArtistBox xm-no-gutter">
        <strong>Ingen registrering funnet</strong>
    </div>
<?php } ?>
        <div class="col-md-12 col-sm-12 col-xs-12 no-gutter">
            <div class="col-md-8 col-sm-12 col-xs-12">
                <?php echo $links; ?>   
             
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 mtl">
                <p class="showingSearchCount">Viser <?php echo count($search_results); ?> Resultater av <?php echo $total_records;?></p>
            </div>
        </div>


