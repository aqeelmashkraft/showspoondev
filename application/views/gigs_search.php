<?php $this->load->view('header'); ?>



<div class="WhiteSection topgap v2 side-collapse-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 no-gutter">

                <div class="col-md-12"><h2>Konserter</h2></div>

                <div class="col-md-12 Searchgap">
                    <a class="btn btn-primary roombutton"><strong>Show / Hide </strong></a>
                    <div class="col-md-3 col-sm-4  col-xs-12 no-gutter boderedfilter SearchParameters LeftGap">
                        <div class="col-md-12">
                            <h4>Konserter  Navn</h4>
                            <?php
                            //if($gigs !='all'){
                            //   echo $venue;
                            // }
                            ?>
<!--                            <input type="text" class="form-control"  value="<?php echo $gigs; ?>" id="gig_name" name="gig_name"/>-->

                            <div class="form-group no-gutter">
                                <div class="input-group customSearchBtn">
                                    <input type="text" name="gig_name" id="gig_name" placeholder="S�ke gigs" class="form-control" value="<?php  if($gigs !='all') echo $gigs; ?>">
                                    <span class="input-group-btn">
                                        <button class="btn" type="submit" id="gigsearchname"><span class="fui-search"></span></button>
                                    </span>
                                </div>                
                            </div>

                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <h4 class="heading-top">by</h4>
                            <?php echo form_dropdown('gig_city', $cities, '', 'class="form-control select select-primary select-block mbl" id="gig_city" placeholder="City"'); ?>
                        </div>
                        <div class="clearfix"></div>
                        <?php /*   <div class="col-md-12">
                          <h4 class="heading-top">Gig Type</h4>
                          <?php echo form_dropdown('art_type_show[]', $artist_gig_type, '', 'class="form-control multiselect mbl" multiple="multiple" id="gig_gig_type" placeholder="Types of Shows"'); ?>

                          </div> */ ?>
                        <div class="clearfix"></div>


                        <div class="col-md-12">
                            <h4 class="box-heading">Konserter  Dato</h4>
                        </div>
                        <div class="col-md-12 no-gutter">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Fra" id="datepicker-01" name="date_start"  />
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control"  placeholder="Til" id="datepicker-02" name="date_end" />
                                    </div>
                                </div>
                            </div>


                        </div><!-- col-md--12 -->    

                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-artist secondary smallbtn" id="ajax_seach_btn_gigs" >S�ke</button>
                        </div>
                        <div class="clearfix"></div>
                    </div><!-- col-md-3 -->


                    <div class="col-md-9 col-sm-8 col-xs-12 no-gutter" id="ajax_results">
                        <?php //print_r($search_results);?>
                        <?php
                        if (count($search_results) > 0) {
                            $count=0;
                            foreach ($search_results as $key => $value) {

                                $imgpath = "" . base_url() . "uploads/gigs/large/" . $value['image_url'];
                                if (empty($value['image_url'])) {
                                    $imgpath = "" . base_url() . "assets/images/no-image.jpg";
                                }
                                $count++;
                                $break = '';
                                if ($count && $count % 3 == 0) {
                                    $break = "<div class='clearfix'></div>";
                                }
                                $date = date_create($value['Accepting_application_start_date']);
                                /* $query = $this->db->query("SELECT (SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (" . $value['type'] . ")  ) AS gener
                                  FROM `artist_gigs` as a
                                  WHERE a.id=" . $value['id'] . "");
                                  foreach ($query->result() as $row) { */
                                ?>
                                <div class="col-md-4 col-sm-6 col-xs-12 ArtistBox xm-no-gutter">
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <a href="<?php echo base_url(); ?>gig_details/id/<?php echo $value['id']; ?>">
                                                <img src="<?php echo $imgpath; ?>" alt="No Image" class="img-responsive center-block searchListThumbmail" />
                                            </a>
                                        </div>
                                        <div class="panel-footer">
                                            <h5 class="artistName"><?php echo $value['gig_name']; ?></h5>
                                            <?php if ($value['venues_id'] == 0) { ?>
                                                <span class="label label-info">Artist</span>
                                            <?php } ?>
                                            <?php if ($value['artist_id'] == 0) { ?>
                                                <span class="label label-primary">Venue</span>
                                            <?php } ?>


                                            <p class="artistGenre"><?php echo date_format($date, 'l jS F Y \@');
                                    echo $value['gig_time']
                                            ?></p>
                                        </div>
                                    </div>
                                </div><!-- col-md-4 -->
                                <?php echo $break;?>
                                <?php
                                //}
                            }
                        } else {
                            ?>
                            <div class="col-md-4 col-sm-6 col-xs-12 ArtistBox xm-no-gutter">
                                <strong>Ingen registrering funnet</strong>
                            </div>
<?php } ?>

                        <div class="col-md-12 col-sm-12 col-xs-12 no-gutter">
                            <div class="col-md-8 col-sm-12 col-xs-12">
								<?php echo $links; ?>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12 mtl">
                                <p class="showingSearchCount">Showing <?php echo count($search_results);?> Results out of <?php echo $total_records;?></p>
                            </div>
                        </div>


                    </div><!-- col-md-9 -->

                </div><!-- col-md-12 -->


            </div><!-- col-md-12 -->
        </div><!-- row -->
    </div><!-- container -->
</div>

<?php $this->load->view('footer'); ?>