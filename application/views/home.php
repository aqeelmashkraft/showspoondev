﻿<?php $this->load->view('header'); ?>
<div class="HeroSection side-collapse-container"  data-speed="6" data-type="background">
    <div class="container">


		<div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h1>
                    <span class="small">På utkikk etter konserter eller musikere?</span>
                    Da er det bare å registrere seg!
                   
                </h1>
          
                <div class="well col-md-4 col-sm-6 col-xs-12 col-md-offset-2">
                    <div class="col-md-12 col-sm-12 col-xs-12 transparentbox">
                        <h3>Musikere</h3>
                        <p class="ctext">Musikere kan på en enkel måte finne spillesteder og konserter  </p>
                          <?php if (isset($_SESSION['user_id'] )) { ?>
                        <a href="<?php echo base_url() ?>artist_dashboard" class="btn btn-primary btn-artist centerbtn">My Dashboard</a>
                          <?php }else{ ?>
                         <a href="<?php echo base_url() ?>signup/0/account" class="btn btn-primary btn-artist centerbtn">Registrer en musiker/band</a>
                          <?php }?>
                    </div><!-- col-md-4 -->
                </div>
                
                <div class="well col-md-4 col-sm-6 col-xs-12">
                    <div class="col-md-12 col-sm-12 col-xs-12 transparentbox">
                        <h3>Spillesteder</h3>
                        <p class="ctext">Spillesteder kan booke etter aktuelle band og musikere</p>
                         <?php if (isset($_SESSION['venue_id'] )) { ?>
                        <a href="<?php echo base_url() ?>venue_dashboard" class="btn btn-primary btn-artist centerbtn">My Dashboard</a>
                          <?php }else{ ?>
                          <a href="<?php echo base_url() ?>signup/1/account" class="btn btn-primary btn-artist centerbtn">Registrer et spillested</a>
                            <?php }?>
                    </div><!-- col-md-4 -->
                </div>
               
            </div><!-- col-md-12 -->
        </div><!-- row -->
    </div><!-- container -->
</div><!-- HeroSection -->

<div class="WhiteSection topgap side-collapse-container">
    <div class="container">
        <div class="row">
           
            <div class="col-md-5 col-sm-12 col-xs-12 col-md-push-7">
                <h2 class="getstarted">Er du en artist og ser etter flere spillejobber?</h2>
                <p class="subheading">Om dere er et band eller en soloartist og vil vise frem musikken til flere publikum, hjelper Showspoon deg med å finne den riktige spillejobben.</p>
                  <?php if (isset($_SESSION['user_id'] )) { ?>
                        <a href="<?php echo base_url() ?>artist_dashboard" class="btn btn-primary btn-artist secondary">My Dashboard</a>
                          <?php }else{ ?>
                 <a href="<?php echo base_url() ?>signup/0/account" class="btn btn-primary btn-artist secondary">Kom i gang!</a>
                          <?php }?>
            </div><!-- col-md-7 --> 
            
            <div class="col-md-7 col-sm-12 col-xs-12 col-md-pull-5">
                <img src="<?php echo base_url();?>assets/images/home/sexophone.png" alt="Sexophone" class="center-block img-responsive sexophone" />
            </div><!-- col-md-5 -->

        </div><!-- row -->
    </div><!-- container -->
</div><!-- WhiteSection -->


<div class="BlackBar side-collapse-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <img src="<?php echo base_url();?>assets/images/home/quote.png" alt="quote" class="center-block img-responsive" />
                <h5 class="quote">The bands themselves will presumably also ensure that the musical style of their<br class="sm-hide-br" />act matches who ever comes on before or after them. That’s definitely a win for Showspoon.</h5>
                <span class="quotename">Tim Vannderberg Smith</span>
            </div><!-- col-md-12 -->
        </div><!-- row -->
    </div><!-- container -->
</div><!-- BlackBar -->


<div class="HuntTalent side-collapse-container" data-speed="6" data-type="background">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <h2>Vil du øke profileringen av spillestedet?</h2>
                 <?php if (isset($_SESSION['venue_id'] )) { ?>
                        <a href="<?php echo base_url() ?>venue_dashboard" class="btn btn-primary btn-artist secondary">My Dashboard</a>
                          <?php }else{ ?>
                <a href="<?php echo base_url() ?>signup/1/account" class="btn btn-primary btn-artist secondary">Kom i gang!</a>
                          <?php }?>
            </div><!-- col-md-6 -->
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="panel panel-body transparentbox">
                    <p class="vtxt">La spillestedet bli eksponert til flere musikere og bidra til økt inntjening..</p>
                </div>
            </div><!-- col-md-6 -->
        </div><!-- row -->
    </div><!-- container -->
</div><!-- HuntTalent -->
<?php $this->load->view('footer'); ?>

