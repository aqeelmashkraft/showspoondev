<?php $this->load->view('header'); ?>



<div class="container-fluid">
	<div class="row">
		<div id="map-canvas"></div> 
	</div><!-- row -->
</div><!-- container -->

<div class="GreyDashboard  side-collapse-container artistDashboard-grey redurecontactheight">
	<div class="container">
		<div class="row">
			
			<div class="col-md-12 col-sm-12 col-xs-12 ArtistDashboarItems ">
                
				<h3 class="nmt">Kom i kontakt med oss</h3>
				<div class="col-md-6 col-sm-12 col-xs-12 ">
			      <form method="post"  action="#" id="contactus" >	                                                                
					<div class="form-group">
					<label>Navn</label>
                                        <input type="text" class="form-control" name="fullname" id="fullname" placeholder="Your Name" value="<?php echo set_value('email'); ?>"  required>
					</div><!-- form-group -->
							
					<div class="form-group">
					<label>E-post-adresse</label>
                                        <input type="email" placeholder="Fyll inn din e-postadresses" name="email" id="email" value="<?php echo set_value('email'); ?>" class="form-control" required>
				   </div>
				   <div class="form-group">
					<label>Emne</label>
                                        <input type="text" placeholder="Subject" name="subject" id="subject" value="<?php echo set_value('subject'); ?>" class="form-control" required>
				   </div>
							 <div class="form-group">
					<label>Beskjed</label>
                                        <textarea placeholder="Fyll inn din melding" id="message" name="message" class="form-control"  ><?php echo set_value('message'); ?></textarea>
				   </div>
				  
                                    <div class="form-group">                     <!-- form-group -->
				<input type="submit" class="btn btn-primary btn-artist secondary lg" id="" value="Sende melding">
                                    </div>		<!--<a class="btn btn-primary btn-artist secondary lg" href="#">Update</a>-->
					</form>	
                                    <div class="alert alert-success" id="successmsg" style="display: none;">
                                        <a class="close" data-dismiss="alert">×</a>
                                        Takk!
                                        Din melding er sendt. Vi vil kontakte deg veldig snart!
                                    </div>
                                   
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
                     <img src="<?php echo base_url();?>assets/images/contact-us.png" alt="Contact Us" class="center-block img-responsive" />
                </div><!-- col-md-6 -->
		
			</div><!-- col-md-12 -->
			
		</div><!-- row -->
	</div><!-- container -->
</div><!-- GreyDashboard -->

<?php $this->load->view('footer'); ?>


