<?php $this->load->view('header'); ?>
<div class="faq-page side-collapse-container">
</div><!-- faq-page -->
<div class="clearfix"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel-group alignment-panel" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading faq-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Where can I use Showspoon?
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        Showspoon works in any crowded location where there is a high demand for taxis and a critical mass of users, in particular, at airports, convention centers, transportation hubs, and large event venues.
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading faq-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Will I go out of my way?
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
      Showspoon's Server ensures that you will never go more than 20% out of your way. 
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading faq-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
         How much does Showspoon cost?
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
       Showspoon estimates your price in advance based on current taxi fare pricing. Whenever another passenger joins your ride, your price drops. By sharing rides, Bandwagon users can save approximately 40% on the cost of their ride.
      </div>
    </div>
  </div>
 <div class="panel panel-default">
    <div class="panel-heading faq-heading" role="tab" id="headingFour">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
        Can I book in advance?
        </a>
      </h4>
    </div>
    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
      <div class="panel-body">
      We’re working on advance booking capability right now. Stay tuned!
      </div>
    </div>
  </div>
                    <div class="panel panel-default">
    <div class="panel-heading faq-heading" role="tab" id="headingFour">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
       Why has no one joined my Gig?
        </a>
      </h4>
    </div>
    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
      <div class="panel-body">
      Timing and route compatibility play a big role in whether or not you get matched. Sometimes there just isn’t another rideshare request matching your specific itinerary!  Submit a request again, new users may have joined the request pool since your last request.
      </div>
    </div>
  </div>
                    <div class="panel panel-default">
    <div class="panel-heading faq-heading" role="tab" id="headingFour">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
       Why has no one joined my Gig?
        </a>
      </h4>
    </div>
    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
      <div class="panel-body">
      Timing and route compatibility play a big role in whether or not you get matched. Sometimes there just isn’t another rideshare request matching your specific itinerary!  Submit a request again, new users may have joined the request pool since your last request.
      </div>
    </div>
  </div>
</div>
            </div><!-- col-md-12 -->
        </div><!-- row -->
    </div><!-- container -->
<?php $this->load->view('footer'); ?>

