<div class="artistRegister side-collapse-container">
    <div class="container">
        <div class="row container-center">
		
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3 panel">
                <h3>Logg inn p� <strong>Showspoon</strong></h3>
                
                <div class="col-md-10 col-md-offset-1 mtl">
                    
                    <?php if (validation_errors()) : ?>
                       
                            <div class="alert alert-danger" role="alert">
                                <?= validation_errors() ?>
                            </div>
                      
                    <?php endif; ?>
                    <?php if (isset($error)) : ?>
                       
                            <div class="alert alert-danger" role="alert">
                              <a class="close" data-dismiss="alert">×</a>  <?= $error ?>
                            </div>
                       
                    <?php endif; ?>
                    
                    <?php
                    if (!isset($error)) :
                        if($this->session->flashdata('flash_access_gigs')){
                              if($this->session->flashdata('flash_access_gigs') == 'access_gigs')
                              {
                                echo '<div class="alert alert-info">';
                                echo '<a class="close" data-dismiss="alert">�</a>';
                                echo 'Du m� logge inn / registrere deg for � se denne linken';
                                echo '</div>';       
                              }
							  if($this->session->flashdata('flash_access_gigs') == 'access_venues_as_artist')
                              {
                                echo '<div class="alert alert-info">';
                                echo '<a class="close" data-dismiss="alert">�</a>';
                                echo 'Du m� logge inn som artist / registrer deg som Musikere for � se denne linken';
                                echo '</div>';       
                              }
                        }
                        endif; 
                    ?>
                    <?= form_open() ?>
                    <div class="form-group bottom-space">
                        <select class="form-control select select-primary select-block mbl" id="artist_type" name="artist_type" >
                            <option value="" >Velg kontotype</option>
                            <option value="0" selected>Musikere</option>
                            <option value="1">spillested</option>

                        </select>
                    </div>
                    <input type="text" placeholder="E-post-adresse" id="username" name="username" class="form-control mbl" />
                    
                    <input type="password" placeholder="passord" id="password" name="password"  class="form-control mbl" />
					
					<input type="submit" class="btn btn-primary btn-artist secondary centerbtn" value="Logg inn">
					<!--<div class="alert alert-info">
					
					  <button class="close fui-cross" data-dismiss="alert"></button>
					  <p>Your email or password is not correct.</p>
					</div><!-- alert -->
					</form>
                                  <div class="col-md-12 col-sm-12 col-xs-12 no-gutter">

                                    <a href="<?php echo site_url("forgot_password"); ?>"  class="forgotpassword">Glemt passord</a>

                                </div>
                 

                </div><!-- col-md-10 -->
                
               
            </div><!-- col-md-6 -->
        </div><!-- row -->
    </div><!-- container -->
</div><!-- artistRegister -->
