<?php $this->load->view('header'); ?>

<?php $this->load->view('mydashboard_links'); ?>

<div class="GreyDashboard  side-collapse-container">
	<div class="container">
		<div class="row">		
			
			<div class="col-md-12 col-sm-12 col-xs-12 ArtistDashboarItems">
				
				<div class="panel">
					<div class="panel-heading">
						<h5>Tilbakemeldinger fra Musikere</h5>						
					</div><!-- panel-heading -->
					
					<div class="panel-body">
                                             
						<div class="table-responsive">
                                                   <?php 
                                                         if (count($feedback) > 0) {
                                                    ?>
							<table class="table table-hover table-condensed">
                                                            <?php //print_r($mygig);exit();?>
								<thead>
									<tr>
										<td>Av Musikere</td>
										<td>kommentar</td>
										<td>Dato</td>
										
										
									</tr>
								</thead>
								<tbody>
                                                                    
                                                                    <?php
								foreach($feedback as $key => $value)
								{
							  
							   ?>
									<tr>
                                                                            <td><a href="<?php echo base_url()?>artist_detail/index/id/<?php echo $value['art_id']; ?>"><?php echo $value['art_name']; ?></a></td>
										<td><?php echo $value['comments'] ?></td>
										<td><?php echo $value['created_on'];?></td>
										
										
									</tr>
								<?php }?>
									
								</tbody>
							</table>
                                                    <?php }else{?>
                                                    <p class="ptagpadding"><strong>Du trenger ikke ha noen tilbakemeldinger</strong></p>
                                                         <?php }?>
						</div><!-- table-responsive -->
					</div><!-- panel-body -->
					
				</div><!-- panel -->
				
			</div><!-- col-md-12 -->
			
		</div><!-- row -->
	</div><!-- container -->
</div><!-- GreyDashboard -->

<?php $this->load->view('footer'); ?>