<?php $this->load->view('header'); ?>

<?php $this->load->view('mydashboard_links'); ?>

<div class="GreyDashboard  side-collapse-container">
	<div class="container">
		<div class="row">		
			
			<div class="col-md-12 col-sm-12 col-xs-12 ArtistDashboarItems">
				
				<div class="panel">
					<div class="panel-heading">
						<h5>Mine Gigs</h5>
						<a class="btn btn-primary btn-artist secondary" href="<?php echo base_url()?>venue_dashboard/add_gig">Legg Gig<i class="fa fa-plus"></i></a>
					</div><!-- panel-heading -->
                                       
					<div class="panel-body">
                                             <?php
						  if($this->session->flashdata('flash_message')){
							if($this->session->flashdata('flash_message') == 'delete')
							{
							  echo '<div class="alert alert-danger">';
							  echo '<a class="close" data-dismiss="alert">×</a>';
							  echo 'Gig slettet.';
							  echo '</div>';       
							}
						  }
						?>
						<div class="table-responsive">
                                                   <?php 
                                                         if (count($mygig) > 0) {
                                                    ?>
							<table class="table table-hover table-condensed">
                                                            <?php //print_r($mygig);exit();?>
								<thead>
									<tr>
										<td>Navn</td>
										<td>Sjanger</td>
										<td>Dato</td>
										<td>By</td>
										<td>Handling</td>
									</tr>
								</thead>
								<tbody>
                                                                    
                                                                    <?php
								foreach($mygig as $key => $value)
								{
							   $query = $this->db->query("SELECT (SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (".$value['type'].")  ) AS gener
                                                                FROM `artist_gigs` as a
                                                                    WHERE a.id=".$value['id']."");
								foreach ($query->result() as $row)
									{
							   ?>
									<tr>
                                                                            <td><a href="<?php echo base_url()?>gig_details/id/<?php echo $value['id']; ?>"><?php echo $value['gig_name']; ?></a></td>
										<td><?php echo $row->gener; ?></td>
										<td><?php echo $value['created_on'];?></td>
										<td><?php echo $value['CityName'];?></td>
										<td>
											<a href="<?php echo base_url()?>venue_dashboard/edit_gig/<?php echo $value['id']; ?>" class="ActionControls"><i class="fa fa-pencil"></i></a>
											<a href="<?php echo base_url()?>venue_dashboard/delete_gig/<?php echo $value['id']; ?>"><i class="fa fa-trash"></i></a>
											<a href="#" onClick="document.getElementById('idExp').value=<?php echo $value['id'];?>"  data-toggle="modal" data-target="#exampleModal3"><i class="fa fa-plus">Invite</i></a>
										</td>
									</tr>
                                                                        
                                                                        
                                                                        
                                                                    
								<?php }}?>
									<!--<tr>
										<td>Abitudini Live</td>
										<td>Death Metal</td>
										<td>30 Sep 2015</td>
										<td>Open Mic</span></td>
										<td>
											<a href="#" class="ActionControls"><i class="fa fa-pencil"></i></a>
											<a href="#"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
									<tr>
										<td>Abitudini Live</td>
										<td>Death Metal</td>
										<td>30 Sep 2015</td>
										<td>Open Mic</span></td>
										<td>
											<a href="#" class="ActionControls"><i class="fa fa-pencil"></i></a>
											<a href="#"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
									<tr>
										<td>Abitudini Live</td>
										<td>Death Metal</td>
										<td>30 Sep 2015</td>
										<td>Open Mic</span></td>
										<td>
											<a href="#" class="ActionControls"><i class="fa fa-pencil"></i></a>
											<a href="#"><i class="fa fa-trash"></i></a>
										</td>
									</tr>-->
								</tbody>
							</table>
                                                    <?php }else{?>
                                                    <p class="ptagpadding"><strong>Du har ikke noen gig</strong></p>
                                                         <?php }?>
						</div><!-- table-responsive -->
					</div><!-- panel-body -->
					
				</div><!-- panel -->
				
				    <!-------------------------------------Send invite------------------------------------------------->
					 <form method="post" action="" id="invite_to_artist_by_venue">
  <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="exampleModalLabel">Send invitation</h4>
                                            </div>
                                          
                                            <div class="modal-body">
                                                <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Artists:</label>
                                                     
                                                    <?php echo form_dropdown('art_id[]', $art_dropdown, '', 'class="form-control multiselect " multiple="multiple" id="art_id" placeholder="Artists"'); ?>
                               
                                                       </div>
                                                    
                                                    <div class="form-group">
                                                        <label for="message-text" class="control-label">Message:</label>
                                                        <textarea name="venue_message" class="form-control" id="venue_message"></textarea>
                                                       
                                                        <input name="ven_id" type="hidden" value="<?php echo $_SESSION['venue_id'];?>" class="form-control" id="ven_id">
                                                       <input type="hidden" name="gig_id" id="idExp">
                                                    </div>
                                               
                                                <div id="successmsg2"></div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <!--<button type="submit" class="btn btn-primary">Send message</button>-->
                                                <input type="submit" name="submit"class="btn btn-primary" value="Invite" />
                                            </div>
                                        
                                        </div>
                                    </div>
                                </div>
				</form>
				
			</div><!-- col-md-12 -->
			
		</div><!-- row -->
	</div><!-- container -->
</div><!-- GreyDashboard -->

<?php $this->load->view('footer'); ?>