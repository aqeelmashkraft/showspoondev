<div class="artistRegister side-collapse-container">
    <div class="container">
        <div class="row">

            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3 panel">
                <h3>Tilbakestill ditt passord</h3>

                <div class="col-md-10 col-md-offset-1 mtl">


                   
                    <?php 
          $id= $this->uri->segment(2);
		  $artist_type= $this->uri->segment(4);
          $attributes = array("class" => "form-horizontal", "id" => "loginform", "name" => "loginform");
          echo form_open("reset_password/$id/type/$artist_type", $attributes);?>
                <div class="form-group">
                   <input class="form-control" id="txt_email" name="passwrod" placeholder="Skriv inn nytt passord" 
         type="password" value="<?php echo set_value('passwrod'); ?>" />
          <span class="<?php echo ((form_error('passwrod') !=NULL))? 'text-warning': 'text-danger' ?>"><?php echo form_error('passwrod'); ?></span>
                </div>
              
                <div class="form-group">
                       
                    <input class="form-control" id="txt_password" name="new_password" placeholder="Skriv inn passord p� nytt" type="password" value="<?php echo set_value('new_password'); ?>" /> <span class="text-danger"><?php echo form_error('new_password'); ?></span>
                   
                </div>
              
                <div class="col-xs-12 col-md-6 col-md-offset-3">
                
                	<div class="col-md-6 col-sm-12 col-xs-12 no-gutter">
                	<input id="btn_login" name="btn_login" type="submit" class="btn btn-primary btn-artist secondary centerbtn" value="Bytt passord" class="pri"/>
                	</div>
                    
                </div>
                
            <?php echo form_close(); ?>
  



                    
                    

                </div><!-- col-md-10 -->


            </div><!-- col-md-6 -->
        </div><!-- row -->
    </div><!-- container -->
</div><!-- artistRegister -->
