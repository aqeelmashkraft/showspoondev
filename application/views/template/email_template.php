<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>[SUBJECT]</title>
        <style type="text/css">
            body {
                background-color: #EEEEEE;
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                margin: 0 !important;
                width: 100% !important;
                -webkit-text-size-adjust: 100% !important;
                -ms-text-size-adjust: 100% !important;
                -webkit-font-smoothing: antialiased !important;
            }

            .tableContent img {
                border: 0 !important;
                display: block !important;
                outline: none !important;
            }

            a {
                color: #382F2E;
            }

            p,
            h1 {
                color: #382F2E;
                margin: 0;
            }

            p {
                text-align: left;
                color: #555555;
                font-size: 12px;
                font-weight: normal;
                line-height: 19px;
            }

            a:link{color: #337ab7; text-decoration: none;}
            a:visited{color: #337ab7; text-decoration: none;}
            a:hover{color: #337ab7; text-decoration: underline;}

            a.link2 {
                font-size: 16px;
                text-decoration: none;
                color: #ffffff;
            }

            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                color: #333;
            }

            h2 {
                text-align: left;
                font-size: 18px;
                font-weight: normal;
            }

            div,
            p,
            ul,
            h1 {
                margin: 0;
            }

            .bgBody {
                /*			background: #eeeeee;*/
            }

            .bgItem {
                background: #ffffff none repeat scroll 0 0;
                border: 1px solid #dadada;
                padding-bottom: 10px;
                padding-top: 0;
                border-radius: 5px;
                overflow: hidden;
            }

            .bgItem2 {
                background: none;
                border: 0;
                padding-bottom: 40px;
                padding-top: 40px;
                border-radius: 5px;
            }
        </style>
        <script type="colorScheme" class="swatch active">
            { "name":"Default", "bgBody":"ffffff", "link":"382F2E", "color":"555555", "bgItem":"ffffff", "title":"222222" }
        </script>
    </head>

    <body paddingwidth="0" paddingheight="0" style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;" offset="0" toppadding="0" leftpadding="0">
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableContent bgBody" align="center" style='font-family: Tahoma, Arial, serif;'>

            <tr>
                <td>
                    <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" class='bgItem2' style="padding-top:20px; padding-bottom:20px;">
                        <tr>
                            <td class='movableContentContainer' valign='top'>
                                <div lass='movableContent'>
                                    <table width="600" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr>
                                            <td valign='top' align='left'>
                                                <div class="contentEditableContainer contentImageEditable">
                                                    <div class="contentEditable">
                                                        <img src="http://178.62.77.181/roomexplorer/assets/roomexplorer_assets/images/logo.png" width='236' height='52' alt='' data-default="placeholder" data-max-width="560">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>

                    <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" class='bgItem'>

                        <tr>
                            <td width='600' style="background:#ff5501; font-size:13px; font-weight:bold; text-transform:uppercase; color:#FFF; padding:12px 20px; border-radius:3px;">Welcome to RoomExplorer</td>
                        </tr>

                        <tr>
                            <td width='600'>
                                <table width="600" border="0" cellspacing="0" cellpadding="0" align="center">								
                                    <tr>
                                        <td class='movableContentContainer' valign='top'>

                                            <div class='movableContent'>
                                                <table width="560" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td height='15'></td>
                                                    </tr>
                                                    <tr>
                                                        <td align='left'>
                                                            <div class="contentEditableContainer contentTextEditable">
                                                                <div class="contentEditable" align='center'>
                                                                    <h2>Dear <?php echo $reg_name; ?>,</h2>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td height='15'> </td>
                                                    </tr>

                                                    <tr>
                                                        <td align='left'>
                                                            <div class="contentEditableContainer contentTextEditable">
                                                                <div class="contentEditable" align='center'>
                                                                    <p style='text-align:left;color:#555555;font-size:14px;font-weight:normal;line-height:19px;'>
                                                                        <?php if ($reg_name == 'Roomexplorer Admin') { ?>
                                                                            <?php echo $msg_unsuitable; ?>
                                                                        <?php } else { ?>
                                                                            Thank you for registering with <a href="http://roomexplorer.com">roomexplorer.com</a>
                                                                            <br />  <br />
                                                                            Your account id is : <?php echo $account_email; ?>
                                                                            <br />  <br />

                                                                            <br />If you require any further assistance please visit our help centre.<br />
                                                                            Thanks again for joining RoomExplorer and good luck with your searches.<br />
                                                                        <?php } ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height='25'></td>
                                                    </tr>
                                                </table>
                                            </div>

                                            <div lass='movableContent'>
                                                <table width="560" border="0" cellspacing="0" cellpadding="0" align="center">
                                                    <tr>
                                                        <td height='40'></td>
                                                    </tr>
                                                    <tr>
                                                        <td style='border-bottom:1px solid #efefef;'></td>
                                                    </tr>

                                                    <tr>
                                                        <td height='25'></td>
                                                    </tr>

                                                    <tr>
                                                        <td>
                                                            <table width="560" border="0" cellspacing="0" cellpadding="0" align="center">
                                                                <tr>
                                                                    <td valign='top' align='left' width='370'>
                                                                        <div class="contentEditableContainer contentTextEditable">
                                                                            <div class="contentEditable" align='center'>
                                                                                <p style='text-align:left; color:#999999; font-size:12px; font-weight:normal; line-height:20px;'>
                                                                                    Kind Regards,<br />
                                                                                    The RoomExplorer Team<br />
                                                                                    <a href="mailto:support@roomexplorer.com">support@roomexplorer.com</a><br />

                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </td>


                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </td>
                                    </tr>
                                    <!-- =============================== footer ====================================== -->

                                </table>
                            </td>
                        </tr>
                    </table>

                    <table width="600" border="0" cellspacing="0" cellpadding="0" align="center" class='bgItem2' style="padding-bottom:10; padding-top:10px;">
                        <tr>
                            <td class='movableContentContainer' valign='top'>
                                <div lass='movableContent'>
                                    <table width="600" border="0" cellspacing="0" cellpadding="0" align="center">
                                        <tr>
                                            <td valign='top' align='left'>
                                                <div class="contentEditableContainer contentImageEditable">
                                                    <div class="contentEditable">
                                                        <p style="line-height:13px; margin-top:0; color: #999; font-size:11px;">We take privacy very 
                                                            seriously!<br> Please note that your email address is safe with us. 
                                                                We’ll never sell or disclose your email to third parties.</p>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>

        </table>

    </body>

</html>