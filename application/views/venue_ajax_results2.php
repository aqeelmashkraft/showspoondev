<?php
if (count($search_results) > 0) {
    foreach ($search_results as $key => $value) {
        ?>
        <div class="col-md-4 col-sm-6 col-xs-12 ArtistBox xm-no-gutter">
            <div class="panel panel-default">
                <div class="panel-body">
                    <a href="<?php echo base_url(); ?>venue_detail/index/id/<?php echo $value['id']; ?>">
                        <img src="<?php echo base_url(); ?>assets/images/sample-images/venue1.jpg" alt="No Image" class="img-responsive center-block" />
                    </a>
                </div>
                <div class="panel-footer">
                    <h5 class="artistName"><?php echo $value['name']; ?></h5>
                    <p class="artistGenre">16 Reviews</p>
                </div>
            </div>
        </div><!-- col-md-4 -->
    <?php }
} else {
    ?>
    <div class="col-md-4 col-sm-6 col-xs-12 ArtistBox xm-no-gutter">
        <strong>No Record Found</strong>
    </div>
<?php } ?>
        <div class="col-md-12 col-sm-12 col-xs-12 no-gutter">
            <div class="col-md-6 col-sm-7 col-xs-12">
                <?php echo $links; ?>   
               <!-- <div class="pagination" id="ajax_pagingsearc">
                    <ul>
                        
                    </ul>
                </div>-->

            </div>
        </div>


