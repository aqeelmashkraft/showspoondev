<?php $this->load->view('header'); ?>

<?php $this->load->view('mydashboard_links'); ?>

<div class="GreyDashboard  side-collapse-container">
	<div class="container">
		<div class="row">
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="media">
					<div class="media-left">
						<img src="<?php echo base_url();?>assets/images/artish-avatar.jpg" alt="The headbangers" class="center-block img-responsive" />
					</div><!-- media-left -->
					<div class="media-body">
                                            <?php //print_r($venue) ;?>
						<h2 class="media-heading"><?php echo $venue[0]['name']?> <a href="<?php echo base_url()?>venue_dashboard/venue_edit_profile"><i class="fa fa-pencil"></i></a></h2>
						<p class="tagband">Spillested</p>
						<p class="artistgenre"><?php echo $venue[0]['gener_name'];?></p>
					</div><!-- media-body -->
				</div><!-- media -->
			</div><!-- col-md-12 -->
			
			<div class="col-md-12 col-sm-12 col-xs-12 ArtistDashboarItems mtl">
				
				<div class="panel">
					<div class="panel-heading">
						<h5>Musikere</h5>
						<div class="col-md-2 form-group pull-right mbn">
<!--                           <input type="text" name="artist_name" id="artistid" placeholder="Search Artist" class="form-control smallSearchDash">-->
							
							<div class="input-group customSearchBtn">
								<input type="text" name="artist_name" id="artistid" placeholder="S�ke Musikere" class="form-control">
								<span class="input-group-btn">
									<button class="btn" type="submit" id="seach_btn_artist"><span class="fui-search"></span></button>
								</span>
							</div>
                                                   
                          </div>
						<!--<a class="btn btn-primary btn-artist secondary" href="#">Search Artist <i class="fa fa-search"></i></a>-->
					</div><!-- panel-heading -->
					
					<div class="panel-body">
                                                 <?php 
                                                         if (count($artist) > 0) {
                                                    ?>
						<div class="table-responsive">
                                                    <?php //print_r($artist);?>
							<table class="table table-hover table-condensed">
								<thead>
									<tr>
										<td>Navn</td>
										<td>Sjanger</td>
										<td>Gig Type</td>
										<td>Type</td>
										<td>By</td>
									</tr>
								</thead>
								<tbody>
									<?php
                                                                        foreach($artist as $key => $value)
                                                                        {
                                                                            
                                                                          $query = $this->db->query("SELECT a.id,b.name AS band_type_name,(SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa"
                                                                                  . " Where aa.id IN (".$value['genre_id'].")  ) AS tt , (SELECT  GROUP_CONCAT(ag.name) From artist_gig_type As ag Where ag.id IN (".$value['gig_type_id'].")) as gigs FROM `artist` AS a"
                                                                                  . " LEFT OUTER JOIN artist_band_type AS b ON b.id=a.band_type_id  WHERE a.id=".$value['id']."");
                                                                   foreach ($query->result() as $row)
                                                                                {

                                                                          echo '<tr>';
                                                                          echo '<td><a href="'.base_url().'artist_detail/index/id/'.$value['id'].'">'.$value['name'].'</a></td>';
                                                                          echo '<td>'.$row->tt.'</td>';
                                                                          echo '<td>'.$row->gigs.'</td>';
                                                                          echo '<td>'.$row->band_type_name.'</td>';
                                                                          echo '<td>'.$value['CityName'].'</td>';

                                                                          echo '</tr>';
                                                                         } 
                                                                        }
                                                                        ?>
									<!--<tr>
										<td>Rock Island</td>
										<td>Rock / Hip Hop</td>
										<td>20</td>
										<td>One time gig</td>
										<td>City Center, Oslo</td>
									</tr>
									<tr>
										<td>Jimmy Breaks All</td>
										<td>Rock / Hip Hop</td>
										<td>28</td>
										<td>Open Mic / Opening Act</td>
										<td>City Center, Oslo</td>
									</tr>-->
								</tbody>
							</table>
						</div><!-- table-responsive -->
                                                 <?php }else{?>
                                                          <p class="ptagpadding"><strong>Ingen Musikere er tilgjengelig i din by</strong></p>
                                                         <?php }?>
					</div><!-- panel-body -->
					
				</div><!-- panel -->
				
				
				<div class="panel">
					<div class="panel-heading">
						<h5>Mine Gigs</h5>
						<a class="btn btn-primary btn-artist secondary" href="<?php echo base_url()?>venue_dashboard/add_gig">Legge Gig <i class="fa fa-plus"></i></a>
					</div><!-- panel-heading -->
					
					<div class="panel-body">
                                             <?php
						  if($this->session->flashdata('flash_message')){
							if($this->session->flashdata('flash_message') == 'delete')
							{
							  echo '<div class="alert alert-danger">';
							  echo '<a class="close" data-dismiss="alert">×</a>';
							  echo 'Gig deleted successfully.';
							  echo '</div>';       
							}
						  }
						?>
						<div class="table-responsive">
                                                   <?php 
                                                         if (count($mygig) > 0) {
                                                    ?>
							<table class="table table-hover table-condensed">
                                                            <?php //print_r($mygig);exit();?>
								<thead>
									<tr>
										<td>Navn</td>
										<td>Sjanger</td>
										<td>Dato</td>
										<td>By</td>
										<td>Handling</td>
									</tr>
								</thead>
								<tbody>
                                                                    
                                                                    <?php
								foreach($mygig as $key => $value)
								{
							   $query = $this->db->query("SELECT (SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (".$value['type'].")  ) AS gener
                                                                FROM `artist_gigs` as a
                                                                    WHERE a.id=".$value['id']."");
								foreach ($query->result() as $row)
									{
							   ?>
									<tr>
                                                                            <td><a href="<?php echo base_url()?>gig_details/id/<?php echo $value['id']; ?>"><?php echo $value['gig_name']; ?></a></td>
										<td><?php echo $row->gener; ?></td>
										<td><?php echo $value['created_on'];?></td>
										<td><?php echo $value['CityName'];?></td>
										<td>
											<a href="<?php echo base_url()?>venue_dashboard/edit_gig/<?php echo $value['id']; ?>" class="ActionControls"><i class="fa fa-pencil"></i></a>
											<a href="<?php echo base_url()?>venue_dashboard/delete_gig/<?php echo $value['id']; ?>"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
								<?php }}?>
									<!--<tr>
										<td>Abitudini Live</td>
										<td>Death Metal</td>
										<td>30 Sep 2015</td>
										<td>Open Mic</span></td>
										<td>
											<a href="#" class="ActionControls"><i class="fa fa-pencil"></i></a>
											<a href="#"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
									<tr>
										<td>Abitudini Live</td>
										<td>Death Metal</td>
										<td>30 Sep 2015</td>
										<td>Open Mic</span></td>
										<td>
											<a href="#" class="ActionControls"><i class="fa fa-pencil"></i></a>
											<a href="#"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
									<tr>
										<td>Abitudini Live</td>
										<td>Death Metal</td>
										<td>30 Sep 2015</td>
										<td>Open Mic</span></td>
										<td>
											<a href="#" class="ActionControls"><i class="fa fa-pencil"></i></a>
											<a href="#"><i class="fa fa-trash"></i></a>
										</td>
									</tr>-->
								</tbody>
							</table>
                                                    <?php }else{?>
                                                          <p class="ptagpadding"><strong>Du trenger ikke ha noen gigs</strong></p>
                                                         <?php }?>
						</div><!-- table-responsive -->
					</div><!-- panel-body -->
					
				</div><!-- panel -->
				
			</div><!-- col-md-12 -->
			
		</div><!-- row -->
	</div><!-- container -->
</div><!-- GreyDashboard -->

<?php $this->load->view('footer'); ?>