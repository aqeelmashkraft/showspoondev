<?php $this->load->view('header'); ?>

<div class="GreySection side-collapse-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 no-gutter">
                <div class="col-md-6 col-sm-12 col-xs-12 ArtistInfo">
                   
                    <h2><?php echo $venueDetail[0]['name']?></h2>
                    <p class="artistDetails"><span class="btn badge">Spillested</span> <?php echo $venueDetail[0]['gener']?></p>
                   <?php /* <p class="artistMapLocation"><i class="fa fa-map-marker"></i> <?php echo $venueDetail[0]['location']?>   <?php echo $venueDetail[0]['zip']?>  <?php echo $venueDetail[0]['CityName']?>, Norway.</p> */?>
                     <?php //print_r($venueDetail);?>
                    <div class="col-md-12 col-sm-12 col-xs-12 no-gutter taligntab">
						<div class="fb-like" data-href="<?php echo base_url()?>venue_detail/index/id/<?php echo $venueDetail[0]['id']?>" data-width="45" data-layout="button_count" data-action="like" data-show-faces="true" data-share="flase"></div>
						<svg height="0" width="0">
						  <filter id="fb-filter">
							<feColorMatrix type="hueRotate" values="0"/>
						  </filter>
						</svg>
						<style>
						  .fb-like, .fb-send, .fb-share-button {
							-webkit-filter: url(#fb-filter); 
							filter: url(#fb-filter);
						  }
						</style>
						
                    </div><!-- col-md-6 -->
					<div class="clearfix"></div>
                    <div class="col-md-12 col-sm-12 col-xs-12 no-gutter text-center-sm">
                        <?php /*<a class="btn btn-primary btn-artist secondary contrast comments pull-left" href="#"><?php echo $commentcount[0]['comment_count']; ?> Comments</a> */?>
                        <button type="button" class="btn btn-primary btn-artist secondary contrast comments pull-left" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" style="margin-left:10px;">Beskjed</button>
                    </div><!-- col-md-12 -->
                    
                </div><!-- col-md-6 -->
                
                <div class="col-md-6 col-sm-12 col-xs-12 SliderBox">
                     <?php //print_r($venuephoto);?>
					<div class="carousel slide article-slide" id="myCarousel">
					  <div class="carousel-inner cont-slider">
                                              <?php if(count($venuephoto)==0){?>
                                              <div class="item active">
						  <img src="<?php echo base_url();?>/assets/images/no-image.jpg" class="center-block img-responsive">
						</div>
                                              <?php }else{?>
                                               <?php
                                               $counter=0;
                                               foreach($venuephoto as $key => $value)
                                               {
                                                   $counter++;
                                                  if($counter==1){
                                              ?>
                                              
						<div class="item active">
						  <img src="<?php echo base_url();?>files/venues_gallery/thumbs_gallary/<?php echo $value['url'];  ?>" class="center-block img-responsive">
						</div>
                                                  <?php }else{?>
                                              <div class="item">
						  <img src="<?php echo base_url();?>files/venues_gallery/thumbs_gallary/<?php echo $value['url'];  ?>" class="center-block img-responsive">
						</div>
                                              <?php }}}?>
						
					  </div>

					  <!-- Indicators -->
					  <ol class="carousel-indicators">
                                               <?php
                                              //$counter2=0;
                                              if(count($venuephoto)==0){?>
                                              
                                                <li class="active" data-slide-to="0" data-target="#myCarousel">
						  <img alt="" title="" src="<?php echo base_url();?>/assets/images/no-image.jpg">
						</li>   
						 
                                             <?php  }else{ ?>
                                              <?php
                                             $counter2=(1-2);
                                               foreach($venuephoto as $key => $value)
                                               {
                                                   $counter2++;
                                                   if($counter2==0){
                                              ?>
                                             
						<li class="active" data-slide-to="<?php echo $counter2;?>" data-target="#myCarousel">
						  <img alt="" title="" src="<?php echo base_url();?>files/venues_gallery/<?php echo $value['url'];  ?>">
						</li>
                                                   <?php }else{?>
                                                
						<li class="" data-slide-to="<?php echo $counter2;?>" data-target="#myCarousel">
						  <img alt="" title="" src="<?php echo base_url();?>files/venues_gallery/<?php echo $value['url'];  ?>">
						</li>
                                             <?php }}}?>
						         
					  </ol>                 
					</div>
                </div><!-- col-md-6 -->
            </div><!-- col-md-12 -->
        </div><!-- row -->
    </div><!-- container -->
</div><!-- GreySection -->

<div class="SkewSection  side-collapse-container">
    <div class="container">
        <div class="row">
            
            <div class="col-md-6 col-sm-12 col-xs-12 GreySkew">
                <div class="col-md-12 panel BioBox">
                    <div class="panel-body">
                        <h4 class="bio">Who we are</h4>
                        <p class="biodetail"> <?php echo $venueDetail[0]['descriptiopn']?></p>
                    </div>
                </div>
				
				<div class="col-md-12">
                                    <?php 
                                        $path=base_url().'uploads/pdf/'.$venueDetail[0]['rider_document_url_pdf'];
                                        if(empty($venueDetail[0]['rider_document_url_pdf'])){
                                            $path='javascript:void(0)';
                                        }
                                    ?>
                                    <a href="<?php echo $path;?>" class="pdficon" target="_blank">
						<img src="<?php echo base_url();?>assets/images/pdf-icon.png" alt="pdf" class="" /> Download Technical Rider Document (PDF 700Kb)
					</a>
				</div><!-- col-md-12 -->
				
            </div><!-- col-md-6 -->
            
            <div class="col-md-5 col-sm-12 col-xs-12 col-md-push-1 BlackSkew">
                <h4>Feedback from Artists</h4>
				<div class="col-md-12 col-sm-12 col-xs-12 no-gutter taligntab">
					
					<div class="col-md-12 col-sm-6 col-xs-12 no-gutter RatingByArtist col-sm-offset-3 col-md-offset-0">
                                            <?php //print_r($rate);?>
						<div class="col-md-4 col-sm-4 col-xs-4">
							<p class="headingtag">Crowd</p>
						</div>
                                            
						<div class="col-md-8 col-sm-8 col-xs-8">
                                <div class="rating" style="width:126px;float:left; display:inline;">
                                    <div class="exemple">
                                        <div class="exemple4 blackStar" data-average="<?php echo round($rate[0]['Crowd'],2);?>" data-class="3" data-id="<?php echo $id; ?>" ></div>
                                        </div>
                                </div>
							<i class="fa"><?php echo round($rate[0]['Crowd'],2).'/5';?></i>
						</div>
                                          
					</div><!-- RatingByArtist -->
					
					<div class="col-md-12 col-sm-6 col-xs-12 no-gutter RatingByArtist col-sm-offset-3 col-md-offset-0">
						<div class="col-md-4 col-sm-4 col-xs-4">
							<p class="headingtag">Stage</p>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-8">
                            <div class="rating" style="width:126px;float:left; display:inline;">
                                                        <div class="exemple">
                                                            <div class="exemple4 blackStar" data-average="<?php echo round($rate[0]['Stage'],2);?>" data-class="3" data-id="<?php echo $id; ?>" ></div>
                                                        </div>
                                                    </div>
							<!--<i class="fa fa-star cStar"></i>
							<i class="fa fa-star cStar"></i>
							<i class="fa fa-star cStar"></i>
							<i class="fa fa-star cStar"></i>
							<i class="fa fa-star-o cStar"></i>-->
                                                        <i class="fa"><?php echo round($rate[0]['Stage'],2).'/5';?></i>
                                                         
						</div>
                                            
					</div><!-- RatingByArtist -->
					
					<div class="col-md-12 col-sm-6 col-xs-12 no-gutter RatingByArtist col-sm-offset-3 col-md-offset-0">
						<div class="col-md-4 col-sm-4 col-xs-4">
							<p class="headingtag">Backstage</p>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-8">
                            <div class="rating" style="width:126px;float:left; display:inline;">
                                                        <div class="exemple">
                                                            <div class="exemple4 blackStar" data-average="<?php echo round($rate[0]['Backstage'],2);?>" data-class="3" data-id="<?php echo $id; ?>" ></div>
                                                        </div>
                                                    </div>
							<!--<i class="fa fa-star cStar"></i>
							<i class="fa fa-star cStar"></i>
							<i class="fa fa-star cStar"></i>
							<i class="fa fa-star-o cStar"></i>
							<i class="fa fa-star-o cStar"></i>-->
							<i class="fa "> <?php echo round($rate[0]['Backstage'],2).'/5';?></i>
						</div>
                                           
					</div><!-- RatingByArtist -->
					
					<a class="btn btn-artist connentsbtn mbh" href="#" data-toggle="modal" data-target="#applygig">Leave Comments</a>
					
				</div><!-- col-md-6 -->
            </div><!-- col-md-4 -->
            
        </div><!-- row -->
    </div><!-- container -->
</div><!-- SkewSection -->

<div class="LightGrey  side-collapse-container">
	<div class="container">
		<div class="row">
		
			<div class="col-md-12 col-sm-12 col-xs-12 gigography">
				<h4>Gigography</h4>
				<?php// print_r($mygigbyid);?>
                                <?php
                                if(isset($mygigbyid) && count($mygigbyid)){
				foreach($mygigbyid as $key => $value)
				{
                                    $imgpath="".base_url()."assets/images/no-image.jpg";
                                    if(!empty($value['image_url'])){
                                         $imgpath="".base_url()."uploads/gigs/thumbs/".$value['image_url'];
                                    }
			      /*  $query = $this->db->query("SELECT (SELECT  GROUP_CONCAT(aa.name) From artist_genre As aa Where aa.id IN (".$value['type'].")  ) AS gener
				FROM `artist_gigs` as a
				WHERE a.id=".$value['id']."");
				foreach ($query->result() as $row)
				 {*/
                                    
				?>
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="panel">
						<div class="panel-body">
							<div class="media">
							  <div class="media-left">
                                  <img class="media-object imagecontrol" src="<?php echo $imgpath;?>" alt="The long Road">
							  </div>
							  <div class="media-body">
								<h4 class="media-heading"><?php echo $value['gig_name'];?></h4>
								<p class="ventuegigdate"><?php echo $value['created_on'];?></p>
								<p class="applicationstatus"><?phpe echo $value['Accepting_application_start_date']; ?></p>
							  </div>
								<p class="gigdetails"><?php echo $value['description']; ?></p>
								<a href="<?php echo base_url() ?>gig_details/id/<?php echo $value['id'];?>" class="btn btn-artist secondary viewdetails nmb nmt smallbtngig">View Details</a>
							</div>
						</div>
					</div>
				</div><!-- col-md-4 -->
                                
                                <?php }}else{ ?>
                                <p><strong>Du har ikke noen gigs</strong></p>
                                <?php }?>
                                <div class="clearfix"></div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<a href="<?php echo base_url() ?>gigs_search/index/gigs/all" class="btn smallbtn mbl viewall" >Se alt</a> 
					</div>
                                <!----------------------Send Message Model------------------------->
                               
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="exampleModalLabel">Send message</h4>
                                            </div>
                                            <form method="post" action="" id="message_to_artist">
                                            <div class="modal-body">
                                                
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="control-label">Subject:</label>
                                                        <input name="artist_subject" type="text" class="form-control" id="artist_subject">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="message-text" class="control-label">Message:</label>
                                                        <textarea name="artist_message" class="form-control" id="artist_message"></textarea>
                                                        <input name="ven_id" type="hidden" value="<?php  echo $id;?>" class="form-control" id="ven_id">
                                                        <input name="art_id" type="hidden" value="<?php echo $_SESSION['user_id'];?>" class="form-control" id="art_id">
                                                        <input name="type" type="hidden" value="2" class="form-control" id="type">
                                                    </div>
                                               
                                                <div id="successmsg"></div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <!--<button type="submit" class="btn btn-primary">Send message</button>-->
                                                <input type="submit" name="submit"class="btn btn-primary btn-brown" value="Send message" />
                                            </div>
                                          </form>
                                        </div>
                                    </div>
                                </div>
                                <!----------------------------Leave Comment modal----------------------------------->
                        
                                <div class="modal fade" id="applygig" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h5 class="modal-title text-center" id="myModalLabel">Feedback </h5>
				<h3 class="modal-title text-center" id="myModalLabel"></h3>
                              
				<h5 class="modal-title text-center applyDate" id="myModalLabel"></h5>
			</div>
			<div class="modal-body">
                            <div class="col-md-12 col-sm-6 col-xs-12 no-gutter RatingByArtist col-sm-offset-3 col-md-offset-0">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <p class="headingtag">Crowd</p>
                                </div>
                                <div class="rating" style="width:126px;float:left; display:inline;">
                                    <div class="exemple">
                                        <div class="basic" data-average="0" data-class="1" data-id="<?php echo $id;?>" ></div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-12 col-sm-6 col-xs-12 no-gutter RatingByArtist col-sm-offset-3 col-md-offset-0">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <p class="headingtag">Stage</p>
                                </div>
                                <div class="rating" style="width:126px;float:left; display:inline;">
                                    <div class="exemple">
                                        <div class="basic2" data-average="0" data-class="2" data-id="<?php echo $id;?>" ></div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-12 col-sm-6 col-xs-12 no-gutter RatingByArtist col-sm-offset-3 col-md-offset-0">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <p class="headingtag">Backstage</p>
                                </div>
                                <div class="rating" style="width:126px;float:left; display:inline;">
                                    <div class="exemple">
                                        <div class="basic3" data-average="0" data-class="3" data-id="<?php echo $id;?>" ></div>
                                    </div>
                                </div>

                            </div>
                <div class="col-md-12 col-sm-6 col-xs-12 no-gutter RatingByArtist col-sm-offset-3 col-md-offset-0">
                             <form method="post" action="" id="comment_post">
                                 <textarea rows="5" class="form-control" name="ven_comment" id="ven_comment" placeholder="Enter your comment" required></textarea>
                                 <input type="hidden" name="ven_id" id="ven_id" value="<?php echo $id;?>"/>
                                <div id="success"></div>
                                <input type="submit" name="submit"class="btn btn-primary btn-artist secondary centerbtn" value="Submit" />
                          </form>
                </div>
			</div>
		</div>
	</div>
</div>
				
			<?php /*	<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="panel">
						<div class="panel-body">
							<div class="media">
							  <div class="media-left">
								  <img class="media-object" src="<?php echo base_url();?>assets/images/longride.jpg" alt="The long Road">
							  </div>
							  <div class="media-body">
								<h4 class="media-heading">The long Road</h4>
								<p class="ventuegigdate">28th August 2015</p>
								<p class="applicationstatus not">Not Accepting Applications</p>
							  </div>
								<p class="gigdetails">A rock festival which will be organized by the citizens foundation and sponsored by Jzz FM 99. A three day concert which will</p>
								<a href="#" class="viewdetails">View Details</a>
							</div>
						</div>
					</div>
				</div><!-- col-md-4 -->
				
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="panel">
						<div class="panel-body">
							<div class="media">
							  <div class="media-left">
								  <img class="media-object" src="<?php echo base_url();?>assets/images/longride.jpg" alt="The long Road">
							  </div>
							  <div class="media-body">
								<h4 class="media-heading">The long Road</h4>
								<p class="ventuegigdate">28th August 2015</p>
								<p class="applicationstatus not">Not Accepting Applications</p>
							  </div>
								<p class="gigdetails">A rock festival which will be organized by the citizens foundation and sponsored by Jzz FM 99. A three day concert which will</p>
								<a href="#" class="viewdetails">View Details</a>
							</div>
						</div>
					</div>
				</div><!-- col-md-4 --> */ ?>
				
			</div><!-- col-md-12 -->
			
		</div><!-- row -->
	</div><!-- container -->
</div><!-- LightGrey -->


















<?php $this->load->view('footer'); ?>