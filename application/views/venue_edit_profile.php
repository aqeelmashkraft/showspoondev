<?php $this->load->view('header'); ?>

<?php $this->load->view('mydashboard_links'); ?>

<div class="GreyDashboard  side-collapse-container">
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>venue_dashboard">Dashbord </a></li>
                    <li class="active"><a href="#">Oppdater profil</a></li>
                </ul>
            </div><!-- col-md-12 -->

            <div class="col-md-12 col-sm-12 col-xs-12 ArtistDashboarItems">
                <?php
                //$gallary = $this->uri->segment(2);

                $activcls = 'active';
                $activ = '';
                //var_dump($this->session->flashdata('flash_active2'));
                if ($this->session->flashdata('flash_active2')) {
                    if ($this->session->flashdata('flash_active2') == 'active2') {
                        $activ = 'active';
                        $activcls = '';
                    }
                }
                ?>

                <ul class="nav nav-tabs nav-append-content">
                    <li class="<?php echo $activcls; ?>"><a href="#profile" data-toggle="tab">Profil</a></li>
                    <li class="<?php echo $activ; ?>"><a href="#gallery" data-toggle="tab">Media</a></li>
                </ul><!-- nav -->

                <!-- Tab content -->
                <div class="tab-content">
                    <div class="tab-pane <?php echo $activcls; ?>" id="profile">
                        <form id="myForm" class=""  method="post" action="<?php echo base_url() ?>venue_dashboard/edit_profile" enctype="multipart/form-data">
                            <div class="col-md-6 col-sm-12 col-xs-12 xm-no-gutter">
                                <?php
                                //var_dump($this->session->flashdata('flash_message'));
                                //flash messages
                                if ($this->session->flashdata('flash_message')) {
                                    if ($this->session->flashdata('flash_message') == 'updated') {
                                        echo '<div class="alert alert-success">';
                                        echo '<a class="close" data-dismiss="alert">×</a>';
                                        echo 'Profile is updated with success.';
                                        echo '</div>';
                                    }
                                }
                                ?>
                                <?php if (isset($msg)) : ?>
                                    <div class="col-md-12">
                                        <div class="alert alert-danger" role="alert">
    <?= $msg ?>
                                        </div>
                                    </div>
<?php endif; ?>
                                <?php
                                //print_r($venuebyid);

                                $genre_id['genre'] = explode(',', $venuebyid[0]['genre']);
                                ?>
                                <div class="form-group">
                                    <label>Spillested navn</label>
                                    <input type="text" class="form-control" value="<?php echo $venuebyid[0]['name'] ?>" name="ven_name" placeholder="Spillested navn"  required/>
                                </div><!-- form-group -->
                                <div class="form-group">
                                    <label>Alder</label>
                                    <input type="text" class="form-control" value="<?php echo $venuebyid[0]['age'] ?>" name="ven_age" placeholder="Alder"  required/>
                                </div><!-- form-group -->

                                <div class="form-group">
                                    <label>Sjanger</label>
                                    <?php echo form_dropdown('art_genre[]', $artist_genre, $genre_id['genre'], 'class="form-control multiselect" multiple="multiple" placeholder="Sjanger"'); ?>

                                </div><!-- form-group -->

                                <div class="form-group">
                                    <label>Postkode</label>
                                    <input type="text" class="form-control" placeholder="Postkode" name="ven_zip" value="<?php echo $venuebyid[0]['zip']; ?>" required />

                                </div>
                               <div class="form-group">
                                    <label>Beliggenhet</label>
                                    <input type="text" placeholder="Beliggenhet" name="ven_location" value="<?php echo $venuebyid[0]['location']; ?>" class="form-control" />
                                </div> 
                                

                                <input type="submit" class="btn btn-primary btn-artist secondary lg" id="submitbtn" value="Update">

                            </div><!-- col-md-6 -->
							<div class="col-md-6 col-sm-12 col-xs-12 xm-no-gutter">
								
								<div class="form-group">
                                    <label>kapasitet</label>
                                    <input type="text" placeholder="kapasitet" name="ven_capacity" value="<?php echo $venuebyid[0]['ven_capacity']; ?>" class="form-control" />
                                </div>
                                <div class="form-group">
                                    <label>By</label>
                                    <?php echo form_dropdown('ven_city', $cities, $venuebyid[0]['city'], 'class="form-control select select-primary select-block"  placeholder="By"'); ?>

                                </div>
                                <div class="form-group">
                                    <label>Last Teknisk Rider dokument (PDF)</label>
                                    <input name="userfile" id="userfile" type="file" />
                                    <input name="userfile" id="userfile" type="hidden" value="<?php echo $venuebyid[0]['rider_document_url_pdf']; ?>" />
                                </div>
                                <div class="form-group">
                                    <label>Beskrivelse</label>
                                    <textarea placeholder="Beskrivelse" name="ven_description" class="form-control" required ><?php echo $venuebyid[0]['descriptiopn']; ?></textarea>
                                </div>
							
							</div><!-- col-md-6 -->
                        </form>
                    </div><!-- profile -->

                    <div class="tab-pane <?php echo $activ; ?>" id="gallery">
                        <div class="col-md-12 no-gutter">
                            <h5>Bildegalleri</h5>
                        </div><!-- col-md-12 -->
                        <div class="col-md-12 col-sm-12 col-xs-12 GalleryTab mbh">
                            <div class="col-md-4 col-sm-6 col-xs-6">
                                <form method="post" action="" id="upload_file_venue">
                                    <div class="form-group">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;">
                                                <img data-src="holder.js/100x100" alt="">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 150px; max-height: 150px;"></div>
                                            <div class="clearfix"></div>
                                            <label class="checkbox col-md-8" for="checkbox1">
                                                <input type="checkbox"  id="checkbox1" name="setfeatured" data-toggle="checkbox"> Set Utvalgt
                                            </label>

                                            <div class="clearfix"></div>
                                            <div>
                                                <span class="btn btn-primary btn-file">
                                                    <span class="fileinput-new">
                                                        <span class="fui-image"></span> Velg bilde
                                                    </span>
                                                    <span class="fileinput-exists"><span class="fui-gear"></span> Endring</span>
                                                    <input type="file" name="userfile" id="userfile2"  >
                                                </span>
                                                <a href="#" class="btn btn-primary btn-file fileinput-exists" data-dismiss="fileinput">
                                                    <span class="fui-trash"></span>  Fjerne
                                                </a>
												<input type="submit" name="submit" class="btn btn-primary btn-file" id="submitvenue" value="Lagre" />
                                            </div>

                                            <small class="help-block" data-fv-validator="notEmpty" id="error" data-fv-for="art_location" data-fv-result="INVALID" style=""></small>
                                        </div><!-- fileinput -->
                                    </div><!-- form-group -->
                                </form> 
                            </div>
                            <div class="col-md-8 col-sm-6 col-xs-6">
                                <div id="files2"></div>
                            </div>

                        </div><!-- col-md-4 -->
                        <!--						<a class="btn btn-primary btn-artist secondary lg" href="#">Update</a>-->

                    </div><!-- gallery -->

                </div><!-- tab-content -->

            </div><!-- col-md-12 -->

        </div><!-- row -->
    </div><!-- container -->
</div><!-- GreyDashboard -->

<?php $this->load->view('footer'); ?>