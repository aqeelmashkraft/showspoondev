<?php $this->load->view('header'); ?>

<?php $this->load->view('mydashboard_links'); ?>

<div class="GreyDashboard  side-collapse-container">
	<div class="container">
		<div class="row">
			
			<div class="col-md-12 col-sm-12 col-xs-12 ArtistDashboarItems">
				
				<div class="panel">
					<div class="panel-heading">
						<h5>Meldinger fra Musikere</h5>
						
					</div><!-- panel-heading -->
					
					<div class="panel-body">
						<div class="table-responsive">
                                                      <?php if(count($messages)>0){ ?>
							<table class="table table-hover table-condensed">
								<thead>
									<tr>
										<td>Musikere Navn</td>
										<td>Emne</td>
										<td>beskjed</td>
										<td>Dato</td>
										
									</tr>
								</thead>
								<tbody>
                                                                    <?php
                                                                        foreach($messages as $key => $value)
                                                                        { ?>
									<tr>
                                                                            <td><a href="<?php echo base_url();?>artist_detail/index/id/<?php echo $value['senderid']?>"><?php echo $value['sender_name'];?></a></td>
										<td><?php echo $value['subject'];?></td>
										<td><?php echo $value['message'];?></td>
										<td><?php echo $value['created_on'];?></td>
										
									</tr>
                                                                        <?php }?>
									
								</tbody>
							</table>
                                                     <?php }else{?>
                                                    <p class="ptagpadding"><strong>Du trenger ikke ha noen meldinger</strong></p>
                                                    <?php }?>
						</div><!-- table-responsive -->
					</div><!-- panel-body -->
					
				</div><!-- panel -->
				
                        </div>
				
			
		</div><!-- row -->
	</div><!-- container -->
</div><!-- GreyDashboard -->


<?php $this->load->view('footer'); ?>