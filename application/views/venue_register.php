<?php $this->load->view('header'); ?>

<div class="artistRegister side-collapse-container">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3 panel">
                <h3>Signup at <strong>Showspoon</strong></h3>
                <p class="subheading">Account Details</p>
                <div id="response" class="subheading"></div>
                	<form id="registerFormVenue" class="form-horizontal"  method="post" action="<?php echo base_url() ?>venue_register/createVenue">
                <div class="col-md-10 col-md-offset-1">
                    
                   <!-- <select class="form-control select select-primary select-block mbl">
                      <optgroup label="Account Type">
                        <option value="0">Artist</option>
                        <option value="1" selected>Venue</option>
                      </optgroup>
                    </select>-->
                      <div class="form-group">
						<input type="email" placeholder="Email" name="ven_email"  id="ven_email_id" class="form-control mbl" />
					</div>
					<div class="form-group">
						<input type="password" placeholder="Password" name="ven_password" class="form-control mbl" />
                    </div>
                </div>
				
				<div class="col-md-10 col-md-offset-1">
                    <p class="subheading">Venue Details</p>
                      <div class="form-group">
                    <input type="text" placeholder="Name" name="ven_name" class="form-control mbl" />
                    </div>
					  <div class="form-group">
                    <select multiple="multiple" class="form-control multiselect mbl" name="ven_genre[]" placeholder="Genre">
                        <optgroup label="Genre">
                            <option value="0">Acapella</option>
                            <option value="1">Alternative</option>
                            <option value="2">Acoustic</option>
                            <option value="3">Blues</option>
                            <option value="4">Childrens</option>
                            <option value="5">Cover</option>
                            <option value="6">Classical</option>
                            <option value="7">Country</option>
                            <option value="8">Dance</option>
                            <option value="9">Electronic</option>
                            <option value="10">Experimental</option>
                            <option value="11">Folk</option>
                            <option value="12">Funk</option>
                            <option value="13">Gospel</option>
                            <option value="14">Hardcore</option>
                            <option value="15">Hip Hop</option>
                            <option value="16">indie</option>
                            <option value="17">Instrumental</option>
                            <option value="18">International</option>
                            <option value="19">Jazz</option>
                            <option value="20">Latin</option>
                            <option value="21">Mariachi</option>
                            <option value="22">Metal</option>
                            <option value="23">Pop</option>
                            <option value="24">Producer</option>
                            <option value="25">Punk</option>
                            <option value="26">R&amp;B</option>
                            <option value="27">Rap</option>
                            <option value="28">Religious</option>
                            <option value="29">Rock</option>
                            <option value="30">Rockabilly</option>
                            <option value="31">Reggae</option>
                            <option value="32">Ska</option>
                            <option value="33">Soul</option>
                            <option value="34">Western</option>
                        </optgroup>
                    </select>
                    </div>
					  <div class="form-group">
                    <input type="text" placeholder="Location" name="ven_location" class="form-control mbl" />
                    </div>
					 <div class="form-group">
                    <label class="checkbox col-md-10 col-md-offset-2 col-sm-8 col-sm-offset-4 col-xs-12" for="checkbox1">
                        <input type="checkbox" value="" name="agree" id="checkbox1" data-toggle="checkbox">I have read and agree to the <a href="#">Term &amp; Use.</a>
                    </label>
					</div>
                    <input type="submit" class="btn btn-primary btn-artist  centerbtn" id="submitbtn" value="Register">
                    <!--<a class="btn btn-primary btn-artist secondary centerbtn" href="#">Register</a>-->
                    
                </div>
				</form>
                
            </div><!-- col-md-6 -->
        </div><!-- row -->
    </div><!-- container -->
</div><!-- artistRegister -->


<?php $this->load->view('footer'); ?>