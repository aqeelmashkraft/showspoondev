$(function() {
    //alert(id);
        refresh_files();
        refresh_files2();
        refresh_files3();
        refresh_files4();
        refresh_files5();
   	
        $('#upload_file').submit(function(e) {
		e.preventDefault();
		$.ajaxFileUpload({
			url 			:baseurl+'admin/artist/upload_file/'+idart, 
			secureuri		:false,
			fileElementId	:'userfile',
			dataType		: 'json',
			data			: {
				'title': $('#title').val(),
				'role': $('#role').val()
			},
			success	: function (data, status)
			{
				if(data.status != 'error')
				{
					$('#files').html('<p>Reloading files...</p>');
					refresh_files();
					$('#title').val('');
					$('#role').val('');
				}
				$('#error2').html(data.msg);
				//alert(data.msg);
			}
		});
		return false;
	});
        ////////////////////venue Gallary Code///////////////////////////
        $('#upload_file_venue').submit(function(e) {
			 var    check = $("#checkbox1").is(":checked");
                   if(check) {
                       alert("Checkbox is checked.");
                      // var setfeaturedval='1';
                   }else {
                       var setfeaturedval='0';
                       //alert("Checkbox is unchecked.");
                   }
		e.preventDefault();
		$.ajaxFileUpload({
			url 			:baseurl+'admin/venues/upload_file/'+idven, 
			secureuri		:false,
			fileElementId	:'userfile2',
			dataType	: 'json',
			data: {
				'setfeatured': setfeaturedval,				
			},			
			success	: function (data, status)
			{
				if(data.status != 'error')
				{
					$('#files2').html('<p>Reloading files...</p>');
					refresh_files2();
					
				}
				//alert(data.msg);
				$('#error').html(data.msg);
			}
                        
                        
		});
		return false;
	});
	
        
        ///artist gallary code 
        $('#upload_file_artist').submit(function(e) {
                   var    check = $("#checkbox1").is(":checked");
                   if(check) {
                       //alert("Checkbox is checked.");
                       var setfeaturedval='1';
                   }else {
                       var setfeaturedval='0';
                      // alert("Checkbox is unchecked.");
                   }
		e.preventDefault();
		$.ajaxFileUpload({
			url 			:baseurl+'admin/artist/upload_file_artist/'+idart, 
			secureuri		:false,
			fileElementId	:'userfile3',
			dataType	: 'json',
                        data			: {
				'setfeatured': setfeaturedval,				
			},
			success	: function (data, status)
			{
				if(data.status != 'error')
				{
					$('#files3').html('<p>Reloading files...</p>');
					refresh_files3();
					
				}
				//alert(data.msg);
				$('#error').html(data.msg);
			}
                        
                        
		});
		return false;
	});
        
        
         /////////////media Link   
     
            $('#upload_file_media').submit(function(e) {
            //alert('ok');
		e.preventDefault();
		$.ajax({
			url 			:baseurl+'admin/artist/upload_media/'+idart, 
                        type:'POST',
			data: {
				'video_title': $('#video_title').val(),
				'video_link': $('#video_link').val(),
                                'media_type': $('#media_type').val()
			},
			success	: function (data)
			{
			  $('#video_title').val('');
			  $('#video_link').val('');                         
                          $('#success').html('<p>Video link inserted successfully</p>');
                             refresh_files4();   
			}
		});
		return false;
	});
        
        
        $('#upload_file_media_sound').submit(function(e) {
            //alert('ok');
		e.preventDefault();
		$.ajax({
			url 			:baseurl+'admin/artist/upload_media/'+idart, 
                        type:'POST',
			data: {
				'video_title': $('#video_title2').val(),
				'video_link': $('#video_link2').val(),
                                'media_type': $('#media_type2').val()
			},
			success	: function (data)
			{
			  $('#video_title2').val('');
			  $('#video_link2').val('');                         
                          $('#success2').html('<p>Soundcloud link inserted successfully</p>');
                             refresh_files5();   
			}
		});
		return false;
	});
        
 });  
 
 function refresh_files5()
{
	$.get(baseurl+'admin/artist/files_media_soundcloud/'+idart)
	.success(function (data){
		$('#files5').html(data);
	});
}

function refresh_files4()
{
	$.get(baseurl+'admin/artist/files_media/'+idart)
	.success(function (data){
		$('#files4').html(data);
	});
}

function refresh_files()
{
	$.get(baseurl+'admin/artist/files/'+idart)
	.success(function (data){
		$('#files').html(data);
	});
}

function refresh_files3()
{
	$.get(baseurl+'admin/artist/files_artist/'+idart)
	.success(function (data){
		$('#files3').html(data);
	});
}

function refresh_files2()
{
	$.get(baseurl+'admin/venues/files/'+idven)
	.success(function (data){
		$('#files2').html(data);
	});
}
function deleteFunctionvenue(id){
   if (confirm("Are you sure?")) {
   $.ajax({
     type: "POST",
     url: baseurl+'admin/venues/venue_delete_media/'+id,
success : function()
                    {
        refresh_files2();

                }
   });
 }
    return false;
}

function setfeaturedFunctionvenue(id){
      $.ajax({
     type: "POST",
     url: baseurl+'admin/venues/set_Featured/'+id+'/'+idven,
success : function()
                    {
         refresh_files2();
                }
   });
}

function setfeaturedFunction(id){
    //alert(id);
      $.ajax({
     type: "POST",
     url:  baseurl+"admin/artist/set_Featured/"+id+'/'+idart,
success : function()
                    {
         refresh_files3();
            //alert('k');
                }
   });
}
function deleteFunction(id){
    // your deletion code
    //alert(id);
    if (confirm("Are you sure?")) {
   $.ajax({
     type: "POST",
     url: baseurl+"admin/artist/artist_delete_media/"+id+'/'+idart,
success : function()
                    {
        refresh_files3();

                }
   });
 }
    return false;
}
function deleteFunctionvideo(id){
    // your deletion code
     if (confirm("Are you sure?")) {
         //alert(id);
          $.ajax({
     type: "POST",
     url: baseurl+"admin/artist/artist_delete_video/"+id,
success : function()
                    {
        refresh_files4();
            //alert('k');
                }
   });
     }
    
}
function deleteFunctionsoundcloud(id){
    // your deletion code
     if (confirm("Are you sure?")) {
         //alert(id);
          $.ajax({
     type: "POST",
     url: baseurl+"admin/artist/artist_delete_soundcloud/"+id,
success : function()
                    {
        refresh_files5();
            //alert('k');
                }
   });
     }
    
}

function getSoundCloudInfo(url){
  var regexp = /^https?:\/\/(soundcloud.com|snd.sc)\/(.*)$/;
  return url.match(regexp) && url.match(regexp)[2]
}