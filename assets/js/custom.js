$('section[data-type="background"]').each(function () {
    // declare the variable to affect the defined data-type
    var $scroll = $(this);

    $(window).scroll(function () {
        // HTML5 proves useful for helping with creating JS functions!
        // also, negative value because we're scrolling upwards                             
        var yPos = -($window.scrollTop() / $scroll.data('speed'));

        // background position
        var coords = '50% ' + yPos + 'px';

        // move the background
        $scroll.css({
            backgroundPosition: coords
        });
    }); // end window scroll
}); // end section function


/** Search hide/show **/
$('.roombutton').click(function (){
     var nav = $('.SearchParameters');
     if(nav.hasClass('showing')){
         nav.removeClass('showing').addClass('hiding');
     }else{
         nav.removeClass('hiding').addClass('showing');
     }
 });
$(window).resize(function(){
    var winwidth = $(window).innerWidth();
    if(winwidth > 768){
        $('.SearchParameters').removeClass('showing').removeClass('hiding');
    }
});


var sideslider = $('[data-toggle=collapse-side]');
var sel = sideslider.attr('data-target');
var sel2 = sideslider.attr('data-target-2');
sideslider.click(function (event) {
    $(sel).toggleClass('in');
    $(sel2).toggleClass('out');
});
$("select").select2({dropdownCssClass: 'dropdown-inverse'});
$(':checkbox').radiocheck();
$("#input-id").rating();
$('#myCarousel').carousel({interval:   4000});

/* Date Picker */
var datepickerSelector = $('#datepicker-01');
datepickerSelector.datepicker({
  showOtherMonths: true,
  selectOtherMonths: true,
  dateFormat: 'yy-mm-dd',
  yearRange: '-1:+1'
}).prev('.input-group-btn').on('click', function (e) {
  e && e.preventDefault();
  datepickerSelector.focus();
});
$.extend($.datepicker, { _checkOffset: function (inst,offset,isFixed) { return offset; } });

// Now let's align datepicker with the prepend button
datepickerSelector.datepicker('widget').css({ 'margin-left': -datepickerSelector.prev('.input-group-btn').find('.btn').outerWidth() + 3 });

var datepickerSelector = $('#datepicker-02');
datepickerSelector.datepicker({
  showOtherMonths: true,
  selectOtherMonths: true,
  dateFormat: 'yy-mm-dd',
  yearRange: '-1:+1'
}).prev('.input-group-btn').on('click', function (e) {
  e && e.preventDefault();
  datepickerSelector.focus();
});
$.extend($.datepicker, { _checkOffset: function (inst,offset,isFixed) { return offset; } });

// Now let's align datepicker with the prepend button
datepickerSelector.datepicker('widget').css({ 'margin-left': -datepickerSelector.prev('.input-group-btn').find('.btn').outerWidth() + 3 });

// Checkbox
$(':checkbox').radiocheck();

// Map Load

var map;

function initialize() {
  var myLatlng = new google.maps.LatLng(59.9500,10.7500);
  var mapOptions = {
    zoom: 14,
    center: myLatlng
  };
  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
  
  var marker = new google.maps.Marker({
    position: myLatlng,
    map: map,
    title: 'Hello World!'
  });
  
}

google.maps.event.addDomListener(window, 'load', initialize);




