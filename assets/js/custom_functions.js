$(document).ready(function() {
    
     $('#app_for_gig').click(function() {        
        var message_gig =$('#message_gig').val();
        var gig_id =$('#gig_id').val();
	$.ajax({
		  url: baseurl+'gig_details/appay_for_gig',
		  type: 'POST',
		  data: { 
                    'message_gig': message_gig, 
                    'gig_id': gig_id
                },
		  success: function(data) {
			//called when successful
			var response= $.parseJSON(data);
			$('#ajax-results').html(response.msg);
		  },
		  error: function(e) {
			//called when there is an error
			//console.log(e.message);
		  }
		});
     });
	/////////////////////////Foget Password/////////////////////////////////// 
   $('#forget_id').click(function() {       
	var email =$('#email').val();
	var artist_type =$('#artist_type').val();
	
       var alldata= {
	    	     	email:email,
			        artist_type:artist_type
	    	     };
	$.ajax({
		  url: baseurl+'register/forget_password',
		  type: 'POST',
		  data:alldata,
		  success: function(data) {
			//called when successful
			var response= $.parseJSON(data);
			$('#ajaxphp-results').html(response.msg);
		  },
		  error: function(e) {
			//called when there is an error
			//console.log(e.message);
		  }
		});

});
    
  ///////////////////////Comment post On Venue Detail////////////////////////////
  
    $('#comment_post').submit(function(e) {
            //alert('ok');
		e.preventDefault();
		$.ajax({
			url :baseurl+'venue_detail/comment', 
                       type:'POST',
			data: {
				'ven_comment': $('#ven_comment').val(),
                                 'ven_id': $('#ven_id').val()
			},
			success	: function (data)
			{
			  $('#ven_comment').val('');
                          $('#success').html('<p>Comment is posted successfully </p>');
			}
		});
		return false;
	});
         $('#gig_comment_post').submit(function(e) {
            //alert('ok');
		e.preventDefault();
		$.ajax({
			url :baseurl+'gig_details/comment', 
                       type:'POST',
			data: {
				'ven_comment': $('#ven_comment').val(),
                                 'gig_id': $('#gig_id').val()
			},
			success	: function (data)
			{
			  $('#ven_comment').val('');
                          $('#success').html('<p>Comment is posted successfully </p>');
			}
		});
		return false;
	});
        
        $('#message_to_artist').submit(function(e) {
            //alert('ok2');
		e.preventDefault();
		$.ajax({
			url :baseurl+'venue_detail/message', 
                        type:'POST',
			data: {
				'artist_subject': $('#artist_subject').val(),
                                'artist_message': $('#artist_message').val(),
                                'art_id': $('#art_id').val(),
                                'ven_id': $('#ven_id').val(),
                                'type': $('#type').val()
			},
			success	: function (data)
			{
			  $('#artist_subject').val('');
			  $('#artist_message').val('');
                          $('#successmsg').html('<p>Message is send successfully </p>');
			}
		});
		return false;
	});
        
          $('#invite_to_artist_by_venue').submit(function(e) {
          e.preventDefault();
           $.ajax({
		   url :baseurl+'venue_dashboard/invite', 
                 type:'POST',
			data: $("form").serialize(),
			success	: function (data)
			{
			  $('#art_id').val('');
			  $('#gig_id').val('');
			  $('#venue_message').val('');
              $('#successmsg2').html('<p>Invitation is send successfully </p>');
                          //alert(data);
			}
		});
		return false;
          });
          $('#invite_to_artist').submit(function(e) {
            //alert('ok4');
		e.preventDefault();
		$.ajax({
			url :baseurl+'artist_detail/invite', 
                        type:'POST',
			data: {
				'gig_id': $('#gig_id').val(),
                                'venue_message': $('#venue_message').val(),
                                'art_id': $('#art_id').val(),
                                'ven_id': $('#ven_id').val(),
			},
			success	: function (data)
			{
			  $('#gig_id').val('');
			  $('#venue_message').val('');
                          $('#successmsg2').html('<p>invitation is send successfully </p>');
			}
		});
		return false;
	});
        
        
        $('#message_to_venue').submit(function(e) {
           // alert('ok');
		e.preventDefault();
		$.ajax({
			url :baseurl+'artist_detail/message', 
                        type:'POST',
			data: {
				'artist_subject': $('#artist_subject').val(),
                                'artist_message': $('#artist_message').val(),
                                'art_id': $('#art_id').val(),
                                'ven_id': $('#ven_id').val(),
                                'type': $('#type').val()
			},
			success	: function (data)
			{
			  $('#artist_subject').val('');
			  $('#artist_message').val('');
                          $('#successmsg').html('<p>Message is send successfully </p>');
			}
		});
		return false;
	});
        
        
        
	
	/////////////////////////////////Check UserName of Artist ///////////////////////
	
 $('#art_email_id').keyup(username_check);
	
	function username_check(){
		
	var username = $('#art_email_id').val();
	if(username == "" || username.length < 4){
	$('#art_email_id').css('border', '3px #CCC solid');
	$('#tick').hide();
	}else{
		$.ajax({
			url:baseurl+'register/checkusername',
			type:'POST',
		    data: 'art_email='+ username,
		    cache: false,
		success: function (response)  {
		if(response == 1){
			$('#art_email_id').css('border', '3px #C33 solid');
			$('#submitbtn').attr("disabled","disabled");				
			$('#tick').show();
                        //alert('ok');
			$('#cross').fadeIn();
		}else{
			$('#submitbtn').removeAttr("disabled");
			$('#art_email_id').css('border', '3px #090 solid');
			$('#cross').hide();
			$('#tick').hide();
		}
	  }

	});
}
}
 ////////////////////////////////Validation Artist Registration///////////////////////////
    $('#registerForm')
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
		artist_type: {
                    validators: {
                        notEmpty: {
                            message: 'Dette kreves'
                        }
                    }
                },
                art_email: {
                    validators: {
                        notEmpty: {
                            message: 'E-postadresse er n�dvendig'
                        },
						emailAddress: {
                        message: 'Vennligst oppgi en gyldig mailadresse'
						}                        
                    }
                },
                art_password: {
                    validators: {
                        notEmpty: {
                            message: 'Passord er p�krevd'
                        }
                    }
                },
                art_name: {
                    validators: {
                        notEmpty: {
                            message: 'Navn er p�krevd'
                        }
                    }
                },
		art_location: {
                    validators: {
                        notEmpty: {
                            message: 'Beliggenhet er n�dvendig'
                        }
                    }
                },
                art_cities: {
                    validators: {
                        notEmpty: {
                            message: 'Byen er p�krevd'
                        }
                    }
                },
                 art_agree: {
                    validators: {
                        notEmpty: {
                            message: 'Dette kreves'
                        }
                    }
                },
				'art_genre[]': {
                    validators: {
                        notEmpty: {
                            message: 'Sjangeren er n�dvendig'
                        }
                    }
                },
				'art_type_show[]':{
					validators: {
                        notEmpty: {
                            message: 'Typer av showet er n�dvendig'
                        }
                    }
				},
				art_type:{
					validators: {
                        notEmpty: {
                            message: 'Artist typen er p�krevd'
                        }
                    }
				}
            }
        }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();

            var $form = $(e.target),
                fv    = $form.data('formValidation');

            // Use Ajax to submit form data
            $.ajax({
                url: $form.attr('action'),
                type: 'POST',
                data: $form.serialize(),
                success: function(result) {
                    
		   window.location.href =baseurl+"thank_you_for_registration";

		//$("#response").html('Thank! For registration');
		//$('#registerForm').hide();
					
                    // ... Process the result ...
                }
            });
        });
		
		
                
                
  	/////////////////////////////////Check UserName of Venue ///////////////////////              
                
    $('#ven_email_id').keyup(venue_check);
	
	function venue_check(){
	var username = $('#ven_email_id').val();
	if(username == "" || username.length < 4){
	$('#ven_email_id').css('border', '3px #CCC solid');
	$('#tick2').hide();
	}else{
		$.ajax({
            url:baseurl+'register/checkVenue',
			type:'POST',
            data: 'ven_email='+ username,
            cache: false,
		success: function (response)  {
		if(response == 1){
			//alert('1');
			$('#ven_email_id').css('border', '3px #C33 solid');
			$('#submitbtnven').attr("disabled","disabled");				
			$('#tick2').show();
			$('#cross').fadeIn();
		}else{
			//alert('0');
			$('#submitbtnven').removeAttr("disabled");
			$('#ven_email_id').css('border', '3px #090 solid');
			$('#cross').hide();
			$('#tick2').hide();
		}
	  }

	});
}
}
 
 ////////////////////////////////Validation Artist Registration/////////////////////////// 
		
    $('#registerFormVenue')
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                ven_email: {
                    validators: {
                        notEmpty: {
                            message: 'E-postadresse er n�dvendig'
                        },
						emailAddress: {
                        message: 'Vennligst oppgi en gyldig mailadresse'
						}                        
                    }
                },
                ven_password: {
                    validators: {
                        notEmpty: {
                            message: 'Passord er p�krevd'
                        }
                    }
                },
                ven_name: {
                    validators: {
                        notEmpty: {
                            message: 'Navn er p�krevd'
                        }
                    }
                },
				ven_agree: {
                    validators: {
                        notEmpty: {
                            message: 'Dette kreves'
                        }
                    }
                },
		ven_location: {
                    validators: {
                        notEmpty: {
                            message: 'Beliggenhet er n�dvendig'
                        }
                    }
                },
                ven_cities: {
                    validators: {
                        notEmpty: {
                            message: 'Byen er p�krevd'
                        }
                    }
                },				
		 'ven_genre[]': {
                    validators: {
                        notEmpty: {
                            message: 'Sjangeren er n�dvendig'
                        }
                    }
                },
			agree:{
					validators: {
                        notEmpty: {
                            message: 'Dette kreves'
                        }
                    }
				}
				
            }
        }).on('success.form.fv', function(e) {
            // Prevent form submission
            e.preventDefault();

            var $form = $(e.target),
                fv    = $form.data('formValidation');

            // Use Ajax to submit form data
            $.ajax({
                url: $form.attr('action'),
                type: 'POST',
                data: $form.serialize(),
                success: function(result) {
					//alert(result);
		//$("#response").html('Thank! For registration');
               // $('#registerFormVenue').hide();
                window.location.href =baseurl+"thank_you_for_registration";
					
                    // ... Process the result ...
                }
            });
        });

/////////////////////////////Other Form Validation/////////////////////////////////////////		
       
        $('#myForm').formValidation();
	$('#gig_add').formValidation();
                //$('#upload_file_media').formValidation();
                
                
/////////////////////////////////Registration page Divs Show Hide//////////////////////////            
		

///////////////////////////////Venue Search On Enter Kye////////////////////////////////////		
		$("#venueid").keypress(function(e) {
			if(e.which == 13 ) {
				var venue = $("#venueid").val();
				if(venue==''){
					venue='all';
				}
		 
	    window.location.href =baseurl+"search/index/venue/"+venue+"";
			}
		});

///////////////////////////////Artist Search On Enter Kye////////////////////////////////////		
		$("#artistid").keypress(function(e) {
			if(e.which == 13 ) {
				var artist = $("#artistid").val();
				if(artist==''){
					artist='all';
				}
		 
	    window.location.href =baseurl+"artist_search/index/artist/"+artist+"";
			}
		});
		
////////////////////////////////Gigs Search on Enter Kye//////////////////////////////////////
$("#gigid").keypress(function(e) {
			if(e.which == 13 ) {
				var gigs = $("#gigid").val();
				if(gigs==''){
					gigs='all';
				}
		 
	    window.location.href =baseurl+"gigs_search/index/gigs/"+gigs+"";
			}
		});
                
    $("#seach_btn").click(function() { 
		var venue = $("#venueid").val();
		if(venue==''){
			venue='all';
		}
		//alert(venue);		 
	    window.location.href =baseurl+"search/index/venue/"+venue+"";

	   });
	   $("#seach_btn_gig").click(function() { 
		var gigs = $("#gigid").val();
		if(gigs==''){
			gigs='all';
		}
		//alert(venue);		 
	    window.location.href =baseurl+"gigs_search/index/gigs/"+gigs+"";

	   });
           $("#seach_btn_artist").click(function() { 
		var artist = $("#artistid").val();
		if(artist==''){
			artist='all';
		}
		//alert(venue);		 
	    window.location.href =baseurl+"artist_search/index/artist/"+artist+"";

	   });
           
///////////////////////////////Add Band member Hide Show Divs//////////////////////////   
	  $("#AddNewMember").hide();
	   $("#btn_add_band").click(function() { 
		   $("#AddNewMember").show();
	   });
	   $("#AddNewsouncloud").hide();
	   $("#btn_add_sound_link").click(function() { 
		   //$("#AddNewsouncloud").show();
		    $('#AddNewsouncloud').slideToggle();
	   });
	    $("#AddNewvideo").hide();
	   $("#btn_add_video").click(function() { 
		   //$("#AddNewsouncloud").show();
		    $('#AddNewvideo').slideToggle();
	   });
	   
	   $("#editmember").click(function(e) {

		 e.preventDefault();
          var editid = $(this).data('id');  
		  alert(editid);
		});	   

});

$( "#ajax_seach_btn" ).click(function() {
  //alert( "Handler for .click() called." );
  applyPagination();
  search_result();
});
$("#venuesearchid").click(function() {
     applyPagination();
    search_result();
});


applyPagination();
 function applyPagination() {
     //alert('ok');
        $("#ajax_pagingsearc a").click(function() {
        var url = $(this).attr("href");
    //alert(url);
   var art_genre_id=$("#art_genre_id").val();
   var start_date=$("#datepicker-01").val();
   var end_date=$("#datepicker-02").val();
   var ven_name=$("#ven_name").val();
   var ven_city=$("#ven_city").val();
		var genre_id = [];
            if(art_genre_id==null){
               // alert(art_genre_id);
                art_genre_id=genre_id;
            };
   
            var alldata= {
	    	     	genre:art_genre_id,
			ajax:"1",
			start_date:start_date,
                        end_date:end_date,
                        ven_name:ven_name,
                        ven_city:ven_city
	    	     };
          $.ajax({
            type: "POST",
            data: alldata,
            url: url,
           success: function(msg) {
			   //alert(msg);
            
              $("#ajax_results").html(msg);
              applyPagination();
            }
          });
        return false;
        });
      }
      
function search_result(){
 
            var art_genre_id=$("#art_genre_id").val();
	    var start_date=$("#datepicker-01").val();
            var end_date=$("#datepicker-02").val();
             var ven_name=$("#ven_name").val();
              var ven_city=$("#ven_city").val();
			var genre_id = [];
            if(art_genre_id==null){
                //alert(art_genre_id);
                art_genre_id=genre_id;
            };
            var alldata= {
	    	    genre:art_genre_id,
		    ajax:"1",
		    start_date:start_date,
                    end_date:end_date,
                    ven_name:ven_name,
                    ven_city:ven_city
	    	     };
                    // alert(start_date);
                    // alert(end_date);
           // alert(alldata);
             var response = '';
            $.ajax({ 
	        type: "POST",              
                url: baseurl+"search/search_filters",
                data: alldata,
		dataType: "html",
		           success : function(text)
		           {
		               response = text;
                               //alert(response);
		               $("#ajax_results").html(text);
                                applyPagination();
		               
		              
	               }
		  });
        }
		
///////////////////////////////////Artist Search Section///////////////////////////////////////


$( "#ajax_seach_btn_artist" ).click(function() {
 // alert( "Handler for .click() called." );
  applyPaginationArtist();
  artist_search_result();
});
$( "#searchartistid" ).click(function() {
 // alert( "Handler for .click() called." );
  applyPaginationArtist();
  artist_search_result();
});

applyPaginationArtist();
 function applyPaginationArtist() {
     //alert('ok');
        $("#ajax_pagingsearc a").click(function() {
        var url = $(this).attr("href");
   // alert(url);
   var art_genre_id=$("#art_genre_id").val();
   var artist_name=$("#artist_name").val();
   var ven_city=$("#ven_city").val();
   var art_gig_type=$("#art_gig_type").val();
       
       var art_type=[];
       if(art_gig_type==null){
           art_gig_type=art_type;
       }
		var genre_id = [];
            if(art_genre_id==null){
                art_genre_id=genre_id;
            };
   
            var alldata= {
	    	     	genre:art_genre_id,
			        ajax:"1",
                    artist_name:artist_name,
                    ven_city:ven_city,
                    art_type:art_gig_type
	    	     };
          $.ajax({
            type: "POST",
            data: alldata,
            url: url,
           success: function(msg) {
			   //alert(msg);            
              $("#ajax_results").html(msg);
              applyPaginationArtist();
            }
          });
        return false;
        });
      }




function artist_search_result(){
 
            var art_genre_id=$("#art_genre_id").val();
			//alert(art_genre_id);
            var artist_name=$("#artist_name").val();
			//alert(artist_name);
            var ven_city=$("#ven_city").val();
             var art_gig_type=$("#art_gig_type").val();
       
       var art_type=[];
       if(art_gig_type==null){
           art_gig_type=art_type;
       }
			var genre_id = [];
            if(art_genre_id==null){
                art_genre_id=genre_id;
            };
            var alldata= {
	    	        genre:art_genre_id,
		            ajax:"1",
                    artist_name:artist_name,
                    ven_city:ven_city,
                    art_type:art_gig_type
	    	     };
            //alert(alldata);
             var response = '';
            $.ajax({ 
	        type: "POST",              
                url: baseurl+"artist_search/search_filters",
                data: alldata,
		        dataType: "html",
		           success : function(text)
		           {
		               response = text;
                               //alert(response);
		               $("#ajax_results").html(text);
                                applyPaginationArtist();
		               
		              
	               }
		  });
        }
        
        
        
 ///////////////////////////////////Gigs Search Section///////////////////////////////////////

$( "#ajax_seach_btn_gigs" ).click(function() {
 //alert( "Handler for .click() called." );
  applyPaginationGigs();
  gigs_search_result();
});
$( "#gigsearchname" ).click(function() {
  //alert( "Handler for .click() called." );
  applyPaginationGigs();
  gigs_search_result();
});

applyPaginationGigs();
 function applyPaginationGigs() {
     //alert('ok');
        $("#ajax_pagingsearc a").click(function() {
        var url = $(this).attr("href");
   //alert(url);
   var gig_name=$("#gig_name").val();
   var gig_city=$("#gig_city").val();
   //var gig_gig_type=$("#gig_gig_type").val();
   var start_date=$("#datepicker-01").val();
   var end_date=$("#datepicker-02").val();
       
     /*  var gig_type=[];
       if(gig_gig_type==null){
           gig_gig_type=gig_type;
       }*/
		
            var alldata= {
                    ajax:"1",
                    gig_name:gig_name,
                    gig_city:gig_city,
                   // gig_type:gig_gig_type,
                    start_date_to:start_date,
                    end_date_to:end_date
	    	     };
          $.ajax({
            type: "POST",
            data: alldata,
            url: url,
           success: function(msg) {
			   //alert(msg);            
              $("#ajax_results").html(msg);
              applyPaginationGigs();
            }
          });
        return false;
        });
      }




function gigs_search_result(){
 
   var gig_name=$("#gig_name").val();
   var gig_city=$("#gig_city").val();
   //var gig_gig_type=$("#gig_gig_type").val();
   var start_date=$("#datepicker-01").val();
   var end_date=$("#datepicker-02").val();
        //alert(start_date);
       // alert(end_date);
      /* var gig_type=[];
       if(gig_gig_type==null){
           gig_gig_type=gig_type;
       }*/
		
            var alldata= {
                    ajax:"1",
                    gig_name:gig_name,
                    gig_city:gig_city,
                    //gig_type:gig_gig_type,
                    start_date_to:start_date,
                    end_date_to:end_date
	    	     };
            //alert(alldata);
             var response = '';
            $.ajax({ 
	        type: "POST",              
                url: baseurl+"gigs_search/search_filters",
                data: alldata,
		        dataType: "html",
		           success : function(text)
		           {
		               response = text;
                               //alert(response);
		               $("#ajax_results").html(text);
                                applyPaginationGigs();
		               
		              
	               }
		  });
        }
  $('#demo').click(function(){
 // alert('ok');
  $.ajax({
     type: "POST",
     url: baseurl+"venue_dashboard/sessions"
   });
});

 $('#link2').click(function(){
  $.ajax({
     type: "POST",
     url: baseurl+"artist_dashboard/sessions"
   });
});
$('#link3').click(function(){
  $.ajax({
     type: "POST",
     url: baseurl+"artist_dashboard/sessions3"
   });
});
$('#link4').click(function(){
  $.ajax({
     type: "POST",
     url: baseurl+"artist_dashboard/sessions4"
   });
});