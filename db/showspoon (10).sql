-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 12, 2015 at 01:59 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `showspoon`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE IF NOT EXISTS `admin_users` (
`id` int(11) NOT NULL,
  `username` varchar(150) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `user_type` enum('SA','A') DEFAULT 'SA' COMMENT 'SA: Super Admin,A: Admin'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `username`, `email`, `password`, `block`, `user_type`) VALUES
(1, 'admin', 'admin@admin.com', '21232f297a57a5a743894a0e4a801fc3', 0, 'SA');

-- --------------------------------------------------------

--
-- Table structure for table `artist`
--

CREATE TABLE IF NOT EXISTS `artist` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `genre_id` varchar(255) NOT NULL,
  `gig_type_id` varchar(255) NOT NULL,
  `band_type_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `zip` int(11) NOT NULL,
  `city` varchar(250) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_on` date NOT NULL,
  `fb_id` varchar(255) NOT NULL,
  `fb_key` varchar(255) NOT NULL,
  `fb_fname` varchar(255) NOT NULL,
  `fb_lname` varchar(255) NOT NULL,
  `fb_email` varchar(255) NOT NULL,
  `short_description` text NOT NULL,
  `biography` text NOT NULL,
  `rating` int(11) NOT NULL,
  `location` varchar(250) NOT NULL,
  `Login_Key` double NOT NULL,
  `fb_page_link` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COMMENT='An Artist table For registration';

--
-- Dumping data for table `artist`
--

INSERT INTO `artist` (`id`, `name`, `genre_id`, `gig_type_id`, `band_type_id`, `status`, `zip`, `city`, `email`, `password`, `created_on`, `fb_id`, `fb_key`, `fb_fname`, `fb_lname`, `fb_email`, `short_description`, `biography`, `rating`, `location`, `Login_Key`, `fb_page_link`) VALUES
(1, 'Blaze', '3,5', '1', 1, 0, 26, '1', 'admin@example.com', '$2y$10$fmfZzyGM8mgY.VILZHcQZ.VUYphSw5Tk1GYP7is.eU3g9D/MN5ZS6', '2015-10-11', '', '', '', '', '', 'The band was formed in the early 1990s as a cover band called the "Village Idiots" by Chad Kroeger, Ryan Peake, Mike Kroeger and Brandon kroeger.', 'The Blaze is a Canadian rock band formed in 1995 in Hanna, Alberta. The band is composed of guitarist and lead vocalist Chad Kroeger, guitarist, keyboardist and backing vocalist Ryan Peake, bassist Mike Kroeger and dunner Daiel Adair. The band went through a few dummer changes betweek 1995 and 2005, achieving it''s current form when Adair replaced drummer Ryan Vikedal.', 0, 'lahore', 18614458880311, 'https://www.facebook.com/'),
(2, 'The Blaze', '1,2', '2', 1, 0, 0, '1', 'admin@example.c', '$2y$10$qvC4wsFJ118XrUNreFVV6uye4p0Rg3LDbGsUI6.hCx3pDrK4Uvpmi', '2015-10-11', '', '', '', '', '', '', '', 0, 'Lahore', 0, ''),
(4, 'Power Rock House', '2', '1', 1, 0, 0, '1', 'admin@example.co', '$2y$10$hhq8de7L8SJPLSRftAM5deNcQ750ypWl5L/C7oVscYu9Snl5e52h6', '2015-10-11', '', '', '', '', '', '', '', 0, 'Lahore', 0, ''),
(5, 'As', '2,4,6', '2,3', 3, 0, 0, '1', 'admin@example.comhh', '$2y$10$Lq4w8D3gNz9723IDzwVBFepTSl.keP7G0YW3cuvRWQT.7hULS7FY6', '2015-10-21', '', '', '', '', '', '', '', 0, 'di', 0, ''),
(6, 'Arif', '1,3', '1,2', 1, 0, 2222, '1', 'admin@example.comm', '$2y$10$Ngnbe10025gGL89Kz.jpOe.Rt4NLA4e6O5xUGuMYCy0yZQu.pGkhm', '2015-10-21', '', '', '', '', '', 'g', 'hg', 0, 'Oslo city', 0, ''),
(7, 'fas', '2', '1', 1, 0, 0, '2', 'admin@example.comm', '$2y$10$T7zqkKkzEGiwd.nvvrXsMOw3nK/jIvkKk2OtL.qpoTc/J/NqUDwu6', '2015-10-21', '', '', '', '', '', '', '', 0, 'sad', 0, ''),
(8, 'sd', '2', '1', 1, 0, 0, '1', 'admin@example.commmm', '$2y$10$qmY21SEAkIF8h5iB5aF6KObU6FvzyuOJL9iAowazW7YjP14N.ruGq', '2015-11-03', '', '', '', '', '', '', '', 0, 'oslo', 0, ''),
(9, 'fsda', '5', '4', 1, 0, 0, '1', 'admin@example.com', '$2y$10$Ms2DzMIHw6UG20ImFDXCDeEApi39xmcSW1mv1IfIhETmZBqiQMdpu', '2015-11-04', '', '', '', '', '', '', '', 0, 'oslo', 0, ''),
(10, 'h', '2', '2', 2, 0, 0, '4', 'admin@example.com', '$2y$10$cGkai7uB792YGjBdm8VPkOdxpKi3kBwJaIQVCKKoH8xO9HZu14qzO', '2015-11-04', '', '', '', '', '', '', '', 0, 'oslo', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `artist_band_type`
--

CREATE TABLE IF NOT EXISTS `artist_band_type` (
`id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist_band_type`
--

INSERT INTO `artist_band_type` (`id`, `name`) VALUES
(1, 'Solo'),
(2, 'Duo'),
(3, 'Group'),
(4, 'Band');

-- --------------------------------------------------------

--
-- Table structure for table `artist_genre`
--

CREATE TABLE IF NOT EXISTS `artist_genre` (
`id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist_genre`
--

INSERT INTO `artist_genre` (`id`, `name`) VALUES
(1, 'Acapella'),
(2, 'Alternative'),
(3, 'Acoustic'),
(4, 'Blues'),
(5, 'Childrens'),
(6, 'Cover'),
(7, 'Classical'),
(8, 'Country'),
(9, 'Dance'),
(10, 'Electronic'),
(11, 'Experimental'),
(12, 'Folk'),
(13, 'Funk'),
(14, 'Gospel'),
(15, 'Hardcore'),
(16, 'Hip Hop'),
(17, 'indie'),
(18, 'Instrumental'),
(19, 'International'),
(20, 'Jazz'),
(21, 'Latin'),
(22, 'Mariachi'),
(23, 'Metal'),
(24, 'Pop'),
(25, 'Producer'),
(26, 'Punk'),
(27, 'R&B'),
(28, 'Rap'),
(29, 'Religious'),
(30, 'Rock'),
(31, 'Rockabilly'),
(32, 'Reggae'),
(33, 'Ska'),
(34, 'Soul'),
(35, 'Western');

-- --------------------------------------------------------

--
-- Table structure for table `artist_gigs`
--

CREATE TABLE IF NOT EXISTS `artist_gigs` (
`id` int(11) NOT NULL,
  `gig_name` varchar(250) NOT NULL,
  `location` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `zip` varchar(100) NOT NULL,
  `city` varchar(250) NOT NULL,
  `created_on` date NOT NULL,
  `type` varchar(250) NOT NULL DEFAULT '1',
  `artist_id` int(11) NOT NULL,
  `venues_id` int(11) NOT NULL,
  `image_url` varchar(250) NOT NULL,
  `Accepting_application_start_date` date NOT NULL,
  `accepting_application_end_date` date NOT NULL,
  `Review_date` date NOT NULL,
  `Max_croner_amount` varchar(250) NOT NULL,
  `travel&accomodation` tinyint(1) NOT NULL,
  `parcentage_of_ticketsale` int(11) NOT NULL,
  `food&drink_ticket` tinyint(1) NOT NULL,
  `numbers_of_bands` int(11) NOT NULL,
  `requirements` varchar(250) NOT NULL,
  `gig_time` time NOT NULL,
  `gig_time_end` time NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist_gigs`
--

INSERT INTO `artist_gigs` (`id`, `gig_name`, `location`, `description`, `zip`, `city`, `created_on`, `type`, `artist_id`, `venues_id`, `image_url`, `Accepting_application_start_date`, `accepting_application_end_date`, `Review_date`, `Max_croner_amount`, `travel&accomodation`, `parcentage_of_ticketsale`, `food&drink_ticket`, `numbers_of_bands`, `requirements`, `gig_time`, `gig_time_end`, `is_active`) VALUES
(1, 'Extreme Power Event', 'Ruselokkveien', 'Ectreme Power event based at the Ospedaletto space yound, trendy place for all young people of Sesto Calende and surroundings, in the province of Varese. A radio young, dynamic with a great desire to entertain and have fun! In particular, Neverwas Radio want to dedicate a space to emerging bands / independent with its program "UNDER THE BRIDGES". "UNDER THE BRIDGES" is a radio streaming on http://www.ospedaletto.org/blog/radio-web or at http://never-wasradio2.listen2myradio.com (SERVER SUPPORT). Our Mission is to offer a program devoted mainly to the best bands and EMERGING INDEPENDENT of varied music.', '26 0251', '1', '2015-10-14', '1,3', 2, 0, '', '2015-10-23', '2015-11-23', '2015-11-12', '40', 1, 2015, 0, 1, 'gdf', '00:00:00', '00:00:00', 1),
(2, 'rrrr', 'klkkjlsdjffffff', 'ook', '444', '1', '2015-10-14', '4', 1, 0, '', '2015-10-24', '2015-11-24', '2015-11-12', '5', 0, 0, 0, 1, '', '00:00:00', '00:00:00', 1),
(3, 'Abitudini Live', 'City Center, Oslo', 'A rock festival which will be organized by the citizens foundation and sponsored by Jzz FM 99. A three day concert which will', '67000', '2', '2015-10-14', '3', 1, 0, '', '2015-10-29', '2015-11-29', '2015-11-12', '2', 1, 0, 0, 1, '', '12:56:00', '00:00:00', 1),
(5, 'Aqeel', 'Has', 'ghhgdf', '53435', '1', '2015-10-14', '1,2', 0, 1, '', '2015-10-30', '2015-11-30', '0000-00-00', '', 0, 0, 0, 1, '', '12:56:00', '00:00:00', 1),
(6, 'orange', 'Market', 'A rock festival which will be organized by the citizens foundation and sponsored by Jzz FM 99. A three day concert which will', '76888', '1', '2015-10-14', '2,3', 0, 1, '', '2015-10-31', '2015-11-30', '0000-00-00', '', 0, 0, 0, 1, '', '12:56:00', '00:00:00', 1),
(7, 'Abitudini Live', 'City Center, Oslo', 'A rock festival which will be organized by the citizens foundation and sponsored by Jzz FM 99. A three day concert which will', '67000', '3', '2015-10-14', '3', 0, 1, '', '2015-10-24', '2015-11-24', '0000-00-00', '', 0, 0, 0, 1, '', '12:56:00', '00:00:00', 1),
(8, 'The long Road', 'olso', '', '4000', '2', '2015-10-14', '3,4,5', 0, 1, '', '2015-10-26', '2015-11-26', '0000-00-00', '40000', 0, 3, 0, 1, '', '12:56:00', '00:00:00', 1),
(13, 'asd', 'asfd', 'ok', '0', '2', '2015-10-20', '1', 0, 2, 'venue1.jpg', '2015-10-27', '2015-11-20', '2015-11-12', '45', 0, 0, 0, 1, '', '12:56:00', '00:00:00', 0),
(15, 'ds', 'fds', 'sa dfsd', 'fds', '4', '2015-10-25', '2,3', 1, 0, '', '2015-10-21', '2015-10-10', '2015-11-09', '53', 1, 24, 0, 3, '', '12:56:00', '00:00:00', 1),
(19, 'rrrr', 'klkkjlsdjffffff', '', '444', '3', '2015-10-25', '3,4', 0, 1, '11705234_10153147699089527_8850920176047805505_n.jpg', '2015-10-28', '2015-11-20', '0000-00-00', '', 0, 0, 0, 1, '', '12:56:00', '00:00:00', 1),
(20, 'rrrr', 'klkkjlsdjffffff', '', '444', '1', '2015-10-25', '4', 0, 1, '11868786_516674191813355_1748757073_n.jpg', '2015-10-27', '2015-11-27', '0000-00-00', '', 0, 0, 0, 1, '', '12:56:00', '00:00:00', 1),
(54, 'daf', 'fa', 'fsda', 'fsd', '4', '2015-11-09', '1', 1, 0, 'cross.png', '0000-00-00', '0000-00-00', '0000-00-00', 'fsd', 0, 0, 0, 0, '', '00:00:00', '00:00:00', 1),
(55, 'Aqeel', 'fsda', 'tt', 'dsa', '3', '2015-11-10', '1', 6, 0, 'slide21.jpg', '2015-11-03', '2015-11-11', '0000-00-00', 'fa', 0, 0, 0, 0, '', '00:00:00', '00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `artist_gig_type`
--

CREATE TABLE IF NOT EXISTS `artist_gig_type` (
`id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist_gig_type`
--

INSERT INTO `artist_gig_type` (`id`, `name`) VALUES
(1, 'Open Mic'),
(2, 'One Time Gig'),
(3, 'Opening Act'),
(4, 'Others');

-- --------------------------------------------------------

--
-- Table structure for table `artist_media`
--

CREATE TABLE IF NOT EXISTS `artist_media` (
`id` int(11) NOT NULL,
  `type` varchar(250) NOT NULL,
  `url` varchar(250) NOT NULL,
  `caption` varchar(250) NOT NULL,
  `artist_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist_media`
--

INSERT INTO `artist_media` (`id`, `type`, `url`, `caption`, `artist_id`) VALUES
(1, '1', 'https://www.youtube.com/watch?v=pWdKf3MneyI', 'ok', 1),
(10, '1', 'https://www.youtube.com/watch?v=pWdKf3MneyI', 'dsa', 1),
(13, '2', 'https://soundcloud.com/mix_songs', 'okds', 6),
(14, '1', 'https://www.youtube.com/watch?v=pWdKf3MneyI', 'ko', 1),
(15, '1', 'https://www.youtube.com/watch?v=pWdKf3MneyI', 'fs', 1),
(22, '2', 'https://soundcloud.com/guardianfootballweekly', 'as', 1),
(24, '2', 'https://soundcloud.com/rahatfatehalikhan01', 'sa', 1),
(25, '2', 'https://soundcloud.com/rahatfatehalikhan01/teri-meri', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `artist_media_type`
--

CREATE TABLE IF NOT EXISTS `artist_media_type` (
`id` int(11) NOT NULL,
  `artist_media_id` int(11) NOT NULL,
  `type_name` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist_media_type`
--

INSERT INTO `artist_media_type` (`id`, `artist_media_id`, `type_name`) VALUES
(1, 0, 'Youtube url'),
(2, 0, 'Soundcloud url');

-- --------------------------------------------------------

--
-- Table structure for table `artist_member`
--

CREATE TABLE IF NOT EXISTS `artist_member` (
`id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `role` varchar(250) NOT NULL,
  `image_url` varchar(250) NOT NULL,
  `artist_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist_member`
--

INSERT INTO `artist_member` (`id`, `name`, `role`, `image_url`, `artist_id`) VALUES
(6, 'Ryan Peake', 'Bass Guiatarist', '', 1),
(7, 'sda', 'asfd', '', 1),
(9, 'dfsg', 'df', '0ea1d9149a60ee393f69c56471a4c7bf.png', 6),
(12, 'dfsa', 'fsa', 'f2cff19762dd6247e0bd3f2103d01fe4.png', 6);

-- --------------------------------------------------------

--
-- Table structure for table `artist_photo`
--

CREATE TABLE IF NOT EXISTS `artist_photo` (
`id` int(11) NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `set_featured` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='one artist has many photo';

--
-- Dumping data for table `artist_photo`
--

INSERT INTO `artist_photo` (`id`, `image_url`, `caption`, `artist_id`, `set_featured`) VALUES
(1, 'f0808994a50decdb2f914282f14d3a75.jpg', '', 6, 1),
(2, '73da5702ce03cfa2905cb8cfa38df24b.jpg', '', 6, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
`CityID` int(11) NOT NULL,
  `CityName` varchar(32) NOT NULL,
  `CountryID` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`CityID`, `CityName`, `CountryID`) VALUES
(1, 'Ardendal', 1),
(2, 'Bergen', 0),
(3, 'Bodø', 0),
(4, 'Drammen', 0),
(5, 'Egersund', 0),
(6, 'Farsund', 0),
(7, 'Flekkefjord', 0),
(8, 'Florø', 0),
(9, 'Fredrikstad', 0),
(10, 'Grimstad', 0),
(11, 'Halden', 0),
(12, 'Hamar', 0),
(13, 'Hammerfest', 0),
(14, 'Harstad', 0),
(15, 'Haugesund', 0),
(16, 'Holmestrand', 0),
(17, 'Horten', 0),
(18, 'Hønefoss', 0),
(19, 'Kongsberg', 0),
(20, 'Kongsvinger', 0),
(21, 'Kristiansand', 0),
(22, 'Kristiansund', 0),
(23, 'Larvik', 0),
(24, 'Lillehammer', 0),
(25, 'Mandal', 0),
(26, 'Molde', 0),
(27, 'Moss', 0),
(28, 'Namsos', 0),
(29, 'Narvik', 0),
(30, 'Notodden', 0),
(31, 'Oslo', 0),
(32, 'Porsgrunn', 0),
(33, 'Risør', 0),
(34, 'Sandefjord', 0),
(35, 'Sandnes', 0),
(36, 'Sarpsborg', 0),
(37, 'Skien', 0),
(38, 'Stavanger', 0),
(39, 'Steinkjer', 0),
(40, 'Søgne', 0),
(41, 'Tromsø', 0),
(42, 'Trondheim', 0),
(43, 'Tønsberg', 0),
(44, 'Vadsø', 0),
(45, 'Vardø', 0),
(46, 'Vennesla', 0),
(47, 'Ålesund', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gig_comments`
--

CREATE TABLE IF NOT EXISTS `gig_comments` (
`id` int(11) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `comments` text NOT NULL,
  `created_on` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gig_comments`
--

INSERT INTO `gig_comments` (`id`, `artist_id`, `venue_id`, `comments`, `created_on`) VALUES
(1, 1, 1, 'sdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfg', '2015-10-29'),
(2, 6, 1, 'fds', '2015-10-26'),
(3, 6, 6, 'fsda', '0000-00-00'),
(4, 6, 4, 'tttttttttttttt', '0000-00-00'),
(5, 1, 2, 'fdsffsdaf', '0000-00-00'),
(6, 1, 5, 'gggggggggggggggggggggggggggggggggggggggg', '0000-00-00'),
(7, 1, 2, 'ok how are you', '0000-00-00'),
(8, 6, 1, 'fsdafsdaf', '0000-00-00'),
(9, 1, 2, 'i am giving feedback to you ', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `gig_feedback`
--

CREATE TABLE IF NOT EXISTS `gig_feedback` (
`id` int(11) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `gig_id` int(11) NOT NULL,
  `created_on` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gig_feedback`
--

INSERT INTO `gig_feedback` (`id`, `artist_id`, `venue_id`, `message`, `gig_id`, `created_on`) VALUES
(1, 0, 1, 'okkh', 40, '2015-11-05'),
(2, 0, 1, 'ok Finr', 40, '2015-11-05'),
(3, 6, 0, 'artist gig', 29, '2015-11-05'),
(4, 6, 0, 'jh', 44, '2015-11-05'),
(5, 6, 0, 'dfsgdsfgsdf', 44, '2015-11-05'),
(6, 0, 1, 'i am giving the feedback to 47 gig details page', 47, '2015-11-06'),
(7, 1, 0, 'i am  artist giving the feedback to 47 gig details page', 47, '2015-11-06'),
(8, 1, 0, 'good one', 13, '2015-11-06'),
(9, 1, 0, 'sdsa', 13, '2015-11-06'),
(10, 0, 6, 'fdf', 19, '2015-11-10');

-- --------------------------------------------------------

--
-- Table structure for table `gig_messages`
--

CREATE TABLE IF NOT EXISTS `gig_messages` (
`id` int(11) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `gig_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gig_messages`
--

INSERT INTO `gig_messages` (`id`, `artist_id`, `venue_id`, `message`, `gig_id`, `created_on`) VALUES
(1, 1, 1, 'We are a rock club located in the heart of Oslo. ', 3, '2015-10-15 12:00:00'),
(2, 6, 1, 'We are a rock club located in the heart of Oslo. ', 8, '2015-10-14 07:33:33'),
(3, 1, 1, 'We are a rock club located in the heart of Oslo. ', 5, '2015-10-03 08:00:00'),
(8, 6, 1, 'good one', 52, '0000-00-00 00:00:00'),
(9, 6, 1, 'kk', 19, '0000-00-00 00:00:00'),
(10, 1, 2, 'kjjf', 13, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `gig_rating`
--

CREATE TABLE IF NOT EXISTS `gig_rating` (
`id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `rating_type` int(11) NOT NULL DEFAULT '0',
  `gig_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gig_rating`
--

INSERT INTO `gig_rating` (`id`, `rating`, `artist_id`, `venue_id`, `rating_type`, `gig_id`) VALUES
(1, 5, 1, 1, 1, 0),
(2, 2, 1, 1, 2, 0),
(3, 1, 1, 1, 3, 0),
(4, 4, 1, 1, 1, 0),
(5, 3, 6, 1, 3, 0),
(6, 3, 6, 1, 2, 0),
(7, 2, 6, 1, 1, 0),
(8, 5, 6, 6, 2, 0),
(9, 2, 6, 6, 1, 0),
(10, 4, 6, 6, 3, 0),
(11, 2, 6, 1, 3, 0),
(12, 2, 6, 1, 1, 0),
(13, 4, 6, 1, 2, 0),
(23, 4, 6, 0, 4, 1),
(24, 3, 6, 0, 4, 1),
(25, 1, 6, 0, 4, 1),
(26, 5, 6, 0, 4, 1),
(27, 5, 6, 0, 4, 1),
(28, 2, 1, 2, 1, 0),
(29, 3, 1, 2, 2, 0),
(30, 3, 1, 2, 3, 0),
(31, 3, 1, 5, 3, 0),
(32, 4, 1, 5, 1, 0),
(33, 3, 1, 5, 2, 0),
(34, 3, 1, 2, 2, 0),
(35, 4, 1, 2, 3, 0),
(36, 2, 1, 2, 1, 0),
(37, 4, 6, 0, 4, 29),
(38, 4, 6, 0, 4, 44),
(39, 5, 6, 0, 4, 44),
(40, 3, 6, 1, 3, 0),
(41, 3, 6, 1, 1, 0),
(42, 2, 6, 1, 2, 0),
(43, 2, 1, 0, 4, 47),
(44, 3, 1, 0, 4, 13),
(45, 4, 1, 0, 4, 13),
(46, 3, 1, 2, 3, 0),
(47, 4, 1, 2, 2, 0),
(48, 5, 1, 2, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
`id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `subject` varchar(250) NOT NULL,
  `type` tinyint(2) NOT NULL,
  `created_on` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `sender_id`, `receiver_id`, `message`, `subject`, `type`, `created_on`) VALUES
(1, 1, 1, 'send to artister', 'i am venue', 1, '0000-00-00'),
(2, 1, 2, 'send to other artiset', 'i am artiset ', 0, '0000-00-00'),
(3, 1, 2, 'send to am venue', 'i am artiset ', 2, '0000-00-00'),
(4, 1, 6, 'send to another venue', 'i am artiset ', 2, '0000-00-00'),
(5, 1, 9, 'send to another artiset', 'i am artiset ', 0, '0000-00-00'),
(6, 6, 2, 'send to arta', 'i am venue', 1, '0000-00-00'),
(7, 1, 7, 'gsg', 'gsdg', 0, '0000-00-00'),
(8, 1, 1, 'sda', 'fa', 0, '0000-00-00'),
(9, 6, 1, 'gdsgsdfgdsfg', 'gsdf', 0, '0000-00-00'),
(10, 6, 6, 'ttttttt', 'tt', 1, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE IF NOT EXISTS `requests` (
`id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `gig_id` int(11) NOT NULL,
  `created_on` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`id`, `sender_id`, `receiver_id`, `message`, `gig_id`, `created_on`) VALUES
(1, 6, 1, 'fd', 21, '2015-10-30'),
(2, 6, 2, 'fd', 21, '2015-10-30'),
(3, 1, 2, 'are you intrested in my giig', 38, '2015-11-05');

-- --------------------------------------------------------

--
-- Table structure for table `venues`
--

CREATE TABLE IF NOT EXISTS `venues` (
`id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `location` varchar(250) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `city` varchar(250) NOT NULL,
  `genre` varchar(250) NOT NULL,
  `age` int(11) NOT NULL,
  `descriptiopn` varchar(255) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `rider_document_url_pdf` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `fb_id` varchar(250) NOT NULL,
  `fb_key` varchar(250) NOT NULL,
  `fb_fname` varchar(250) NOT NULL,
  `fb_lname` varchar(250) NOT NULL,
  `fb_email` varchar(250) NOT NULL,
  `Login_Key` double NOT NULL,
  `ven_capacity` varchar(250) NOT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `venues`
--

INSERT INTO `venues` (`id`, `name`, `location`, `zip`, `city`, `genre`, `age`, `descriptiopn`, `latitude`, `longitude`, `rider_document_url_pdf`, `email`, `password`, `fb_id`, `fb_key`, `fb_fname`, `fb_lname`, `fb_email`, `Login_Key`, `ven_capacity`, `is_active`) VALUES
(1, 'Solid Rock Ring', 'oslo', '54', '1', '1,2', 40, 'We are a rock club located in the heart of Oslo. We have conducted over 100 gigs in the last 5 months. A favourite haven for rock artists and pop singers.', 0, 0, '', 'admin@example.com', '$2y$10$y2s71Buzhi2w84DEbDgRXeLpbmCuzN4OCxl1aWwSUDomQnEjv7dKO', '', '', '', '', '', 19014458881201, '34', 1),
(2, 'La', 'Kasur', '0', '1', '2,3,5', 0, '0', 0, 0, '', 'adin@example.com', '$2y$10$uqj/uCUO88.ZGpKuG1zJvuxlV/LPEw97SgTi45xZg9AC62fNqBWUa', '', '', '', '', '', 0, '45', 1),
(4, 'Asdsda', 'Kasuar', '0', '2', '0,2,4', 0, '0', 0, 0, '', 'ch.aqeel_asghar@hot', '$2y$10$VFzZTake9TSfJCp36iBleeEysXvLxMFGRYfBdzP8vhwJJar6raslK', '', '', '', '', '', 0, '77', 1),
(5, 'kj', 'kajal', '0', '2', '1,2,3', 0, '0', 0, 0, '', 'ch.aqeel_asghar@hotmail.com', '$2y$10$bdlM57xB4fnP4pGuRuhWkudIha8yxqHNkeexUQVBQggvtItDE1rFS', '', '', '', '', '', 0, '77', 1),
(6, 'jgf', 'jhggfdsg', '34', '2', '2', 34, 'ok', 0, 0, '', 'admin@example.comm', '$2y$10$9nHXIDhEmF4TjDYBONdG4uV4s.MEXQZGu22MpR1DY60z27nqlDNzC', '', '', '', '', '', 0, '34', 1),
(7, 'Solid Rock Ring', 'oslo', '54', '1', '1,2', 40, 'We are a rock club located in the heart of Oslo. We have conducted over 100 gigs in the last 5 months. A favourite haven for rock artists and pop singers.', 0, 0, 'aqeelasghar_(2).pdf', 'admin@example.com', '$2y$10$9s1FxxhX2kRLMMS4k1YrhOvK1CRQVpe28oZzRDqV0x/vOvVDkt8UO', '', '', '', '', '', 0, '10', 1),
(8, 'La', 'Kasur', '0', '1', '2,3,5', 0, '0', 0, 0, '', 'admin@example.com', '$2y$10$uqj/uCUO88.ZGpKuG1zJvuxlV/LPEw97SgTi45xZg9AC62fNqBWUa', '', '', '', '', '', 0, '664', 1),
(9, 'as', 'lahore', '0', '1', '1,2', 0, '0', 0, 0, '', 'admin@example.com', '$2y$10$i.E5uccuAEV9yK.bbysueui5tUGL1/eFM7KJcdPUNBrYxnyJxPBMq', '', '', '', '', '', 0, '56', 1),
(10, 'Asdsda', 'Kasuar', '0', '2', '0,2,4', 0, '0', 0, 0, '', 'ch.aqeel_asghar@hot', '$2y$10$VFzZTake9TSfJCp36iBleeEysXvLxMFGRYfBdzP8vhwJJar6raslK', '', '', '', '', '', 0, '67', 1),
(11, 'kj', 'kajal', '0', '2', '1,2,3', 0, '0', 0, 0, '', 'ch.aqeel_asghar@hotmail.com', '$2y$10$bdlM57xB4fnP4pGuRuhWkudIha8yxqHNkeexUQVBQggvtItDE1rFS', '', '', '', '', '', 0, '77', 1),
(12, 'Solid Rock Ring', 'oslo', '54', '1', '1,2', 40, 'We are a rock club located in the heart of Oslo. We have conducted over 100 gigs in the last 5 months. A favourite haven for rock artists and pop singers.', 0, 0, '', 'admin@example.comm', '$2y$10$9nHXIDhEmF4TjDYBONdG4uV4s.MEXQZGu22MpR1DY60z27nqlDNzC', '', '', '', '', '', 0, '34', 0),
(13, 'sd', 'gds', '', '1', '5', 0, '', 0, 0, '', 'admin@example.commn', '$2y$10$9hi8rlpcAKYsmOvNmVYAPezc5NhKMzXwf1aVJm/8hqiEFBQVMt31e', '', '', '', '', '', 0, '77', 1),
(14, 's', 'dsfa', '', '1', '1', 0, '', 0, 0, '', 'admin@example.comms', '$2y$10$WuUAwxKAhiA/UKNnFgQcPeF467VlMxSNtPJVqVe0gby2D6Bta98U.', '', '', '', '', '', 0, '77', 1),
(15, 'Solid Rock Ring', 'dfds', '546546', '4', '3', 0, '', 0, 0, '', 'admin@example.commm', '$2y$10$IhLjuf4LvdE3DIBf94Rfuevdex9hodtj.awZOC3HBL34cRcJPDcGS', '', '', '', '', '', 0, '9', 1);

-- --------------------------------------------------------

--
-- Table structure for table `venue_photo`
--

CREATE TABLE IF NOT EXISTS `venue_photo` (
`id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `set_featured` smallint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `venue_photo`
--

INSERT INTO `venue_photo` (`id`, `venue_id`, `url`, `set_featured`) VALUES
(2, 6, 'b84046636d26cbc6b5b06729c51bf0a4.jpg', 1),
(4, 2, '4efeeaa81b8081926d76e08711ae90f1.jpg', 1),
(5, 1, 'dc88c7d2f117bc7f08c33917afa6be27.jpg', 0),
(6, 1, 'ff37945381060be6a82dcf275a7b9708.jpg', 1),
(7, 2, 'd7c0d6142f545d535c485dd283bb9cb6.jpg', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist`
--
ALTER TABLE `artist`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist_band_type`
--
ALTER TABLE `artist_band_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist_genre`
--
ALTER TABLE `artist_genre`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist_gigs`
--
ALTER TABLE `artist_gigs`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist_gig_type`
--
ALTER TABLE `artist_gig_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist_media`
--
ALTER TABLE `artist_media`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist_media_type`
--
ALTER TABLE `artist_media_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist_member`
--
ALTER TABLE `artist_member`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist_photo`
--
ALTER TABLE `artist_photo`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
 ADD PRIMARY KEY (`CityID`);

--
-- Indexes for table `gig_comments`
--
ALTER TABLE `gig_comments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gig_feedback`
--
ALTER TABLE `gig_feedback`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gig_messages`
--
ALTER TABLE `gig_messages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gig_rating`
--
ALTER TABLE `gig_rating`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venues`
--
ALTER TABLE `venues`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venue_photo`
--
ALTER TABLE `venue_photo`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `artist`
--
ALTER TABLE `artist`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `artist_band_type`
--
ALTER TABLE `artist_band_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `artist_genre`
--
ALTER TABLE `artist_genre`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `artist_gigs`
--
ALTER TABLE `artist_gigs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `artist_gig_type`
--
ALTER TABLE `artist_gig_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `artist_media`
--
ALTER TABLE `artist_media`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `artist_media_type`
--
ALTER TABLE `artist_media_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `artist_member`
--
ALTER TABLE `artist_member`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `artist_photo`
--
ALTER TABLE `artist_photo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
MODIFY `CityID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `gig_comments`
--
ALTER TABLE `gig_comments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `gig_feedback`
--
ALTER TABLE `gig_feedback`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `gig_messages`
--
ALTER TABLE `gig_messages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `gig_rating`
--
ALTER TABLE `gig_rating`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `venues`
--
ALTER TABLE `venues`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `venue_photo`
--
ALTER TABLE `venue_photo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
