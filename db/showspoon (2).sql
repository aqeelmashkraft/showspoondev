-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 25, 2015 at 08:03 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `showspoon`
--

-- --------------------------------------------------------

--
-- Table structure for table `artist`
--

CREATE TABLE IF NOT EXISTS `artist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `genre_id` varchar(255) NOT NULL,
  `gig_type_id` varchar(255) NOT NULL,
  `band_type_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `zip` int(11) NOT NULL,
  `city` varchar(250) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_on` date NOT NULL,
  `fb_id` varchar(255) NOT NULL,
  `fb_key` varchar(255) NOT NULL,
  `fb_fname` varchar(255) NOT NULL,
  `fb_lname` varchar(255) NOT NULL,
  `fb_email` varchar(255) NOT NULL,
  `short_description` text NOT NULL,
  `biography` text NOT NULL,
  `rating` int(11) NOT NULL,
  `location` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='An Artist table For registration' AUTO_INCREMENT=8 ;

--
-- Dumping data for table `artist`
--

INSERT INTO `artist` (`id`, `name`, `genre_id`, `gig_type_id`, `band_type_id`, `status`, `zip`, `city`, `email`, `password`, `created_on`, `fb_id`, `fb_key`, `fb_fname`, `fb_lname`, `fb_email`, `short_description`, `biography`, `rating`, `location`) VALUES
(1, 'Blaze', '3,5', '2', 2, 0, 26, '1', 'admin@example.com', '$2y$10$gGV0APeb5AifPiPBHATsau4ogYdmn3YUUWiB4QOsgyiGAUx5aSUuS', '2015-10-11', '', '', '', '', '', 'The band was formed in the early 1990s as a cover band called the "Village Idiots" by Chad Kroeger, Ryan Peake, Mike Kroeger and Brandon kroeger.', 'The Blaze is a Canadian rock band formed in 1995 in Hanna, Alberta. The band is composed of guitarist and lead vocalist Chad Kroeger, guitarist, keyboardist and backing vocalist Ryan Peake, bassist Mike Kroeger and dunner Daiel Adair. The band went through a few dummer changes betweek 1995 and 2005, achieving it''s current form when Adair replaced drummer Ryan Vikedal.', 0, 'lahore'),
(2, 'The Blaze', '1,2', '2', 1, 0, 0, '1', 'admin@example.c', '$2y$10$qvC4wsFJ118XrUNreFVV6uye4p0Rg3LDbGsUI6.hCx3pDrK4Uvpmi', '2015-10-11', '', '', '', '', '', '', '', 0, 'Lahore'),
(4, 'Power Rock House', '2', '1', 1, 0, 0, '1', 'admin@example.co', '$2y$10$hhq8de7L8SJPLSRftAM5deNcQ750ypWl5L/C7oVscYu9Snl5e52h6', '2015-10-11', '', '', '', '', '', '', '', 0, 'Lahore'),
(5, 'As', '2,4,6', '2,3', 3, 0, 0, '1', 'admin@example.comhh', '$2y$10$Lq4w8D3gNz9723IDzwVBFepTSl.keP7G0YW3cuvRWQT.7hULS7FY6', '2015-10-21', '', '', '', '', '', '', '', 0, 'di'),
(6, 'Arif', '1,3', '1', 1, 0, 0, '2', 'admin@example.comm', '$2y$10$Ngnbe10025gGL89Kz.jpOe.Rt4NLA4e6O5xUGuMYCy0yZQu.pGkhm', '2015-10-21', '', '', '', '', '', '', '', 0, 'Oslo city'),
(7, 'fas', '2', '1', 1, 0, 0, '2', 'admin@example.comm', '$2y$10$T7zqkKkzEGiwd.nvvrXsMOw3nK/jIvkKk2OtL.qpoTc/J/NqUDwu6', '2015-10-21', '', '', '', '', '', '', '', 0, 'sad');

-- --------------------------------------------------------

--
-- Table structure for table `artist_band_type`
--

CREATE TABLE IF NOT EXISTS `artist_band_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `artist_band_type`
--

INSERT INTO `artist_band_type` (`id`, `name`) VALUES
(1, 'Solo'),
(2, 'Duo'),
(3, 'Group'),
(4, 'Band');

-- --------------------------------------------------------

--
-- Table structure for table `artist_genre`
--

CREATE TABLE IF NOT EXISTS `artist_genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `artist_genre`
--

INSERT INTO `artist_genre` (`id`, `name`) VALUES
(1, 'Acapella'),
(2, 'Alternative'),
(3, 'Acoustic'),
(4, 'Blues'),
(5, 'Childrens'),
(6, 'Cover'),
(7, 'Classical'),
(8, 'Country'),
(9, 'Dance'),
(10, 'Electronic'),
(11, 'Experimental'),
(12, 'Folk'),
(13, 'Funk'),
(14, 'Gospel'),
(15, 'Hardcore'),
(16, 'Hip Hop'),
(17, 'indie'),
(18, 'Instrumental'),
(19, 'International'),
(20, 'Jazz'),
(21, 'Latin'),
(22, 'Mariachi'),
(23, 'Metal'),
(24, 'Pop'),
(25, 'Producer'),
(26, 'Punk'),
(27, 'R&B'),
(28, 'Rap'),
(29, 'Religious'),
(30, 'Rock'),
(31, 'Rockabilly'),
(32, 'Reggae'),
(33, 'Ska'),
(34, 'Soul'),
(35, 'Western');

-- --------------------------------------------------------

--
-- Table structure for table `artist_gigs`
--

CREATE TABLE IF NOT EXISTS `artist_gigs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gig_name` varchar(250) NOT NULL,
  `location` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `zip` varchar(100) NOT NULL,
  `city` varchar(250) NOT NULL,
  `created_on` date NOT NULL,
  `type` varchar(250) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `venues_id` int(11) NOT NULL,
  `image_url` varchar(250) NOT NULL,
  `Accepting_application_start_date` date NOT NULL,
  `accepting_application_end_date` date NOT NULL,
  `Review_date` date NOT NULL,
  `Max_croner_amount` varchar(250) NOT NULL,
  `travel&accomodation` tinyint(1) NOT NULL,
  `parcentage_of_ticketsale` int(11) NOT NULL,
  `food&drink_ticket` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `artist_gigs`
--

INSERT INTO `artist_gigs` (`id`, `gig_name`, `location`, `description`, `zip`, `city`, `created_on`, `type`, `artist_id`, `venues_id`, `image_url`, `Accepting_application_start_date`, `accepting_application_end_date`, `Review_date`, `Max_croner_amount`, `travel&accomodation`, `parcentage_of_ticketsale`, `food&drink_ticket`) VALUES
(1, 'Extreme Power Event', 'Ruselokkveien', 'Ectreme Power event based at the Ospedaletto space yound, trendy place for all young people of Sesto Calende and surroundings, in the province of Varese. A radio young, dynamic with a great desire to entertain and have fun! In particular, Neverwas Radio want to dedicate a space to emerging bands / independent with its program "UNDER THE BRIDGES". "UNDER THE BRIDGES" is a radio streaming on http://www.ospedaletto.org/blog/radio-web or at http://never-wasradio2.listen2myradio.com (SERVER SUPPORT). Our Mission is to offer a program devoted mainly to the best bands and EMERGING INDEPENDENT of varied music.', '26 0251', '1', '2015-10-14', '1,3', 1, 0, '', '2015-10-22', '2015-11-22', '0000-00-00', '40000', 0, 3, 1),
(2, 'rrrr', 'klkkjlsdjffffff', 'ook', '444', '1', '2015-10-14', '4', 1, 0, '', '0000-00-00', '0000-00-00', '0000-00-00', '', 0, 0, 0),
(3, 'Abitudini Live', 'City Center, Oslo', 'A rock festival which will be organized by the citizens foundation and sponsored by Jzz FM 99. A three day concert which will', '67000', '1', '2015-10-14', '3', 1, 0, '', '0000-00-00', '0000-00-00', '0000-00-00', '', 0, 0, 0),
(5, 'Aqeel', 'Has', 'ghhgdf', '53435', '1', '2015-10-14', '1,2', 0, 1, '', '2015-10-19', '2015-11-19', '0000-00-00', '', 0, 0, 0),
(6, 'orange', 'Market', 'A rock festival which will be organized by the citizens foundation and sponsored by Jzz FM 99. A three day concert which will', '76888', '1', '2015-10-14', '2,3', 0, 1, '', '2015-10-22', '2015-11-22', '0000-00-00', '', 0, 0, 0),
(7, 'Abitudini Live', 'City Center, Oslo', 'A rock festival which will be organized by the citizens foundation and sponsored by Jzz FM 99. A three day concert which will', '67000', '1', '2015-10-14', '3', 0, 1, '', '2015-10-24', '2015-11-24', '0000-00-00', '', 0, 0, 0),
(8, 'The long Road', 'olso', '', '4000', '2', '2015-10-14', '3,4,5', 0, 1, '', '2015-10-26', '2015-11-26', '0000-00-00', '40000', 0, 3, 0),
(13, 'asd', 'asfd', '', '0', '2', '2015-10-20', '1', 0, 1, 'venue1.jpg', '0000-00-00', '0000-00-00', '0000-00-00', '', 0, 0, 0),
(14, 'g', 'fsda', '', '0', '2', '2015-10-20', '1', 0, 1, 'artist-register-bg.jpg', '0000-00-00', '0000-00-00', '0000-00-00', '', 0, 0, 0),
(15, 'ds', 'fds', 'sa', 'fds', '2', '2015-10-25', '2,3', 1, 0, 'showspoon.sql', '0000-00-00', '0000-00-00', '0000-00-00', '', 0, 0, 0),
(19, 'rrrr', 'klkkjlsdjffffff', '', '444', '3', '2015-10-25', '3,4', 0, 1, '11705234_10153147699089527_8850920176047805505_n.jpg', '0000-00-00', '0000-00-00', '0000-00-00', '', 0, 0, 0),
(20, 'rrrr', 'klkkjlsdjffffff', '', '444', '1', '2015-10-25', '4', 0, 1, '11868786_516674191813355_1748757073_n.jpg', '0000-00-00', '0000-00-00', '0000-00-00', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `artist_gig_type`
--

CREATE TABLE IF NOT EXISTS `artist_gig_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `artist_gig_type`
--

INSERT INTO `artist_gig_type` (`id`, `name`) VALUES
(1, 'Open Mic'),
(2, 'One Time Gig'),
(3, 'Opening Act'),
(4, 'Others');

-- --------------------------------------------------------

--
-- Table structure for table `artist_media`
--

CREATE TABLE IF NOT EXISTS `artist_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(250) NOT NULL,
  `url` varchar(250) NOT NULL,
  `caption` varchar(250) NOT NULL,
  `artist_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `artist_media`
--

INSERT INTO `artist_media` (`id`, `type`, `url`, `caption`, `artist_id`) VALUES
(1, 'asa', 'https://www.youtube.com/', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `artist_media_type`
--

CREATE TABLE IF NOT EXISTS `artist_media_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `artist_media_id` int(11) NOT NULL,
  `type_name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `artist_media_type`
--

INSERT INTO `artist_media_type` (`id`, `artist_media_id`, `type_name`) VALUES
(1, 0, 'Video Link');

-- --------------------------------------------------------

--
-- Table structure for table `artist_member`
--

CREATE TABLE IF NOT EXISTS `artist_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `role` varchar(250) NOT NULL,
  `image_url` varchar(250) NOT NULL,
  `artist_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `artist_member`
--

INSERT INTO `artist_member` (`id`, `name`, `role`, `image_url`, `artist_id`) VALUES
(6, 'Ryan Peake', 'Bass Guiatarist', 'dfff146fa112613b627f9f9ccb875dc3.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `artist_photo`
--

CREATE TABLE IF NOT EXISTS `artist_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_url` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `set_featured` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='one artist has many photo' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `artist_photo`
--

INSERT INTO `artist_photo` (`id`, `image_url`, `caption`, `artist_id`, `set_featured`) VALUES
(1, '08799fb1ee3fecdfb7e22fcc35b903b8.jpg', '', 1, 0),
(2, '377b818b6b1e5fbf5b1bdaa9599b259a.jpg', '', 1, 0),
(3, '9a1981c0f015dd47379953816b7d67b0.jpg', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `CityID` int(11) NOT NULL AUTO_INCREMENT,
  `CityName` varchar(32) NOT NULL,
  `CountryID` int(11) NOT NULL,
  PRIMARY KEY (`CityID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`CityID`, `CityName`, `CountryID`) VALUES
(1, 'Ardendal', 1),
(2, 'Bergen', 0),
(3, 'Bodø', 0),
(4, 'Drammen', 0),
(5, 'Egersund', 0),
(6, 'Farsund', 0),
(7, 'Flekkefjord', 0),
(8, 'Florø', 0),
(9, 'Fredrikstad', 0),
(10, 'Grimstad', 0),
(11, 'Halden', 0),
(12, 'Hamar', 0),
(13, 'Hammerfest', 0),
(14, 'Harstad', 0),
(15, 'Haugesund', 0),
(16, 'Holmestrand', 0),
(17, 'Horten', 0),
(18, 'Hønefoss', 0),
(19, 'Kongsberg', 0),
(20, 'Kongsvinger', 0),
(21, 'Kristiansand', 0),
(22, 'Kristiansund', 0),
(23, 'Larvik', 0),
(24, 'Lillehammer', 0),
(25, 'Mandal', 0),
(26, 'Molde', 0),
(27, 'Moss', 0),
(28, 'Namsos', 0),
(29, 'Narvik', 0),
(30, 'Notodden', 0),
(31, 'Oslo', 0),
(32, 'Porsgrunn', 0),
(33, 'Risør', 0),
(34, 'Sandefjord', 0),
(35, 'Sandnes', 0),
(36, 'Sarpsborg', 0),
(37, 'Skien', 0),
(38, 'Stavanger', 0),
(39, 'Steinkjer', 0),
(40, 'Søgne', 0),
(41, 'Tromsø', 0),
(42, 'Trondheim', 0),
(43, 'Tønsberg', 0),
(44, 'Vadsø', 0),
(45, 'Vardø', 0),
(46, 'Vennesla', 0),
(47, 'Ålesund', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gig_comments`
--

CREATE TABLE IF NOT EXISTS `gig_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `artist_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `comments` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `gig_comments`
--

INSERT INTO `gig_comments` (`id`, `artist_id`, `venue_id`, `comments`) VALUES
(1, 1, 1, 'sdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfg'),
(2, 6, 1, 'fds');

-- --------------------------------------------------------

--
-- Table structure for table `gig_rating`
--

CREATE TABLE IF NOT EXISTS `gig_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rating` int(11) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `rating_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `gig_rating`
--

INSERT INTO `gig_rating` (`id`, `rating`, `artist_id`, `venue_id`, `rating_type`) VALUES
(1, 5, 1, 1, 1),
(2, 2, 1, 1, 2),
(3, 1, 1, 1, 3),
(4, 4, 1, 1, 1),
(5, 3, 6, 1, 3),
(6, 3, 6, 1, 2),
(7, 2, 6, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `venues`
--

CREATE TABLE IF NOT EXISTS `venues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `location` varchar(250) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `city` varchar(250) NOT NULL,
  `genre` varchar(250) NOT NULL,
  `age` int(11) NOT NULL,
  `descriptiopn` varchar(255) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `rider_document_url_pdf` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `fb_id` varchar(250) NOT NULL,
  `fb_key` varchar(250) NOT NULL,
  `fb_fname` varchar(250) NOT NULL,
  `fb_lname` varchar(250) NOT NULL,
  `fb_email` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `venues`
--

INSERT INTO `venues` (`id`, `name`, `location`, `zip`, `city`, `genre`, `age`, `descriptiopn`, `latitude`, `longitude`, `rider_document_url_pdf`, `email`, `password`, `fb_id`, `fb_key`, `fb_fname`, `fb_lname`, `fb_email`) VALUES
(1, 'Solid Rock Ring', 'oslo', '54', '1', '1,2', 40, 'We are a rock club located in the heart of Oslo. We have conducted over 100 gigs in the last 5 months. A favourite haven for rock artists and pop singers.', 0, 0, 'aqeelasghar_(2).pdf', 'admin@example.com', '$2y$10$9s1FxxhX2kRLMMS4k1YrhOvK1CRQVpe28oZzRDqV0x/vOvVDkt8UO', '', '', '', '', ''),
(2, 'La', 'Kasur', '0', '1', '2,3,5', 0, '0', 0, 0, '', 'admin@example.com', '$2y$10$uqj/uCUO88.ZGpKuG1zJvuxlV/LPEw97SgTi45xZg9AC62fNqBWUa', '', '', '', '', ''),
(3, 'as', 'lahore', '0', '1', '1,2', 0, '0', 0, 0, '', 'admin@example.com', '$2y$10$i.E5uccuAEV9yK.bbysueui5tUGL1/eFM7KJcdPUNBrYxnyJxPBMq', '', '', '', '', ''),
(4, 'Asdsda', 'Kasuar', '0', '2', '0,2,4', 0, '0', 0, 0, '', 'ch.aqeel_asghar@hot', '$2y$10$VFzZTake9TSfJCp36iBleeEysXvLxMFGRYfBdzP8vhwJJar6raslK', '', '', '', '', ''),
(5, 'kj', 'kajal', '0', '2', '1,2,3', 0, '0', 0, 0, '', 'ch.aqeel_asghar@hotmail.com', '$2y$10$bdlM57xB4fnP4pGuRuhWkudIha8yxqHNkeexUQVBQggvtItDE1rFS', '', '', '', '', ''),
(6, 'jgf', 'jhg', '', '2', '2', 0, '', 0, 0, '', 'admin@example.comm', '$2y$10$9nHXIDhEmF4TjDYBONdG4uV4s.MEXQZGu22MpR1DY60z27nqlDNzC', '', '', '', '', ''),
(7, 'Solid Rock Ring', 'oslo', '54', '1', '1,2', 40, 'We are a rock club located in the heart of Oslo. We have conducted over 100 gigs in the last 5 months. A favourite haven for rock artists and pop singers.', 0, 0, 'aqeelasghar_(2).pdf', 'admin@example.com', '$2y$10$9s1FxxhX2kRLMMS4k1YrhOvK1CRQVpe28oZzRDqV0x/vOvVDkt8UO', '', '', '', '', ''),
(8, 'La', 'Kasur', '0', '1', '2,3,5', 0, '0', 0, 0, '', 'admin@example.com', '$2y$10$uqj/uCUO88.ZGpKuG1zJvuxlV/LPEw97SgTi45xZg9AC62fNqBWUa', '', '', '', '', ''),
(9, 'as', 'lahore', '0', '1', '1,2', 0, '0', 0, 0, '', 'admin@example.com', '$2y$10$i.E5uccuAEV9yK.bbysueui5tUGL1/eFM7KJcdPUNBrYxnyJxPBMq', '', '', '', '', ''),
(10, 'Asdsda', 'Kasuar', '0', '2', '0,2,4', 0, '0', 0, 0, '', 'ch.aqeel_asghar@hot', '$2y$10$VFzZTake9TSfJCp36iBleeEysXvLxMFGRYfBdzP8vhwJJar6raslK', '', '', '', '', ''),
(11, 'kj', 'kajal', '0', '2', '1,2,3', 0, '0', 0, 0, '', 'ch.aqeel_asghar@hotmail.com', '$2y$10$bdlM57xB4fnP4pGuRuhWkudIha8yxqHNkeexUQVBQggvtItDE1rFS', '', '', '', '', ''),
(12, 'jgf', 'jhg', '', '2', '2', 0, '', 0, 0, '', 'admin@example.comm', '$2y$10$9nHXIDhEmF4TjDYBONdG4uV4s.MEXQZGu22MpR1DY60z27nqlDNzC', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `venue_photo`
--

CREATE TABLE IF NOT EXISTS `venue_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `venue_id` int(11) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `set_featured` smallint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `venue_photo`
--

INSERT INTO `venue_photo` (`id`, `venue_id`, `url`, `set_featured`) VALUES
(1, 1, 'fb546b34fb39a1fdab34490d21422269.jpg', 0),
(2, 1, 'd6db2fb6f62dd383e0ae79028852d097.jpg', 0),
(7, 1, 'fa228d7bda751fa87b3d083c523240cb.jpg', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
