-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 02, 2015 at 02:25 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `showspoon`
--

-- --------------------------------------------------------

--
-- Table structure for table `artist`
--

CREATE TABLE IF NOT EXISTS `artist` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `genre_id` varchar(255) NOT NULL,
  `gig_type_id` varchar(255) NOT NULL,
  `band_type_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `zip` int(11) NOT NULL,
  `city` varchar(250) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_on` date NOT NULL,
  `fb_id` varchar(255) NOT NULL,
  `fb_key` varchar(255) NOT NULL,
  `fb_fname` varchar(255) NOT NULL,
  `fb_lname` varchar(255) NOT NULL,
  `fb_email` varchar(255) NOT NULL,
  `short_description` text NOT NULL,
  `biography` text NOT NULL,
  `rating` int(11) NOT NULL,
  `location` varchar(250) NOT NULL,
  `Login_Key` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COMMENT='An Artist table For registration';

--
-- Dumping data for table `artist`
--

INSERT INTO `artist` (`id`, `name`, `genre_id`, `gig_type_id`, `band_type_id`, `status`, `zip`, `city`, `email`, `password`, `created_on`, `fb_id`, `fb_key`, `fb_fname`, `fb_lname`, `fb_email`, `short_description`, `biography`, `rating`, `location`, `Login_Key`) VALUES
(1, 'Blaze', '3,5', '1', 1, 0, 26, '1', 'admin@example.com', '$2y$10$fmfZzyGM8mgY.VILZHcQZ.VUYphSw5Tk1GYP7is.eU3g9D/MN5ZS6', '2015-10-11', '', '', '', '', '', 'The band was formed in the early 1990s as a cover band called the "Village Idiots" by Chad Kroeger, Ryan Peake, Mike Kroeger and Brandon kroeger.', 'The Blaze is a Canadian rock band formed in 1995 in Hanna, Alberta. The band is composed of guitarist and lead vocalist Chad Kroeger, guitarist, keyboardist and backing vocalist Ryan Peake, bassist Mike Kroeger and dunner Daiel Adair. The band went through a few dummer changes betweek 1995 and 2005, achieving it''s current form when Adair replaced drummer Ryan Vikedal.', 0, 'lahore', 18614458880311),
(2, 'The Blaze', '1,2', '2', 1, 0, 0, '1', 'admin@example.c', '$2y$10$qvC4wsFJ118XrUNreFVV6uye4p0Rg3LDbGsUI6.hCx3pDrK4Uvpmi', '2015-10-11', '', '', '', '', '', '', '', 0, 'Lahore', 0),
(4, 'Power Rock House', '2', '1', 1, 0, 0, '1', 'admin@example.co', '$2y$10$hhq8de7L8SJPLSRftAM5deNcQ750ypWl5L/C7oVscYu9Snl5e52h6', '2015-10-11', '', '', '', '', '', '', '', 0, 'Lahore', 0),
(5, 'As', '2,4,6', '2,3', 3, 0, 0, '1', 'admin@example.comhh', '$2y$10$Lq4w8D3gNz9723IDzwVBFepTSl.keP7G0YW3cuvRWQT.7hULS7FY6', '2015-10-21', '', '', '', '', '', '', '', 0, 'di', 0),
(6, 'Arif', '1,3', '1', 1, 0, 0, '2', 'admin@example.comm', '$2y$10$Ngnbe10025gGL89Kz.jpOe.Rt4NLA4e6O5xUGuMYCy0yZQu.pGkhm', '2015-10-21', '', '', '', '', '', '', '', 0, 'Oslo city', 0),
(7, 'fas', '2', '1', 1, 0, 0, '2', 'admin@example.comm', '$2y$10$T7zqkKkzEGiwd.nvvrXsMOw3nK/jIvkKk2OtL.qpoTc/J/NqUDwu6', '2015-10-21', '', '', '', '', '', '', '', 0, 'sad', 0);

-- --------------------------------------------------------

--
-- Table structure for table `artist_band_type`
--

CREATE TABLE IF NOT EXISTS `artist_band_type` (
`id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist_band_type`
--

INSERT INTO `artist_band_type` (`id`, `name`) VALUES
(1, 'Solo'),
(2, 'Duo'),
(3, 'Group'),
(4, 'Band');

-- --------------------------------------------------------

--
-- Table structure for table `artist_genre`
--

CREATE TABLE IF NOT EXISTS `artist_genre` (
`id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist_genre`
--

INSERT INTO `artist_genre` (`id`, `name`) VALUES
(1, 'Acapella'),
(2, 'Alternative'),
(3, 'Acoustic'),
(4, 'Blues'),
(5, 'Childrens'),
(6, 'Cover'),
(7, 'Classical'),
(8, 'Country'),
(9, 'Dance'),
(10, 'Electronic'),
(11, 'Experimental'),
(12, 'Folk'),
(13, 'Funk'),
(14, 'Gospel'),
(15, 'Hardcore'),
(16, 'Hip Hop'),
(17, 'indie'),
(18, 'Instrumental'),
(19, 'International'),
(20, 'Jazz'),
(21, 'Latin'),
(22, 'Mariachi'),
(23, 'Metal'),
(24, 'Pop'),
(25, 'Producer'),
(26, 'Punk'),
(27, 'R&B'),
(28, 'Rap'),
(29, 'Religious'),
(30, 'Rock'),
(31, 'Rockabilly'),
(32, 'Reggae'),
(33, 'Ska'),
(34, 'Soul'),
(35, 'Western');

-- --------------------------------------------------------

--
-- Table structure for table `artist_gigs`
--

CREATE TABLE IF NOT EXISTS `artist_gigs` (
`id` int(11) NOT NULL,
  `gig_name` varchar(250) NOT NULL,
  `location` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `zip` varchar(100) NOT NULL,
  `city` varchar(250) NOT NULL,
  `created_on` date NOT NULL,
  `type` varchar(250) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `venues_id` int(11) NOT NULL,
  `image_url` varchar(250) NOT NULL,
  `Accepting_application_start_date` date NOT NULL,
  `accepting_application_end_date` date NOT NULL,
  `Review_date` date NOT NULL,
  `Max_croner_amount` varchar(250) NOT NULL,
  `travel&accomodation` tinyint(1) NOT NULL,
  `parcentage_of_ticketsale` int(11) NOT NULL,
  `food&drink_ticket` tinyint(1) NOT NULL,
  `numbers_of_bands` int(11) NOT NULL,
  `requirements` varchar(250) NOT NULL,
  `gig_time` time NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist_gigs`
--

INSERT INTO `artist_gigs` (`id`, `gig_name`, `location`, `description`, `zip`, `city`, `created_on`, `type`, `artist_id`, `venues_id`, `image_url`, `Accepting_application_start_date`, `accepting_application_end_date`, `Review_date`, `Max_croner_amount`, `travel&accomodation`, `parcentage_of_ticketsale`, `food&drink_ticket`, `numbers_of_bands`, `requirements`, `gig_time`) VALUES
(1, 'Extreme Power Event', 'Ruselokkveien', 'Ectreme Power event based at the Ospedaletto space yound, trendy place for all young people of Sesto Calende and surroundings, in the province of Varese. A radio young, dynamic with a great desire to entertain and have fun! In particular, Neverwas Radio want to dedicate a space to emerging bands / independent with its program "UNDER THE BRIDGES". "UNDER THE BRIDGES" is a radio streaming on http://www.ospedaletto.org/blog/radio-web or at http://never-wasradio2.listen2myradio.com (SERVER SUPPORT). Our Mission is to offer a program devoted mainly to the best bands and EMERGING INDEPENDENT of varied music.', '26 0251', '1', '2015-10-14', '1,3', 1, 0, '', '2015-10-23', '2015-11-23', '2015-10-26', '40000', 1, 2015, 0, 1, '', '00:00:00'),
(2, 'rrrr', 'klkkjlsdjffffff', 'ook', '444', '1', '2015-10-14', '4', 1, 0, '', '0000-00-00', '0000-00-00', '0000-00-00', '', 0, 0, 0, 1, '', '00:00:00'),
(3, 'Abitudini Live', 'City Center, Oslo', 'A rock festival which will be organized by the citizens foundation and sponsored by Jzz FM 99. A three day concert which will', '67000', '1', '2015-10-14', '3', 1, 0, '', '0000-00-00', '0000-00-00', '0000-00-00', '', 0, 0, 0, 1, '', '12:56:00'),
(5, 'Aqeel', 'Has', 'ghhgdf', '53435', '1', '2015-10-14', '1,2', 0, 1, '', '2015-10-19', '2015-11-19', '0000-00-00', '', 0, 0, 0, 1, '', '12:56:00'),
(6, 'orange', 'Market', 'A rock festival which will be organized by the citizens foundation and sponsored by Jzz FM 99. A three day concert which will', '76888', '1', '2015-10-14', '2,3', 0, 1, '', '2015-10-22', '2015-11-22', '0000-00-00', '', 0, 0, 0, 1, '', '12:56:00'),
(7, 'Abitudini Live', 'City Center, Oslo', 'A rock festival which will be organized by the citizens foundation and sponsored by Jzz FM 99. A three day concert which will', '67000', '1', '2015-10-14', '3', 0, 1, '', '2015-10-24', '2015-11-24', '0000-00-00', '', 0, 0, 0, 1, '', '12:56:00'),
(8, 'The long Road', 'olso', '', '4000', '2', '2015-10-14', '3,4,5', 0, 1, '', '2015-10-26', '2015-11-26', '0000-00-00', '40000', 0, 3, 0, 1, '', '12:56:00'),
(13, 'asd', 'asfd', '', '0', '2', '2015-10-20', '1', 0, 1, 'venue1.jpg', '2015-10-27', '0000-00-00', '0000-00-00', '', 0, 0, 0, 1, '', '12:56:00'),
(15, 'ds', 'fds', 'sa', 'fds', '2', '2015-10-25', '2,3', 1, 0, '', '2015-10-21', '2015-10-10', '0000-00-00', '', 0, 0, 0, 1, '', '12:56:00'),
(19, 'rrrr', 'klkkjlsdjffffff', '', '444', '3', '2015-10-25', '3,4', 0, 1, '11705234_10153147699089527_8850920176047805505_n.jpg', '0000-00-00', '0000-00-00', '0000-00-00', '', 0, 0, 0, 1, '', '12:56:00'),
(20, 'rrrr', 'klkkjlsdjffffff', '', '444', '1', '2015-10-25', '4', 0, 1, '11868786_516674191813355_1748757073_n.jpg', '0000-00-00', '0000-00-00', '0000-00-00', '', 0, 0, 0, 1, '', '12:56:00'),
(21, 'Bracknell', 'fdsa', 'ssd', 'WC2N 4AA', '1', '2015-10-26', '4', 0, 6, 'unnamed.png', '2015-09-29', '2015-10-24', '2015-10-30', '34', 1, 0, 0, 1, 'ok', '12:56:00'),
(22, 'kkkk', 'klk00000', 'kkkkkkkkkkkk', '444', '3', '2015-10-26', '4', 1, 0, 'artist-register-bg2.jpg', '0000-00-00', '0000-00-00', '0000-00-00', '', 0, 0, 0, 1, '', '12:56:00'),
(23, 'kkkk', 'klk00000', 'kkk', '444', '1', '2015-10-26', '4', 1, 0, 'artist-register-bg5.jpg', '0000-00-00', '0000-00-00', '0000-00-00', '', 0, 0, 0, 1, '', '12:56:00'),
(24, 'kkkk', 'klk00000', 'oo', '444', '1', '2015-10-26', '3', 1, 0, 'artist-register-bg6.jpg', '0000-00-00', '0000-00-00', '0000-00-00', '', 0, 0, 0, 1, '', '12:56:00'),
(25, 'rrrr', 'klk00000', 'oo', '444', '3', '2015-10-26', '4', 1, 0, 'artist-register-bg8.jpg', '2015-10-21', '2015-11-13', '0000-00-00', '', 0, 0, 0, 1, '', '12:56:00'),
(26, 'kkkk', 'klkkjlsdjffffff', 'ok', '444', '1', '2015-10-26', '4', 1, 0, 'artist-register-bg9.jpg', '2015-10-15', '2015-11-13', '0000-00-00', '7000', 0, 3, 0, 1, '', '12:56:00'),
(27, 'kkkk', 'klkkjlsdjffffff', 'oo', '888', '1', '2015-10-26', '2,4', 1, 0, 'artist-register-bg12.jpg', '2015-10-01', '2015-10-16', '0000-00-00', '9000', 0, 3, 0, 1, '', '12:56:00'),
(28, 'kkkk', 'klkkjlsdjffffff', 'oo', '444', '2', '2015-10-26', '1,2,6', 1, 0, 'artist-register-bg18.jpg', '2015-10-07', '2015-10-15', '0000-00-00', '60', 1, 1, 1, 1, '', '12:56:00'),
(29, 'kkkk', 'klkkjlsdjffffff', 'ookkkkkk', '444', '1', '2015-10-26', '2', 1, 0, 'artist-register-bg20.jpg', '2015-10-01', '2015-11-07', '0000-00-00', '9000', 1, 3, 0, 1, '', '12:56:00'),
(30, 'kkkk', 'klkkjlsdjffffff', 'ooooook', '444', '1', '2015-10-26', '3,5', 0, 1, 'artist-register-bg21.jpg', '2015-10-09', '2015-11-14', '2015-10-26', '8000', 1, 7, 1, 1, '', '12:56:00'),
(31, 'hkjh', 'fdsa', 'yyyyyyy', 'WC2N 4AA', '1', '2015-10-28', '2', 6, 0, '3_target3.png', '2015-10-09', '2015-10-17', '2015-10-29', '40', 1, 5, 1, 1, '', '12:56:00'),
(32, 'Grudge connection', 'oslo', 'L''organizzatore non ha fornito ulteriori dettagli sulla data.', '45', '1', '2015-10-29', '2,3', 0, 1, '', '2015-10-08', '2015-10-09', '0000-00-00', '40', 0, 2, 0, 1, '', '12:56:00'),
(33, 'gtabd', 'fsda', 'hhhhhhhhhhhhhhf', '34', '1', '2015-10-29', '1', 0, 1, '', '2015-10-07', '2015-10-10', '0000-00-00', '4', 0, 3, 0, 1, '', '12:56:00'),
(34, 'asddddddd', 'dfa', 'ty', '43', '3', '2015-10-29', '2,4', 0, 1, '', '2015-10-14', '2015-10-13', '2015-10-29', '4', 1, 5, 1, 2, 'uk person only hh', '12:56:00'),
(35, 'fsda', 'fsa', 'sdf', 'fsda', '1', '2015-10-30', '1', 0, 6, '', '2015-11-13', '2015-11-13', '2015-11-02', '3', 1, 2, 1, 3, '434', '12:56:00'),
(36, 'dsa', 'fsda', 'gdf', '3', '4', '2015-10-30', '2', 0, 6, '', '2015-10-13', '2015-10-20', '0000-00-00', '34', 1, 3, 1, 2, '43', '12:56:00'),
(37, 'ff', 'ff', 'ok', '00', '2', '2015-11-02', '4', 0, 6, '', '2015-11-17', '2015-12-11', '0000-00-00', '56', 1, 2, 1, 2, '9 iui', '00:00:00'),
(38, 'tt', '76hg', 'hfghh', '654', '5', '2015-11-02', '2', 0, 1, '', '2015-11-17', '2015-11-17', '0000-00-00', '5', 1, 3, 1, 4, '5656rhgfhg', '00:00:00'),
(39, 'aaa', 'gag aaaaaaaa', 'olkjk', '343', '4', '2015-11-02', '1,5', 0, 6, '', '2015-11-11', '2015-11-10', '0000-00-00', '8989', 1, 2, 0, 7, 'hdfgfdsgffffd', '07:03:00'),
(40, 'fsadf', 'fgdsaf', 'fdassssssssssssssssss', '434', '1', '2015-11-02', '1', 0, 6, '', '2015-11-10', '2015-12-18', '0000-00-00', '4', 1, 3, 1, 2, 'fsda', '09:14:00'),
(41, 'tty', 'ty', 'hg', 'y', '1', '2015-11-02', '1', 0, 6, '', '2015-11-17', '2015-11-26', '2015-11-02', '4', 1, 5, 1, 5, 'rdth', '03:12:00'),
(42, '2', 'w', 'fdsa', '43', '1', '2015-11-02', '2', 6, 0, '', '2015-11-10', '2015-11-10', '0000-00-00', '40', 0, 0, 0, 0, '', '02:02:00'),
(43, 'rew', 'uu', 'dfg', 'fsd', '5', '2015-11-02', '1', 6, 0, '', '2015-11-11', '2015-11-08', '2015-11-02', 'r4', 1, 3, 0, 0, 'gdfgdsfg', '14:58:00');

-- --------------------------------------------------------

--
-- Table structure for table `artist_gig_type`
--

CREATE TABLE IF NOT EXISTS `artist_gig_type` (
`id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist_gig_type`
--

INSERT INTO `artist_gig_type` (`id`, `name`) VALUES
(1, 'Open Mic'),
(2, 'One Time Gig'),
(3, 'Opening Act'),
(4, 'Others');

-- --------------------------------------------------------

--
-- Table structure for table `artist_media`
--

CREATE TABLE IF NOT EXISTS `artist_media` (
`id` int(11) NOT NULL,
  `type` varchar(250) NOT NULL,
  `url` varchar(250) NOT NULL,
  `caption` varchar(250) NOT NULL,
  `artist_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist_media`
--

INSERT INTO `artist_media` (`id`, `type`, `url`, `caption`, `artist_id`) VALUES
(1, '1', 'https://www.youtube.com/watch?v=pWdKf3MneyI', 'ok', 1),
(10, '1', 'https://www.youtube.com/watch?v=pWdKf3MneyI', 'dsa', 1),
(12, '2', 'https://soundcloud.com/guardianfootballweekly', 'fgsd', 1);

-- --------------------------------------------------------

--
-- Table structure for table `artist_media_type`
--

CREATE TABLE IF NOT EXISTS `artist_media_type` (
`id` int(11) NOT NULL,
  `artist_media_id` int(11) NOT NULL,
  `type_name` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist_media_type`
--

INSERT INTO `artist_media_type` (`id`, `artist_media_id`, `type_name`) VALUES
(1, 0, 'Youtube url'),
(2, 0, 'Soundcloud url');

-- --------------------------------------------------------

--
-- Table structure for table `artist_member`
--

CREATE TABLE IF NOT EXISTS `artist_member` (
`id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `role` varchar(250) NOT NULL,
  `image_url` varchar(250) NOT NULL,
  `artist_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artist_member`
--

INSERT INTO `artist_member` (`id`, `name`, `role`, `image_url`, `artist_id`) VALUES
(6, 'Ryan Peake', 'Bass Guiatarist', '', 1),
(7, 'sda', 'asfd', '', 1),
(8, 'fdasfa', 'sdafsadf', '144e7f44030389bce1f6c5ac4d3b0d2c.png', 1),
(9, 'dfsg', 'df', '0ea1d9149a60ee393f69c56471a4c7bf.png', 6),
(10, 'sda', 'fsad', 'e62745f25de7f74c69c90199a9afd9b7.png', 6),
(11, 'sda', 'fsadfd', '', 6),
(12, 'dfsa', 'fsa', 'f2cff19762dd6247e0bd3f2103d01fe4.png', 6);

-- --------------------------------------------------------

--
-- Table structure for table `artist_photo`
--

CREATE TABLE IF NOT EXISTS `artist_photo` (
`id` int(11) NOT NULL,
  `image_url` varchar(255) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `set_featured` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1 COMMENT='one artist has many photo';

--
-- Dumping data for table `artist_photo`
--

INSERT INTO `artist_photo` (`id`, `image_url`, `caption`, `artist_id`, `set_featured`) VALUES
(1, '08799fb1ee3fecdfb7e22fcc35b903b8.jpg', '', 1, 0),
(5, '486874828662ceb5fca464a6ada51848.jpg', '', 1, 1),
(8, '8cf0decad6dcac388a221f5cd9f61375.png', '', 6, 0),
(9, '7da0311b0258ebed09049445dafd9201.png', '', 6, 0),
(10, '7964f875ef1610d5bf5b2f5ba068116e.png', '', 6, 0),
(11, '22700903d526454cac20377fec028f04.png', '', 6, 0),
(17, 'a692b35d703020f231f1a8be898d8f11.png', '', 6, 0),
(18, '1ab57e1a6e93dd441889d13c4573dd5a.png', '', 6, 0),
(19, '62cb8c7b2f9c949b00868218e399be13.png', '', 6, 0),
(20, 'c562a4c2ca4fada49bdc7c0e3f9d6245.png', '', 6, 1),
(21, '348e125166e920c80fa39496d47b9703.png', '', 6, 0),
(22, '9dc66789bceecec192af266cac740794.jpg', '', 6, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
`CityID` int(11) NOT NULL,
  `CityName` varchar(32) NOT NULL,
  `CountryID` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`CityID`, `CityName`, `CountryID`) VALUES
(1, 'Ardendal', 1),
(2, 'Bergen', 0),
(3, 'Bodø', 0),
(4, 'Drammen', 0),
(5, 'Egersund', 0),
(6, 'Farsund', 0),
(7, 'Flekkefjord', 0),
(8, 'Florø', 0),
(9, 'Fredrikstad', 0),
(10, 'Grimstad', 0),
(11, 'Halden', 0),
(12, 'Hamar', 0),
(13, 'Hammerfest', 0),
(14, 'Harstad', 0),
(15, 'Haugesund', 0),
(16, 'Holmestrand', 0),
(17, 'Horten', 0),
(18, 'Hønefoss', 0),
(19, 'Kongsberg', 0),
(20, 'Kongsvinger', 0),
(21, 'Kristiansand', 0),
(22, 'Kristiansund', 0),
(23, 'Larvik', 0),
(24, 'Lillehammer', 0),
(25, 'Mandal', 0),
(26, 'Molde', 0),
(27, 'Moss', 0),
(28, 'Namsos', 0),
(29, 'Narvik', 0),
(30, 'Notodden', 0),
(31, 'Oslo', 0),
(32, 'Porsgrunn', 0),
(33, 'Risør', 0),
(34, 'Sandefjord', 0),
(35, 'Sandnes', 0),
(36, 'Sarpsborg', 0),
(37, 'Skien', 0),
(38, 'Stavanger', 0),
(39, 'Steinkjer', 0),
(40, 'Søgne', 0),
(41, 'Tromsø', 0),
(42, 'Trondheim', 0),
(43, 'Tønsberg', 0),
(44, 'Vadsø', 0),
(45, 'Vardø', 0),
(46, 'Vennesla', 0),
(47, 'Ålesund', 0);

-- --------------------------------------------------------

--
-- Table structure for table `gig_comments`
--

CREATE TABLE IF NOT EXISTS `gig_comments` (
`id` int(11) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `comments` text NOT NULL,
  `created_on` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gig_comments`
--

INSERT INTO `gig_comments` (`id`, `artist_id`, `venue_id`, `comments`, `created_on`) VALUES
(1, 1, 1, 'sdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfsdfg', '2015-10-29'),
(2, 6, 1, 'fds', '2015-10-26'),
(3, 6, 6, 'fsda', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `gig_messages`
--

CREATE TABLE IF NOT EXISTS `gig_messages` (
`id` int(11) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `gig_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gig_messages`
--

INSERT INTO `gig_messages` (`id`, `artist_id`, `venue_id`, `message`, `gig_id`, `created_on`) VALUES
(1, 1, 1, 'We are a rock club located in the heart of Oslo. ', 3, '2015-10-15 12:00:00'),
(2, 6, 1, 'We are a rock club located in the heart of Oslo. ', 8, '2015-10-14 07:33:33'),
(3, 1, 1, 'We are a rock club located in the heart of Oslo. ', 5, '2015-10-03 08:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `gig_rating`
--

CREATE TABLE IF NOT EXISTS `gig_rating` (
`id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `artist_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `rating_type` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gig_rating`
--

INSERT INTO `gig_rating` (`id`, `rating`, `artist_id`, `venue_id`, `rating_type`) VALUES
(1, 5, 1, 1, 1),
(2, 2, 1, 1, 2),
(3, 1, 1, 1, 3),
(4, 4, 1, 1, 1),
(5, 3, 6, 1, 3),
(6, 3, 6, 1, 2),
(7, 2, 6, 1, 1),
(8, 5, 6, 6, 2),
(9, 2, 6, 6, 1),
(10, 4, 6, 6, 3),
(11, 2, 6, 1, 3),
(12, 2, 6, 1, 1),
(13, 4, 6, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
`id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `subject` varchar(250) NOT NULL,
  `type` tinyint(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `sender_id`, `receiver_id`, `message`, `subject`, `type`) VALUES
(1, 6, 1, 'testing', 'ok', 0),
(2, 6, 1, 'are you', 'fine', 0),
(3, 1, 1, 'g', 'hgf', 0),
(4, 1, 1, 'fgfgfgfgfgfgfgfgd', 'gf', 0),
(5, 1, 1, 'fgfgfgfgfgfgfgfgfgfgsd', 'ffffffff', 0),
(6, 6, 6, 'uuuuuuuuuuuuuu', 'uy', 1),
(7, 6, 4, 'kkkkkkkkkk', 'ooo', 0);

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE IF NOT EXISTS `requests` (
`id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `gig_id` int(11) NOT NULL,
  `created_on` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`id`, `sender_id`, `receiver_id`, `message`, `gig_id`, `created_on`) VALUES
(1, 6, 1, 'fd', 21, '2015-10-30'),
(2, 6, 2, 'fd', 21, '2015-10-30');

-- --------------------------------------------------------

--
-- Table structure for table `venues`
--

CREATE TABLE IF NOT EXISTS `venues` (
`id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `location` varchar(250) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `city` varchar(250) NOT NULL,
  `genre` varchar(250) NOT NULL,
  `age` int(11) NOT NULL,
  `descriptiopn` varchar(255) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `rider_document_url_pdf` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `fb_id` varchar(250) NOT NULL,
  `fb_key` varchar(250) NOT NULL,
  `fb_fname` varchar(250) NOT NULL,
  `fb_lname` varchar(250) NOT NULL,
  `fb_email` varchar(250) NOT NULL,
  `Login_Key` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `venues`
--

INSERT INTO `venues` (`id`, `name`, `location`, `zip`, `city`, `genre`, `age`, `descriptiopn`, `latitude`, `longitude`, `rider_document_url_pdf`, `email`, `password`, `fb_id`, `fb_key`, `fb_fname`, `fb_lname`, `fb_email`, `Login_Key`) VALUES
(1, 'Solid Rock Ring', 'oslo', '54', '1', '1,2', 40, 'We are a rock club located in the heart of Oslo. We have conducted over 100 gigs in the last 5 months. A favourite haven for rock artists and pop singers.', 0, 0, 'aqeelasghar_(2).pdf', 'admin@example.com', '$2y$10$y2s71Buzhi2w84DEbDgRXeLpbmCuzN4OCxl1aWwSUDomQnEjv7dKO', '', '', '', '', '', 19014458881201),
(2, 'La', 'Kasur', '0', '1', '2,3,5', 0, '0', 0, 0, '', 'admin@example.com', '$2y$10$uqj/uCUO88.ZGpKuG1zJvuxlV/LPEw97SgTi45xZg9AC62fNqBWUa', '', '', '', '', '', 0),
(3, 'as', 'lahore', '0', '1', '1,2', 0, '0', 0, 0, '', 'admin@example.com', '$2y$10$i.E5uccuAEV9yK.bbysueui5tUGL1/eFM7KJcdPUNBrYxnyJxPBMq', '', '', '', '', '', 0),
(4, 'Asdsda', 'Kasuar', '0', '2', '0,2,4', 0, '0', 0, 0, '', 'ch.aqeel_asghar@hot', '$2y$10$VFzZTake9TSfJCp36iBleeEysXvLxMFGRYfBdzP8vhwJJar6raslK', '', '', '', '', '', 0),
(5, 'kj', 'kajal', '0', '2', '1,2,3', 0, '0', 0, 0, '', 'ch.aqeel_asghar@hotmail.com', '$2y$10$bdlM57xB4fnP4pGuRuhWkudIha8yxqHNkeexUQVBQggvtItDE1rFS', '', '', '', '', '', 0),
(6, 'jgf', 'jhg', '34', '2', '2', 0, 'ok', 0, 0, '', 'admin@example.comm', '$2y$10$9nHXIDhEmF4TjDYBONdG4uV4s.MEXQZGu22MpR1DY60z27nqlDNzC', '', '', '', '', '', 0),
(7, 'Solid Rock Ring', 'oslo', '54', '1', '1,2', 40, 'We are a rock club located in the heart of Oslo. We have conducted over 100 gigs in the last 5 months. A favourite haven for rock artists and pop singers.', 0, 0, 'aqeelasghar_(2).pdf', 'admin@example.com', '$2y$10$9s1FxxhX2kRLMMS4k1YrhOvK1CRQVpe28oZzRDqV0x/vOvVDkt8UO', '', '', '', '', '', 0),
(8, 'La', 'Kasur', '0', '1', '2,3,5', 0, '0', 0, 0, '', 'admin@example.com', '$2y$10$uqj/uCUO88.ZGpKuG1zJvuxlV/LPEw97SgTi45xZg9AC62fNqBWUa', '', '', '', '', '', 0),
(9, 'as', 'lahore', '0', '1', '1,2', 0, '0', 0, 0, '', 'admin@example.com', '$2y$10$i.E5uccuAEV9yK.bbysueui5tUGL1/eFM7KJcdPUNBrYxnyJxPBMq', '', '', '', '', '', 0),
(10, 'Asdsda', 'Kasuar', '0', '2', '0,2,4', 0, '0', 0, 0, '', 'ch.aqeel_asghar@hot', '$2y$10$VFzZTake9TSfJCp36iBleeEysXvLxMFGRYfBdzP8vhwJJar6raslK', '', '', '', '', '', 0),
(11, 'kj', 'kajal', '0', '2', '1,2,3', 0, '0', 0, 0, '', 'ch.aqeel_asghar@hotmail.com', '$2y$10$bdlM57xB4fnP4pGuRuhWkudIha8yxqHNkeexUQVBQggvtItDE1rFS', '', '', '', '', '', 0),
(12, 'jgf', 'jhg', '', '2', '2', 0, '', 0, 0, '', 'admin@example.comm', '$2y$10$9nHXIDhEmF4TjDYBONdG4uV4s.MEXQZGu22MpR1DY60z27nqlDNzC', '', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `venue_photo`
--

CREATE TABLE IF NOT EXISTS `venue_photo` (
`id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `set_featured` smallint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `venue_photo`
--

INSERT INTO `venue_photo` (`id`, `venue_id`, `url`, `set_featured`) VALUES
(1, 1, 'fb546b34fb39a1fdab34490d21422269.jpg', 0),
(2, 1, 'd6db2fb6f62dd383e0ae79028852d097.jpg', 0),
(7, 1, 'fa228d7bda751fa87b3d083c523240cb.jpg', 1),
(8, 6, '7364536b3414672154ebfa25d44fc8f4.jpg', 0),
(9, 6, '59958988a58dbafb149e020d5846d7a2.png', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artist`
--
ALTER TABLE `artist`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist_band_type`
--
ALTER TABLE `artist_band_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist_genre`
--
ALTER TABLE `artist_genre`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist_gigs`
--
ALTER TABLE `artist_gigs`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist_gig_type`
--
ALTER TABLE `artist_gig_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist_media`
--
ALTER TABLE `artist_media`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist_media_type`
--
ALTER TABLE `artist_media_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist_member`
--
ALTER TABLE `artist_member`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `artist_photo`
--
ALTER TABLE `artist_photo`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
 ADD PRIMARY KEY (`CityID`);

--
-- Indexes for table `gig_comments`
--
ALTER TABLE `gig_comments`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gig_messages`
--
ALTER TABLE `gig_messages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gig_rating`
--
ALTER TABLE `gig_rating`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venues`
--
ALTER TABLE `venues`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `venue_photo`
--
ALTER TABLE `venue_photo`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artist`
--
ALTER TABLE `artist`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `artist_band_type`
--
ALTER TABLE `artist_band_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `artist_genre`
--
ALTER TABLE `artist_genre`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `artist_gigs`
--
ALTER TABLE `artist_gigs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `artist_gig_type`
--
ALTER TABLE `artist_gig_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `artist_media`
--
ALTER TABLE `artist_media`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `artist_media_type`
--
ALTER TABLE `artist_media_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `artist_member`
--
ALTER TABLE `artist_member`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `artist_photo`
--
ALTER TABLE `artist_photo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
MODIFY `CityID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `gig_comments`
--
ALTER TABLE `gig_comments`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `gig_messages`
--
ALTER TABLE `gig_messages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `gig_rating`
--
ALTER TABLE `gig_rating`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `venues`
--
ALTER TABLE `venues`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `venue_photo`
--
ALTER TABLE `venue_photo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
